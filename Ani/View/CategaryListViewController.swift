//
//  CategaryListViewController.swift
//  Ani
//
//  Created by Rohit Changediya on 13/06/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import Floaty
import CoreData
import SVProgressHUD


class CategaryListViewController: UIViewController,PopUpDoneDelegate,PopCategoryPage {
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeightConstant: NSLayoutConstraint! // 50
    var searchTempString = ""
    @IBOutlet weak var searchBar: UISearchBar!
      var productSubMenuArray = [ProductListModelClass]()
    var vcFor : ValidationPopupViewController? = nil
    var vcforCategory : PopupViewController? = nil
    var searchActive : Bool = false
    @IBOutlet weak var lblNoCategoryFound: UILabel!
    
    var allergntArray1 = [productallergnsClass]()
    var nutritianArray1 = [nutritianArrayObject]()
    var ingredientsArray1 = [ingredientsArrayObject]()
    
    
    @IBOutlet weak var lblOfflineMerchand: UILabel!
    
    
    
    var allergensDetail = [productallergnsClass]()
    var nutritianDetails = [nutritianArrayObject]()
    var ingredientsDetails = [ingredientsArrayObject]()
    
    
    var name : String!
    let context=AppDelegate().persistentContainer.viewContext
    var allergntArray = [allergentModelClass]()
    
    var ProductMenuArray = [productMenuListModelClass]()
    var filterArray = [productMenuListModelClass]()
    var marchantArray = [marchantModelClass]()
    var account_id = ""
    var accountID =  loginClass()
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    var activityIndicator = UIActivityIndicatorView()
    
    var categoryArray = [globleClass]()
    var searchCategoryArray = [globleClass]()
    
    var merchantId = ""
    var MerIDFromParent = ""
    var MerNameFromParent = ""
    var FinalMerID = ""
    
    var comeFrom = ""
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!


    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(CategaryListViewController.handleRefresh),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.black
            return refreshControl
    }()
    
    
    
    
    fileprivate func FloatyButton() {
        let floaty = Floaty()
        floaty.buttonColor = UIColor(hex: "41b93d")
        floaty.itemButtonColor = UIColor(hex: "41b93d")
        floaty.itemTitleColor = UIColor.white
        floaty.plusColor = UIColor.white
        
        floaty.addItem("Favourite Menu Items" ,icon:  UIImage(named: "fab_fav_icon.png")) { (item) in
            
            let fav = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteViewController")as! FavouriteViewController
            self.navigationController?.pushViewController(fav, animated: true)
        }
       
        
        floaty.addItem("Recent View Items" ,icon: UIImage(named: "recent_view.png")) { (item) in
            //            item.buttonColor = UIColor(hex: "41b93d")
            //            item.titleColor = UIColor(hex: "41b93d")
            //            item.icon = UIImage(named: "star_green.png")
            let recent = self.storyboard?.instantiateViewController(withIdentifier: "RecentViewController")as! RecentViewController
            self.navigationController?.pushViewController(recent, animated: true)
        }
        
        self.view.addSubview(floaty)
    }
    
//    fileprivate func SVProgressHUD1() {
//        SVProgressHUD.setBackgroundColor(UIColor.clear)
//        SVProgressHUD.AnimationCurve.easeOut
//        SVProgressHUD.AnimationTransition.curlUp
//        SVProgressHUD.setBackgroundLayerColor(.clear)
//    }
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
          super.viewDidLoad()
        
        print("CategoryListViewController")
        
        print("MerNameFromParent = \(MerNameFromParent)")
        print("MerIDFromParent = \(MerIDFromParent)")
        
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
        
        self.categoryCollectionView.alwaysBounceVertical = true
        
        self.categoryCollectionView.addSubview(self.refreshControl)

        addToolBar(textField: searchBar)
        // self.stopSpinner()
        self.navigationController?.navigationBar.isHidden = false
        self.lblNoCategoryFound.isHidden = true
        
        //        self.getMerchantList()
        
        self.setView()
        
        // Do any additional setup after loading the view.
        //         self.stopSpinner()
        self.startSpinner()
        self.searchBar.delegate = self
        self.searchViewHeightConstant.constant = 0
        self.searchView.clipsToBounds = true
        
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        vcforCategory = self.storyboard?.instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController
        vcforCategory?.delegate = self
        
        //self.categoryArray = ["Coffee","Juice","Cold Drink","Sandwiches","Non Veg Mains","All Day Snacks Tandoori Bfast","BALTI"]
        //   SVProgressHUD1()
       
        
        //        FloatyButton()
        self.categoryCollectionView.reloadData()
        
        //SVProgressHUD.dismiss()
        self.categoryCollectionView.addSubview(self.refreshControl)
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        //        self.startSpinner()
        merchantListWebservices()
        categoryCollectionView.reloadData()
        refreshControl.endRefreshing()
    }
    
    
//    @objc func update()
//    {
//
//    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.lblNoCategoryFound.isHidden = true
//        super.viewWillAppear(true)
        self.searchBar.resignFirstResponder()
        self.searchViewHeightConstant.constant = 0
        self.view.endEditing(true)
        
        lblOfflineMerchand.isHidden = true
        
        if Reachability.isConnectedToNetwork()
        {
            //            self.startSpinner()
            searchBar.text = ""
            merchantListWebservices()
            self.categoryCollectionView.reloadData()
            self.navigationController?.isNavigationBarHidden = false
            
            //            let timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
            
        }
        else
        {
            //   SVProgressHUD.dismiss()
            DispatchQueue.main.async {
                self.categoryCollectionView.reloadData()
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  Internet_Connection_Alert
                self.present(self.vcFor!, animated: true, completion: nil)
            }
            self.stopSpinner()
        }
       
        
    }
//
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
    func PopUpOkClickCategory(status: Bool) {
        if status {
            self.navigationController?.popViewController(animated: true)
//             if appdelegate.closePopUpFlag == 0
//             {
//            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
//            self.navigationController?.pushViewController(home, animated: true)
//            }
//            else
//             {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "RecentVisitViewController") as! RecentVisitViewController
//                self.navigationController?.pushViewController(home, animated: true)
//            }
        }
    }
    
    
    func setView() {
        
//        SVProgressHUD.show()
 //       self.title = self.MerNameFromParent
        
        self.title = getValueForKey(keyValue: marchentName)
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(self.btnBackTapped(_:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnRightbutton = UIButton(type: .custom)
        btnRightbutton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnRightbutton.backgroundColor = UIColor.clear
        btnRightbutton.addTarget(self, action: #selector(self.btnInfoTapped(_:)), for: .touchUpInside)
        btnRightbutton.setImage(UIImage(named: "ic_information_outline_white_48dp.png"), for: .normal)
        let barButton = UIBarButtonItem(customView: btnRightbutton)
        

        let btnright = UIButton(type: .custom)
        btnright.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnright.backgroundColor = UIColor.clear
        btnright.addTarget(self, action: #selector(self.btnSearchTapped(_:)), for: .touchUpInside)
        btnright.setImage(UIImage(named: "ic_search_white"), for: .normal)
        let sideMenubarButton = UIBarButtonItem(customView: btnright)

        
        self.navigationItem.rightBarButtonItems = [barButton,sideMenubarButton]
        
       
        
    }
    
    
    var shopCheck = "0"
    
    func merchantListWebservices()
    {
        //let headers = ["Content-Type":"Application/json"]
        let _url = get_all_prefered_merchants_new
        
        let MetaString =  getValueForKey(keyValue: kmetaString)
        print(MetaString)
        let newMetaString = MetaString.replacingOccurrences(of: "ANI-", with: "")
        print("\(newMetaString)")
        setDefaultValue(keyValue: knewMetaString, valueIs: newMetaString)
//        var merID = UserDefaults.standard.value(forKey: "merchandID") as! String
        
        print("Final mer ID From Recent : \(FinalMerID)")
        
        if FinalMerID == ""
        {
            FinalMerID = newMetaString
        }
        
        let parameters: Parameters = ["merchant_id" : FinalMerID, "account_id": getValueForKey(keyValue: kregisterAccountId)]
        
        print("parameters are: \(parameters)")
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: nil).responseJSON { (response) in
                            //                            self.activityIndicator.startAnimating()
                            
//                            print("Request  \(response.request)")
//                            print("RESPONSE \(response.result.value)")
//                            print("RESPONSE \(response.result)")
//                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess
                            {
                                print("SUKCES with \(response)")
                                
                                self.stopSpinner()
                                if (response.result != nil)
                                {
                                    
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            
                                            
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            setDefaultValue(keyValue: kmsg, valueIs: msg!)
//                                            self.categoryCollectionView.isHidden = true
                                            self.ProductMenuArray.removeAll()
                                            self.categoryCollectionView.reloadData()
                                            self.lblOfflineMerchand.isHidden = false
                                            self.lblNoCategoryFound.isHidden = true
                                            self.lblOfflineMerchand.text = msg
                                            
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
//                                            self.vcFor?.titleFor =  msg!
//                                            self.present(self.vcFor!, animated: true, completion: nil)
//                                            self.navigationController?.popViewController(animated: true)
//                                            self.navigationController?.popToRootViewController(animated: true)
                                            //    var pop = self.storyboard?.instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                            //    self.navigationController?.pushViewController(pop!, animated: true)
                                            //
                                            //                                            let str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            //
                                            //                                            setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                                            //                                            var pop = self.storyboard?.instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                            //                                            self.navigationController?.pushViewController(pop!, animated: true)
                                            //
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                        }
                                        else
                                        {
                                            self.lblOfflineMerchand.isHidden = true
                                            self.categoryCollectionView.reloadData()
//                                            self.categoryCollectionView.isHidden = false
                                            
                                            let resultarray = jsonDic.object(forKey: "data")as! NSArray
                                            //                                            self.activityIndicator.stopAnimating()
                                            let temp = marchantModelClass()
                                            for i in resultarray
                                            {
                                                let tempobj = i as! NSDictionary
                                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String
                                                temp.merchant_id = tempobj.object(forKey: "merchant_id") as? String
                                                print(temp.shop_open)
                                          
                                                let keyExists = tempobj.object(forKey: "hide_nutritional_info") as? String != nil
                                                    
                                                if keyExists {
                                                    
                                                    temp.hide_nutritional_info = tempobj.object(forKey: "hide_nutritional_info") as? String
                                                    setDefaultValue(keyValue: Khide_nutritional_info, valueIs:  temp.hide_nutritional_info)
                                                    print("hide_nutritional_info = \(temp.hide_nutritional_info ?? "")")
                                                }
                                                
                                                self.merchantId = temp.merchant_id
                                                if temp.shop_open! == "true"
                                                {
                                                    self.shopCheck = "1"
//                                                    self.categoryCollectionView.isHidden = true
                                                    
                                                    
                                                }
                                                
                                                print(self.shopCheck)
                                                
                                                //added date 9 jun 2020
                                                
                                                if self.comeFrom == "MasterScan"{
                                                    
                                                    self.appdelegate.merchant_id = self.MerIDFromParent
                                                                                              
                                                    setDefaultValue(keyValue: kmarchentId, valueIs: self.MerIDFromParent)
                                                }
                                                else{
                                                    
                                                    self.appdelegate.merchant_id = temp.merchant_id
                                                                                              
                                                    setDefaultValue(keyValue: kmarchentId, valueIs: temp.merchant_id)
                                                }
                                                
                                                //////
                                                
                                                print("merchant_id123 = \(String(describing: temp.merchant_id))")
                                                
                                                temp.address = tempobj.object(forKey: "address") as? String
                                                
                                                setDefaultValue(keyValue: KAddress, valueIs:  temp.address)
                                                
                                                temp.avgPreparationTime = tempobj.object(forKey: "avgPreparationTime") as? String
                                                
                                                temp.closing_time = tempobj.object(forKey: "closing_time") as? String
                                                
                                                temp.opening_time = tempobj.object(forKey: "opening_time") as? String
                                                
                                                temp.latitude = tempobj.object(forKey: "latitude") as? String
                                                
                                                setDefaultValue(keyValue: Klatitude, valueIs:  temp.latitude)
                                                
                                                temp.longitude = tempobj.object(forKey: "longitude") as? String
                                                
                                                setDefaultValue(keyValue: Klongitude, valueIs: temp.longitude)
                                                
                                                temp.email = tempobj.object(forKey: "email") as? String
                                                setDefaultValue(keyValue: KkEmail, valueIs:  temp.email)
                                                
                                                temp.delivery_charges = tempobj.object(forKey: "delivery_charges") as? String
                                                
                                                temp.distance = tempobj.object(forKey: "distance") as? String
                                                
                                                temp.delivery_time = tempobj.object(forKey: "delivery_time") as? String
                                                
                                                temp.delivery_type = tempobj.object(forKey: "delivery_type") as? String
                                                
                                                temp.flag = tempobj.object(forKey: "flag") as? String
                                                
                                                temp.food_type = tempobj.object(forKey: "food_type") as? String
                                                
                                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String
                                                
                                                temp.percentage = tempobj.object(forKey: "percentage") as? String
                                                
                                                temp.mobile_number = tempobj.object(forKey: "mobile_number") as? String
                                                
                                                setDefaultValue(keyValue: KMobile, valueIs: temp.mobile_number)
                                                
                                                temp.mininum_order = tempobj.object(forKey: "mininum_order") as? String
                                                
                                                temp.merchant_tagline = tempobj.object(forKey: "merchant_tagline") as? String
                                                
                                                setDefaultValue(keyValue: ktagline, valueIs:  temp.merchant_tagline)
                                                
                                                
                                                temp.merchant_name = tempobj.object(forKey: "merchant_name") as? String
                                                
                                                temp.home_number = tempobj.object(forKey: "home_number") as? String
                                                
                                                //                                     setDefaultValue(keyValue: khome, valueIs:  temp.home_number)
                                                //
                                                let allergentArray = tempobj.object(forKey: "allergans")as! NSArray
                                                self.allergntArray.removeAll()
                                                for item in allergentArray
                                                {
                                                    let tempitem = item as! NSDictionary
                                                    let tempobjItem = allergentModelClass()
                                                    tempobjItem.allerganName = tempitem.object(forKey: "allergan")as? String
                                                    tempobjItem.allergan_id = tempitem.object(forKey: "allergan_id")as? String
                                                    
                                                   tempobjItem.isSelected = false
                                                    
                                                    self.allergntArray.append(tempobjItem)
                                                 
                                                }
                                                
                                                print(self.allergntArray)
                                                
                                                self.marchantArray.append(temp)
//                                                SVProgressHUD.show()
                                                
                                                if Reachability.isConnectedToNetwork() {
                                                    self.startSpinner()
                                                    self.CategoryList()
                                                }
                                                else {
                                               //     self.stopSpinner()
                                                    DispatchQueue.main.async {
                                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                                    self.vcFor?.isLogin = true
                                                    self.vcFor?.actionFor = ""
                                                    self.vcFor?.titleFor =  Internet_Connection_Alert
                                                    self.present(self.vcFor!, animated: true, completion: nil)
                                                    
                                                    }
                                                }
                                                
                                                
                                              
                                                //                                    print(self.marchantArray)
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            else if response.result.isFailure
                            {
                                    self.stopSpinner()
                                    
                                    var str = response.result.error?.localizedDescription
                                     print(response.result.error?.localizedDescription)
                                    
                                    if str ==
                                        "The Internet connection appears to be offline."
                                    {
                                        str = "You are offline please check your internet connection"
                                    }
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  str!
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                            }
                            
                     
                            //                                self.activityIndicator.stopAnimating()
                            
            self.stopSpinner()
        }
        //         SVProgressHUD.dismiss()
        //          showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        //
//        self.stopSpinner()
    }
    
    
    
    
    func CategoryList()
    {
        
        var data1 = [Ani]()
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let  result = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        let headers = ["Content-Type":"Application/json"]
        let _url = merchant_wise_menuitems
        account_id = appdelegate.kaccountIdString
        print("var account_id : \(account_id)")
        
        var idForAPI = ""
        if self.MerIDFromParent == nil || self.MerIDFromParent == ""{
            idForAPI = self.merchantId
        }
        else{
            idForAPI = self.MerIDFromParent
        }
        
        let parameters: Parameters = ["current_merchant_id" : idForAPI, "account_id" :  (data1[0].accId!) ,"type" : kprefferedmerchant]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
   
                            print("Request  \(String(describing: response.request))")
                            print("RESPONSE \(String(describing: response.result.value))")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess
                            {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    self.stopSpinner()
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        self.ProductMenuArray.removeAll()
                                        
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }
                                        else
                                        {
                                            self.stopSpinner()
                                            self.ProductMenuArray.removeAll()
                                            let resultArray = jsonDic.object(forKey: "data") as! NSArray
                                            
                                            for i in resultArray
                                            {
                                                let tempobj = i as! [String:Any]
                                                self.ProductMenuArray.append(productMenuListModelClass(fromDictionary: tempobj))
                                                
                                            }
                                            
                                        }
                                        
                                        self.categoryCollectionView?.reloadData()
                                        self.viewDidLayoutSubviews()
                                    }
                                    self.stopSpinner()
                                }
                            }
                            else if response.result.isFailure
                            {
                                self.stopSpinner()
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                            }
                            
        }
        
        
    }
    
    
    func globleSearch() {
        
        //let headers = ["Content-Type":"Application/json"]
        let _url = global_search
        
        var MetaString =  getValueForKey(keyValue: kmetaString)
        print(MetaString)
        
        let newMetaString = MetaString.replacingOccurrences(of: "ANI-", with: "")
        print("\(newMetaString)")
        setDefaultValue(keyValue: knewMetaString, valueIs: newMetaString)
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let  result = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
        {
            data1.append(data as! Ani)
        }
        
        var stringvalue = ""
        var newstr = ""
//        
//        if  appdelegate.clearArray == 1
//        {
//            appdelegate.appdeldgtenewArray.removeAll()
//            appdelegate.clearArray = 0
//        }
        for str in appdelegate.appdeldgtenewArray
        {
            stringvalue += str + ","
            if let lastchar = stringvalue.characters.last {
                if [",", ".", "-", "?"].contains(lastchar) {
                    newstr = String(stringvalue.characters.dropLast())
                    print("newstr : \(newstr)")
                }
            }
        }
            print("stringvalue : \(stringvalue)")
      
//        var merchandID = UserDefaults.standard.value(forKey: "merchandID") as! String
//        UserDefaults.standard.synchronize()
//        print(merchandID)
        
        
        
//        let parameters: Parameters = ["merchant_id" : newMetaString,"account_id" : (data1[0].accId!),"text" : searchBar.text ?? "","allerganid" : newstr ]
        
        
        var parentID = ""
                               if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                                    parentID = ""
                               }
                               else{
                                   if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                                           //  parentID = ""
                                    parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                         }
                                         else{
                                             parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                         }
                               }
                               print("Parent ID from UserDefaults : \(parentID)")
        
        
        let parameters: Parameters = ["merchant_id" : FinalMerID,"account_id" : (data1[0].accId!),"text" : searchBar.text ?? "","allerganid" : newstr,"partner_id":parentID]

        
        print(parameters)
        
    //   http://test.theaniapp.com/ws/global_search.php?merchant_id=234&account_id=1046&allerganid=&text=Apple
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default,
            headers: nil).responseJSON { (response) in
                //                            self.activityIndicator.startAnimating()
                print("Request  \(response.request)")
                
                print("RESPONSE \(response.result.value)")
                print("RESPONSE \(response.result)")
                print("RESPONSE \(response)")
                
                if response.result.isSuccess {
                    print("SUKCES with \(response)")
                    
                    
                    if (response.result != nil)
                    {
                        self.stopSpinner()
                        let jsonDic = response.result.value as! NSDictionary
                        print(jsonDic)
                        if let res = jsonDic.value(forKey: "result") as? String
                        {
                            
                            self.ProductMenuArray.removeAll()
                            
                            if res == "failed"
                            {
                                self.stopSpinner()
                                
                                var msg = jsonDic.object(forKey: "msg")as? String
                                
                                
                                
        
                                
                                //            self.vcFor?.modalTransitionStyle = .crossDissolve
                                //            self.vcFor?.modalPresentationStyle = .overFullScreen
                                //            self.vcFor?.isLogin = true
                                //            self.vcFor?.actionFor = ""
                                //            self.vcFor?.titleFor =  msg!
                                //            self.present(self.vcFor!, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                self.stopSpinner()
                                let resultArray = jsonDic.object(forKey: "data") as! NSArray

                                for i in resultArray
                                {
                             self.ProductMenuArray.append(productMenuListModelClass(fromDictionary: i as! [String:Any]))
                                }
                                self.categoryCollectionView.reloadData()
                            }
                            
                            self.categoryCollectionView.reloadData()
                            self.viewDidLayoutSubviews()
                        }
                        
                    }
                }
                else if response.result.isFailure {
                    self.stopSpinner()
                    
                    var str = response.result.error?.localizedDescription
                    print(response.result.error?.localizedDescription)
                    
                    if str ==
                        "The Internet connection appears to be offline."
                    {
                        str = "You are offline please check your internet connection"
                    }
                    
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  str!
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
                }
                
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.ProductMenuArray.count == 0 {
            self.lblNoCategoryFound.isHidden = false
        }
        else {
            self.lblNoCategoryFound.isHidden = true
        }
        
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        
        self.vcforCategory?.modalTransitionStyle = .crossDissolve
        self.vcforCategory?.modalPresentationStyle = .overFullScreen
        self.vcforCategory?.isLogin = true
        self.vcforCategory?.actionFor1 = ""
//        self.vcFor?.titleFor =  msg!
        self.present(self.vcforCategory!, animated: true, completion: nil)
        
//        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnInfoTapped(_ sender: Any) {
        
        
        
        
        
        let svc = storyboard?.instantiateViewController(withIdentifier: "marchentInfoViewController")as! marchentInfoViewController
        self.navigationController?.pushViewController(svc, animated: true)
        
    }
    
    @IBAction func btnSearchTapped(_ sender: Any) {
        self.searchBar.text = ""
        
        if self.searchViewHeightConstant.constant == 0 {
            self.searchViewHeightConstant.constant = self.navigationController!.navigationBar.frame.size.height
            self.searchBar.becomeFirstResponder()
        }
        else {
            self.startSpinner()
            self.CategoryList()
            self.searchViewHeightConstant.constant = 0
            _ = self.searchBar.resignFirstResponder()
        }
        
    }
    
    func getMerchantList() {
        
        self.searchCategoryArray = self.categoryArray
        
        //        if Reachability.isConnectedToNetwork() {
        //            self.setLoading()
        //
        //            var mID = ""
        //            if comeWith == "fav" {
        //                mID = favMerchantDetails.merchantId!
        //            }
        //            else {
        //                mID = merchantDetails.merchantId!
        //            }
        //
        //            self.getMenuItemsList(currentMerchant: mID, typeOF: comeFrom, offerID: offerID)
        //            { (success, arr) in
        //                if success {
        //                    print("true")
        //                    self.categoryArray.removeAll()
        //                    self.searchCategoryArray.removeAll()
        //                    self.categoryArray = arr
        //                    self.searchCategoryArray = arr
        //
        //                    self.categoryCollectionView.reloadData()
        //                    self.viewDidLayoutSubviews()
        //
        //                } else {
        //                    print("false")
        //                }
        //            }
        //        }
        //        else {
        //            self.simpleAlert(title: "", details: Internet_Connection_Alert)
        //        }
    }
    
}

extension CategaryListViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if self.searchTempString == ""
        {
            self.navigationController?.isNavigationBarHidden = false
            self.merchantListWebservices()
            self.navigationController?.isNavigationBarHidden = false
        }
        else
        {
            self.CategoryList()
            self.searchBar.showsCancelButton = true
            self.navigationController?.isNavigationBarHidden = false
        }
//        self.CategoryList()
//        self.searchBar.showsCancelButton = true
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        self.navigationController?.isNavigationBarHidden = false
//        self.CategoryList()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //self.getSearchResult(str: searchText)
        self.searchTempString = searchText
        if searchText == ""
        {
            self.navigationController?.isNavigationBarHidden = false
            self.merchantListWebservices()
            self.navigationController?.isNavigationBarHidden = false
        }
        else
        {
            self.navigationController?.isNavigationBarHidden = false
            self.globleSearch()
            self.navigationController?.isNavigationBarHidden = false
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print(self.searchBar.text)
        self.navigationController?.isNavigationBarHidden = false
//        self.globleSearch()
        self.navigationController?.isNavigationBarHidden = false
        //self.getSearchResult(str: self.searchBar.text ?? "")
        //self.dismissKeyboard()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.isNavigationBarHidden = false
        print(self.ProductMenuArray.count)
        self.searchViewHeightConstant.constant = 0
        self.searchBar.showsCancelButton = false
        self.searchBar.text = ""
        
//        self.categoryArray = self.searchCategoryArray
        self.searchBar.resignFirstResponder()
//        self.startSpinner()
        self.merchantListWebservices()
        self.navigationController?.isNavigationBarHidden = false
//        self.CategoryList()
//        self.categoryCollectionView.reloadData()
//        self.viewDidLayoutSubviews()
    }
    
    //    func getSearchResult(str: String) {
    //
    //        if str == "" {
    //            self.categoryArray = self.searchCategoryArray
    //            self.categoryCollectionView.reloadData()
    //        }
    //        else {
    //            let filterdObject = self.searchCategoryArray.filter { $0.contains(str) ?? false }
    //            self.categoryArray.removeAll()
    //            self.categoryArray = filterdObject
    //            self.categoryCollectionView.reloadData()
    //        }
    //
    //        self.viewDidLayoutSubviews()
    //
    //    }
    
    
}

extension CategaryListViewController: UITextFieldDelegate{
    func addToolBar(textField: UISearchBar){
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        //      var cancelButton = UIBarButtonItem(title: “Cancel”, style: UIBarButtonItemStyle.plain, target: self, action: “cancelPressed”)
        var spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.autocorrectionType = .no
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
   @objc func donePressed()
   {
        view.endEditing(true)
    }
    func cancelPressed(){
        view.endEditing(true) // or do something
    }
}

extension CategaryListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.categoryCollectionView.frame.size.width / 2, height: 150.0) //self.categoryCollectionView.frame.size.width / 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ProductMenuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ProductMenuListCollectionViewCell
        
        let temp1 = ProductMenuArray[indexPath.row]
        
        let placeholderImage = UIImage(named: "background_placeholder.png")!
        
        let imgUrl = temp1.menu_item_group_image
        let downloadURL = NSURL(string: imgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? " ")!
        cell.menuImage.af_setImage(withURL: downloadURL as URL, placeholderImage: placeholderImage)
        
        cell.lblMenu_Item_Name.text = ProductMenuArray[indexPath.row].menu_item_group_name
        
        if ProductMenuArray[indexPath.row].menu_item_flag == "deactive"
        {
            cell.menuImage.alpha = 0.25
            cell.lblMenu_Item_Name.alpha = 0.25
        }
        else
        {
            cell.menuImage.alpha = 1
            cell.lblMenu_Item_Name.alpha = 1
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       self.view.endEditing(true)
        
        if self.shopCheck == "0"
        {
           var msg = "This ANI Partner Menu is unavailable at the moment please check with your server"
//            var msg = "Your account has been deactivated, please contact ANI support."
            self.view.makeToast(msg)
        }
        else
        {
            self.categoryCollectionView.deselectItem(at: indexPath, animated: true)
            self.startSpinner()
            let temp = ProductMenuArray[indexPath.row]
            //        let tempCheck = temp.ingredient_deceleration_info
            if temp.type != nil
            { // After search use this
                
                
                
                if temp.type == "0"
                { // detail screen
                    
                    
                    if ProductMenuArray[indexPath.row].menu_item_flag == "deactive"
                    {
//                        var offlineMSG = "This ANI Partner Menu is unavailable at the moment please check with your server"
//                        self.view.makeToast(offlineMSG)
//                        self.vcFor?.modalTransitionStyle = .crossDissolve
//                        self.vcFor?.modalPresentationStyle = .overFullScreen
//                        self.vcFor?.isLogin = true
//                        self.vcFor?.actionFor = ""
//                        self.vcFor?.titleFor =  offlineMSG
//                        self.present(self.vcFor!, animated: true, completion: nil)
//                        self.dismiss(animated: true, completion: nil)
                        //                        self.blankView.isHidden = true
                       
                        
                        
                        let controller:CustomAlertBypratikViewController =
                            self.storyboard!.instantiateViewController(withIdentifier: "customAlertVC") as!
                        CustomAlertBypratikViewController
                        controller.view.frame = self.view.bounds;
                        controller.willMove(toParentViewController: self)
                        self.view.addSubview(controller.view)
                        self.addChildViewController(controller)
                        controller.didMove(toParentViewController: self)
                        
                        
                        
                        self.stopSpinner()
                    }
                    else
                    {
                        var count = productSubMenuArray.count
                        print("count : \(count)")
                        
                        self.appdelegate.allergentArray = temp.allengsArray
                        self.appdelegate.ingredientsArray = temp.ingredientsarray
                        self.appdelegate.nutritianArray = temp.nutritianarray
                        self.appdelegate.MenuName = temp.menu_item_group_name ?? ""
                        
                        
                        let buffer = ProductListModelClass()
                        buffer.menu_item_name = temp.menu_item_group_name
                        buffer.menu_item_description = temp.menu_item_description
                        buffer.ingredient_deceleration_info = temp.ingredient_deceleration_info
                        buffer.menu_item_image = temp.menu_item_group_image
                        
                        let productlist = storyboard?.instantiateViewController(withIdentifier: "productDetailViewController")
                            as! productDetailViewController
                        productlist.buffer = buffer
                        //               productlist.issearch = "1"
                        self.navigationController?.pushViewController(productlist, animated: true)
                        //                navigationController?.hidesBarsOnSwipe = true
                        //                self.navigationController?.isNavigationBarHidden = true
                        //                navigationController?.navigationBar.isHidden = true
                        self.stopSpinner()
                        

                    }
                    
                }
                else
                { // list screen
                    
                    let gp_item_id : String =  temp.menu_item_group_id!
                    
                    setDefaultValue(keyValue: kmenu_item_group_id, valueIs: gp_item_id)
                    
                    print("gp_item_id: \(gp_item_id)")
                    _ = temp.menu_item_group_name
                    
                    appdelegate.Mname = temp.menu_item_group_name ?? ""
//                    print("array12345 : \([allergntArray[indexPath.row]])")
                    
                    let productlist = storyboard?.instantiateViewController(withIdentifier: "productSecondViewController")
                        as! productSecondViewController
                    productlist.buffer = allergntArray
                    //productlist.buffer1 = ProductMenuArray
                    self.navigationController?.pushViewController(productlist, animated: true)
                    self.stopSpinner()
                }
                
            }
            else { // Without search use this
                
                let gp_item_id : String =  temp.menu_item_group_id!
                
                setDefaultValue(keyValue: kmenu_item_group_id, valueIs: gp_item_id)
                
                print("gp_item_id: \(gp_item_id)")
                _ = temp.menu_item_group_name
                
                appdelegate.Mname = temp.menu_item_group_name ?? ""
                print("array12345 : \([allergntArray[indexPath.row]])")
                
                let productlist = storyboard?.instantiateViewController(withIdentifier: "productSecondViewController")
                    as! productSecondViewController
                productlist.buffer = allergntArray
                //productlist.buffer1 = ProductMenuArray
                self.navigationController?.pushViewController(productlist, animated: true)
                self.stopSpinner()
                
            }
//            self.stopSpinner()
        }
        }
        

    //     func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    //
    //        if (kind == UICollectionElementKindSectionHeader) {
    //            let headerView:UICollectionReusableView =  collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "collectionViewReuseIdentifire", for: indexPath)
    //
    //            return headerView
    //        }
    //
    //        return UICollectionReusableView()
    //
    //    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        let size = collectionView.frame.size
    //        //  if UIDevice.current.orientation.isPortrait {
    //        //  return CGSize(width: size.width / 2 , height: 2.5)
    //
    //
    //        return CGSize(width: size.width / 2 - 10, height: 150)
    //
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 2.0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 2.0
    //    }
    //
    
    
}

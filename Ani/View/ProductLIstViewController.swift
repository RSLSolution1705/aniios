//
//  ProductLIstViewController.swift
//  Ani
//
//  Created by RSL-01 on 23/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import Floaty
import CoreData
import SVProgressHUD

class ProductLIstViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UISearchBarDelegate {

    
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let SearchBarHeight : Int = 40
    var name : String!
    let context=AppDelegate().persistentContainer.viewContext
    var allergntArray = [allergentModelClass]()
    var searchActive : Bool = false
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet weak var gradientView: UIView!
    
    var ProductMenuArray = [ productMenuListModelClass]()
    var filterArray = [globleClass]()
    var marchantArray = [marchantModelClass]()
    var account_id = ""
    var accountID =  loginClass()
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    var activityIndicator = UIActivityIndicatorView()
    
    @IBOutlet weak var searchtextfield: UITextField!
    
    var merchantId = ""
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         print("ProductListViewController")
        
        self.startSpinner()
        createGradientLayer(view: self.gradientView, width: self.gradientView.frame.size.width, height: self.gradientView.frame.size.height)
        self.title = getValueForKey(keyValue: marchentName)
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
        
        
        let btnRightbutton = UIButton(type: .custom)
        btnRightbutton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnRightbutton.backgroundColor = UIColor.clear
        btnRightbutton.addTarget(self, action: #selector(infobutton(sender:)), for: .touchUpInside)
        btnRightbutton.setImage(UIImage(named: "ic_information_outline_white_48dp.png"), for: .normal)
        let barButton = UIBarButtonItem(customView: btnRightbutton)
        
        //
        //        let btnright = UIButton(type: .custom)
        //        btnright.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        //        btnright.backgroundColor = UIColor.clear
        //        btnright.addTarget(self, action: #selector(search(sender:)), for: .touchUpInside)
        //        btnright.setImage(UIImage(named: "ic_search_white"), for: .normal)
        //        let sideMenubarButton = UIBarButtonItem(customView: btnright)
        //
        
        
        self.navigationItem.rightBarButtonItems = [barButton]
        
        
   //     SVProgressHUD1()
        self.stopSpinner()
        merchantListWebservices()
        FloatyButton()
        
        
        
        
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    @objc func backBtn(sender:UIButton)
    {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    //search
    @objc func infobutton(sender:UIButton)
    {
        
        let svc = storyboard?.instantiateViewController(withIdentifier: "marchentInfoViewController")as! marchentInfoViewController
        self.navigationController?.pushViewController(svc, animated: true)
    }
    
    fileprivate func FloatyButton() {
        let floaty = Floaty()
        floaty.buttonColor = UIColor(hex: "41b93d")
        floaty.itemButtonColor = UIColor(hex: "41b93d")
        floaty.itemTitleColor = UIColor.white
        
        
        floaty.addItem("Favorite MenuItems" ,icon:  UIImage(named: "nav_fevorite.png")) { (item) in
            
            let fav = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteViewController")as! FavouriteViewController
            self.navigationController?.pushViewController(fav, animated: true)
        }
        
        
        floaty.addItem("Recents ViewItems" ,icon: UIImage(named: "ic_speical_offers.png")) { (item) in
            item.buttonColor = UIColor(hex: "41b93d")
            item.titleColor = UIColor(hex: "41b93d")
            item.icon = UIImage(named: "star_green.png")
            let recent = self.storyboard?.instantiateViewController(withIdentifier: "RecentViewController")as! RecentViewController
            self.navigationController?.pushViewController(recent, animated: true)
        }
        
        self.view.addSubview(floaty)
    }
    
//    fileprivate func SVProgressHUD1() {
//        SVProgressHUD.setBackgroundColor(UIColor.clear)
//        SVProgressHUD.AnimationCurve.easeOut
//        SVProgressHUD.AnimationTransition.curlUp
//        SVProgressHUD.setBackgroundLayerColor(.clear)
//    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if searchtextfield.text?.count != 0
        {
            //            globleSearch()
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (searchtextfield.text?.count != 0){
            //            globleSearch()
            self.collectionView?.reloadData()
        }
        return true
    }
    
    var FinalMerID = ""
    
    func merchantListWebservices()
    {
        let headers = ["Content-Type":"Application/json"]
        let _url = get_all_prefered_merchants_new
        let MetaString =  getValueForKey(keyValue: kmetaString)
        print(MetaString)
        let newMetaString = MetaString.replacingOccurrences(of: "ANI-", with: "")
        print("\(newMetaString)")
        setDefaultValue(keyValue: knewMetaString, valueIs: newMetaString)
        FinalMerID = newMetaString
        
        
        
        let parameters: Parameters = ["merchant_id" : FinalMerID]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            //                            self.activityIndicator.startAnimating()
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
//                                            let str = response.result.error?.localizedDescription
//                                            print(response.result.error?.localizedDescription)
//
                                                var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:msg!)
                                        }
                                        else
                                        {
                                            
                                            
                                            let resultarray = jsonDic.object(forKey: "data")as! NSArray
                                            //                                            self.activityIndicator.stopAnimating()
                                            let temp = marchantModelClass()
                                            for i in resultarray
                                            {
                                                let tempobj = i as! NSDictionary
                                                
                                                temp.merchant_id = tempobj.object(forKey: "merchant_id") as? String
                                                
                                                self.merchantId = temp.merchant_id
                                                
                                                self.appdelegate.merchant_id = temp.merchant_id
                                                
                                                setDefaultValue(keyValue: kmarchentId, valueIs: temp.merchant_id)
                                                
                                                print("merchant_id123 = \(String(describing: temp.merchant_id))")
                                                
                                            let keyExists = tempobj.object(forKey: "hide_nutritional_info") as? String != nil
                                                                                                                                          
                                                    if keyExists {
                                                                                                                                          
                                                        temp.hide_nutritional_info = tempobj.object(forKey: "hide_nutritional_info") as? String
                                                        setDefaultValue(keyValue: Khide_nutritional_info, valueIs:  temp.hide_nutritional_info)
                                                        print("hide_nutritional_info = \(temp.hide_nutritional_info ?? "")")
                                            }
                                                
                                                
                                                temp.address = tempobj.object(forKey: "address") as? String
                                                
                                                setDefaultValue(keyValue: KAddress, valueIs:  temp.address)
                                                
                                                temp.avgPreparationTime = tempobj.object(forKey: "avgPreparationTime") as? String
                                                
                                                temp.closing_time = tempobj.object(forKey: "closing_time") as? String
                                                
                                                temp.opening_time = tempobj.object(forKey: "opening_time") as? String
                                                
                                                temp.latitude = tempobj.object(forKey: "latitude") as? String
                                                
                                                setDefaultValue(keyValue: Klatitude, valueIs:  temp.latitude)
                                                
                                                temp.longitude = tempobj.object(forKey: "longitude") as? String
                                                
                                                setDefaultValue(keyValue: Klongitude, valueIs: temp.longitude)
                                                
                                                temp.email = tempobj.object(forKey: "email") as? String
                                                setDefaultValue(keyValue: kemail, valueIs:  temp.email)
                                                
                                                temp.delivery_charges = tempobj.object(forKey: "delivery_charges") as? String
                                                
                                                temp.distance = tempobj.object(forKey: "distance") as? String
                                                
                                                temp.delivery_time = tempobj.object(forKey: "delivery_time") as? String
                                                
                                                temp.delivery_type = tempobj.object(forKey: "delivery_type") as? String
                                                
                                                temp.flag = tempobj.object(forKey: "flag") as? String
                                                
                                                temp.food_type = tempobj.object(forKey: "food_type") as? String
                                                
                                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String
                                                
                                                temp.percentage = tempobj.object(forKey: "percentage") as? String
                                                
                                                temp.mobile_number = tempobj.object(forKey: "mobile_number") as? String
                                                
                                                setDefaultValue(keyValue: KMobile, valueIs: temp.mobile_number)
                                                
                                                temp.mininum_order = tempobj.object(forKey: "mininum_order") as? String
                                                
                                                temp.merchant_tagline = tempobj.object(forKey: "merchant_tagline") as? String
                                                
                                                setDefaultValue(keyValue: ktagline, valueIs:  temp.merchant_tagline)
                                                
                                                
                                                temp.merchant_name = tempobj.object(forKey: "merchant_name") as? String
                                                
                                                temp.home_number = tempobj.object(forKey: "home_number") as? String
                                                
                                                //                                     setDefaultValue(keyValue: khome, valueIs:  temp.home_number)
                                                //
                                                let allergentArray = tempobj.object(forKey: "allergans")as! NSArray
                                                
                                                for item in allergentArray
                                                {
                                                    let tempitem = item as! NSDictionary
                                                    let tempobjItem = allergentModelClass()
                                                    tempobjItem.allerganName = tempitem.object(forKey: "allergan")as? String
                                                    tempobjItem.allergan_id = tempitem.object(forKey: "allergan_id")as? String
                                                    self.allergntArray.append(tempobjItem)
                                                    //                                        print("ashu : \(allergentArray)")
                                                    
                                                    
                                                    
                                                    
                                                }
                                                
                                                
                                                self.marchantArray.append(temp)
                                                self.CategoryList()
                                                //                                    print(self.marchantArray)
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            else
                                if response.result.isFailure
                                {
                                    self.stopSpinner()
                                    
                                    showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please check internet connection")
                            }
                            
                            
                            
                            //                                self.activityIndicator.stopAnimating()
                            
                            
                            
        }
        //         SVProgressHUD.dismiss()
        //          showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        //
    }
    
    func CategoryList()
    {
        
        var data1 = [Ani]()
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let  result = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        let headers = ["Content-Type":"Application/json"]
        let _url = merchant_wise_menuitems
        account_id = appdelegate.kaccountIdString
        print("var account_id : \(account_id)")
        let parameters: Parameters = ["current_merchant_id" : self.merchantId, "account_id" :  (data1[0].accId!) ,"type" : kprefferedmerchant]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            print("Request  \(String(describing: response.request))")
                            print("RESPONSE \(String(describing: response.result.value))")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
//                                            let str = response.result.error?.localizedDescription
//                                            print(response.result.error?.localizedDescription as Any)
                                            
                                                var msg = jsonDic.object(forKey: "msg")as? String
                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:msg!)
                                        }else
                                        {
                                            self.stopSpinner()
                                            
                                            //                                let jsonDic = response.result.value as! NSDictionary
                                            //                                print(jsonDic)
                                            let resultArray = jsonDic.object(forKey: "data") as! NSArray
                                            for i in resultArray
                                            {
                                                let tempobj = i as! NSDictionary
                                                let temp = productMenuListModelClass()
                                                temp.menu_item_group_image = (tempobj.object(forKey: "menu_item_group_image") as! String)
                                                //                                    var imageData = try! Data.init(contentsOf: URL(string: img)!)
                                                //                                    temp.menu_item_group_image = UIImage(data: imageData)
                                                
                                                
                                                
                                                
                                                temp.menu_item_group_id = tempobj.object(forKey: "menu_item_group_id")as? String
                                                temp.menu_item_group_name = tempobj.object(forKey: "menu_item_group_name")as? String
                                                
                                                self.ProductMenuArray.append(temp)
                                                //                                   dispatch_async(
                                                DispatchQueue.main.async
                                                    {
                                                        self.collectionView?.reloadData()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if response.result.isFailure
                            {
                                self.stopSpinner()
                                
                                showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please check internet connection")
                            }
                            
        }
        
        //
        //        SVProgressHUD.dismiss()
        //        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        //
        
    }
    
  func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ProductMenuArray.count
    }
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ProductMenuListCollectionViewCell
        
        if searchActive == false
        {
            
            
            
            let temp1 = ProductMenuArray[indexPath.row]
            
            
            
            let placeholderImage = UIImage(named: "background_placeholder.png")!
            
            let imgUrl = temp1.menu_item_group_image
            let downloadURL = NSURL(string: imgUrl ?? "")!
            cell.menuImage.af_setImage(withURL: downloadURL as URL, placeholderImage: placeholderImage)
            
            cell.lblMenu_Item_Name.text = ProductMenuArray[indexPath.row].menu_item_group_name
            
            
        }
        else
        {
            
            let temp1 = filterArray[indexPath.row]
            let placeholderImage = UIImage(named: "background_placeholder.png")!
            
            let imgUrl = temp1.menu_item_image
            let downloadURL = NSURL(string: imgUrl ?? "")!
            cell.menuImage.af_setImage(withURL: downloadURL as URL, placeholderImage: placeholderImage)
            
            cell.lblMenu_Item_Name.text = filterArray[indexPath.row].menu_item_name
            
            
        }
        
        
        return cell
        // Configure the cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let temp = ProductMenuArray[indexPath.row]
        
        let gp_item_id : String =  temp.menu_item_group_id!
        
        setDefaultValue(keyValue: kmenu_item_group_id, valueIs: gp_item_id)
        
        print("gp_item_id: \(gp_item_id)")
        
        _ = temp.menu_item_group_name
        
        appdelegate.Mname = temp.menu_item_group_name ?? ""
        
        let productlist = storyboard?.instantiateViewController(withIdentifier: "productSecondViewController")
            as! productSecondViewController
        
        print("array12345 : \([allergntArray[indexPath.row]])")
        
        productlist.buffer = allergntArray
        //        productlist.buffer1 = ProductMenuArray
        self.navigationController?.pushViewController(productlist, animated: true)
        self.stopSpinner()
    }
    
    
    
   func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView:UICollectionReusableView =  collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "collectionViewReuseIdentifire", for: indexPath)
            
            return headerView
        }
        
        return UICollectionReusableView()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

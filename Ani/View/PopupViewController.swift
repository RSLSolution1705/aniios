//
//  PopupViewController.swift
//  Ani
//
//  Created by Mac on 04/06/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

protocol PopCategoryPage: class {
    func PopUpOkClickCategory(status: Bool)
}

class PopupViewController: UIViewController {
    
    weak var delegate: PopCategoryPage?
    
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var lblPopUp: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var titleFor1 = ""
    var actionFor1 = ""
    var isLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("PopupViewController")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setView()
        
    }
    
    func setView() {
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.lblPopUp.text =  " Do you really want to leave this restaurant?"
        
        self.outerView.layer.cornerRadius = 5.0
        self.outerView.clipsToBounds = true
        
        self.btnOk.layer.cornerRadius = self.btnOk.frame.size.height / 2.0
        self.btnOk.clipsToBounds = true
        
        self.btnCancel.layer.cornerRadius = self.btnCancel.frame.size.height / 2.0
        self.btnCancel.clipsToBounds = true
        
    }
    
    @IBAction func btnClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
    }
    
    @IBAction func btnOK(_ sender: Any) {
        
        
        self.delegate?.PopUpOkClickCategory(status: true)
        //        if self.actionFor1 == "home" {
        //
        //
        //
        //            self.delegate?.PopUpOkClick1(status: true)
        //        }
        //        else {
        //            self.delegate?.PopUpOkClick1(status: false)
        //        }
        //
        self.dismiss(animated: true, completion: nil)
        
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
    }
    
    
    
}

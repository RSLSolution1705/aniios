//
//  sideMenuViewController.swift
//  Ani
//
//  Created by RSL-01 on 16/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SVProgressHUD

class sideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PopUpDoneDelegate,PopUpDoneDelegate1 {
    
    @IBOutlet weak var lblContamination: UILabel!
    var checkRes = "0"
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var buttonMenu: UIBarButtonItem!
    @IBOutlet weak var sideMenuVIew: UIView!
    @IBOutlet weak var sideMenuTableView: UITableView!
    @IBOutlet weak var lblMarchentName: UILabel!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var qrCodePressButton: UIButton!
    @IBOutlet weak var marchantImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet var staticPopUp: UIView!
    @IBOutlet weak var MarchantName: UILabel!
    @IBOutlet var popupView: UIView!
     var vcFor : ValidationPopupViewController? = nil
    var vcForlogout : signUpPopupViewController? = nil
    let context=AppDelegate().persistentContainer.viewContext
    var btnRightbutton = UIButton(type: .custom)
    @IBOutlet weak var btncontinue: UIButton!
    
    @IBAction func popupCancel(_ sender: Any) {
       
        self.internetPopup.isHidden = true
 self.qrCodePressButton.isHidden = false
         self.qrCodePressButton.isEnabled = false
        self.btnRightbutton.isEnabled = true;
    }
 
    @IBOutlet weak var btnshare: UIButton!
   
    @IBOutlet weak var internetPopup: UIView!
    @IBOutlet weak var poplbl: UILabel!
    @IBOutlet weak var btnShowDetail: UIImageView!
    
    
    var marchantArray = [marchantModelClass]()
    var issideViewOpen: Bool = false
    var marImage :String!
    var ProductMenuArray = [ productMenuListModelClass]()
    var allergntArray = [allergentModelClass]()
    var marchantName = ""
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    
//    var menuNameArray  = [ "ANI","Terms & Conditions","Contact Us","Recommend Restaurant", "Help","Logout"]
//    var menuImageArray = [ UIImage(named:"6n.png") ,UIImage(named:"ic_terms_conditions.png") ,UIImage(named:"nav_contactus_white (1).png") ,UIImage(named:"hand_new.png"),UIImage(named: "help_new.png") ,UIImage(named: "_logout_white_new.png")]
    
    var menuNameArray  = [ "ANI","Recent Visits","Favourite Products","Recently Viewed Products","Recommend Restaurant", "Terms & Conditions","Contact Us","Help","Logout",""]
    
    var menuImageArray = [ UIImage(named:"6n.png") ,UIImage(named:"sideMenu_recent.png"),UIImage(named:"sideMenu_heart_new.png"),UIImage(named:"sideMenu_recent.png"),UIImage(named:"hand_new.png"),UIImage(named:"ic_terms_conditions.png") ,UIImage(named:"nav_contactus_white (1).png") ,UIImage(named: "help_new.png") ,UIImage(named: "_logout_white_new.png"),UIImage(named:"")]
    
    var flafDelegateMethod = true
//    "fab_fav_icon.png"
//    recent_view.png
    //_logout_white_new.png
  //  hand_new
    //help_new
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?

    var gesture = UITapGestureRecognizer(target: self, action: #selector(menuIcon))
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    
    
    @IBAction func closeButton(_ sender: Any) {
        staticPopUp.removeFromSuperview()
        popupView.removeFromSuperview()
          self.btnRightbutton.isEnabled = true;
         self.qrCodePressButton.isHidden = false
         self.qrCodePressButton.isEnabled = true
    }
    
    @IBAction func buttonShare(_ sender: Any) {
        
        let originalString = "Using the ANI app at " + self.marchantName + " to check my food allergy and nutritional information.To get more details click on link"
        
        let image = appdelegate.MarImage
        
        self.appdelegate.marchentNameForshare = self.marchantName
        
        let sent = "Check out our ANI Partner at this location."
        let msg = "for more info."
        let addres = getValueForKey(keyValue: KAddress)
        print(addres)
        let strurl = addres.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(strurl)
        
        
        let link = NSURL(string:"http://maps.google.com/maps?q=\(strurl ?? "")")
        print(link!)

       let myWebsite1 = NSURL(string: "http://www.theaniapp.com/")
        
        
        let str = [originalString + "http://www.theaniapp.com/" + sent + "http://maps.google.com/maps?q=\(strurl ?? "")"] as? [Any]
       
        //        kmarchentId
        var marchentId = getValueForKey(keyValue: kmarchentId)
        if let site = NSURL(string: "https://www.theaniapp.com/merchant_info.php?mid=\(marchentId)")
        {
            let objectsToShare = [originalString,site]as! [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
            activityVC.setValue("Find food allergy and nutritional value", forKey: "subject")
            activityVC.popoverPresentationController?.sourceView = sender as! UIView
            self.present(activityVC, animated: true, completion: nil)
        }
        
        
//        if let myWebsite = NSURL(string: "http://www.theaniapp.com/")        {
//        let objectsToShare = str
//            print(objectsToShare)
//            let activityVC = UIActivityViewController(activityItems: objectsToShare as! [Any], applicationActivities: nil)
//            //activityVC.setValue("Find food allergy and nutritional value", forKey: "Subject")
//            activityVC.popoverPresentationController?.sourceView = self.btnshare
//            self.present(activityVC, animated: true, completion: nil)
//        }
}
    
    
//    let text = "This is the text...."
//    let image = UIImage(named: "Product")
//    let myWebsite = NSURL(string:"https://stackoverflow.com/users/4600136/mr-javed-multani?tab=profile")
//    let shareAll= [text , image! , myWebsite]
//    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
//    activityViewController.popoverPresentationController?.sourceView = self.view
//    self.present(activityViewController, animated: true, completion: nil)
    
//}
    
    
    @IBAction func updateProfilButton(_ sender: Any) {
        
        var edit = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController")as? EditProfileViewController
        self.navigationController?.pushViewController(edit!, animated: true)
        
    }
    
    @IBAction func buttonContinue(_ sender: Any)
    {
     
//        merchantListWebservicesToCheckStatus()
//        if self.checkRes == "true"
//        {

        self.btnRightbutton.isEnabled = true;
        self.popupView.removeFromSuperview()
        self.qrCodePressButton.isHidden = false
        self.qrCodePressButton.isEnabled = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategaryListViewController") as! CategaryListViewController
        self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else
//        {
        
//            self.navigationController?.popToRootViewController(animated: true)
//        }
        
    }
    
    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.stopSpinner()
        appdelegate.closePopUpFlag = 0
         self.qrCodePressButton.isEnabled = true
        self.blankView.isHidden = true
        self.internetPopup.isHidden = true
        gesture = UITapGestureRecognizer(target: self, action: #selector(menuIcon))
        self.btncontinue.layer.cornerRadius = self.btncontinue.frame.size.height / 2.0
        self.btncontinue.clipsToBounds = true
        
        self.btnshare.layer.cornerRadius = self.btnshare.frame.size.height / 2.0
        self.btnshare.clipsToBounds = true
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        vcForlogout =  self.storyboard?.instantiateViewController(withIdentifier: "signUpPopupViewController") as? signUpPopupViewController
        vcForlogout?.delegate = self
        
        self.btnShowDetail.layer.cornerRadius = self.btnShowDetail.frame.size.height / 2.0
        self.btnShowDetail.clipsToBounds = true
        
        profileImage.setRounded()

         profileImage.clipsToBounds = true
        let navigationBarHeight: CGFloat = navigationController!.navigationBar.frame.height
        let viewHeight = view.frame.size.height
        let popupHeight = popupView.frame.size.height
        popupView.frame = CGRect(x: 0, y: 0 , width: 290, height: 380)
        self.popupView.center = self.view.center
        let deviceTokan = getValueForKey(keyValue: kdeviceToken)
        print("\(deviceTokan)")
        self.navigationController?.isNavigationBarHidden = false
     
      self.staticPopUp.alpha = 1
        
         btnRightbutton = UIButton(type: .custom)
        btnRightbutton.setImage(#imageLiteral(resourceName: "icons-menu_new").withRenderingMode(.alwaysOriginal), for: .normal)
       // btnRightbutton.setImage(UIImage(named: "menubar")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btnRightbutton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
       // btnRightbutton.contentMode = .scaleAspectFit
        btnRightbutton.addTarget(self, action: #selector(menuIcon), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnRightbutton)
        
        qrCodePressButton.layer.cornerRadius = CGFloat(10)
        
        qrCodePressButton.clipsToBounds = true
 
        
       let Name = getValueForKey(keyValue: kfirstName)
        userName.text = getValueForKey(keyValue: kfirstName) + " " + getValueForKey(keyValue: klastName)
        
        navigationController?.navigationBar.barTintColor = UIColor(hex: "41b93d")

         self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        0
        
        sideMenuVIew.isHidden = true
        profileView.isHidden = true
        blankView.isHidden = true
        sideMenuVIew.backgroundColor = UIColor.clear
        issideViewOpen = false
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func merchantListWebservicesToCheckStatus()   // By Pratik
    {
        self.qrCodePressButton.isHidden = true
        let headers = ["Content-Type":"Application/json"]
        let _url = get_all_prefered_merchants_new
        var MetaString =  getValueForKey(keyValue: kmetaString)
        print(MetaString)
        if(MetaString.range(of:"ANI-") == nil)
        {
            var alert = UIAlertController(title: "QRCode", message: "Invalid QR code is detected", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert,animated : true ,completion: nil)
            return
        }
        
        //   This ANI Partner Location is unavailable at the moment please scan again later.
        
        let newMetaString = MetaString.replacingOccurrences(of: "ANI-", with: "")
        print("newMetaString :\(newMetaString)")
        setDefaultValue(keyValue: knewMetaString, valueIs: newMetaString)
        
        let parameters: Parameters = ["merchant_id" : newMetaString, "account_id": getValueForKey(keyValue: kregisterAccountId)]
        
        print(parameters)
       
        Alamofire.request(_url,method: .post,parameters: parameters,encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            
            print("Request  \(response.request)")
            print("RESPONSE \(response.result.value)")
            print("RESPONSE \(response.result)")
            print("RESPONSE \(response)")
            
            if response.result.isSuccess
            {
                print("SUKCES with \(response)")
                if (response.result != nil)
                {
                    self.stopSpinner()
                    self.blankView.isHidden = true
                    let jsonDic = response.result.value as! NSDictionary
                    print(jsonDic)
                    if let res = jsonDic.value(forKey: "result") as? String
                    {
                        if res == "failed"
                        {
                            self.stopSpinner()
                            self.blankView.isHidden = true
                            self.qrCodePressButton.isHidden = false
                            self.qrCodePressButton.isEnabled = true
                            let jsonDic = response.result.value as! NSDictionary
                            print(jsonDic)
                            var msg = "This ANI Partner Menu is unavailable at the moment please check with your server"
                          //  var msg = "Your account has been deactivated, please contact ANI support."
                            self.vcFor?.modalTransitionStyle = .crossDissolve
                            self.vcFor?.modalPresentationStyle = .overFullScreen
                            self.vcFor?.isLogin = true
                            self.vcFor?.actionFor = ""
                            self.vcFor?.titleFor =  msg
                            self.present(self.vcFor!, animated: true, completion: nil)
                        }
                        else
                        {
                   
                            let jsonDic = response.result.value as! NSDictionary
                            print(jsonDic)
                            self.stopSpinner()
                            self.blankView.isHidden = true
                            let resultarray = jsonDic.object(forKey: "data")as! NSArray
                            let temp = marchantModelClass()
                            for i in resultarray
                            {
                                let tempobj = i as! NSDictionary
                                temp.merchant_id = tempobj.object(forKey: "merchant_id") as? String
                                print("merchant_id123 = \(String(describing: temp.merchant_id))")
                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String
                                self.checkRes = temp.shop_open
                                
                                 let keyExists = tempobj.object(forKey: "hide_nutritional_info") as? String != nil
                                                                                                                              
                                        if keyExists {
                                                                                                                              
                                        temp.hide_nutritional_info = tempobj.object(forKey: "hide_nutritional_info") as? String
                                        setDefaultValue(keyValue: Khide_nutritional_info, valueIs:  temp.hide_nutritional_info)
                                        print("hide_nutritional_info = \(temp.hide_nutritional_info ?? "")")
                                    }
                                
//                                self.checkRes = (tempobj.object(forKey: "shop_open") as? String)!
                               
//                                if temp.shop_open == "true"
//                                {
//                                    self.btnRightbutton.isEnabled = true;
//                                    self.popupView.removeFromSuperview()
//                                    self.qrCodePressButton.isHidden = false
//                                    self.qrCodePressButton.isEnabled = true
//                                }
//                                else
//                                {
//                                    self.navigationController?.popToRootViewController(animated: true)
//                                }
//
                                
                                print(self.checkRes)
                                self.stopSpinner()
                            }
                        }
                    }
                }
            }
            else if response.result.isFailure
            {
                self.stopSpinner()
                var str = response.result.error?.localizedDescription
                print(response.result.error?.localizedDescription)
                
                if str == "A server with the specified hostname could not be found."
                {
                    str = "You are offline please check your internet connection"
                }
                self.view.addSubview(self.staticPopUp)
                self.stopSpinner()
                self.blankView.isHidden = true
                self.qrCodePressButton.isHidden = false
                self.popupView.isHidden = false
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  str!
                self.present(self.vcFor!, animated: true, completion: nil)
                self.staticPopUp.removeFromSuperview()
            }
        }
    }
    
    
    
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    func PopUpOkClick1(status: Bool) {
        
        if status {
            if Reachability.isConnectedToNetwork() {
//                SVProgressHUD.show()
                logout()
            }
            else {
        //        SVProgressHUD.dismiss()
                self.stopSpinner()
                DispatchQueue.main.async {
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  Internet_Connection_Alert
                self.present(self.vcFor!, animated: true, completion: nil)
                }
                
            }
           
            
//            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
//            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          self.qrCodePressButton.isEnabled = true
        appdelegate.recentflag = 0
        appdelegate.favflag = 0
        appdelegate.categoryflag = 0
             //self.view.addSubview(self.staticPopUp)
       
        
        //        if let fbImg = defaults.object(forKey: "FBProfile") as? String{
        //
        //            let imageData = try! Data.init(contentsOf: URL(string: fbImg)!)
        //            self.profileImage.image = UIImage(data: imageData)
        //        }
        
        if let imgUrl = defaults.object(forKey: "picture") as? String
        {
            
            Alamofire.request(imgUrl, method: .get).response { response in
                guard let image = UIImage(data:response.data!) else {
                    // Handle error
                    autoreleasepool {
                        self.profileImage.image = UIImage(named: "ic_profil 4")
                        
                    }
                    return
                }
                autoreleasepool {
                    let imageData = UIImageJPEGRepresentation(image,1.0)
                    self.profileImage.image = UIImage(data : imageData!)
                }
            }
        }
        else {
            self.profileImage.image = UIImage(named: "ic_profil 4")

        }
        
        
        if  appdelegate.flagscanQr == 1
        {
            appdelegate.flagscanQr = 0
//           self.poplbl.text = Internet_Connection_Alert
            
            if Reachability.isConnectedToNetwork() {
        
                 blankView.isHidden = false
                self.startSpinner()
                self.merchantListWebservices()
            }else
            {
               
                
                DispatchQueue.main.async {
                    self.stopSpinner()
                    self.blankView.isHidden = false
                    self.popupView.isHidden = false
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                }
                
                
//                DispatchQueue.main.async {
//                let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
//                let viewHeight = self.view.frame.size.height
//                let popupHeight = self.staticPopUp.frame.size.height
//
//                self.staticPopUp.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
//                self.staticPopUp.center = self.view.center
//                self.staticPopUp.isMultipleTouchEnabled = true
//                self.staticPopUp.isExclusiveTouch = true
//                self.btnRightbutton.isEnabled = false
//                self.qrCodePressButton.isHidden = true
//                self.internetPopup.isHidden = false
//
//                 self.poplbl.text = Internet_Connection_Alert
//
//                self.internetPopup.isUserInteractionEnabled = true
//                self.internetPopup.isExclusiveTouch = true
//                //self.view.addSubview(self.staticPopUp)
//              //self.view.addSubview(self.staticPopUp)
//               // self.stopSpinner()
//                //self.view.addSubview(self.internetPopup)
//                   //self.poplbl.text = Internet_Connection_Alert
//                }

            }



        }
    }
    
   
    @IBAction func okButtonstaticPopup(_ sender: Any) {
         self.popupView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            // HERE
//                                    self.popupView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            self.popupView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            
            let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
            let viewHeight = self.view.frame.size.height
            let popupHeight = self.staticPopUp.frame.size.height
            
            //                                                    self.popupView.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
            self.popupView.center = self.view.center
          self.view.addSubview(self.popupView)
            //
        }) { (finished) in
            UIView.animate(withDuration: 1, animations: {
//                 self.popupView.transform = CGAffineTransform.identity
//                            self.popupView.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
                
            })
        }
        
         self.staticPopUp.removeFromSuperview()
        
       
        
    }
    
    @IBAction func addImageButton(_ sender: Any) {
        
        
        
        
    }
    
    
    
    @IBAction func qrCodePressButton(_ sender: Any) {
        self.qrCodePressButton.isEnabled = false
//        appdelegate.flagscanQr = 0
        
    }
    func merchantListWebservices()
    {
        self.qrCodePressButton.isHidden = true
        let headers = ["Content-Type":"Application/json"]
        let _url = get_all_prefered_merchants_new
        var MetaString =  getValueForKey(keyValue: kmetaString)
        print(MetaString)
        if(MetaString.range(of:"ANI-") == nil){
            var alert = UIAlertController(title: "QRCode", message: "Invalid QR code is detected", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert,animated : true ,completion: nil)
            return
            
        }
        
        //   This ANI Partner Location is unavailable at the moment please scan again later.
        
        let newMetaString = MetaString.replacingOccurrences(of: "ANI-", with: "")
        print("newMetaString :\(newMetaString)")
        setDefaultValue(keyValue: knewMetaString, valueIs: newMetaString)
        
        let parameters: Parameters = ["merchant_id" : newMetaString, "account_id": getValueForKey(keyValue: kregisterAccountId)]
        
        print(parameters)
        
        
        
        
        
        Alamofire.request(_url,method: .post,parameters: parameters,encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                             //         SVProgressHUD.dismiss()
                                    self.stopSpinner()
                                    self.blankView.isHidden = true
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                          //   SVProgressHUD.dismiss()
                                            self.stopSpinner()
                                             self.blankView.isHidden = true
                                             self.qrCodePressButton.isHidden = false
                                             self.qrCodePressButton.isEnabled = true
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            var msg = jsonDic.object(forKey: "msg")as? String
//                                            setDefaultValue(keyValue: kmsg, valueIs: msg!)
//                                          var msg = "This ANI Partner Menu is unavailable at the moment please check with your server"
                                         //  var msg = "Your account has been deactivated, please contact ANI support."

                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)

                                        }
                                        else{
                                            
                                            
                                        
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                             self.stopSpinner()
                                             self.blankView.isHidden = true
                                            let resultarray = jsonDic.object(forKey: "data")as! NSArray
                                            let temp = marchantModelClass()
                                            for i in resultarray
                                            {
                                                let tempobj = i as! NSDictionary
                                                
                                                temp.merchant_id = tempobj.object(forKey: "merchant_id") as? String
                                                //      self.merchantId = temp.merchant_id
                                                //      self.appdelegate.merchant_id = temp.merchant_id
                                                setDefaultValue(keyValue: kmarchentId, valueIs: temp.merchant_id)
                                                print("merchant_id123 = \(String(describing: temp.merchant_id))")
                                                temp.address = tempobj.object(forKey: "address") as? String
                                                setDefaultValue(keyValue: KmarchentAddress, valueIs:  temp.address)
                                                
                                                let keyExists = tempobj.object(forKey: "hide_nutritional_info") as? String != nil
                                                                                                 
                                                    if keyExists {
                                                                                                 
                                                        temp.hide_nutritional_info = tempobj.object(forKey: "hide_nutritional_info") as? String
                                                        setDefaultValue(keyValue: Khide_nutritional_info, valueIs:  temp.hide_nutritional_info)
                                                            print("hide_nutritional_info = \(temp.hide_nutritional_info ?? "")")
                                                }
                                                
                                                temp.merchant_name = (tempobj.object(forKey: "merchant_name")as! String)
                                                self.marchantName = temp.merchant_name
                                                temp.avgPreparationTime = tempobj.object(forKey: "avgPreparationTime") as? String
                                                temp.closing_time = tempobj.object(forKey: "closing_time") as? String
                                                temp.opening_time = tempobj.object(forKey: "opening_time") as? String
                                                temp.latitude = tempobj.object(forKey: "latitude") as? String
                                                temp.longitude = tempobj.object(forKey: "longitude") as? String
                                                temp.email = tempobj.object(forKey: "email") as? String
                                                temp.delivery_charges = tempobj.object(forKey: "delivery_charges") as? String
                                                
                                                temp.distance = tempobj.object(forKey: "distance") as? String
                                                temp.delivery_time = tempobj.object(forKey: "delivery_time") as? String
                                                temp.delivery_type = tempobj.object(forKey: "delivery_type") as? String
                                                temp.flag = tempobj.object(forKey: "flag") as? String
                                                temp.food_type = tempobj.object(forKey: "food_type") as? String
                                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String
                                                temp.percentage = tempobj.object(forKey: "percentage") as? String
                                                temp.mobile_number = tempobj.object(forKey: "mobile_number") as? String
                                                temp.mininum_order = tempobj.object(forKey: "mininum_order") as? String
                                                temp.merchant_tagline = tempobj.object(forKey: "merchant_tagline") as? String
                                                temp.merchant_name = tempobj.object(forKey: "merchant_name") as? String
                                                setDefaultValue(keyValue: kmarchentName, valueIs:  temp.merchant_name)
                                                temp.home_number = tempobj.object(forKey: "home_number") as? String
                                                temp.merchant_image = tempobj.object(forKey: "merchant_image") as? String ?? ""
                                                
                                                
                                                temp.is_master = tempobj.object(forKey: "is_master") as? String
                                              
                                                var arrayToPass = [ProductListModelClass]()
                                                var tempArray = tempobj.object(forKey: "sub_partners_data") as? NSArray
                                                if tempArray != nil
                                                {
                                                    for temp in tempArray!{
                                                         var modelClassObj = ProductListModelClass()
                                                        var i = temp as! NSDictionary
                                                        
                                                        modelClassObj.merchantName = (i.value(forKey: "merchant_name") as! String)
                                                        modelClassObj.merchantImage = (i.value(forKey: "merchant_image") as! String)
                                                        modelClassObj.merchantID = (i.value(forKey: "merchant_id") as! String)
                                                        arrayToPass.append(modelClassObj)
                                                        
                                                    }
                                                    
                                                    print(arrayToPass)
                                                    for i in arrayToPass
                                                    {
                                                        var modelClassObj2 = ProductListModelClass()
                                                        print(modelClassObj2.merchantName)
                                                    }
                                                }
                                                
//                                                print(modelClassObj)
                                                print(temp.is_master)
                                                
                                                
                                                    temp.contamination_msg = tempobj.object(forKey: "cross_contamination")as? String ?? ""
                                                    self.MarchantName.text = self.marchantName
                                                    setDefaultValue(keyValue: marchentName, valueIs: self.marchantName)
                                                    self.lblMarchentName.text = "Using the ANI app at " + self.marchantName + " to check my food allergy and nutritional information."
                                                    if temp.merchant_image == ""{
                                                        self.marchantImage.image = UIImage(named: "background_placeholder")
                                                    }
                                                    else{
                                                        var urlString = temp.merchant_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                                                        
                                                        
                                                        
                                                        self.marchantImage.image = UIImage(url: URL(string: urlString ?? ""))
                                                        
                                                        self.appdelegate.MarImage =  UIImage(url: URL(string: urlString ?? ""))
                                                        setDefaultValue(keyValue: kMerchantImage, valueIs: temp.merchant_image)
                                                        
                                                        
                                                    }
                                                    
                                                
                                                    
                                                    var allergentArray = tempobj.object(forKey: "allergans")as! NSArray
                                                    for item in allergentArray
                                                    {
                                                        let tempitem = item as! NSDictionary
                                                        let tempobjItem = allergentModelClass()
                                                        tempobjItem.allerganName = tempitem.object(forKey: "allergan")as! String
                                                        tempobjItem.allergan_id = tempitem.object(forKey: "allergan_id")as! String
                                                        self.allergntArray.append(tempobjItem)
                                                        
                                                        //((viewHeight + navigationBarHeight)/2)-(popupHeight/2)
                                                        
                                                    }
                                                    
                                                    
                                                    self.marchantArray.append(temp)
                                                
                                                UserDefaults.standard.setValue("", forKey: "fromParent")
                                                UserDefaults.standard.synchronize()
                                                
                                                if temp.is_master == "true"{
                                                    print("This is master - Go to listing")
                                                    //                                                    staticPopUp.removeFromSuperview()
                                                    //      popupView.removeFromSuperview()
                                                    self.btnRightbutton.isEnabled = true;
                                                    self.qrCodePressButton.isHidden = false
                                                    self.qrCodePressButton.isEnabled = true
                                                    
                                                    let masterVC = self.storyboard?.instantiateViewController(withIdentifier: "MasterScanViewController")as! MasterScanViewController
                                                    masterVC.childDataArray = arrayToPass
                                                    self.navigationController?.pushViewController(masterVC, animated: true)
                                                   
                                                    let masterParentID = tempobj.object(forKey: "merchant_id") as? String
                                                    UserDefaults.standard.setValue("fromParent", forKey: "fromParent")
                                                    UserDefaults.standard.setValue(masterParentID, forKey: "parentID")
                                                    UserDefaults.standard.synchronize()
                                                    
                                                }
                                                else{
                                                    //      SVProgressHUD.dismiss()
                                                    self.stopSpinner()
                                                    
//                                                    var parentID = tempobj.object(forKey: "parent_id") as? String
//                                                    if parentID == nil || parentID == "0"{
//                                                        parentID = ""
//                                                    }
//                                                    UserDefaults.standard.setValue("", forKey: "fromParent")
//                                                    UserDefaults.standard.setValue(parentID, forKey: "parentID")
//                                                    UserDefaults.standard.synchronize()
//
//                                                    let checkStoredParentID = UserDefaults.standard.value(forKey: "parentID") as! String
//                                                    print(checkStoredParentID)
                                                    
                                                    var parentID = tempobj.object(forKey: "partner_id") as? String
                                                     if parentID == nil || parentID == "0"{
                                                         parentID = ""
                                                    }
                                                    UserDefaults.standard.setValue("", forKey: "fromParent")
                                                    UserDefaults.standard.setValue(parentID, forKey: "parentID")
                                                    UserDefaults.standard.synchronize()
                                                                                                    
                                                        let checkStoredParentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                                        print(checkStoredParentID)
                                                    
                                                    self.blankView.isHidden = true
                                                    
                                                    let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
                                                    let viewHeight = self.view.frame.size.height
                                                    let popupHeight = self.staticPopUp.frame.size.height
                                                    
                                                    self.lblContamination.text = temp.contamination_msg
                                                    self.staticPopUp.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
                                                    self.staticPopUp.center = self.view.center
                                                    self.btnRightbutton.isEnabled = false;
                                                    self.view.addSubview(self.staticPopUp)
                                                    
                                                 
                                                }
                                                
                                               
                                              
                                            }
                                        }
                                    }
                                }
                               
                            }
                            else if response.result.isFailure
                            {
                            
                                self.stopSpinner()
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "A server with the specified hostname could not be found."
                                {
                                    str = "You are offline please check your internet connection"
                                    
                                }
                                self.view.addSubview(self.staticPopUp)
                    
                                self.stopSpinner()
                                self.blankView.isHidden = true
                                self.qrCodePressButton.isHidden = false
                                self.popupView.isHidden = false
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                    self.staticPopUp.removeFromSuperview()
                                
                            }
                            
                            
                            
                            
        }
       
        
    }
    
    
    
    
    
    
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return menuNameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! sideMenuTableViewCell
        cell.sideMenuLeftimg.image = menuImageArray[indexPath.row]
        cell.sideMenuLeftLabel.text = menuNameArray[indexPath.row]
        return cell
        
    }
   
      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //[ "ANI","Terms & Condition","Contact Us","Recommend Restaurant","logout"]
        if (indexPath.row == 0 ) // for Ani menu
        {
            sideMenuTableView.isHidden = true
            sideMenuVIew.isHidden = true
            profileView.isHidden = true
             blankView.isHidden = true
        }
        else if (indexPath.row == 1)
        {
            appdelegate.categoryflag = 1
            var vc = storyboard?.instantiateViewController(withIdentifier: "RecentVisitViewController")as! RecentVisitViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 2)
        {
            appdelegate.favflag = 3
            var vc = storyboard?.instantiateViewController(withIdentifier: "RecentVisitViewController")as! RecentVisitViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 3)
        {
              appdelegate.recentflag = 2
            var vc = storyboard?.instantiateViewController(withIdentifier: "RecentVisitViewController")as! RecentVisitViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if (indexPath.row == 4 ) // for lagout menu
        {
            var vc = storyboard?.instantiateViewController(withIdentifier: "RecommendedViewController")as! RecommendedViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if( indexPath.row == 5)
        {
            var vc = storyboard?.instantiateViewController(withIdentifier: "termsAndConditionViewController")as! termsAndConditionViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 6)
        {
            var vc = storyboard?.instantiateViewController(withIdentifier: "contactUsViewController")as! contactUsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
       else if (indexPath.row == 7)
        {
            var vc = storyboard?.instantiateViewController(withIdentifier: "HelpViewController")as! HelpViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (indexPath.row == 8)
        {
            self.startSpinner()
            self.vcForlogout?.modalTransitionStyle = .crossDissolve
            self.vcForlogout?.modalPresentationStyle = .overFullScreen
            self.vcForlogout?.isLogin = true
            self.vcForlogout?.actionFor1 = ""
            //  self.vcFor?.titleFor = "Are you sure you want to logout"
            self.stopSpinner()
            self.present(self.vcForlogout!, animated: true, completion: nil)
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
        
    }
    
    func logout()
    {
        
        var role = getValueForKey(keyValue: krole)
        var acc1 : String!
        let headers = ["Content-Type":"Application/json"]
        let _url = user_logout
        var data1 = [Ani]()
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let result = try! self.context.fetch(fetch)
     //   let account_id = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
            {
                data1.append(data as! Ani)

            }
        
     let parameters: Parameters = ["account_id": getValueForKey(keyValue: kregisterAccountId),"type": "Consumer"]
      //  let parameters: Parameters = ["account_id": getValueForKey(keyValue: kregisterAccountId),"type": getValueForKey(keyValue: krole),
//]
        print(parameters)
        
        Alamofire.request(_url,method: .post,parameters: parameters,encoding: URLEncoding.queryString,headers: headers).responseJSON { (response) in
                            
            print("Request  \(response.request)")
                            
            print("RESPONSE \(response.result.value)")
            print("RESPONSE \(response.result)")
            print("RESPONSE \(response)")
            
            if response.result.isSuccess {
                print("SUKCES with \(response)")
                
                if (response.result != nil)
                {
                    let jsonDic = response.result.value as! NSDictionary
                    print(jsonDic)
                    if let res = jsonDic.value(forKey: "result") as? String
                    {
                        if res == "failed"
                        {
                            
                            self.stopSpinner()
                             self.blankView.isHidden = true
                            //popup
                            
//                            var str = response.result.error?.localizedDescription
//                            print(response.result.error?.localizedDescription)
                            var str = jsonDic.object(forKey: "msg")as? String
                            
                            self.vcFor?.modalTransitionStyle = .crossDissolve
                            self.vcFor?.modalPresentationStyle = .overFullScreen
                            self.vcFor?.isLogin = true
                            self.vcFor?.actionFor = ""
                            self.vcFor?.titleFor =  str!
                            self.present(self.vcFor!, animated: true, completion: nil)
                            
                            
                            
                         //   showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                        }
                        else{
                            
                         self.stopSpinner()
                             self.blankView.isHidden = true
                            
                            let jsonDic = response.result.value as! NSDictionary
                            print(jsonDic)
                            
                            UserDefaults.standard.set(false, forKey: "isUserLog")
                            

                            
                            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Ani")
                            
                            // Create Batch Delete Request
                            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                            
                            do {
                                try self.context.execute(batchDeleteRequest)
                                
                            } catch {
                                // Error Handling
                            }

                            let defaults = UserDefaults.standard
                            defaults.removeObject(forKey: "picture")
//                            let dictionary = defaults.dictionaryRepresentation()
//                            dictionary.keys.forEach { key in
//                                defaults.removeObject(forKey: key)
//                            }
//                             setDefaultValue(keyValue: allergentIdString, valueIs: str)
                       
//                        let defaults = UserDefaults.standard
                        defaults.removeObject(forKey:allergentIdString )
                          GIDSignIn.sharedInstance().signOut()
                            self.appdelegate.appdeldgtenewArray.removeAll()
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
                                self.navigationController?.pushViewController(home, animated: true)

                            }
                                    }}}
                                
                            else {
                                
                self.stopSpinner()
                 self.blankView.isHidden = true
                let str = response.result.error?.localizedDescription
                print(response.result.error?.localizedDescription)
                
                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  str!
                self.present(self.vcFor!, animated: true, completion: nil)
                
                
                
//                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
//                self.addChildViewController(pop!)
//                pop!.view.frame = self.view.frame
//                self.view.addSubview(pop!.view)
//                pop?.didMove(toParentViewController: self)
                            }
                            
                            
        }
    }
}
//----------------------------------------------SideMenuAction-------------------------------------------

extension sideMenuViewController {
    @objc func menuIcon(sender:UIButton)
    {
        print("******")
        
        profileView.isHidden = false
        sideMenuTableView.isHidden = false
        sideMenuVIew.isHidden = false
         blankView.isHidden = false
        if !(issideViewOpen) {
            
            issideViewOpen = true
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationDelegate(self)
            UIView.beginAnimations("TableAnimation", context: nil)
            UIView.commitAnimations()
             self.blankView.addGestureRecognizer(gesture)
        }else
        {
            
            print("else")
            sideMenuTableView.isHidden = true
            sideMenuVIew.isHidden = true
            profileView.isHidden = true
             blankView.isHidden = true
            issideViewOpen = false
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationDelegate(self)
            UIView.beginAnimations("TableAnimation", context: nil)
            UIView.commitAnimations()
            self.blankView.removeGestureRecognizer(gesture)
        }
        
        
        
    }
}

//
//  scanQRcodeViewController.swift
//  Ani
//
//  Created by RSL-01 on 16/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Alamofire
import PKHUD
import SVProgressHUD

class scanQRcodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, PopUpDoneDelegate
{
    
    //
    //    var captureSession: AVCaptureSession!
    //    var previewLayer: AVCaptureVideoPreviewLayer!
    var video = AVCaptureVideoPreviewLayer()
    
    var Metaobject : String!
    
    var allergntArray = [allergentModelClass]()
    var vcFor : ValidationPopupViewController? = nil
    var marchantName = ""
    
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    var ProductMenuArray = [ productMenuListModelClass]()
    
    var marchantArray = [marchantModelClass]()
    
    @IBOutlet weak var qrCodeImage: UIImageView!
    
    var flafDelegateMethod = true
    
    
    
    var marImage :String!
    
    var activityIndicator = UIActivityIndicatorView()
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    
    
    
    
    @IBAction func buttonShare(_ sender: Any) {
        //        var
        //        var text = "Using the ANI app at" + sp.getString("merchant_name","Ani")+" to check my food allergy and nutritional information.";
        //
        //
        let originalString = "Using the ANI app at " + self.marchantName + " to check my food allergy and nutritional information."
        
        //        let image  = UIImage(url: URL(string: marImage ?? ""))
        let image = UIImage(named: marImage)
        if let myWebsite = NSURL(string: "http://www.theaniapp.com/")
        {
            let objectsToShare = [originalString, myWebsite, image] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("scanQRcodeViewController")
        
        self.title = "ANI"
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        // MARK: Check Camera Permisssion
        self.goToCamera()
        
        // Do any additional setup after loading the view.
    }
    
    func goToCamera()
    {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch (status)
        {
        case .authorized:
            self.startScaning()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                if (granted)
                {
                    self.startScaning()
                }
                else
                {
                    self.camDenied()
                }
            }
            
        case .denied:
            self.camDenied()
            
        case .restricted:
            let alert = UIAlertController(title: "Restricted",
                                          message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func camDenied()
    {
        DispatchQueue.main.async
            {
                var alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n5. Open this app and try again."
                
                var alertButton = "OK"
                var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)
                
                if UIApplication.shared.canOpenURL(URL(string: UIApplicationOpenSettingsURLString)!)
                {
                    alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again."
                    
                    alertButton = "Go"
                    
                    goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                    })
                }
                
                let alert = UIAlertController(title: "Error", message: alertText, preferredStyle: .alert)
                alert.addAction(goAction)
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    func startScaning() {
        var deviceTokan = getValueForKey(keyValue: kdeviceToken)
        print("\(deviceTokan)")
        //        var accountID =   getValueForKey(keyValue: kaccountIdString)
        //        print("kaccount_id: \(accountID)")
        //        setDefaultValue(keyValue: accountId, valueIs: accountID)
        //creating session
        
        var session = AVCaptureSession()
        //define capture Device
        var captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do
        {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        }
        catch
        {
            print("ERROR")
        }
        
        let Output = AVCaptureMetadataOutput()
        
        session.addOutput(Output)
        
        Output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        Output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        DispatchQueue.main.async
        {
            self.video.frame = self.view.layer.bounds
            
            self.view.layer.addSublayer(self.video)
        
            self.view.bringSubview(toFront: self.qrCodeImage)
        }
        session.startRunning()
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            //            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            //            self.navigationController?.pushViewController(home, animated: true)
            
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    @objc func backBtn(sender:UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection)
    {
        if metadataObjects == nil
        {
            self.stopSpinner()
            
            var alert = UIAlertController(title: "QRCode", message: "Sorry! Restaurant Not Found", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert,animated : true ,completion: nil)
            return
        }
        else
        {
            
            if flafDelegateMethod
            {
                flafDelegateMethod = false
                
                if isConnectedToInternet()
                {
                    
                    // Get the metadata object.
                    let metadataObj = metadataObjects.first as? AVMetadataMachineReadableCodeObject
                    
                    if metadataObj?.type == AVMetadataObject.ObjectType.qr
                    {
                        // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
                        
                        
                        Metaobject = metadataObj?.stringValue
                        
                        if !Metaobject.contains("ANI-")
                        {
                            
                            
                            self.stopSpinner()
                            
                            var alert = UIAlertController(title: "QRCode", message: "Sorry! Restaurant Not Found", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (ok) in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            present(alert,animated : true ,completion: nil)
                            
                            return
                        }
                        
                        //                    let data = NSData(contentsOf: NSURL(string: "ani.png")! as URL)
                        //                    var img = UIImage(data: data! as Data)
                        
                        //                    SVProgressHUD.setBackgroundColor(UIColor.clear)
                        //                    SVProgressHUD.AnimationCurve.easeOut
                        //                    SVProgressHUD.AnimationTransition.curlUp
                        //                    SVProgressHUD.setBackgroundLayerColor(.green)
                        self.startSpinner()
                        setDefaultValue(keyValue: kmetaString , valueIs: Metaobject)
                        appdelegate.flagscanQr = 1
                        self.navigationController?.popViewController(animated: true)
                        print("scan QR code ")
                        
                        print(Metaobject)
                    }

                }
                else
                {
                    let str = "You are offline please check your internet connection"
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor = str
                    self.present(self.vcFor!, animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            
        }
        
        
    }
    
    
    func showActivityIndicatory(uiView: UIView) {
        let container: UIView = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = .green
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = .blue
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }
    
    
    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    
    func isConnectedToInternet() ->Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
    
}

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}


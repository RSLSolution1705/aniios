//
//  loginViewController.swift
//  Ani
//
//  Created by RSL-01 on 12/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//
import UIKit
import Alamofire
import CoreData
import SVProgressHUD

class loginViewController: UIViewController,UITextFieldDelegate,PopUpDoneDelegate {
    
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    var deviceTokan = ""
    @IBOutlet weak var blurview: UIVisualEffectView!
    
    @IBOutlet weak var extraView: UIView!
     var vcFor : ValidationPopupViewController? = nil
    @IBOutlet weak var mainBlurView: UIVisualEffectView!
    let context = AppDelegate().persistentContainer.viewContext
     var flagShowPassword = true
    var effect : UIVisualEffect!
    @IBOutlet weak var forgotpassword: UITextField!
    @IBOutlet var forgetPasswordView: UIView!
    
    @IBOutlet weak var showPassword: UIButton!
    @IBOutlet weak var loginbutton: UIButton!
    
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.hideKeyboardWhenTappedAround()
        
        showPassword.setImage(UIImage(named: "ic_custom_hide_new.png") , for: .normal)
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        effect = blurview.effect
        blurview.effect = nil
        
      //  self.extraView.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        deviceTokan  = getValueForKey(keyValue: kdeviceToken)
        print("\(deviceTokan)")
        emailTextField.delegate = self
        passwordTextField.delegate = self
       
        // Do any additional setup after loading the view.
        
        loginbutton.layer.cornerRadius = 5
        
    
        NotificationCenter.default.addObserver(self, selector: #selector(loginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.navigationController?.isNavigationBarHidden = false
        
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.backgroundColor = UIColor(hex: "41b93d")
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        self.navigationController?.isNavigationBarHidden = false
        navigationItem.hidesBackButton = true
        
        navigationController?.navigationBar.backgroundColor = UIColor(hex: "41b93d")
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - 150
                
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func backBtn(sender:UIButton)
    {
        var signin = storyboard?.instantiateViewController(withIdentifier: "signuppageViewController")as? signuppageViewController
        self.navigationController?.pushViewController(signin!, animated: true)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPassward(_ sender: Any) {
       
    
        showPassword.setImage(UIImage(named: "ic_custom_hide_new.png") , for: .normal)
        
        if flagShowPassword == true
        {
            showPassword.setImage(UIImage(named: "ic_custom_show_new.png") , for: .normal)
            //    showPassword.currentBackgroundImage =
            //     self.showPassword.setImage(UIImage(named: "eye_grey"))
            passwordTextField.isSecureTextEntry =  false
        }else
        {
            showPassword.setImage(UIImage(named: "ic_custom_hide_new.png") , for: .normal)
            passwordTextField.isSecureTextEntry = true
        }
        
        flagShowPassword = !flagShowPassword
        
        
        
        
    }
    func animateIn()
    {
        self.blurview.isHidden = false
        self.mainBlurView.isHidden = false
      //  self.extraView.isHidden = false
        self.view.addSubview(forgetPasswordView)
        forgetPasswordView.center = self.view.center
        forgetPasswordView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        forgetPasswordView.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.blurview.effect = self.effect
            self.forgetPasswordView.alpha = 1
            self.forgetPasswordView.transform = CGAffineTransform.identity
        }
    }
    func animateOut()
    {
        self.blurview.isHidden = true
        self.mainBlurView.isHidden = true
//        self.extraView.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.blurview.effect = nil
            self.forgetPasswordView.alpha = 0
            self.forgetPasswordView.transform = CGAffineTransform.identity
        }) { (success: Bool) in
            self.forgetPasswordView.removeFromSuperview()
        }
        
        
        
    }
    
    
    @objc func tap(gesture: UITapGestureRecognizer) {
        forgotpassword.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        forgotpassword.resignFirstResponder()
        return true
    }
    
    @IBAction func closeButton(_ sender: Any) {
        //      self.extraView.isHidden = true
        animateOut()
        //         self.forgetPasswordView.removeFromSuperview()
        
    }
    
    
    @IBAction func forgetSendButton(_ sender: Any) {
        
         forgotpassword.resignFirstResponder()
        let text: String = self.forgotpassword.text!
        
        let isValudateemailAddress = isValidEmail(testStr: ( self.forgotpassword.text)!);
        if((text.count) <= 0)
        {
            setDefaultValue(keyValue: kmsg, valueIs: loginEmailNotvalid)
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  "Please enter your email"
            self.present(self.vcFor!, animated: true, completion: nil)
            
            
            

        }
        else if(isValudateemailAddress == false)
        {
            
            setDefaultValue(keyValue: kmsg, valueIs: loginEmailIsNotValid)
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  "Please enter valid email"
            self.present(self.vcFor!, animated: true, completion: nil)
            
            
            

            
        }
        else{

            if Reachability.isConnectedToNetwork() {
                self.startSpinner()
                  DispatchQueue.main.async {
                self.ForgotpasswordWebservice()
                }
            }
            else {
                DispatchQueue.main.async {
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
                }
               
            }
            
            
            
        }
        
    }
    @IBAction func forgetCancelButton(_ sender: Any) {
        // self.extraView.isHidden = true
        //        self.forgetPasswordView.removeFromSuperview()
         forgotpassword.text = ""
        animateOut()
    }
    
    
    func ForgotpasswordWebservice()
    {
//         SVProgressHUD.show()
        let headers = ["Content-Type":"Application/json"]
        let _url = forgot_password
        
    
        
        let parameters: Parameters = ["email_id" : forgotpassword.text!]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            //popup
                                            self.stopSpinner()
//                                            var str = response.result.error?.localizedDescription
//                                            print(response.result.error?.localizedDescription)
                                                var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            
                                            
//                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:msg!)
                                            
                                            
                                        }else
                                        {
                                            
                                           self.stopSpinner()
                                            var msg = jsonDic.object(forKey: "msg")as? String
//                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:msg!)
                                            
                                            
                                            
                                            DispatchQueue.main.async {
                                                
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = "home"
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            }
//
//                                            var alert = UIAlertController(title: "Ani", message: msg!, preferredStyle: .alert)
////                                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//                                            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (ok) in
//                                                var login = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController")as? loginViewController
//                                                self.navigationController?.pushViewController(login!, animated: true)
//                                                        }))
//                                                        self.present(alert, animated: true, completion: nil)
                                            
                                            //"Password reset link sent to your email.";
                                            
                                        }
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                            {
                                   self.stopSpinner()
                                
                                 DispatchQueue.main.async {
                                    
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  Internet_Connection_Alert
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                                }
//                                let str = response.result.error?.localizedDescription
//                                print(response.result.error?.localizedDescription)
//                                
//                                
//                                var alert = UIAlertController(title: "Ani", message: (response.result.error?.localizedDescription), preferredStyle: .alert)
//                                //                                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//                                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (ok) in
//                                    var login = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController")as? loginViewController
//                                    self.navigationController?.pushViewController(login!, animated: true)
//                                }))
//                                self.present(alert, animated: true, completion: nil)
//                              
                                
                                
//                                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
//                                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
//                                self.addChildViewController(pop!)
//                                pop!.view.frame = self.view.frame
//                                self.view.addSubview(pop!.view)
//                                pop?.didMove(toParentViewController: self)
                                
                                
                                
                                
                                
                                
                                //                                let httpError: NSError = response.result.error! as NSError
                                //                                let statusCode = httpError.code
                                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                                //                                print(error)
                                //
                                print(response.result.error?.localizedDescription)
                                
                                //                                completion(dic,0)
                                
                            }
                            
                            
        }
        //        SVProgressHUD.dismiss()
        //        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        //
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
//        let emailRegEx = “[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}”
//        let emailTest = NSPredicate(format:“SELF MATCHES %@“, emailRegEx)
        
        if textField == self.emailTextField {
            let maxLength = 50
            let currentString: NSString = emailTextField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            //            if newString.length <= maxLength {
            //                return true
            //            }else {
            //                return false
            //            }
            
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@_!#$%^&*-. "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            //return alphabet
            
            if newString.length <= maxLength {
                return alphabet//return true
            }else {
                return false
            }
            //return newString.length <= maxLength
        }
        return true
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if(validateLoginform() == true)
        {
        //    SVProgressHUD.setInfoImage(UIImage(named: "ani.png")!)
            //                    SVProgressHUD.setInfoImage(img!)
      
            emailTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
           
            if Reachability.isConnectedToNetwork() {
              self.startSpinner()
              loginWebservices()
            }
            else {
              
DispatchQueue.main.async {
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  Internet_Connection_Alert
                self.present(self.vcFor!, animated: true, completion: nil)
                
                }
            }
            
           
            
        }
        
    }
    func loginWebservices()
    {
        let headers = ["Content-Type":"Application/json"]
       
        let _url = user_login_new
        

        //

        let parameters: Parameters = [
            "email_id": emailTextField.text!,
            
            //"account_id": kregisterAccountId,
           // "password": passwordTextField.text!,
            "devicetoken": "asdasdasdasdasdasddasdasdasdasdasdasdas",  //getValueForKey(keyValue: kdeviceToken),
            "device_type": kDeviceType
        ]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            var msg = jsonDic.object(forKey: "msg")as? String
//                                            let str = response.result.error?.localizedDescription
//                                            print(response.result.error?.localizedDescription)
                                            
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            if msg!.contains("password")
                                          
                                            {
                                                  self.passwordTextField.text = ""
                                            }
                                          
                                 
                                        }
                                        else
                                        {
                                            self.stopSpinner()
                                            let mutableDic = NSMutableDictionary()
                                            UserDefaults.standard.set(true, forKey: "isUserLog")
                                            let temp = loginClass()
                                            
                                            
                                            temp.account_id = jsonDic.object(forKey: "account_id") as? String
                                            if temp.account_id != nil
                                            {
                                                temp.account_id = "\(jsonDic.value(forKey: "account_id") as! String)"
                                            }
                                            else
                                            {
                                                temp.account_id = "\(jsonDic.value(forKey: "account_id") as! Int)"
                                                temp.account_id = temp.account_id!
                                            }
                                            
                                            
                             
                                            UserDefaults.standard.set(temp.account_id, forKey: kregisterAccountId)
                                            
                                            UserDefaults.standard.synchronize()
                                            
                                            print("\( temp.account_id)")
                                            
                                            let c1 = NSEntityDescription.insertNewObject(forEntityName: "Ani", into: self.context)as! Ani
                                            //                                c1.setValue(temp.account_id, forKey: accId)
                                            c1.accId = temp.account_id
                                            
                                            // txtName.text=""
                                            // txtNumber.text=""
                                            
                                            print(c1)
                                            
                                            try! self.context.save()
                                            
                                            
                                            
                                            
                                            //                                temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as! String
                                            temp.first_name = jsonDic.object(forKey: "first_name") as! String
                                            var firstName: String = temp.first_name
                                            
                                            setDefaultValue(keyValue: kfirstName, valueIs: firstName)
                                            
                                            //                                            getValueForKey(keyValue: T##String)
                                            
                                            temp.last_name = jsonDic.object(forKey: "last_name") as! String
                                            
                                            setDefaultValue(keyValue: klastName, valueIs: temp.last_name)
                                            
                                            temp.email_id = jsonDic.object(forKey: "email_id") as! String
                                            
                                            setDefaultValue(keyValue: kemail, valueIs: temp.email_id)
                                            
                                            
                                            var img = jsonDic.object(forKey: "profile_image") as! String
                                            let defaults = UserDefaults.standard
                                            if (img != nil && img != "")
                                            {
                                                if let data = NSData(contentsOf:URL(string: img)!)
                                                {
                                                    
                                                    //                                                    var imageData = try! Data.init(contentsOf: URL(string: img)!)
                                                    temp.profile_image = UIImage(data: data as Data)
                                                }
                                                
                                                //                                                self.appdelegate.profile_image =  temp.profile_image
                                                
                                                //var image = temp.profile_image
                                                //defaults.set(temp.profile_image, forKey: "picture")
                                                
                                            }else
                                            {
                                                img = ""
                                                
                                            }
                                            
                                          
                                            defaults.set(img, forKey: "picture")
                                            defaults.synchronize()
                                            
                                            
                
                                            if temp.mobile_number == nil
                                            {
                                                setDefaultValue(keyValue: kmobNumber, valueIs: "")
                                            }
                                            else
                                            {
                                                setDefaultValue(keyValue: kmobNumber, valueIs:  temp.mobile_number)
                                            }
                                            
                                            
                                            temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as? String
                                            if temp.date_of_birth == nil
                                            {
                                                setDefaultValue(keyValue: KDOB, valueIs: "")
                                            }
                                            else
                                            {
                                                setDefaultValue(keyValue: KDOB, valueIs: temp.date_of_birth)
                                            }
                                            temp.role = jsonDic.object(forKey: "role") as! String
                                            setDefaultValue(keyValue: krole, valueIs:  temp.role )
                                            print("role : \(temp.role)")
                                            //
                                            //                                temp.home_number = jsonDic.object(forKey: "home_number") as! String
                                            let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                                            self.navigationController?.pushViewController(secondVC, animated: true)
                                        }
                                    }
                                }
                                
                                
                                
                            }
                            else if (response.result.isFailure)
                                
                                
                            {
                                self.stopSpinner()
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline." || str == "A server with the specified hostname could not be found."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                

                            }
                            
                            
        }
        
        
    }
    
    func validateLoginform() -> Bool {
        
        var ischeckValidation = true;
        var showMsg = "";
        
        var text: String = self.emailTextField.text!
        
        let isValudateemailAddress = isValidEmail(testStr: ( self.emailTextField.text)!);
         if((text.characters.count) <= 0) && ((self.passwordTextField?.text?.characters.count)! <= 0)
        {
            ischeckValidation  = false;
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Please enter email "
            self.present(self.vcFor!, animated: true, completion: nil)
        }
       else if((text.characters.count) <= 0)
        {
            ischeckValidation  = false;
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  loginEmailNotvalid
            self.present(self.vcFor!, animated: true, completion: nil)
            
            
        }
           
        else if(isValudateemailAddress == false)
        {
            ischeckValidation  = false;
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  loginEmailIsNotValid
            self.present(self.vcFor!, animated: true, completion: nil)
            
            

        }
            
//        else if((self.passwordTextField?.text?.characters.count)! <= 0)
//        {
//            ischeckValidation  = false;
//
//            self.vcFor?.modalTransitionStyle = .crossDissolve
//            self.vcFor?.modalPresentationStyle = .overFullScreen
//            self.vcFor?.isLogin = true
//            self.vcFor?.actionFor = ""
//            self.vcFor?.titleFor =  loginPaswordNotvalid
//            self.present(self.vcFor!, animated: true, completion: nil)
//
//
//
//        }
        
      
        
        return ischeckValidation;
    }
    
    @IBOutlet weak var forgetPasswordbutton: UIButton!
    
    @IBAction func forgetPasswordButton(_ sender: Any) {
        //        animateIn
        //        self.extraView.backgroundColor = UIColor(white: 0, alpha: 1.3)
        
        
        animateIn()
        
      
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

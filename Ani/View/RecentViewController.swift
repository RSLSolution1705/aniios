//
//  RecentViewController.swift
//  Ani
//
//  Created by RSL-01 on 04/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import CoreData
import Floaty
import SVProgressHUD
import SDWebImage


var productStatusCheck2 = ""


class RecentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PopUpDoneDelegate{
    
    
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    let cellSpacingHeight: CGFloat = 10
    let context = AppDelegate().persistentContainer.viewContext
    var newAllergentIdArray = [String]()
    var productSubMenuArray = [ProductListModelClass]()
    var finalArray = [ProductListModelClass]()
    
    
    var account_id = ""
    var passdata = [productallergnsClass]()
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    //    var isSelected = false
    var tableposition = 0
    var allergens_id_collectionview = ""
    var buffer = [allergentModelClass]()
    var isFavouriteRes = false
    @IBOutlet weak var lblMerchandInactive: UILabel!
    
    var allergntArray = [allergentModelClass]()
    var marchantArray = [marchantModelClass]()
    var accountID =  loginClass()
    var allergntArray1 = [productallergnsClass]()
    var allergensDetail = [productallergnsClass]()
    var nutritianDetails = [nutritianArrayObject]()
    var ingredientsDetails = [ingredientsArrayObject]()
    var merchantId = ""
    var ProductMenuArray = [ productMenuListModelClass]()
    var searchArray = [ProductListModelClass]()
    var menu_item_id_fav = ""
    var recentMenuId = ""
    var flagfav = ""
    var nutritianArray1 = [nutritianArrayObject]()
    var ingredientsArray1 = [ingredientsArrayObject]()
    @IBOutlet weak var tableview: UITableView!
    var vcFor : ValidationPopupViewController? = nil
    
    
    
    
    
    lazy var refreshControl: UIRefreshControl =
        {
           
            
            
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(RecentViewController.handleRefresh),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.black
            return refreshControl
            
            
    }()
    
    
    
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableview.addSubview(self.refreshControl)

        
        print("RecentViewController")
    }

    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        
        
        self.lblNoDataFound.isHidden = true
        self.searchViewHeight.constant = 0
        self.searchView.showsCancelButton = true
        self.searchView.text = ""
        
        //        self.startSpinner()
        RecentItemShow()
        tableview.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
//        self.searchViewHeight.constant = 0
//        self.searchBar.text = ""
//        self.searchView.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        self.lblNoDataFound.isHidden = true
        self.title = "Recently Viewed Products"
        self.searchView.delegate = self
        self.searchViewHeight.constant = 0
        self.searchView.text = ""
        self.searchView.clipsToBounds = true
        lblMerchandInactive.isHidden = true
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.automaticallyAdjustsScrollViewInsets = false
        let btnRightbutton = UIButton(type: .custom)
        btnRightbutton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnRightbutton.backgroundColor = UIColor.clear
        btnRightbutton.addTarget(self, action: #selector(infobutton(sender:)), for: .touchUpInside)
        btnRightbutton.setImage(UIImage(named: "ic_information_outline_white_48dp.png"), for: .normal)
        let barButton = UIBarButtonItem(customView: btnRightbutton)
        tableview.tableFooterView = UIView()
        let btnright = UIButton(type: .custom)
        btnright.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnright.backgroundColor = UIColor.clear
        btnright.addTarget(self, action: #selector(self.btnSearchTapped(_:)), for: .touchUpInside)
        btnright.setImage(UIImage(named: "ic_search_white"), for: .normal)
        let sideMenubarButton = UIBarButtonItem(customView: btnright)
        self.navigationItem.rightBarButtonItems = [barButton,sideMenubarButton]
        if Reachability.isConnectedToNetwork()
        {
            //  SVProgressHUD.show()
            self.startSpinner()
            RecentItemShow()
            tableview.reloadData()
        }
        else
        {
            //    SVProgressHUD.dismiss()
            DispatchQueue.main.async
                {
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
            }
        }
        

    }
    
    
    
    @objc func btnSearchTapped(_ sender: Any)
    {
       
        if self.searchViewHeight.constant == 0 {
            self.searchViewHeight.constant = self.navigationController!.navigationBar.frame.size.height
            self.searchView.becomeFirstResponder()
        }
        else {
//              self.lblNoDataFound.isHidden = true
//            self.searchViewHeight.constant = 0
//            self.searchView.text = ""
//            _ = self.searchView.resignFirstResponder()
            
            self.searchViewHeight.constant = 0
            self.searchView.showsCancelButton = true
            self.lblNoDataFound.isHidden = true
            self.searchView.text = ""
            self.productSubMenuArray = self.finalArray
            self.tableview.reloadData()
            self.dismissKeyboard()
        }
    }
    
    
    @objc func infobutton(sender:UIButton)
    {
        
        let svc = storyboard?.instantiateViewController(withIdentifier: "marchentInfoViewController")as! marchentInfoViewController
        self.navigationController?.pushViewController(svc, animated: true)
    }
    
    
    func getTrueAllergensArray(arr: [productallergnsClass]) -> [productallergnsClass] {
        var trueArray = [productallergnsClass]()
        
        for dic in arr {
            if dic.flag == "true" {
                trueArray.append(dic)
            }
        }
        
        return trueArray
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    

    @objc func backBtn(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
//        let back = storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController")as! sideMenuViewController
//
//        self.navigationController?.pushViewController(back, animated: true)
        
    }
    
    
    
    
    func RecentItemShow()
    {
        let headers = ["Content-Type":"Application/json"]
        let _url = get_view_menuitems
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
        }
        
        
        var allergentId = getValueForKey(keyValue: allergentIdString)
        
        //            var allergentId = appdelegate.allergentIdString
        
        
        var parentID = ""
                               if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                                    parentID = ""
                               }
                               else{
                                   if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                                          //   parentID = ""
                                       parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                         }
                                         else{
                                             parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                         }
                               }
                               print("Parent ID from UserDefaults : \(parentID)")
        
        print("getViewMenuItems")
    
        let parameters: Parameters = ["account_id" :  (data1[0].accId!),"page_number" :  "1" ,"allerganid" :  allergentId ,"merchant_id" : self.appdelegate.merchant_id,"partner_id":parentID]
        
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                        
                             print("Request  \(response.request)")
                            
                            if response.result.isSuccess
                            {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            var str = jsonDic.object(forKey: "msg")as?String
                                            print(response.result.error?.localizedDescription)
                                            self.lblMerchandInactive.isHidden = false
                                            self.lblMerchandInactive.text = str
                                            self.lblNoDataFound.isHidden = true
                                            self.productSubMenuArray.removeAll()
                                            self.tableview.reloadData()
                                            
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
//                                            self.vcFor?.titleFor =  str!
//                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            //
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                        }else
                                        {
//                                            self.lblNoDataFound.isHidden = false
                                            self.lblMerchandInactive.isHidden = true
                                            self.productSubMenuArray.removeAll()
                                            self.finalArray.removeAll()
                                            self.nutritianArray1.removeAll()
                                            self.ingredientsArray1.removeAll()
                                            self.stopSpinner()
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            var resultArray = jsonDic.object(forKey: "data") as! NSArray
                                            for i in resultArray
                                            {
                                                var tempobj = i as! NSDictionary
                                                var temp = ProductListModelClass()
                                                temp.menu_item_image = tempobj.object(forKey: "menu_item_image") as! String
                                                //                                    var imageData = try! Data.init(contentsOf: URL(string: img)!)
                                                //                                    temp.menu_item_image = UIImage(data: imageData)
                                                temp.menu_item_description = tempobj.object(forKey: "menu_item_description")as! String
                                                temp.menu_item_name = tempobj.object(forKey: "menu_item_name")as! String

                                                temp.menu_item_flag = tempobj.object(forKey: "menuitem_flag")as! String
                                                temp.ingredient_deceleration_info  = tempobj.object(forKey: "ingredient_deceleration_info")as! String
                                                
                                                
                                                temp.menu_item_id = tempobj.object(forKey: "menu_item_id") as! String
                                                print("temp.menu_item_id : \(temp.menu_item_id)")
                                                temp.flag = tempobj.object(forKey: "flag")as! String
                                                self.flagfav = temp.flag
                                                self.menu_item_id_fav = temp.menu_item_id
                                                
                                              
//                                                temp.partner_id = tempobj.object(forKey: "partner_id") as! String
                                                temp.partner_id = tempobj.object(forKey: "partner_id") as? String
                                                
                                                print("temp.partner_id = \(String(describing: temp.partner_id))")
                                                
                                                var AllerngensArray = tempobj.object(forKey: "allergan_array")as! NSArray
                                                let nutritianArray = tempobj.object(forKey: "nutritian_array")as! NSArray
                                                let ingredientsArray = tempobj.object(forKey: "ingredients_array")as! NSArray
                                                
                                                self.allergntArray1 = [productallergnsClass]()
                                                
                                                self.nutritianArray1 = [nutritianArrayObject]()
                                                self.ingredientsArray1 = [ingredientsArrayObject]()
                                                //                                    self.allergntArray1.removeAll()
                                                
                                                for j in AllerngensArray
                                                {
                                                    
                                                    
                                                    var tempallegens = j as! NSDictionary
                                                    var tempmodel = productallergnsClass()
                                                    tempmodel.allergan_Img = tempallegens.object(forKey: "allergan_images")as! String
                                                    tempmodel.allergensName = tempallegens.object(forKey: "allergan")as! String
                                                    
                                                    tempmodel.allergen_id = tempallegens.object(forKey: "allergan_id")as! String
                                                    self.allergens_id_collectionview =  tempmodel.allergen_id
                                                    
                                                    
                                                    tempmodel.flag = tempallegens.object(forKey: "flag")as? String ?? ""
                                                    
                                                    if tempmodel.flag == "true"
                                                    {
                                                        self.allergntArray1.append(tempmodel)
                                                        self.allergensDetail.append(tempmodel)
                                                    }
                                                    
                                                }
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                temp.allengsArray = self.allergntArray1
                                                self.appdelegate.allergentArray = self.allergntArray1
                                                
                                                
                                                for j in nutritianArray
                                                {
                                                    
                                                    
                                                    let tempallegens = j as! NSDictionary
                                                    let tempmodel = nutritianArrayObject()
                                                    
                                                    tempmodel.name = (tempallegens.object(forKey: "name")as! String)
                                                    
                                                    tempmodel.quantity = (tempallegens.object(forKey: "quantity")as! String)
                                                    tempmodel.unit = (tempallegens.object(forKey: "unit")as! String)
                                                    
                                                    print(tempmodel.name)
                                                    
                                                    
                                                    
                                                    self.nutritianArray1.append(tempmodel)
                                                    
                                                }
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                
                                                temp.nutritianarray = self.nutritianArray1
                                                self.appdelegate.nutritianArray = self.nutritianArray1
                                                //self.productSubMenuArray.append(temp)
                                                
                                                for j in ingredientsArray
                                                {
                                                    
                                                    
                                                    var tempallegens = j as! NSDictionary
                                                    var tempmodel = ingredientsArrayObject()
                                                    
                                                    tempmodel.name = tempallegens.object(forKey: "name")as! String
                                                    tempmodel.quantity = tempallegens.object(forKey: "quantity")as! String
                                                    tempmodel.unit = tempallegens.object(forKey: "unit")as! String
                                                    
                                                    //            self.allergens_id_collectionview =  tempmodel.allergen_id!
                                                    
                                                    //      self.nutritianArray.append(tempmodel)
                                                    self.ingredientsArray1.append(tempmodel)
                                                    
                                                }
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                
                                                temp.ingredientsarray = self.ingredientsArray1
                                                self.appdelegate.ingredientsArray = self.ingredientsArray1
                                                
                                                
                                                
                                                
                                                self.productSubMenuArray.append(temp)
                                                self.finalArray.append(temp)
                                                DispatchQueue.main.async
                                                {
                                                        self.tableview.reloadData()
                                                }
                                            }
                                            
//                                            DispatchQueue.main.async
//                                            {
//                                                self.tableview.reloadData()
//                                            }
                                            
                                            
                                        }  //                                    print(self.marchantArray)
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                                
                            {
                                self.stopSpinner()
                                
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                                //                                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                                //                                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                //                                self.addChildViewController(pop!)
                                //                                pop!.view.frame = self.view.frame
                                //                                self.view.addSubview(pop!.view)
                                //                                pop?.didMove(toParentViewController: self)
                            }
                            
        }
        //        self.stopSpinner()
        //        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        
    }
    
    func RecentWebservice()
    {
        
        let headers = ["Content-Type":"Application/json"]
        let _url = view_menuitems
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        var parentID = ""
                         if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                              parentID = ""
                         }
                         else{
                             if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                                      // parentID = ""
                                parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                   }
                                   else{
                                       parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                   }
                         }
                         print("Parent ID from UserDefaults : \(parentID)")
        
        
        let parameters: Parameters = ["account_id" :  (data1[0].accId!),"menuitem_id" : recentMenuId ,"merchant_id" : self.appdelegate.merchant_id,"partner_id":parentID]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            //popup
                                            self.stopSpinner()
                                            //                                            var str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            //
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                        }else
                                        {
                                            
                                            
                                            
                                            self.stopSpinner()
                                            
                                        }  //                                    print(self.marchantArray)
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                            {
                                //                                let httpError: NSError = response.result.error! as NSError
                                //                                let statusCode = httpError.code
                                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                                //                                print(error)
                                //
                                self.stopSpinner()
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                                
                                
                                //                                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                                //                                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                //                                self.addChildViewController(pop!)
                                //                                pop!.view.frame = self.view.frame
                                //                                self.view.addSubview(pop!.view)
                                //                                pop?.didMove(toParentViewController: self)
                                
                                //                                completion(dic,0)
                                
                            }
                            
                            
        }
        
        //    self.stopSpinner()
        //    showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        
    }
    
    
    
    //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return productSubMenuArray[collectionView.tag].allengsArray.count
    //    }
    //    
    //    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RecentItemCollectionViewCell
    //        
    //        print("tableposition : \(tableposition)")
    //        
    //        
    //        
    //        
    //        
    //        print("allengsArray count : \( productSubMenuArray[tableposition].allengsArray.count)")
    //        
    //        print("allengsArray[indexPath.row] : \( indexPath.row)")
    //        
    //        if productSubMenuArray[collectionView.tag].allengsArray[indexPath.row].flag == "true"
    //        {
    //             cell.recentAllergentImage.image = UIImage(named: "allergens_placeholder.png")
    //          Alamofire.request(self.productSubMenuArray[collectionView.tag].allengsArray[indexPath.row].allergan_Img, method: .get).response { response in
    //                guard let image = UIImage(data:response.data!) else {
    //                    // Handle error
    //                    autoreleasepool {
    //                        cell.recentAllergentImage.image = UIImage(named: "allergens_placeholder.png")
    //                       
    //                    }
    //                    return
    //                }
    //                autoreleasepool {
    //                    let imageData = UIImagePNGRepresentation(image)
    //                    cell.recentAllergentImage.image = UIImage(data : imageData!)
    //                    cell.backgroundColor = UIColor.clear
    //                }
    //            }
    //        }
    //        
    //        //cell.recentAllergentImage.image = productSubMenuArray[collectionView.tag].allengsArray[indexPath.row].allergan_Img
    //        
    //        //        passdata = [productSubMenuArray[collectionView.tag].allengsArray[indexPath.row]]
    //        //        self.passdata.append(productSubMenuArray[collectionView.tag].allengsArray[indexPath.row])
    //        cell.backgroundColor = UIColor.clear
    //        
    //        return cell
    //    }
    //    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        let collectionWidth = collectionView.bounds.width
    //        var count: CGFloat =  CGFloat(productSubMenuArray[tableposition].allengsArray.count)
    //        return CGSize(width: collectionWidth/count, height: collectionWidth/count)
    //    }
    //    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        
        return productSubMenuArray.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? RecentItemTableViewCell else {
            fatalError("Dequeued cell is not an instance of CharacterDetailsTableViewCell class.")
        }
        
        
    //    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecentItemTableViewCell
        
        var temp1 = productSubMenuArray[indexPath.row]
        
        
        if temp1.menu_item_flag == "active"
        {
            cell.recentImage.alpha = 1
            cell.btnFav.alpha = 1
            cell.recentLabel.alpha = 1
            cell.allergensFlagRecent = "1"
            productStatusCheck2 = "0"
        }
        else
        {
            cell.allergensFlagRecent = "0"
            productStatusCheck2 = "1"
            cell.recentImage.alpha = 0.25
            cell.btnFav.alpha = 0.25
            cell.recentLabel.alpha = 0.25
        }
        
        
        
        cell.recentLabel.text = productSubMenuArray[indexPath.row].menu_item_name
        tableposition = indexPath.row
        
        
        let placeholderImage = UIImage(named: "background_placeholder.png")!
        
        let imgUrl = temp1.menu_item_image
        
        let screenSize: CGRect = UIScreen.main.bounds
        let calculateHeight = (screenSize.width*600)/1024
        print(screenSize.width)
        print(calculateHeight)
        
        cell.recentImage.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: calculateHeight)
        
        Alamofire.request(imgUrl!, method: .get).response { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                autoreleasepool {
                    DispatchQueue.main.async {
                          cell.recentImage.image = UIImage(named: "background_placeholder.png")
                    }
                  
                }
                return
            }
            autoreleasepool {
                 DispatchQueue.main.async {
                let imageData = UIImageJPEGRepresentation(image,1.0)
                cell.recentImage.image = UIImage(data : imageData!)
                }
            }
        }
        
        
//        if let downloadURL = NSURL(string: imgUrl ?? "")
//        {
//            cell.recentImage.af_setImage(withURL: downloadURL as URL, placeholderImage: placeholderImage)
//        }
        
        
        
        
        
        
        
        //    if temp1.menu_item_image == ""
        //    {
        //        cell.recentImage.image = UIImage(named: "kplaceHolderImge")
        //
        //    }else
        //    {
        //        DispatchQueue.main.async
        //            {
        //        var urlString = temp1.menu_item_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //
        //        cell.recentImage.image = UIImage(url: URL(string: urlString ?? ""))
        //        }
        //        //self.marchantImage.imageFromUrl(urlString: temp.merchant_image)
        //    }
        //
        if(temp1.flag == "dislike") {
            
            let image = UIImage(named: "unfav_icon_1")
            cell.btnFav?.setImage(image, for: .normal)
        }
        else {
            let image = UIImage(named: "fav_icon_1")
            cell.btnFav?.setImage(image, for: .normal)
        }
        
        
        cell.btnFav!.tag = indexPath.row
        cell.btnFav?.addTarget(self, action: #selector(clickFavButton(sender:)), for: .touchUpInside)
        
        ///////
//        cell.leftBtn!.tag = indexPath.row
//        cell.leftBtn?.addTarget(self, action: #selector(clickLeftArrowButton(sender:)), for: .touchUpInside)
//
//        cell.rightBtn!.tag = indexPath.row
//        cell.rightBtn?.addTarget(self, action: #selector(clickRightArrowButton(sender:)), for: .touchUpInside)
        
        //
        //        cell.recentCollectionView.delegate = self
        //        cell.recentCollectionView.dataSource = self
        //        cell.recentCollectionView.tag = indexPath.row
        
        
        cell.allergensArray = self.getTrueAllergensArray(arr: temp1.allengsArray)
        
        if UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone SE"
                         {
                            if  cell.allergensArray.count > 5{
                                
                                cell.rightBtn.isHidden = false
                                cell.rightArrowImg.isHidden = false
                                cell.leftBtn.isHidden = true
                                cell.leftArrowImg.isHidden = true
                            }
                            else{
                                cell.rightBtn.isHidden = true
                                cell.rightArrowImg.isHidden = true
                                cell.leftBtn.isHidden = true
                                cell.leftArrowImg.isHidden = true
                                
                                cell.leftBtnWidthConstant.constant = 0
                                cell.leftBtnImgWidthConstant.constant = 0
                                cell.rightBtnWidthConstant.constant = 0
                                cell.rightBtnImgWidthConstant.constant = 0
                            }
                            
                        }
                      else  if UIDevice.current.modelName == "iPhone 6" || UIDevice.current.modelName == "iPhone 6s" || UIDevice.current.modelName == "iPhone 7" || UIDevice.current.modelName == "iPhone 8" || UIDevice.current.modelName == "iPhone X" || UIDevice.current.modelName == "iPhone XS"
                        {
                                  if  cell.allergensArray.count > 6{
                                                         
                                        cell.rightBtn.isHidden = false
                                        cell.rightArrowImg.isHidden = false
                                        cell.leftBtn.isHidden = true
                                        cell.leftArrowImg.isHidden = true
                                    }
                                    else{
                                        cell.rightBtn.isHidden = true
                                        cell.rightArrowImg.isHidden = true
                                        cell.leftBtn.isHidden = true
                                        cell.leftArrowImg.isHidden = true
                                    
                                    cell.leftBtnWidthConstant.constant = 0
                                    cell.leftBtnImgWidthConstant.constant = 0
                                    cell.rightBtnWidthConstant.constant = 0
                                    cell.rightBtnImgWidthConstant.constant = 0
                                    }
                        }
                        else  if UIDevice.current.modelName == "iPhone 6 Plus" || UIDevice.current.modelName == "iPhone 6s Plus" || UIDevice.current.modelName == "iPhone 7 Plus" || UIDevice.current.modelName == "iPhone 8 Plus" || UIDevice.current.modelName == "iPhone XS Max" || UIDevice.current.modelName == "iPhone XR"
                            {
                                         if  cell.allergensArray.count > 7{
                                                                
                                            cell.rightBtn.isHidden = false
                                            cell.rightArrowImg.isHidden = false
                                            cell.leftBtn.isHidden = true
                                            cell.leftArrowImg.isHidden = true
                                            }
                                            else{
                                            cell.rightBtn.isHidden = true
                                            cell.rightArrowImg.isHidden = true
                                            cell.leftBtn.isHidden = true
                                            cell.leftArrowImg.isHidden = true
                                            
                                            cell.leftBtnWidthConstant.constant = 0
                                            cell.leftBtnImgWidthConstant.constant = 0
                                            cell.rightBtnWidthConstant.constant = 0
                                            cell.rightBtnImgWidthConstant.constant = 0
                                        }
                            }
                    else{
                        if  cell.allergensArray.count > 7{
                                               
                                cell.rightBtn.isHidden = false
                                cell.rightArrowImg.isHidden = false
                                cell.leftBtn.isHidden = true
                                cell.leftArrowImg.isHidden = true
                                }
                                else{
                                cell.rightBtn.isHidden = true
                                cell.rightArrowImg.isHidden = true
                                cell.leftBtn.isHidden = true
                                cell.leftArrowImg.isHidden = true
                            
                            cell.leftBtnWidthConstant.constant = 0
                            cell.leftBtnImgWidthConstant.constant = 0
                            cell.rightBtnWidthConstant.constant = 0
                            cell.rightBtnImgWidthConstant.constant = 0
                            }
                    }
        
        
        
        cell.recentCollectionView.reloadData()
        
        DispatchQueue.main.async
        {
            cell.recentCollectionView.reloadData()
        }
        
        
        
        
        //        print("all123 : \(buffer[indexPath.row].allerganName)")
        
        
    //    print(allergensDetail.count)
   //     print("allergensDetal=\(allergensDetail)")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var temp = productSubMenuArray[indexPath.row]
        
        ///////

        ///////
        
        if temp.menu_item_flag == "active"
        {
   
        recentMenuId = temp.menu_item_id
        self.RecentWebservice()
         var dvc = storyboard?.instantiateViewController(withIdentifier: "productDetailViewController")as! productDetailViewController
        
            print("allergensDetail.count = \(allergensDetail.count)")
            
            if allergensDetail.count > 0
       //     if temp.allengsArray.count > 0
            {
                
            var temp1 = allergensDetail[indexPath.row]
            
          //  var allergens_id =
                
                dvc.buffer = temp
            appdelegate.MenuName = temp.menu_item_name
            //        setDefaultValue(keyValue: MenuName, valueIs: temp.menu_item_name)
            //        dvc.product = [temp1]
            //       appdelegate.allergentArray = allergensDetail
            appdelegate.allergentArray = temp.allengsArray
            //        appdelegate.allergentArray = temp.allengsArray
            self.appdelegate.ingredientsArray = temp.ingredientsarray
            self.appdelegate.nutritianArray = temp.nutritianarray
            //        appdelegate.detailImage = temp.menu_item_image
            
            print("temp.menu_item_name : \(temp.menu_item_name)")
            print("temp.allengsArray.count : \(temp.allengsArray.count)")
            print("temp.ingredientsarray.count : \(temp.ingredientsarray.count)")
            print("temp.nutritianarray.count : \(temp.nutritianarray.count)")
            print("temp1 : \([temp1])")
            
            print("passdata : \(passdata.count)")
            
            }
            self.navigationController?.pushViewController(dvc, animated: true)
        }
        else
        {
            var msg = "This Menu Item info is unavailable at the moment please check with your server"
           self.vcFor?.modalTransitionStyle = .crossDissolve
           self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  msg
            self.present(self.vcFor!, animated: true, completion: nil)
            
//            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ValidationPopupViewController") as! ValidationPopupViewController
//
//            vc.isLogin = true
//            vc.actionFor = ""
//            vc.titleFor =  msg
//            addChildViewController(vc)
//            view.addSubview(vc.view)
//            vc.didMove(toParentViewController: self)
            
        
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenScale = UIScreen.main.scale
        let calculateHeight = ((screenSize.width*600)/1024)
        print(screenSize.width)
        print(calculateHeight)
        return calculateHeight
        
        
//
//        return 160
        
    }
  

    
    /////
    @objc func clickFavButton(sender : UIButton)
    {
        self.view.endEditing(true)
        self.startSpinner()
        let tag = sender.tag
        
        var temp1 = productSubMenuArray[tag]
        let indexPath = IndexPath(row: tag, section: 0)
        //        let image = UIImage(named: "ic_action_fav_icon")
        let cell: RecentItemTableViewCell = self.tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecentItemTableViewCell
        
        if(temp1.flag == "dislike")
        {
            
            let headers = ["Content-Type":"Application/json"]
            let _url = favourite_menuitem
            
            var group_id = getValueForKey(keyValue: kmenu_item_group_id)
            print("group_id : \(group_id)")
            var merchant = getValueForKey(keyValue: kmarchentId)
            print("merchant : \(merchant)")
            var stringvalue = ""
            
            var data1 = [Ani]()
            
            let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
            var  result = try! self.context.fetch(fetch)
            for data in result as! [NSManagedObject]
            {
                data1.append(data as! Ani)
                
            }
            
            print("menu_item_id_fav : \(temp1.menu_item_id!)")
            
            var parentID = ""
                                          if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                                               parentID = ""
                                          }
                                          else{
                                              if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                                                      //  parentID = ""
                                                parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                                    }
                                                    else{
                                                        parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                                    }
                                          }
                                          print("Parent ID from UserDefaults : \(parentID)")
            
            let parameters: Parameters = ["merchant_id" :self.appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"type" : "add", "menuitem_id" : temp1.menu_item_id!,"partner_id":parentID]
            
            
            print(parameters)
            
            Alamofire.request(_url,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding.queryString,
                              headers: headers).responseJSON { (response) in
                                print("--------------------------------------------------------------------------)")
                                print("Request  \(response.request)")
                                print("RESPONSE \(response.result.value)")
                                print("RESPONSE \(response.result)")
                                print("RESPONSE \(response)")
                                
                                if response.result.isSuccess {
                                    print("SUKCES with \(response)")
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    let res = jsonDic.value(forKey: "result") as? String
                                    if (res == "success"){
                                        let fav = jsonDic.value(forKey: "favourite") as? String
                                        if (fav == "yes") {
                                            temp1.flag="like"
                                        } else {
                                            temp1.flag="dislike"
                                        }
                                    }
                                    
                                    
                                    DispatchQueue.main.async{
                                        self.tableview.reloadData()
                                        self.stopSpinner()
                                        let msg = jsonDic.value(forKey: "msg") as? String
                                        self.view.makeToast(msg)
//                                        showDialogWithOneButton(viewControl: self, titleMsg: "ANI", msgTitle: jsonDic.value(forKey: "msg") as? String ?? "favrouites changed")
                                    }
                                    //                                sender.btnFav!.setImage(image, for: .normal)
                                    
                                    
                                }
                                else   if (response.result.isFailure) {
                                    self.stopSpinner()
                                    var str = response.result.error?.localizedDescription
                                    print(response.result.error?.localizedDescription)
                                    
                                    if str ==
                                        "The Internet connection appears to be offline."
                                    {
                                        str = "You are offline please check your internet connection"
                                    }
                                    
                                    setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                                    var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                    self.addChildViewController(pop!)
                                    pop!.view.frame = self.view.frame
                                    self.view.addSubview(pop!.view)
                                    pop?.didMove(toParentViewController: self)
                                    
                                    
                                }
            }
            
            //        self.stopSpinner()
            //        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
            //
        }
        else
        {
            
            
            let headers = ["Content-Type":"Application/json"]
            let _url = favourite_menuitem
            
            var group_id = getValueForKey(keyValue: kmenu_item_group_id)
            print("group_id : \(group_id)")
            var merchant = getValueForKey(keyValue: kmarchentId)
            print("merchant : \(merchant)")
            var stringvalue = ""
            
            var data1 = [Ani]()
            
            let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
            var  result = try! self.context.fetch(fetch)
            for data in result as! [NSManagedObject]
            {
                data1.append(data as! Ani)
                
            }
            
            print("menu_item_id_fav : \(menu_item_id_fav)")
            
            var parentID = ""
            if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                 parentID = ""
            }
            else{
                if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                    //  parentID = ""
                    parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                }
                else{
                    parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                    }
                }
                    print("Parent ID from UserDefaults : \(parentID)")
            
            
            let parameters: Parameters = ["merchant_id" :self.appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"type" : "delete", "menuitem_id" : temp1.menu_item_id!,"partner_id":parentID]
            
            
            print(parameters)
            
            Alamofire.request(_url,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding.queryString,
                              headers: headers).responseJSON { (response) in
                                print("Request  \(response.request)")
                                print("RESPONSE \(response.result.value)")
                                print("RESPONSE \(response.result)")
                                print("RESPONSE \(response)")
                                
                                if response.result.isSuccess {
                                    print("SUKCES with \(response)")
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    let res = jsonDic.value(forKey: "result") as? String
                                    if (res == "success"){
                                        let fav = jsonDic.value(forKey: "favourite") as? String
                                        if (fav == "yes") {
                                            temp1.flag="like"
                                        } else {
                                            temp1.flag="dislike"
                                        }
                                    }
                                    
                                    
                                    DispatchQueue.main.async{
                                        self.tableview.reloadData()
                                        self.stopSpinner()
                                        let msg = jsonDic.value(forKey: "msg") as? String
                                        self.view.makeToast(msg)
                                        
                                        
//                                        showDialogWithOneButton(viewControl: self, titleMsg: "ANI", msgTitle: jsonDic.value(forKey: "msg") as? String ?? "favrouites changed")
                                    }
                                    //                                sender.btnFav!.setImage(image, for: .normal)
                                    
                                    
                                }
                                else   if (response.result.isFailure) {
                                    
                                    self.stopSpinner()
                                    
                                    var str = response.result.error?.localizedDescription
                                    print(response.result.error?.localizedDescription)
                                    
                                    if str ==
                                        "The Internet connection appears to be offline."
                                    {
                                        str = "You are offline please check your internet connection"
                                    }
                                    
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  str!
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                                    //    var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                    //    self.addChildViewController(pop!)
                                    //    pop!.view.frame = self.view.frame
                                    //    self.view.addSubview(pop!.view)
                                    //    pop?.didMove(toParentViewController: self)
                                    //
                                }
            }
            
            
            
        }
        
    }
    
    
    
    
    
}
extension RecentViewController : UISearchBarDelegate {
    
  
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchView.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.getSearchResult(str: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print(self.searchView.text)
        
        self.getSearchResult(str: self.searchView.text ?? "")
        self.dismissKeyboard()
        
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        
        if lblMerchandInactive.isHidden == false
        {
            self.lblNoDataFound.isHidden = true
            self.searchViewHeight.constant = 0
            self.searchView.showsCancelButton = true
            self.searchView.text = ""
            self.productSubMenuArray.removeAll()
            DispatchQueue.main.async
            {
                self.tableview.reloadData()
            }
            
        }
        else
        {
        
        
        self.lblNoDataFound.isHidden = true
        self.searchViewHeight.constant = 0
        self.searchView.showsCancelButton = true
        self.searchView.text = ""
        self.productSubMenuArray = self.finalArray
        self.tableview.reloadData()
          self.dismissKeyboard()
        //        self.viewDidLayoutSubviews()
        }
    }
    
    func getSearchResult(str: String) {
        
        if lblMerchandInactive.isHidden == false
        {
            
            self.productSubMenuArray.removeAll()
            DispatchQueue.main.async {
                self.tableview.reloadData()
            }
        }
        else
        {
            
            if str == "" {
                self.productSubMenuArray = self.finalArray
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                }
            }
            else {
                self.productSubMenuArray.removeAll()
                // Commented by Pratik for removing case sensitivity to the filter
                
                //            self.productSubMenuArray = self.finalArray.filter ({
                //                $0.menu_item_name.contains(str)
                //            })
                self.productSubMenuArray = self.finalArray.filter
                    {
                        $0.menu_item_name.localizedCaseInsensitiveContains(str)
                        
                }
                
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                }
                
            }
            if (self.productSubMenuArray.count == 0){
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
            //        self.viewDidLayoutSubviews()
            
        }
        }

    
}


// Done by Pratik for handling the offline products

extension RecentItemTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return allergensArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RecentItemCollectionViewCell
        let dic = allergensArray[indexPath.row]
        
        
//        if productStatusCheck2 == "1"
//        {
//            cell.recentAllergentImage.alpha = 0.25
//        }
//        else
//        {
//            cell.recentAllergentImage.alpha = 1
//        }
        
        if allergensFlagRecent == "0"
            {
                cell.recentAllergentImage.alpha = 0.25
            }
            else
            {
                cell.recentAllergentImage.alpha = 1
            }
        
        if dic.flag == "true"
        {
            if dic.allergan_Img != nil && dic.allergan_Img != "" {
                let imgURL = dic.allergan_Img!
                let url = URL(string: imgURL)
                
                
                cell.recentAllergentImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "allergens_placeholder"), options: .retryFailed, completed: nil)
                cell.recentAllergentImage.contentMode = .scaleAspectFill
                
            }
            else {
                cell.recentAllergentImage.image = #imageLiteral(resourceName: "allergens_placeholder")
                cell.recentAllergentImage.contentMode = .scaleAspectFit
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //    let collectionWidth = collectionView.bounds.width
        //    var count: CGFloat =  CGFloat(productSubMenuArray[tableposition].allengsArray.count)
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
     //for Scroll Arrow displaying
       func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            
            if UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone SE"
                           {
                              if  allergensArray.count > 5{
                                  if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                      
                                           //Load more data & reload your collection view
                                            
                                            print("Last cell is visible")
                                            
                                            rightBtn.isHidden = true
                                            rightArrowImg.isHidden = true
                                           self.rightBtnWidthConstant.constant = 0
                                           self.rightBtnImgWidthConstant.constant = 0
                                                       
                                         }
                                         else{
                                            print("Last cell is invisible")
                                            
                                            rightBtn.isHidden = false
                                            rightArrowImg.isHidden = false
                                            self.rightBtnWidthConstant.constant = 25
                                            self.rightBtnImgWidthConstant.constant = 20
                                                                  
                                        }
                                 
                            //    let lastElement = allergensArray.last
                            //    print("lastElement = \(String(describing: lastElement))")
                                
    //                          let  firstElementIndex = self.allergencollectionview.indexPathsForVisibleItems.first
    //                             print("firstElementIndex = \(String(describing: firstElementIndex))")
    //
    //                            let isCellVisible = self.allergencollectionview.visibleCells.map { self.allergencollectionview.indexPath(for: $0) }.contains([0,0])
    //                            print("isCellVisible = \(String(describing: isCellVisible))")
                                
                                
    //                            if (self.appdelegate.firstLoadScrollFlag == true){
    //
    //                                self.appdelegate.firstLoadScrollFlag = false
    //
    //                                print("first load cell is visible")
    //                                print("indexPath.row = \(indexPath.row)")
    //
    //                                leftBtn.isHidden = true
    //                                leftArrowImgView.isHidden = true
    //                                self.leftBtnWidthConstant.constant = 0
    //                                self.leftBtnImgWidthConstant.constant = 0
    //                            }
    //                            else{
                                   if (indexPath.row == 0 ) { //it's your first cell
                                            //Load more data & reload your collection view
                                                                                  
                                    print("first cell is visible")
                                    print("indexPath.row = \(indexPath.row)")
                                                                          
                                    leftBtn.isHidden = true
                                    leftArrowImg.isHidden = true
                                    self.leftBtnWidthConstant.constant = 0
                                    self.leftBtnImgWidthConstant.constant = 0
                                                                          
                                    }
                                    else{
                                    print("first cell is invisible")
                                    print("indexPath.row = \(indexPath.row)")
                                                                          
                                    leftBtn.isHidden = false
                                    leftArrowImg.isHidden = false
                                    self.leftBtnWidthConstant.constant = 25
                                    self.leftBtnImgWidthConstant.constant = 20
                                  }
                         //       }
                                
                                       
                                
    //                            let visible = allergencollectionview.indexPathsForVisibleItems
    //                            print("visibleCell = \(visible))")
    //                            print("indexPath = \(indexPath)")
                                
                              }
                            
                          }
                        else  if UIDevice.current.modelName == "iPhone 6" || UIDevice.current.modelName == "iPhone 6s" || UIDevice.current.modelName == "iPhone 7" || UIDevice.current.modelName == "iPhone 8" || UIDevice.current.modelName == "iPhone X" || UIDevice.current.modelName == "iPhone XS"
                          {
                                    if  allergensArray.count > 6{
                                                           if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                                               
                                                                    //Load more data & reload your collection view
                                                                     
                                                                     print("Last cell is visible")
                                                                     
                                                                     rightBtn.isHidden = true
                                                                     rightArrowImg.isHidden = true
                                                                    self.rightBtnWidthConstant.constant = 0
                                                                    self.rightBtnImgWidthConstant.constant = 0
                                                                                
                                                                  }
                                                                  else{
                                                                     print("Last cell is invisible")
                                                                     
                                                                     rightBtn.isHidden = false
                                                                     rightArrowImg.isHidden = false
                                                                     self.rightBtnWidthConstant.constant = 25
                                                                     self.rightBtnImgWidthConstant.constant = 20
                                                                                           
                                                                 }
                                                                 
                                                                  if (indexPath.row == 0 ) { //it's your first cell
                                                                            //Load more data & reload your collection view
                                                                             
                                                                     print("first cell is visible")
                                                                        
                                                                     leftBtn.isHidden = true
                                                                     leftArrowImg.isHidden = true
                                                                     self.leftBtnWidthConstant.constant = 0
                                                                     self.leftBtnImgWidthConstant.constant = 0
                                                                     
                                                                     }
                                                                     else{
                                                                       print("first cell is invisible")
                                                                     
                                                                     leftBtn.isHidden = false
                                                                     leftArrowImg.isHidden = false
                                                                     self.leftBtnWidthConstant.constant = 25
                                                                     self.leftBtnImgWidthConstant.constant = 20
                                                                 }
                                        
                                      }
                                     
                          }
                          else  if UIDevice.current.modelName == "iPhone 6 Plus" || UIDevice.current.modelName == "iPhone 6s Plus" || UIDevice.current.modelName == "iPhone 7 Plus" || UIDevice.current.modelName == "iPhone 8 Plus" || UIDevice.current.modelName == "iPhone XS Max" || UIDevice.current.modelName == "iPhone XR"
                              {
                                        if  allergensArray.count > 7{
                                                                  if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                                                      
                                                                           //Load more data & reload your collection view
                                                                            
                                                                            print("Last cell is visible")
                                                                            
                                                                            rightBtn.isHidden = true
                                                                            rightArrowImg.isHidden = true
                                                                           self.rightBtnWidthConstant.constant = 0
                                                                           self.rightBtnImgWidthConstant.constant = 0
                                                                                       
                                                                         }
                                                                         else{
                                                                            print("Last cell is invisible")
                                                                            
                                                                            rightBtn.isHidden = false
                                                                            rightArrowImg.isHidden = false
                                                                            self.rightBtnWidthConstant.constant = 25
                                                                            self.rightBtnImgWidthConstant.constant = 20
                                                                                                  
                                                                        }
                                                                        
                                                                         if (indexPath.row == 0 ) { //it's your first cell
                                                                                   //Load more data & reload your collection view
                                                                                    
                                                                            print("first cell is visible")
                                                                               
                                                                            leftBtn.isHidden = true
                                                                            leftArrowImg.isHidden = true
                                                                            self.leftBtnWidthConstant.constant = 0
                                                                            self.leftBtnImgWidthConstant.constant = 0
                                                                            
                                                                            }
                                                                            else{
                                                                              print("first cell is invisible")
                                                                            
                                                                            leftBtn.isHidden = false
                                                                            leftArrowImg.isHidden = false
                                                                            self.leftBtnWidthConstant.constant = 25
                                                                            self.leftBtnImgWidthConstant.constant = 20
                                                                        }
                                            
                                        }
                                             
                              }
                      else{
                          if  allergensArray.count > 7{
                                                 if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                                     
                                                          //Load more data & reload your collection view
                                                           
                                                           print("Last cell is visible")
                                                           
                                                           rightBtn.isHidden = true
                                                           rightArrowImg.isHidden = true
                                                          self.rightBtnWidthConstant.constant = 0
                                                          self.rightBtnImgWidthConstant.constant = 0
                                                                      
                                                        }
                                                        else{
                                                           print("Last cell is invisible")
                                                           
                                                           rightBtn.isHidden = false
                                                           rightArrowImg.isHidden = false
                                                           self.rightBtnWidthConstant.constant = 25
                                                           self.rightBtnImgWidthConstant.constant = 20
                                                                                 
                                                       }
                                                       
                                                        if (indexPath.row == 0 ) { //it's your first cell
                                                                  //Load more data & reload your collection view
                                                                   
                                                           print("first cell is visible")
                                                              
                                                           leftBtn.isHidden = true
                                                           leftArrowImg.isHidden = true
                                                           self.leftBtnWidthConstant.constant = 0
                                                           self.leftBtnImgWidthConstant.constant = 0
                                                           
                                                           }
                                                           else{
                                                             print("first cell is invisible")
                                                           
                                                           leftBtn.isHidden = false
                                                           leftArrowImg.isHidden = false
                                                           self.leftBtnWidthConstant.constant = 25
                                                           self.leftBtnImgWidthConstant.constant = 20
                                                       }
                                 
                            }
                               
                      }
            
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
             print("didEndDisplaying = \(indexPath.row)")
        }
    //
}

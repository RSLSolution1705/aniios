//
//  EditProfileViewController.swift
//  Ani
//
//  Created by RSL-01 on 10/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SVProgressHUD
import AVFoundation

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PopUpDoneDelegate {
    
    let context = AppDelegate().persistentContainer.viewContext
    private var datePicker : UIDatePicker?
    var oldPhoneNumber = ""
    var penFlag = 0
    @IBOutlet weak var profileImagge: UIImageView!
    
    var tempProfileImage = UIImageView()
    var changeflag = 1
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var profileName: UILabel!
    var picker = UIImagePickerController()
    @IBOutlet weak var txteditbutton: UIImageView!
    
    @IBOutlet weak var txteditbutton2: UIImageView!
    
    @IBOutlet weak var editbutton: UIBarButtonItem!
    @IBOutlet weak var emailID: designableTextfield!
    @IBOutlet weak var mobileNo: designableTextfield!
    @IBOutlet weak var dateOfBirth: designableTextfield!
    
    var vcFor : ValidationPopupViewController? = nil
    
    @IBOutlet weak var okbtn: UIButton!
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var popupLbl: UILabel!
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    @IBOutlet weak var cancleBtn: UIButton!
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startSpinner()
           self.hideKeyboardWhenTappedAround()
        
        
        blankView.isHidden = true
        popupView.isHidden = true
      
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        
        okbtn.layer.cornerRadius = 15
        cancleBtn.layer.cornerRadius = 15
        
        self.title = "Account Details"
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationController?.isNavigationBarHidden = false
        
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
        
        // let button2 = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_write") , style: .plain, target: self, action: #selector(infobutton(sender:)))
        let button2 = UIBarButtonItem(title: "EDIT", style: .plain, target: self, action: #selector(infobutton(sender:)))
        self.navigationItem.rightBarButtonItem  = button2
        
        self.navigationController?.isNavigationBarHidden = false
        
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
         var components = DateComponents()
        components.year = -13
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
//        datePicker.minimumDate = minDate
        self.datePicker?.maximumDate = maxDate
        
//        self.datePicker?.maximumDate = Date()
        self.datePicker?.minimumDate = Calendar.current.date(byAdding: .year, value: -119, to: Date())
        let defaults = UserDefaults.standard
        
        emailID.isUserInteractionEnabled = false
        mobileNo.isUserInteractionEnabled = false
        dateOfBirth.isUserInteractionEnabled = false
        profileName.isUserInteractionEnabled = false
        //        profileImagge.isUserInteractionEnabled = false
        //        addimage.isUserInteractionEnabled = false"
        //editbutton.image = UIImage(named: "ic_plus_white")
        txteditbutton.isHidden = true
        txteditbutton2.isHidden = true
    
        emailID.text = getValueForKey(keyValue: kemail)
        mobileNo.text = getValueForKey(keyValue: kmobNumber)
        oldPhoneNumber = mobileNo.text ?? ""
        dateOfBirth.text = getValueForKey(keyValue: KDOB)
        profileName.text = getValueForKey(keyValue: kProfileName)
        
//        setDefaultValue(keyValue: kemail, valueIs:  self.emailID.text!)
//        
//        setDefaultValue(keyValue: KDOB, valueIs:  self.dateOfBirth.text!)
//        
//        setDefaultValue(keyValue: kmobNumber, valueIs:  self.mobileNo.text!)
//        
//        setDefaultValue(keyValue: kProfileName, valueIs:   self.profileName.text!)
//        
        
        
        dateOfBirth.inputView = datePicker
        datePicker?.addTarget(self, action:#selector(EditProfileViewController.datechanged(datePicker :)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditProfileViewController.viewtapped(GestureRecognizer:)))
        
        self.view.addGestureRecognizer(tapGesture)
        //      self.profileImagge.image =  appdelegate.profile_image
        //        profileImagge.setRounded()
        
        
        //        let imgUrl = defaults.object(forKey: "picture") as? String
        //        let imageData = try! Data.init(contentsOf: URL(string: imgUrl!)!)
        //        self.profileImagge.image = UIImage(data: imageData)
        //         profileImagge.setRounded()
        
        if let imgUrl = defaults.object(forKey: "picture") as? String
        {
            print("imgUrl : \(imgUrl)")
            Alamofire.request(imgUrl, method: .get).response { response in
                guard let image = UIImage(data:response.data!) else {
                    // Handle error
                    autoreleasepool {
                        self.profileImagge.image = UIImage(named: "ic_profil 4")
                        self.profileImagge.setRounded()
                    }
                    return
                }
                autoreleasepool {
                    let imageData = UIImageJPEGRepresentation(image,1.0)
                    self.profileImagge.image = UIImage(data : imageData!)
                    self.tempProfileImage.image = self.profileImagge.image
                    self.profileImagge.setRounded()
                }
            }
        }
        
        
        //        if let imgData = defaults.object(forKey: "picture") as? NSData
        //        {
        //            if let image = UIImage(data: imgData as Data)
        //            {
        //                //set image in UIImageView imgSignature
        //                self.profileImagge.image = image
        //                profileImagge.setRounded()
        //                //remove cache after fetching image data
        //                //                defaults.removeObject(forKey: "image")
        //            }
        //        }
        
        
         getInfo()
        NotificationCenter.default.addObserver(self, selector: #selector(loginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height-100
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
if textField == mobileNo{
            guard let textFieldText = mobileNo.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 14
        }
            
        else {
            return true
        }
    }
    
func getInfo()
{
    var data1 = [Ani]()
    
    let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
    let  result = try! self.context.fetch(fetch)
    for data in result as [NSManagedObject]
    {
        data1.append(data as! Ani)
    }
    
    let headers = ["Content-Type":"Application/json"]
    let _url = consumer_information
   
    
    let parameters: Parameters = ["account_id" : (data1[0].accId!)]
    print(parameters)
    
    
    Alamofire.request(_url,
                      method: .post,
                      parameters: parameters,
                      encoding: URLEncoding.queryString,
                      headers: headers).responseJSON { (response) in
                        
                        print("Request  \(response.request)")
                        
                        print("RESPONSE \(response.result.value)")
                        print("RESPONSE \(response.result)")
                        print("RESPONSE \(response)")
                        
                        if response.result.isSuccess {
                            print("SUKCES with \(response)")
                            
                            if (response.result != nil)
                            {
                                let jsonDic = response.result.value as! NSDictionary
                                print(jsonDic)
                                if let res = jsonDic.value(forKey: "result") as? String
                                {
                                    if res == "failed"
                                    {
                                        self.stopSpinner()
                                        self.blankView.isHidden = false
                                        self.popupView.isHidden = false
                                        //popup
                                        var str = jsonDic.object(forKey: "msg")as? String
                                        print(response.result.error?.localizedDescription)
                                        if str == "The Internet connection appears to be offline." || str == "A server with the specified hostname could not be found."
                                        {
                                            str = "You are offline please check your internet connection"
                                        }
                                        //                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                        //                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                        //                                            self.vcFor?.isLogin = true
                                        //                                            self.vcFor?.actionFor = ""
                                        self.popupLbl.text =  str!
                                        //                                           self.present(self.vcFor!, animated: true, completion: nil)
                                        
                                    }else
                                    {
                                        
                                        self.stopSpinner()
                                        
//                                        self.blankView.isHidden = false
//                                        self.popupView.isHidden = false
                                        let jsonDic = response.result.value as! NSDictionary
                                        print(jsonDic)
                                  self.stopSpinner()
                                        self.emailID.text = jsonDic.object(forKey: "email_id")as? String
                                                self.mobileNo.text = jsonDic.object(forKey: "mobile_number")as? String
                                                self.dateOfBirth.text = jsonDic.object(forKey: "date_of_birth")as? String
                                         var firstName = jsonDic.object(forKey: "first_name")as? String
                                        var lastName = jsonDic.object(forKey: "last_name")as? String
                                      
                                        self.profileName.text = firstName! + " " + lastName!


                                        setDefaultValue(keyValue: kemail, valueIs:  self.emailID.text!)
                                        
                                        setDefaultValue(keyValue: KDOB, valueIs:  self.dateOfBirth.text!)
                                        
                                        setDefaultValue(keyValue: kmobNumber, valueIs:  self.mobileNo.text!)
//                                        self.oldPhoneNumber = getValueForKey(keyValue: kmobNumber)
//
                                        
                                        setDefaultValue(keyValue: kProfileName, valueIs:   self.profileName.text!)
                                        
                            
                                      
                                        
                                        
                                        
                                        
                                    }
                                }
                            }
                        }
                            
                        else if response.result.isFailure
                        {
                            self.stopSpinner()
                            self.blankView.isHidden = false
                            self.popupView.isHidden = false
                            var str = response.result.error?.localizedDescription
                            print(response.result.error?.localizedDescription)
                            
                            if str == "The Internet connection appears to be offline." || str == "A server with the specified hostname could not be found."
                            {
                                str = "You are offline please check your internet connection"
                            }
                            //                                self.vcFor?.modalTransitionStyle = .crossDissolve
                            //                                self.vcFor?.modalPresentationStyle = .overFullScreen
                            //                                self.vcFor?.isLogin = true
                            //                                self.vcFor?.actionFor = ""
                            self.popupLbl?.text =  (str)!
                            //                                self.present(self.vcFor!, animated: true, completion: nil)
                            
                        }
                        
    }
    
    }
    
    
    @IBOutlet weak var addimage: UIButton!
    
    
    func changeImage()
    {
        self.startSpinner()
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let  result = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
        {
            
            data1.append(data as! Ani)
            
        }
        
        let myUrl = NSURL(string: change_profile_image1);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = ["account_id" : (data1[0].accId!) ]
        
        print(param)
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let imageData = UIImageJPEGRepresentation(profileImagge.image!,0.1)
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "profile_image", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                //showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "Error", msgTitle:error?.localizedDescription ?? "Something went wrong!!")
                self.stopSpinner()
//                DispatchQueue.main.async {
                
                
                self.blankView.isHidden = false
                self.popupView.isHidden = false
                
                self.profileImagge.image  = self.tempProfileImage.image
                
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
 //               self.vcFor?.actionFor = ""
                self.popupLbl.text = error?.localizedDescription ?? "Something went wrong!!"
  //              self.present(self.vcFor!, animated: true, completion: nil)
//                }
                return
            }
            
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print(jsonDic)
                
                var temp = signUpResponse()
                var temp2 = editpic()
                //var image = jsonDic?.object(forKey: "profile_image")as! String
                
                var img = jsonDic?.object(forKey: "profile_image") as! String
                let defaults = UserDefaults.standard
                
                if (img != nil && img != "")
                {
                    var imageData = try! Data.init(contentsOf: URL(string: img)!)
                    temp2.profile = UIImage(data: imageData)
                    
                    
                    defaults.set(img, forKey: "picture")
                    defaults.synchronize()
                    
                    var str = jsonDic?.object(forKey: "msg")as! String
                    var imgData = UIImageJPEGRepresentation(temp2.profile!, 1)
                    
                    
                    DispatchQueue.main.async {
                        self.stopSpinner()
                        self.blankView.isHidden = false
                        self.popupView.isHidden = false
                         self.popupLbl.text = "Profile image update successfully"
                    }
                   
//                    self.vcFor?.modalTransitionStyle = .crossDissolve
//                    self.vcFor?.modalPresentationStyle = .overFullScreen
//                    self.vcFor?.isLogin = true
//                    self.vcFor?.actionFor = ""
                   
  //                  self.present(self.vcFor!, animated: true, completion: nil)
                    
                }else
                {
                    img = (defaults.object(forKey: "picture") as? String) ?? ""
                    self.profileImagge.image  = self.tempProfileImage.image
                    
                }
                
                
            }
            catch {
                
                //let str = response.result.error?.localizedDescription
                //print(response.result.error?.localizedDescription)
                self.stopSpinner()
                self.blankView.isHidden = false
                self.popupView.isHidden = false
                self.profileImagge.image  = self.tempProfileImage.image
                
                setDefaultValue(keyValue: kmsg, valueIs: error as! String)
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
//                self.vcFor?.actionFor = ""
                self.popupLbl.text = error as? String
//                self.present(self.vcFor!, animated: true, completion: nil)
            }
            
        }
        
        task.resume()
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        var body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func addimage(_ sender: Any)
        
    {
        //        changeImage()
    }
    
    
    @objc func viewtapped(GestureRecognizer : UIGestureRecognizer)
    {
        view.endEditing(true)
    }
    
    @objc func datechanged(datePicker : UIDatePicker)
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateOfBirth.text = dateFormatter.string(from: datePicker.date)
        
      //  view.endEditing(true)
        
    }
    
    @objc func backBtn(sender:UIButton)
    {
        
        
       if  (changeflag == 1)
      {
        
            _ = navigationController?.popViewController(animated: true)
        }
        else{

            let alertController = UIAlertController(title: "ANI", message: "Are you sure you want to save changes ?", preferredStyle: .alert)
    
    
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.validateLoginform()
//                self.edit()

                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
                var home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController")as! sideMenuViewController
                self.navigationController?.pushViewController(home, animated: true)

            }

            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)


        }
    }
    
    
    
    
    
    @IBAction func EditAction(_ sender: Any) {
        print("ashu")
        // if editbutton.image == #imageLiteral(resourceName: "ic_write")
        if editbutton.title == "EDIT"
        {
            changeflag = 0
            emailID.isUserInteractionEnabled = true
            mobileNo.isUserInteractionEnabled = true
            dateOfBirth.isUserInteractionEnabled = true
            profileName.isUserInteractionEnabled = true
            //            profileImagge.isUserInteractionEnabled = true
            //            addimage.isUserInteractionEnabled = true
            txteditbutton.isHidden = false
            txteditbutton2.isHidden = false
            editbutton.title = "SAVE"
            //   editbutton.image = #imageLiteral(resourceName: "ic_check_white_48dp")
            
        }else
        {
            
            validateLoginform()
            
            //changeImage()
        }
        
    }
    
   
    
    
    @objc func infobutton(sender:UIButton)
    {
        
        //  if self.navigationItem.rightBarButtonItem?.image == #imageLiteral(resourceName: “ic_write”)
        if self.navigationItem.rightBarButtonItem?.title == "EDIT"
        {
            changeflag = 0
            //emailID.isUserInteractionEnabled = true
            mobileNo.isUserInteractionEnabled = true
            dateOfBirth.isUserInteractionEnabled = true
            profileName.isUserInteractionEnabled = true
            profileImagge.isUserInteractionEnabled = true
            //addimage.isUserInteractionEnabled = true
            txteditbutton.isHidden = false
            txteditbutton2.isHidden = false
            self.navigationItem.rightBarButtonItem?.title = "SAVE"
            
            //self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: “ic_check_white_48dp”)
            
        }
        else{
            self.navigationItem.rightBarButtonItem?.title = "SAVE"
            changeflag = 1
//            mobileNo.isUserInteractionEnabled = false
//            dateOfBirth.isUserInteractionEnabled = false
//            profileName.isUserInteractionEnabled = false
//            profileImagge.isUserInteractionEnabled = false
            //addimage.isUserInteractionEnabled = true
            txteditbutton.isHidden = false
            txteditbutton2.isHidden = false
            validateLoginform()
            //edit()
            
        }
        
    }
    
    
    @IBOutlet weak var editImage: UIButton!
    
    @IBAction func editImage(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let isCameraAvalible = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            
            if(isCameraAvalible)
            {
                self.goToCamera()
            }
            else
            {
                // showDialogWithOneButton(animated: true, viewControl: self, titleMsg: kErrorMsg, msgTitle: kScanQrCodeError)
            }
        })
        let saveAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.PhotoLibray()
        })
        
        let removeAction = UIAlertAction(title: "Remove Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
//            SVProgressHUD.show()
            
            if Reachability.isConnectedToNetwork() {
//                self.startSpinner()
                self.removePhoto()
            }
            else {
                self.stopSpinner()
                DispatchQueue.main.async {
                self.blankView.isHidden = false
                self.popupView.isHidden = false
                
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
//                self.vcFor?.actionFor = ""
                self.popupLbl?.text =  Internet_Connection_Alert
              //  self.present(self.vcFor!, animated: true, completion: nil)
                }
                
            }
           
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        
        if (UserDefaults.standard.object(forKey: "picture") as? String) != "" && (UserDefaults.standard.object(forKey: "picture") as? String) != nil {
            optionMenu.addAction(removeAction)
        }
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
        
        
        
    }
    
    
    
    func takePhoto()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        //profileImagge.setRounded()
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func PhotoLibray()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate =  self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        profileImagge.image = info [UIImagePickerControllerOriginalImage]  as? UIImage
        DispatchQueue.main.async {
            
            if Reachability.isConnectedToNetwork() {
  //              self.startSpinner()
               self.changeImage()
            }
            else {
                self.stopSpinner()
                self.blankView.isHidden = false
                self.popupView.isHidden = false
                
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
//                self.vcFor?.actionFor = ""
                self.popupLbl?.text =  Internet_Connection_Alert
       //         self.present(self.vcFor!, animated: true, completion: nil)
                
                
            }
            
            
          
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func goToCamera()
    {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch (status)
        {
        case .authorized:
            self.takePhoto()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                if (granted)
                {
                    self.takePhoto()
                }
                else
                {
                    self.camDenied()
                }
            }
            
        case .denied:
            self.camDenied()
            
        case .restricted:
            let alert = UIAlertController(title: "Restricted",
                                          message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func camDenied()
    {
        DispatchQueue.main.async
            {
                var alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n5. Open this app and try again."
                
                var alertButton = "OK"
                var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)
                
                if UIApplication.shared.canOpenURL(URL(string: UIApplicationOpenSettingsURLString)!)
                {
                    alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again."
                    
                    alertButton = "Go"
                    
                    goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                    })
                }
                
                let alert = UIAlertController(title: "Error", message: alertText, preferredStyle: .alert)
                alert.addAction(goAction)
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    //  func changeImage()
    //  {
    //    var data1 = [Ani]()
    //
    //    let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
    //    let  result = try! self.context.fetch(fetch)
    //    for data in result as [NSManagedObject]
    //    {
    //        data1.append(data as! Ani)
    //
    //    }
    //
    ////    params.put("account_id", sp.getString("account_id", ""));
    ////    params.put("profile_image", finalEncodedImage);
    //
    //    let headers = ["Content-Type":"Application/json"]
    //    let _url = "http://52.202.56.230/ANI/ws/change_profile_image.php"
    //    var emailId = getValueForKey(keyValue: "kemail")
    //    var name = getValueForKey(keyValue: "first_name")
    //
    //
    //    let parameters: Parameters = ["account_id" : (data1[0].accId!) ,"profile_image" : profileImagge.image]
    //    print(parameters)
    //
    //
    //    Alamofire.request(_url,
    //                      method: .post,
    //                      parameters: parameters,
    //                      encoding: URLEncoding.queryString,
    //                      headers: headers).responseJSON { (response) in
    //
    //                        print("Request  \(response.request)")
    //
    //                        print("RESPONSE \(response.result.value)")
    //                        print("RESPONSE \(response.result)")
    //                        print("RESPONSE \(response)")
    //
    //                        if response.result.isSuccess {
    //                            print("SUKCES with \(response)")
    //
    //                            if (response.result != nil)
    //                            {
    //                                let jsonDic = response.result.value as! NSDictionary
    //                                print(jsonDic)
    //                                if let res = jsonDic.value(forKey: "result") as? String
    //                                {
    //                                    if res == "failed"
    //                                    {
    //                                        SVProgressHUD.dismiss()
    //                                        //popup
    //                                        var str = response.result.error?.localizedDescription
    //                                        print(response.result.error?.localizedDescription)
    //
    //                                        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msg0.: (response.result.error?.localizedDescription)!)
    //                                    }else
    //                                    {
    //
    //                                        SVProgressHUD.dismiss()
    //                                        let jsonDic = response.result.value as! NSDictionary
    //                                        print(jsonDic)
    //
    //                                        var img = jsonDic.object(forKey: "profile_image") as! String
    //
    //                                        if (img != nil)
    //                                        {
    //                                            var imageData = try! Data.init(contentsOf: URL(string: img)!)
    //                                           var image1 = UIImage(data: imageData)
    //                                            var image = image1
    //
    //                                            var imgData = UIImageJPEGRepresentation(image!, 1)
    //                                            //
    //                                            let defaults = UserDefaults.standard
    //                                            setDefaultValue(keyValue: "picture", valueIs: (imgData as? String)! )
    //
    //
    //                                            //                                                self.appdelegate.profile_image =  temp.profile_image
    //
    ////                                            var image  = image1 as? String
    //                                        }else
    //                                        {
    //                                            img = ""
    //                                        }
    //
    //
    //                                        var str = jsonDic.object(forKey: "msg")as! String
    //                                        showDialogWithOneButton(viewControl: self, titleMsg: "Ani", msgTitle: str )
    ////
    //
    //
    //
    ////                                        var home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController")as! sideMenuViewController
    ////                                        self.navigationController?.pushViewController(home, animated: true)
    //
    //                                    }  //                                    print(self.marchantArray)
    //                                }
    //                            }
    //                        }
    //
    //                        else if response.result.isFailure
    //
    //
    //                        {
    //                            SVProgressHUD.dismiss()
    //
    //                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please check internet connection")
    //                        }
    //
    //
    //
    //
    //
    //
    //
    //
    //    }
    //
    //    }
    
    
    func validateLoginform() {
        
        var ischeckValidation = true;
        
        if((self.dateOfBirth?.text?.count)! <= 0)
        {
            self.blankView.isHidden = false
            self.popupView.isHidden = false
            ischeckValidation  = false;
            
//            self.vcFor?.modalTransitionStyle = .crossDissolve
//            self.vcFor?.modalPresentationStyle = .overFullScreen
//            self.vcFor?.isLogin = true
//            self.vcFor?.actionFor = ""
            self.popupLbl?.text = "Please enter Date of Birth"
//      self.present(self.vcFor!, animated: true, completion: nil)
            
        }
//        else  if((self.mobileNo.text?.count) != nil  && (self.mobileNo?.text?.count)! <= 0)
//        {
//            self.blankView.isHidden = false
//            self.popupView.isHidden = false
//            ischeckValidation  = false;
//            
////            self.vcFor?.modalTransitionStyle = .crossDissolve
////            self.vcFor?.modalPresentationStyle = .overFullScreen
////            self.vcFor?.isLogin = true
////            self.vcFor?.actionFor = ""
//            self.popupLbl?.text = "Please enter mobile Number"
////            mobileNo.text =  oldPhoneNumber
// //           self.present(self.vcFor!, animated: true, completion: nil)
//            
//        }
        else if ((self.mobileNo.text) != "" && (self.mobileNo?.text?.characters.count)! > 14)
        {
            self.blankView.isHidden = false
            self.popupView.isHidden = false
            self.stopSpinner()
            ischeckValidation  = false;
            
//            self.vcFor?.modalTransitionStyle = .crossDissolve
//            self.vcFor?.modalPresentationStyle = .overFullScreen
//            self.vcFor?.isLogin = true
//            self.vcFor?.actionFor = ""
            self.popupLbl?.text =  "please enter maximum 14 digit phone number"
         mobileNo.text =  oldPhoneNumber
    //        self.present(self.vcFor!, animated: true, completion: nil)
            
        }
        else
            if ((self.mobileNo.text) != ""  && (self.mobileNo?.text?.characters.count)! < 8)
            {
                self.blankView.isHidden = false
                self.popupView.isHidden = false
                self.stopSpinner()
                ischeckValidation  = false;
                
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
//                self.vcFor?.actionFor = ""
                self.popupLbl?.text = "Please enter minimum 8 to 14 digit mobile number"
                mobileNo.text =  oldPhoneNumber
      //          self.present(self.vcFor!, animated: true, completion: nil)
            }
            else
            {
                if Reachability.isConnectedToNetwork() {
                    self.startSpinner()
                  
                   edit()
                    
                }
                else {
                    self.stopSpinner()
                    self.blankView.isHidden = false
                    self.popupView.isHidden = false
                    
//                    self.vcFor?.modalTransitionStyle = .crossDissolve
//                    self.vcFor?.modalPresentationStyle = .overFullScreen
//                    self.vcFor?.isLogin = true
//                    self.vcFor?.actionFor = ""
                    self.popupLbl?.text =  Internet_Connection_Alert
             //       self.present(self.vcFor!, animated: true, completion: nil)
                    
                    
                }
               
                
        }
        
    }

    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//
//         if textField == mobileNo
//        {
//            if (mobileNo.text?.utf16.count)! > 14
//            {
//                return false
//            }
//            else
//            {
//                if string.containsOnlyCharactersIn("0123456789")
//                {
//                    return true
//                }
//                else
//                {
//
//                    return false
//                }
//
//            }
//
//
//        }
//         return true
//    }
//
    func edit()
    {
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let  result = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
        {
            data1.append(data as! Ani)
        }
        
        let headers = ["Content-Type":"Application/json"]
        let _url = update_consumer_information
        var emailId = getValueForKey(keyValue: "kemail")
        var name = getValueForKey(keyValue: "first_name")
        
        let parameters: Parameters = ["account_id" : (data1[0].accId!) ,"gender" :""  , "date_of_birth":  dateOfBirth.text! ,"mobile_number" :mobileNo.text! , "home_number" : "","profile_image" : ""]
        print(parameters)
        
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                             self.blankView.isHidden = false
                                            self.popupView.isHidden = false
                                            //popup
                                            var str = jsonDic.object(forKey: "msg")as? String
                                            print(response.result.error?.localizedDescription)
                                            if str == "The Internet connection appears to be offline." || str == "A server with the specified hostname could not be found."
                                            {
                                                str = "You are offline please check your internet connection"
                                            }
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
                                            self.popupLbl.text =  str!
//                                            Internet_Connection_Alert
//
//                                           self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }else
                                        {
                                            
                                            self.stopSpinner()
                                            self.navigationItem.rightBarButtonItem?.title = "EDIT"
                                            self.changeflag = 1
                                            self.txteditbutton.isHidden = true
                                            self.txteditbutton2.isHidden = true
                                            self.blankView.isHidden = false
                                            self.popupView.isHidden = false
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            setDefaultValue(keyValue: KDOB, valueIs:  self.dateOfBirth.text!)
                                            
                                            setDefaultValue(keyValue: kmobNumber, valueIs:  self.mobileNo.text!)
                                            
                                            let str = jsonDic.object(forKey: "msg") as! String
                                            
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
                                            self.popupLbl.text =  str
//                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                            {
                                self.stopSpinner()
                                self.blankView.isHidden = false
                                self.popupView.isHidden = false
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str == "The Internet connection appears to be offline." || str == "A server with the specified hostname could not be found."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
//                                self.vcFor?.modalTransitionStyle = .crossDissolve
//                                self.vcFor?.modalPresentationStyle = .overFullScreen
//                                self.vcFor?.isLogin = true
//                                self.vcFor?.actionFor = ""
                                self.popupLbl?.text =  (str)!
//                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                            }
                            
        }
        
    }
    
    func removePhoto()
    {
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        let  result = try! self.context.fetch(fetch)
        for data in result as [NSManagedObject]
        {
            data1.append(data as! Ani)
        }
        
        let headers = ["Content-Type":"Application/json"]
        let _url = remove_profile_image
        
        let parameters: Parameters = ["account_id" : (data1[0].accId!)]
        print(parameters)
        
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            self.blankView.isHidden = false
                                            self.popupView.isHidden = false
                                            var str = jsonDic.object(forKey: "msg")as? String
                                            print(response.result.error?.localizedDescription)
                                            
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
                                            self.popupLbl.text =  str!
 //                                           self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }else
                                        {
                                            
                                            self.stopSpinner()
                                            
                                            self.blankView.isHidden = false
                                            self.popupView.isHidden = false
                                            self.profileImagge.image = UIImage(named: "ic_profil 4")
                                            DispatchQueue.main.async {
                                                self.profileImagge.setRounded()
                                                
                                            }
                                         
                                            //UserDefaults.standard.set("", forKey: "picture")
                                            UserDefaults.standard.removeObject(forKey: "picture")
                                            UserDefaults.standard.synchronize()
                                            
                                            var str = jsonDic.object(forKey: "msg")as! String
                                            
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
                                           self.popupLbl.text = "Profile photo is removed successfully"
//                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                            {
                                self.stopSpinner()
                                self.blankView.isHidden = false
                                self.popupView.isHidden = false
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str == "The Internet connection appears to be offline." || str == "A server with the specified hostname could not be found."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
//                                self.vcFor?.modalTransitionStyle = .crossDissolve
//                                self.vcFor?.modalPresentationStyle = .overFullScreen
//                                self.vcFor?.isLogin = true
//                                self.vcFor?.actionFor = ""
                                self.popupLbl?.text =  (response.result.error?.localizedDescription)!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                            }
                            
        }
        
    }
    
    
    @IBAction func okBtn(_ sender: Any) {
        self.blankView.isHidden = true
        self.popupView.isHidden = true
    }
    
}

//
//  LunchScreenViewController.swift
//  Ani
//
//  Created by Mac on 14/06/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire

class LunchScreenViewController: UIViewController
{
    @IBOutlet weak var lblKnowYourFood: UILabel!
    
    let kVersion = "CFBundleShortVersionString"  // for version check
    var appVersion = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
         print("LunchScreenViewController")
        
        
    }
    
    
   
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        checkForUpdateWebservice()
        self.navigationController?.isNavigationBarHidden = true
        
        lblKnowYourFood.transform = CGAffineTransform(scaleX: 0.05, y: 0.05);
        
        UIView.animate(withDuration: 3.0, delay: 0, options: .curveEaseIn , animations:
            {
                self.lblKnowYourFood.transform = CGAffineTransform(scaleX: 1,y: 1);
        }, completion: { finished in
            sleep(1)
          

        })
        
        //
        _ = Timer.scheduledTimer(timeInterval: 4.0, target:self, selector: #selector(redirection), userInfo: nil, repeats: false)

    }
    
    
    @objc func redirection() {
        
        //   Normal App Flow
            if  UserDefaults.standard.bool(forKey: "isUserLog") == true
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                self.navigationController?.pushViewController(vc, animated: false)

            }
            else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
        
    }
    
    //   code for version check
    
    func getVersion() -> String
    {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary[kVersion] as! String
        
        return "\(version)"
    }
    
    // code for version check ends here
    
    func checkForUpdateWebservice()
    {
//

        var v_name : String! = ""
        //get the current version of app
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print(version)
            v_name = version
        }
        else
        {
            v_name = ""
            print("not getting version")
        }

//        let Webservicelink = WebserviceLink()

        let myclass = WebServiceClass()
        let identifier = "com.gsgroup.Ani.app"

        //        "http://itunes.apple.com/lookup?bundleId=\(identifier)"

        let url : NSString = "http://itunes.apple.com/lookup?bundleId=\(identifier)" as NSString

        let urlStr : NSString = url.addingPercentEncoding (withAllowedCharacters: NSCharacterSet.urlQueryAllowed)! as NSString

        let searchURL : NSURL = NSURL(string: urlStr as String)!
        print(searchURL)

        myclass.getDataFromURL(searchURL as URL, completion: {(result: AnyObject?, error: NSError?) -> Void in
            // callback here

            if result==nil
            {
            }
            else
            {
                print(result!)

                let mystring = String(data: (result as! NSData) as Data, encoding: String.Encoding.utf8)

                let results = self.convertStringToDictionary(text: mystring!) // ["name": "James"]

                print("Results are : \(results!)")
                let name = (results?["resultCount"])! as? Int

                if name == 1
                {
                    var liveVersion = ""
                    let resultArray = (results?["results"])! as? NSArray

                    if let resultZero = resultArray?[0] {
                        //statements using 'constantName'
                        print(resultZero)
                        if let dict = resultZero as? NSDictionary{
                            liveVersion = dict.value(forKey: "version") as! String
                            print("Current Live Version of ANI App is : \(liveVersion)")
                        } else{
                        }

                    } else{
                        // the value of someOptional is not set (or nil).
                        return
                    }

                    //                    let version_name : String = (results?["version"])! as! String

                    if liveVersion != v_name
                    {

                        let alertController = UIAlertController(title: "ANI", message: "Update is available", preferredStyle: .alert)

                        let okAction = UIAlertAction(title: "Now", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                            let urls : URL! = URL(string: "https://itunes.apple.com/in/app/ani/id1470074950?ls=1")

                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(urls, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]) as [String : Any], completionHandler: nil)
//                                UIApplication.shared.open(urls, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(urls)
                            }
                        }
                        let cancelAction = UIAlertAction(title: "Later", style: UIAlertAction.Style.cancel) {
                            UIAlertAction in
                            NSLog("Cancel Pressed")

                            self.stopSpinner()
                            //   Normal App Flow
//                            if  UserDefaults.standard.bool(forKey: "isUserLog") == true
//                            {
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
//                                self.navigationController?.pushViewController(vc, animated: false)
//
//                            }
//                            else{
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
//                                self.navigationController?.pushViewController(vc, animated: false)
//                            }


                        }
                        alertController.addAction(okAction)
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion: nil)
//                        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    else
                    {
                         NSLog("Equel to version")
                        //   Normal App Flow
//                        if  UserDefaults.standard.bool(forKey: "isUserLog") == true
//                        {
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
//                            self.navigationController?.pushViewController(vc, animated: false)
//                        }
//                        else
//                        {
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
//                            self.navigationController?.pushViewController(vc, animated: false)
//                        }
                        
                    }
                }
            }
        })

    }
    
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
           if let data = text.data(using: String.Encoding.utf8) {
               do {
                   let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                   return json
               } catch {
                   print("Something went wrong")
               }
           }
           return nil
       }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(string: key), value)})
    }

    //--------------------------------------------------------------------------------------------------------
}

//
//  RecommendedViewController.swift
//  Ani
//
//  Created by RSL-01 on 10/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SVProgressHUD

class RecommendedViewController: UIViewController,UITextFieldDelegate,PopUpDoneDelegate {
    
    @IBOutlet weak var blurrView: UIView!
    
    @IBOutlet weak var Textfields: UITextField! // name (Required)
    @IBOutlet weak var Textfields1: UITextField! // Address
    
    @IBOutlet weak var lblAddressPlaceholder: UILabel!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtAddressHeightConstant: NSLayoutConstraint! // 30
    
    @IBOutlet weak var Textfields2: UITextField! // twitter
    @IBOutlet weak var Textfields3: UITextField! // facebook
    @IBOutlet weak var Textfields4: UITextField! // Phone
    
    @IBOutlet weak var RecommendButton: UIButton!
    
    var vcFor : ValidationPopupViewController? = nil
    
    let context = AppDelegate().persistentContainer.viewContext
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("RecommendedViewController")
        
           self.hideKeyboardWhenTappedAround()
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(loginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationItem.title = "Recommend Restaurant"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        
        self.setView()
        
    }
    
    func setView() {
        
        Placeholderextention()
        Textfields.delegate = self
        Textfields1.delegate = self
        Textfields2.delegate = self
        Textfields3.delegate = self
        Textfields4.delegate = self
        
        self.txtAddress.delegate = self
        
        blurrView.layer.cornerRadius = 10
        RecommendButton.layer.cornerRadius = 5
        
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height-150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func backBtn(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func RecommendedButton(_ sender: Any) {
        
        Textfields.resignFirstResponder()
        Textfields1.resignFirstResponder()
        Textfields2.resignFirstResponder()
        Textfields3.resignFirstResponder()
        Textfields4.resignFirstResponder()
        txtAddress.resignFirstResponder()
        validateLoginform()
    }
    
    func validateLoginform() {
        
        if self.Textfields.text?.count == 0 { // name (Required)
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Please enter restaurant name"
            self.present(self.vcFor!, animated: true, completion: nil)
        }
        else
            if (self.Textfields.text?.count)! < 2 { // name (Required)
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Restaurant name should contain at least two characters"
            self.present(self.vcFor!, animated: true, completion: nil)
        }
        else if self.Textfields2.text != "" && !isValidEmail(testStr: self.Textfields2.text!) {
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Please enter valid twitter id"
            self.present(self.vcFor!, animated: true, completion: nil)
        }
        else if self.Textfields3.text != "" && !isValidEmail(testStr: self.Textfields3.text!) {
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Please enter valid facebook id"
            self.present(self.vcFor!, animated: true, completion: nil)
        }
        else if self.Textfields4.text != "" && (self.Textfields4?.text?.count)! > 14 {
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "please enter maximum 14 digit phone number"
            self.present(self.vcFor!, animated: true, completion: nil)
        }
        else if self.Textfields4.text != "" && (self.Textfields4?.text?.count)! < 8 {
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  "please enter minimum 8 digit phone no"
            self.present(self.vcFor!, animated: true, completion: nil)
        }
        else {
//            self.startSpinner()
            if Reachability.isConnectedToNetwork() {
                self.startSpinner()
               recommendedWebservice()
            }
            else {
           //     self.stopSpinner()
                DispatchQueue.main.async {
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  Internet_Connection_Alert
                self.present(self.vcFor!, animated: true, completion: nil)
                
                }
            }
            
        }
        
    }
    
    func recommendedWebservice()
    {
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        let headers = ["Content-Type":"Application/json"]
        let _url = recommanded_merchant
        
        var emailId = getValueForKey(keyValue: "kemail")
        var name = getValueForKey(keyValue: "first_name")
        
        let parameters: Parameters = ["account_id" : (data1[0].accId!) ,
                                      "merchant_name" :Textfields.text!  ,
                                      "address" : Textfields1.text! ,
                                      "twitter_id" : Textfields2.text!,
                                      "facebook_id" :Textfields3.text!,
                                      "mobile_no" :Textfields4.text!]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            //                                            var str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            //
                                            let msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            //showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                            
                                        }
                                        else {
                                            
                                            self.stopSpinner()
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            let str = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = "home"
                                            self.vcFor?.titleFor =  str!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }
                                    }
                                }
                            }
                            else if response.result.isFailure {
                                self.stopSpinner()
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor = (response.result.error?.localizedDescription)!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                            }
                            
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        Textfields.resignFirstResponder()
        Textfields1.resignFirstResponder()
        Textfields2.resignFirstResponder()
        Textfields3.resignFirstResponder()
        Textfields4.resignFirstResponder()
        txtAddress.resignFirstResponder()
        
        return true
    }
    
    fileprivate func Placeholderextention() {
        
        var placeHolder = NSMutableAttributedString()
        var placeHolder1 = NSMutableAttributedString()
        var placeHolder2 = NSMutableAttributedString()
        var placeHolder3 = NSMutableAttributedString()
        var placeHolder4 = NSMutableAttributedString()
        
        let Name   = "Restaurant name (required)"
        let Name1  = " Address (optional)"
        let Name2  = "Twitter id  (optional)"
        let Name3  = "Facebook id  (optional)"
        let Name4  = "Phone number  (optional)"
        
        // Set the Font
        placeHolder = NSMutableAttributedString(string:Name, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 17.0)!])
        placeHolder1 = NSMutableAttributedString(string:Name1, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 17.0)!])
        placeHolder2 = NSMutableAttributedString(string:Name2, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 17.0)!])
        placeHolder3 = NSMutableAttributedString(string:Name3, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 17.0)!])
        placeHolder4 = NSMutableAttributedString(string:Name4, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 17.0)!])
        
        // Set the color
        placeHolder.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range:NSRange(location:0,length:Name.count))
        placeHolder1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range:NSRange(location:0,length:Name1.count))
        placeHolder2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range:NSRange(location:0,length:Name2.count))
        placeHolder3.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range:NSRange(location:0,length:Name3.count))
        placeHolder4.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range:NSRange(location:0,length:Name4.count))
        
        // Add attribute
        Textfields.attributedPlaceholder = placeHolder
        Textfields1.attributedPlaceholder = placeHolder1
        Textfields2.attributedPlaceholder = placeHolder2
        Textfields3.attributedPlaceholder = placeHolder3
        Textfields4.attributedPlaceholder = placeHolder4
    }
    
   
    
}

extension RecommendedViewController: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {

        if self.txtAddress.text.count == 0 {
            self.lblAddressPlaceholder.isHidden = false
        }
        else {
            self.lblAddressPlaceholder.isHidden = true
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

         self.lblAddressPlaceholder.isHidden = true
        if self.txtAddress.text.count == 0 {
            self.lblAddressPlaceholder.isHidden = false
        }
        else {
            self.lblAddressPlaceholder.isHidden = true
        }

        return textView.text.count + (text.count - range.length) <= 250
    }

    func textViewDidChange(_ textView: UITextView) {
        if textView == self.txtAddress {

            if self.txtAddress.text.count < 250 {
                print(self.txtAddress.text.count)
                let sizeToFitIn = CGSize(width: self.txtAddress.bounds.size.width, height: CGFloat(MAXFLOAT))
                let newSize = self.txtAddress.sizeThatFits(sizeToFitIn)
                self.txtAddressHeightConstant.constant = newSize.height
            }
        }
    }

    func textFieldDidBeginEditing(textField: UITextField) {
       self.txtAddress.toolbarPlaceholder = ""
//        if self.txtAddress.text == "" {
//            self.lblAddressPlaceholder.isHidden = false
//        }
//        if txtAddress.isFirstResponder == true {
//            txtAddress.toolbarPlaceholder = nil
//        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.txtAddress.text == "" {
            self.lblAddressPlaceholder.isHidden = false
        }
        else {
            self.lblAddressPlaceholder.isHidden = true
        }
    }

}


//
//  productDetailViewController.swift
//  Ani
//
//  Created by RSL-01 on 16/04/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SDWebImage
//import MXParallaxHeader
class productDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var lblingrident: UILabel!
    var productStatusCheckDetail = ""
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var scroller: UIScrollView!
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailMarchantName: UILabel!
    
    @IBOutlet weak var ViewAllergens: UIView!
    @IBOutlet weak var allergensTableView: UITableView!
    @IBOutlet weak var allergensTableViewHeightConstant: NSLayoutConstraint! // 150
    @IBOutlet weak var lblNoAllergens: UILabel!
    
    @IBOutlet weak var viewNutrition: UIView!
    @IBOutlet weak var nutritionTableView: UITableView!
    @IBOutlet weak var nutritionTableViewHeightConstant: NSLayoutConstraint! // 150
    @IBOutlet weak var lblNoNutritions: UILabel!
    
    @IBOutlet weak var lblNutritionTitle: UILabel!
    @IBOutlet weak var ViewNutritionTopConstant: NSLayoutConstraint! //16
    @IBOutlet weak var viewNutritionBottomConstant: NSLayoutConstraint!//16
    
    @IBOutlet weak var lblNutritionTitleTopConstant: NSLayoutConstraint!//0
    @IBOutlet weak var lblNoNutritionBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var lblNoNutritionTopConstant: NSLayoutConstraint!
    @IBOutlet weak var noNutritionHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var nutritionTableViewTopConstant: NSLayoutConstraint!
    
    @IBOutlet weak var nutritionTableViewBottomConstant: NSLayoutConstraint!
    
    @IBOutlet weak var viewIngrident: UIView!
    @IBOutlet weak var ingridiantTableView: UITableView!
    @IBOutlet weak var ingridiantTableViewHeightConstant: NSLayoutConstraint! // 150
    @IBOutlet weak var lblNoIngridiants: UILabel!
    
    @IBOutlet weak var viewDiscription: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    let cellSpacingHeight: CGFloat = 10
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    var buffer = ProductListModelClass()
    
    @IBOutlet weak var ingredientsHeightContraint: NSLayoutConstraint!
    //    var product = [appdelegate.]()
    var menu_item_image : String!
    
    var menu_item_name : String!
    
    var issearch = ""
    
    private var headerContainerView: UIView!
    private var headerImageView: UIImageView!
    
    var alliangenceArray = [allergentModelClass]()
    var nutricenArray = [allergentModelClass]()
    var ingridentArray = [allergentModelClass]()
    
    var newNutritianalArray = [ nutritianArrayObject]()
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
  
    override func viewDidDisappear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        self.navigationController?.navigationBar.isHidden = true
//        self.navigationController?.isNavigationBarHidden = true
        
        print("ProductDetailViewController")
        
        print("appdelegate.allergentArray.count = \(appdelegate.allergentArray.count)")
        
//        if issearch == "1"
//        {
//            self.btnBackOutlet.isHidden = true
//        }
//        else if issearch == "0"
//        {
//            self.btnBackOutlet.isHidden = true
//        }
//        else
//        {
//            self.btnBackOutlet.isHidden = false
//        }
        
        
        //
        //       if appdelegate.nutritianArray.count == 0
        //       {
        //        self.viewNutrition.removeFromSuperview()
        //        self.viewNutrition.isHidden = true
        //
        //        }else
        //        if appdelegate.ingredientsArray.count == 0
        //        {
        //             self.viewIngrident.removeFromSuperview()
        ////            self.viewIngrident.isHidden = true
        //
        //        }
        createGradientLayer(view: self.gradientView, width: self.gradientView.frame.size.width, height: self.gradientView.frame.size.height)
        for i in appdelegate.nutritianArray
        {
            
            if i.name == "Energ_KJ"
            {
                newNutritianalArray.append(i)
                
            }else
                
                if i.name == "Energ_Kcal"
                {
                    
                    newNutritianalArray.append(i)
                }else
                    
                    if i.name == "FA_Sat"  //  Key changed from Lipid_Tot
                    {
                        newNutritianalArray.append(i)
                        
                    }else
                        
                        if i.name == "Lipid_Tot"  // Key changed from FA_Sat
                        {
                            
                            newNutritianalArray.append(i)
                        }else
                            
                            if i.name == "Carbohydrt"
                            {
                                newNutritianalArray.append(i)
                                
                            }else
                                
                                if i.name == "Sugar_Tot"
                                {
                                    newNutritianalArray.append(i)
                                    
                                    
                                }else
                                    
                                    if i.name == "Sugar_Added"
                                    {
                                        newNutritianalArray.append(i)
                                    }
                                    else
                                        
                                        if i.name == "Protein"
                                        {
                                            
                                            newNutritianalArray.append(i)
                                            
                                        }else
                                            
                                            if i.name == "Salt"
                                            {
                                                newNutritianalArray.append(i)
                                                
            }
            
            if newNutritianalArray.count == 9
            {
                break
            }
            
        }
        appdelegate.nutritianArray = newNutritianalArray
        
        
        //ingridiantTableView.estimatedRowHeight = 200
        //ingridiantTableView.rowHeight = UITableViewAutomaticDimension
        
        //                if appdelegate.ingredientsArray.count == 1
        //                {
        //        ingredientsHeightContraint = 50 as? NSLayoutConstraint
        //                }
        //                else
        //
        //                {
        //                ingredientsHeightContraint = 250 as? NSLayoutConstraint
        //
        //                }
        //
        
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        detailMarchantName.text = appdelegate.MenuName
        self.navigationController?.isNavigationBarHidden = false
        
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
//        self.navigationController?.view.backgroundColor = .clear
        
        
        //allergensTableView.tableFooterView = UIView()
        
        lblDescription.text = buffer.menu_item_description
        if buffer.ingredient_deceleration_info == "" {
            
            lblingrident.text = "No Ingredients Listed in this Dish"
            lblingrident.textAlignment = .center
        }
        else
        {
            
            if buffer.ingredient_deceleration_info == nil
            {
                lblingrident.text = "Ingredients not available"
                lblDescription.text = "Description not available"
                lblDescription.textAlignment = .center
                lblingrident.textAlignment = .center
            }
            else
            {
                let MyString = "<p style = 'font-family:Roboto-Regular;font-size:17px;'>" + buffer.ingredient_deceleration_info + "</p>"
                
                lblingrident.attributedText = MyString.htmlToAttributedString
            }
      

        }
        
//        lblingrident.text = b
      
        //          cell.name_ingridiant_lbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDescription.sizeToFit()
        lblDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        //scroller.contentSize = CGSize(width: 400, height: 2300)
        let screenSize: CGRect = UIScreen.main.bounds
        
        let calculateHeight = (screenSize.width*600)/1024
        print(screenSize.width)
        print(calculateHeight)
        
        detailImageView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: calculateHeight)
//        detailImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//        detailImageView.clipsToBounds = true
//        detailImageView.contentMode = .scaleAspectFit
        
        if buffer.menu_item_image == nil
        {
            detailImageView.image = UIImage(named: "background_placeholder.png")
            
        }
        else
        {
            
            Alamofire.request(buffer.menu_item_image, method: .get).response { response in
                guard let image = UIImage(data:response.data!) else {
                    // Handle error
                    return
                }
                autoreleasepool
                {
                    let imageData = UIImageJPEGRepresentation(image,1.0)
                    self.detailImageView.image = UIImage(data : imageData!)
//                    self.detailImageView.contentMode = .scaleToFill
//                    self.detailImageView.image = UIImage(named: "PratikTest")
                }
            }
            
            
        }
        
        
        // Do any additional setup after loading the view.
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
//        self.lblNutritionTitle.text = "Nutritional Information per 100g :"
//        self.lblNoNutritions.text = "No Nutritional information available for this Dish"
        
        if productStatusCheckDetail != ""
        {
            self.view.makeToast(productStatusCheckDetail)
        }
        self.lblNoNutritions.sizeToFit()
    //    self.lblNoNutritions.layoutIfNeeded()
        
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
//        self.navigationItem.leftBarButtonItem  = .none
//        self.navigationItem.title = "123456789"
//        self.navi
//        self.navigationController?.isNavigationBarHidden = true
//         navigationController?.navigationBar.backgroundColor = UIColor(hex: "41b93d")
//        navigationController?.navigationBar.backgroundColor = UIColor.black
        self.lblNoAllergens.isHidden = true
        self.lblNoNutritions.isHidden = true
        self.lblNutritionTitle.isHidden = true
        nutritionTableView.reloadData()
        
//        self.lblNoIngridiants.isHidden = true
        
     //   Nutritional Information per 100g :
      
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        if appdelegate.allergentArray.count == 0 {
            self.lblNoAllergens.isHidden = false
        }
        else {
            self.lblNoAllergens.isHidden = true
        }
        
//        if appdelegate.nutritianArray.count == 0 {
//            self.lblNoNutritions.isHidden = false
//        }
//        else {
//            self.lblNoNutritions.isHidden = true
//        }
        
        //Check hide nutrition flag
        
        let hideFlag =  getValueForKey(keyValue: Khide_nutritional_info)
        print("hideFlag = \(hideFlag)")
        
        if hideFlag == "Yes" {
         //hide nutritionView
           self.ViewNutritionTopConstant.constant = 0
            self.lblNutritionTitle.text = ""
            self.lblNoNutritions.text = ""
            self.lblNoNutritions.isHidden = true
            self.lblNutritionTitle.isHidden = true
            
            //for tableView check numberofrowsInSection method
        }
        else{

            if appdelegate.nutritianArray.count == 0 {
                self.lblNutritionTitle.isHidden = false
                self.lblNoNutritions.isHidden = false
            }
            else {
                    self.lblNutritionTitle.isHidden = false
                    self.lblNoNutritions.isHidden = true
            }
    }
        

        ////////
        if appdelegate.ingredientsArray.count == 0 {
//            self.lblNoIngridiants.isHidden = false
        }
        else {
//            self.lblNoIngridiants.isHidden = true
        }
        
        self.allergensTableViewHeightConstant.constant = self.allergensTableView.contentSize.height
        self.nutritionTableViewHeightConstant.constant = self.nutritionTableView.contentSize.height
//        self.ingridiantTableViewHeightConstant.constant = self.ingridiantTableView.contentSize.height
        
    }
    
    @objc func backBtn(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == allergensTableView {
            return 50
        }
        else if tableView == nutritionTableView {
            return 35
        }
        else {
            return 52
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == allergensTableView {
            return 50
        }
        else if tableView == nutritionTableView {
            return 30
//            return UITableViewAutomaticDimension
        }
        else {
            //            if appdelegate.ingredientsArray[indexPath.row].name.count >= 100 {
            //                return UITableViewAutomaticDimension
            //            }
            //            else {
            //                return UITableViewAutomaticDimension
            //            }
            return UITableViewAutomaticDimension
        }
        
    }
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == allergensTableView {
            return appdelegate.allergentArray.count
        }
        else if tableView == nutritionTableView {
            let hideFlag =  getValueForKey(keyValue: Khide_nutritional_info)
                 print("hideFlag = \(hideFlag)")
                 
                 if hideFlag == "Yes" {
                    return 0
                }
                 else{
                     return appdelegate.nutritianArray.count
            }
        }
        else {
            return 0//appdelegate.ingredientsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == allergensTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! detailAllergensTableViewCell
            
            if appdelegate.allergentArray.count > 0{
                
                Alamofire.request(self.appdelegate.allergentArray[indexPath.row].allergan_Img, method: .get).response { response in
                              guard let image = UIImage(data:response.data!) else {
                                  // Handle error
                                  return
                              }
                              autoreleasepool {
                                  let imageData = UIImageJPEGRepresentation(image,1.0)
                                  cell.allergensImage.image = UIImage(data : imageData!)
                              }
                          }
                          
                          // cell.allergensImage.image =
                          
                          cell.allergensName.text = appdelegate.allergentArray[indexPath.row].allergensName
                          print("111: \(appdelegate.allergentArray.count)")
            }
            
            return cell
        }
        else if tableView == nutritionTableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NutritionTableViewCell
            
            print("appdelegate.nutritianArray[indexPath.row].name : \(appdelegate.nutritianArray[indexPath.row].name)")
            
            if appdelegate.nutritianArray[indexPath.row].name == "Energ_KJ"
            {
                cell.name_nutrition_lbl.text = "Energy"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Energ_Kcal" {
                cell.name_nutrition_lbl.text = "Energy"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "FA_Sat"  // Key changed from Lipid_Tot
            {
                cell.name_nutrition_lbl.text = "  " +  " " + "of which saturates"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Lipid_Tot"  // Key changed from FA_Sat
            {
                cell.name_nutrition_lbl.text = "Fat"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Carbohydrt" {
                cell.name_nutrition_lbl.text = "Carbohydrate"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Sugar_Tot" {
                cell.name_nutrition_lbl.text = " " + " " + "of which sugars"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Sugar_Added" {
                cell.name_nutrition_lbl.text = "Added Sugar"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Protein" {
                cell.name_nutrition_lbl.text = "Protein"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
                
            }
            else if appdelegate.nutritianArray[indexPath.row].name == "Salt" {
                cell.name_nutrition_lbl.text = "Salt"
                cell.quantity_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].quantity + appdelegate.nutritianArray[indexPath.row].unit
                cell.unit_nutritian_lbl.text = appdelegate.nutritianArray[indexPath.row].unit
            }
            
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ingridiantTableViewCell

            
            return cell

        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0//cellSpacingHeight
    }
    
    
    

    
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension UILabel {
    func set(html: String) {
        if let htmlData = html.data(using: .unicode) {
            do {
                self.attributedText = try NSAttributedString(data: htmlData,
                                                             options: [.documentType: NSAttributedString.DocumentType.html],
                                                             documentAttributes: nil)
            } catch let e as NSError {
                print("Couldn’t parse \(html): \(e.localizedDescription)")
            }
        }
    }
}

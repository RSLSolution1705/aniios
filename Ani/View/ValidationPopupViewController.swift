//
//  ValidationPopupViewController.swift
//  Ani
//
//  Created by Mac on 05/06/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit


protocol PopUpDoneDelegate: class {
    func PopUpOkClick(status: Bool)
}

class ValidationPopupViewController: UIViewController {
    
    weak var delegate: PopUpDoneDelegate?
    
    @IBOutlet weak var outerView: UIView!

    @IBOutlet weak var lblPopUp: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var titleFor = ""
    var actionFor = ""
    var isLogin = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    print("ValidationPopupViewController")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setView()
        
    }
    
    func setView() {
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.lblPopUp.text = titleFor
        
        self.outerView.layer.cornerRadius = 5.0
        self.outerView.clipsToBounds = true
        
        self.btnOk.layer.cornerRadius = self.btnOk.frame.size.height / 2.0
        self.btnOk.clipsToBounds = true
        
//        self.btnCancel.layer.cornerRadius = self.btnCancel.frame.size.height / 2.0
//        self.btnCancel.clipsToBounds = true
        
    }
    
    @IBAction func btnClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
     //   self.view.removeFromSuperview()
     //   self.removeFromParentViewController()
    }
    
    @IBAction func btnOK(_ sender: Any) {
        DispatchQueue.main.async {
            
        
        if self.actionFor == "home" {
            self.delegate?.PopUpOkClick(status: true)
        }
        else {
            self.delegate?.PopUpOkClick(status: false)
        }
        
       self.dismiss(animated: true, completion: nil)
         
        //    self.view.removeFromSuperview()
        //    self.removeFromParentViewController()
            
        }
    }
    


}

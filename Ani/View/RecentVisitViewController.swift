//
//  RecentVisitViewController.swift
//  Ani
//
//  Created by Mac on 22/07/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SDWebImage

var shopStatusCheck = ""

class RecentVisitViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, PopUpDoneDelegate {
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
    @IBOutlet weak var searchViewheight: NSLayoutConstraint!
    
  
    var searchActive = false
    @IBOutlet var staticPopUp: UIView!
    
    @IBOutlet weak var lblInactiveMerchand: UILabel!
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var tableview: UITableView!
    var vcFor : ValidationPopupViewController? = nil
    @IBOutlet weak var lblNoDataFound: UILabel!
    var viewActivityLarge : SHActivityView?
    var flagdidselect = 0
    var flag = 0
  
    var allergntArray1 = [productallergnsClass]()
    var nutritianArray1 = [nutritianArrayObject]()
    var ingredientsArray1 = [ingredientsArrayObject]()
    
    @IBOutlet weak var lblCrossContanimation: UILabel!
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var allergensDetail = [productallergnsClass]()
    var nutritianDetails = [nutritianArrayObject]()
    var ingredientsDetails = [ingredientsArrayObject]()
     var allergntArray = [allergentModelClass]()
    var recentVisitArray = [marchantModelClass]()
    var searchArray = [marchantModelClass]()
    var finalArray = [marchantModelClass]()
      var appdelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var tablviewtop: NSLayoutConstraint!
    
    
    
    
    
    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(FavouriteViewController.handleRefresh),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.black
            return refreshControl
    }()
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
         print("RecentVisitController")
        
        self.tableview.addSubview(self.refreshControl)

        
        self.lblNoDataFound.isHidden = true
        if recentVisitArray.count == nil
        {
            self.lblNoDataFound.isHidden = false
            
        }else
        {
            self.lblNoDataFound.isHidden = true
        }
  //      startSpinner()
        
        appdelegate.closePopUpFlag = 1
        self.searchBar.delegate = self
        self.searchViewheight.constant = 0
        self.searchView.clipsToBounds = true
        searchActive = false;
        blankView.isHidden = true
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        

    }

    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    
    
    @IBAction func staticBackButton(_ sender: Any)
    {
         staticPopUp.removeFromSuperview()
        blankView.isHidden = true
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    
    
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        //        self.startSpinner()
        ShowRecentVisit()
        tableview.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    
    @IBAction func staticOkButton(_ sender: Any)
    {
        blankView.isHidden = true
         staticPopUp.removeFromSuperview()
        var catgoryVC = storyboard?.instantiateViewController(withIdentifier: "CategaryListViewController")as?CategaryListViewController
        catgoryVC?.FinalMerID = merIDRecent
        self.navigationController?.pushViewController(catgoryVC!, animated: true)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.searchViewheight.constant = 0
        self.searchBar.text = ""
        lblInactiveMerchand.isHidden = true
        setView()
        startSpinner()
        ShowRecentVisit()
        tableview.tableFooterView = UIView()
    }
    
    
    func setView()
    {
//        appdelegate.favflag = 0
      if  appdelegate.categoryflag == 1
      {
         self.title = "Recent Visits"
       // self.ShowRecentVisit()
       }
        else if (appdelegate.recentflag == 2)
        {
            self.title = "Recently Viewed Restaurants"
           // self.ShowRecentVisit()
        }
        else
        {
                self.title = " Favourite Restaurants"
               // self.ShowRecentVisit()
        }
    
    
        if UIDevice.current.modelName == "iPhone 5c"
        {
//                    tablviewtop.constant = -60
                    //            titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 12.0).isActive = true
        }
       
        navigationController?.navigationBar.backgroundColor = UIColor(hex: "41b93d")
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
//        let btnRightbutton = UIButton(type: .custom)
//        btnRightbutton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
//        btnRightbutton.backgroundColor = UIColor.clear
//        btnRightbutton.addTarget(self, action: #selector(self.btnInfoTapped(_:)), for: .touchUpInside)
//        btnRightbutton.setImage(UIImage(named: "ic_information_outline_white_48dp.png"), for: .normal)
//        let barButton = UIBarButtonItem(customView: btnRightbutton)
        
        
        let btnright = UIButton(type: .custom)
        btnright.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnright.backgroundColor = UIColor.clear
        btnright.addTarget(self, action: #selector(self.btnSearchTapped(_:)), for: .touchUpInside)
        btnright.setImage(UIImage(named: "ic_search_white"), for: .normal)
        let sideMenubarButton = UIBarButtonItem(customView: btnright)


        self.navigationItem.rightBarButtonItems = [sideMenubarButton]
        
        
        
    }
 @IBAction func btnSearchTapped(_ sender: Any)
 {
    self.lblNoDataFound.isHidden = true
    if self.searchViewheight.constant == 0 {
        if UIDevice.current.modelName == "iPhone 5c"
        {
             self.searchViewheight.constant  = 56
            
            //            titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 12.0).isActive = true
            
        }
        else
        {
        self.searchViewheight.constant =   self.navigationController!.navigationBar.frame.size.height
        }
//            self.navigationController!.navigationBar.frame.size.height
        self.searchBar.becomeFirstResponder()
    }
    else
    {
         self.lblNoDataFound.isHidden = true
        self.recentVisitArray = self.finalArray
        tableview.reloadData()
        self.searchViewheight.constant = 0
        _ = self.searchBar.text = ""
    _ = self.searchBar.resignFirstResponder()
    }
    
    
    }
    
    @objc func backBtn(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func ShowRecentVisit()
    {
        
        let headers = ["Content-Type":"Application/json"]
        var url = ""
        if  appdelegate.categoryflag == 1
        {
            url = get_scanned_partner
        }
        else if (appdelegate.recentflag == 2)
        {
            
             url = partner_lists_by_recentview
        }
        else
        {
             url = partner_lists_by_favourite
        }

        
//        let _url = get_scanned_partner
        var MetaString =  getValueForKey(keyValue: kmetaString)
        print(MetaString)
       //http://99.81.230.36/ANI_TEST/ws/get_scanned_partner.php?account_id=677
        
        let newMetaString = MetaString.replacingOccurrences(of: "ANI-", with: "")
        print("newMetaString :\(newMetaString)")
        setDefaultValue(keyValue: knewMetaString, valueIs: newMetaString)
        
        let parameters: Parameters = ["account_id": getValueForKey(keyValue: kregisterAccountId)]
        
        print(parameters)
        
        
        Alamofire.request(url,method: .post,parameters: parameters,encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            
            print("Request  \(response.request)")
            
            print("RESPONSE \(response.result.value)")
            print("RESPONSE \(response.result)")
            print("RESPONSE \(response)")
            
            if response.result.isSuccess {
                print("SUKCES with \(response)")
                
                if (response.result != nil)
                {
                    //         SVProgressHUD.dismiss()
                    self.stopSpinner()
//                    self.blankView.isHidden = true
                    let jsonDic = response.result.value as! NSDictionary
                    print(jsonDic)
                    if let res = jsonDic.value(forKey: "result") as? String
                    {
                        if res == "failed"
                        {
                            //   SVProgressHUD.dismiss()
                            self.stopSpinner()

                            let jsonDic = response.result.value as! NSDictionary
                            print(jsonDic)
                            let msg = jsonDic.object(forKey: "msg")as? String
                          
                            //                                            setDefaultValue(keyValue: kmsg, valueIs: msg!)
                            
                            DispatchQueue.main.async {
//                             self.ShowRecentVisit()
                            
                                self.finalArray.removeAll()
                                self.recentVisitArray.removeAll()
                           
                           self.tableview.reloadData()
                            
                            self.vcFor?.modalTransitionStyle = .crossDissolve
                            self.vcFor?.modalPresentationStyle = .overFullScreen
                            self.vcFor?.isLogin = true
                            self.vcFor?.actionFor = ""
                            self.vcFor?.titleFor =  msg!
                            self.present(self.vcFor!, animated: true, completion: nil)
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            
                            let jsonDic = response.result.value as! NSDictionary
                            print(jsonDic)
                            self.stopSpinner()
//                            self.blankView.isHidden = true
                            let resultarray = jsonDic.object(forKey: "data")as! NSArray
                           self.finalArray.removeAll()
                            self.recentVisitArray.removeAll()
                            for i in resultarray
                            {
                                 let temp = marchantModelClass()
                                let tempobj = i as! NSDictionary
                                let check = tempobj.value(forKey: "shop_open") as! String
                                if check == "true"
                                {
                                    shopStatusCheck = "0"
                                    let shopStatus = temp.shop_open
                                    UserDefaults.standard.setValue(shopStatus, forKey: "offlineMSG")
                                    UserDefaults.standard.synchronize()
                                }
                                else
                                {
                                    shopStatusCheck = "1"
                                    UserDefaults.standard.setValue(nil, forKey: "offlineMSG")
                                    UserDefaults.standard.synchronize()
                                }
                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String ?? ""
                                temp.merchant_id = tempobj.object(forKey: "merchant_id") as? String ?? ""
                                //                                    self.merchantId = temp.merchant_id
                                self.appdelegate.merchant_id = temp.merchant_id
                                setDefaultValue(keyValue: kmarchentId, valueIs: temp.merchant_id)
                                print("merchant_id123 = \(String(describing: temp.merchant_id))")
                                temp.address = tempobj.object(forKey: "address") as? String ?? ""
                                setDefaultValue(keyValue: KmarchentAddress, valueIs:  temp.address)
                                
                                
                                let keyExists = tempobj.object(forKey: "hide_nutritional_info") as? String != nil
                                                                                 
                                        if keyExists {
                                                                                 
                                            temp.hide_nutritional_info = tempobj.object(forKey: "hide_nutritional_info") as? String
                                          
                                            print("hide_nutritional_info = \(temp.hide_nutritional_info ?? "")")
                                                                            
                                 }
                                
                                
                                temp.merchant_name = (tempobj.object(forKey: "merchant_name")as! String ?? "")
                              
//                                self.marchantName = temp.merchant_name
                                temp.avgPreparationTime = tempobj.object(forKey: "avgPreparationTime") as? String ?? ""
                                temp.closing_time = tempobj.object(forKey: "closing_time") as? String ?? ""
                                temp.opening_time = tempobj.object(forKey: "opening_time") as? String ?? ""
                                temp.latitude = tempobj.object(forKey: "latitude") as? String ?? ""
                                temp.longitude = tempobj.object(forKey: "longitude") as? String ?? ""
                                temp.email = tempobj.object(forKey: "email") as? String ?? ""
                                temp.delivery_charges = tempobj.object(forKey: "delivery_charges") as? String ?? ""
                                
                                temp.distance = tempobj.object(forKey: "distance") as? String ?? ""
                                temp.delivery_time = tempobj.object(forKey: "delivery_time") as? String ?? ""
                                temp.delivery_type = tempobj.object(forKey: "delivery_type") as? String ?? ""
                                temp.flag = tempobj.object(forKey: "flag") as? String
                                temp.food_type = tempobj.object(forKey: "food_type") as? String ?? ""
                                temp.shop_open = tempobj.object(forKey: "shop_open") as? String ?? ""
                                temp.percentage = tempobj.object(forKey: "percentage") as? String ?? ""
                                temp.mobile_number = tempobj.object(forKey: "mobile_number") as? String ?? ""
                                temp.mininum_order = tempobj.object(forKey: "mininum_order") as? String ?? ""
                                temp.merchant_tagline = tempobj.object(forKey: "merchant_tagline") as? String ?? ""
                                temp.merchant_name = tempobj.object(forKey: "merchant_name") as? String ?? ""
                                setDefaultValue(keyValue: kmarchentName, valueIs:  temp.merchant_name)
                                temp.home_number = tempobj.object(forKey: "home_number") as? String ?? ""
                                temp.merchant_image = tempobj.object(forKey: "merchant_image") as? String ?? ""
                                temp.contamination_msg = tempobj.object(forKey: "cross_contamination")as? String ?? ""
                                
                                temp.partner_id = tempobj.object(forKey: "partner_id")as? String ?? ""
                                
                                //setDefaultValue(keyValue: KcontaminationMsg, valueIs: contaminationMsg)
                                let allergentArray = tempobj.object(forKey: "allergans")as! NSArray
                                
                                for item in allergentArray
                                {
                                    let tempitem = item as! NSDictionary
                                    let tempobjItem = allergentModelClass()
                                    tempobjItem.allerganName = tempitem.object(forKey: "allergan")as? String
                                    tempobjItem.allergan_id = tempitem.object(forKey: "allergan_id")as? String
                                    
                                    tempobjItem.isSelected = false
                                    
                                    self.allergntArray.append(tempobjItem)
                                    //                                        print("ashu : \(allergentArray)")
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                //                                            SVProgressHUD.dismiss()
                                self.stopSpinner()
                                
                                self.recentVisitArray.append(temp)
                                self.finalArray.append(temp)
                                
                            }
                             DispatchQueue.main.async
                            {
                            self.tableview.reloadData()
                            }
                            
                        }
                    }
                }
            }
            else if response.result.isFailure
            {
                
                self.stopSpinner()
                
                
                DispatchQueue.main.async
                {
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                
         
                var str = response.result.error?.localizedDescription
                print(response.result.error?.localizedDescription)
                
                if str ==
                    "The Internet connection appears to be offline."
                {
                    
                    str = "You are offline please check your internet connection"
                    
                    
                }
             
                
            }
            
        }
        
        
    }
   
    
    //MARK : TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return self.recentVisitArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //var cellToReturn = UITableViewCell()
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecentVisitTableViewCell
         cell.SubMenuimageView.image = UIImage(named: "background_placeholder.png")
//        var cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RecentVisitTableViewCell
         cell.selectionStyle = .none
        var temp1 = self.recentVisitArray[indexPath.row]
        if temp1.shop_open == "true"
        {
            cell.lblimenuName.alpha = 1
            cell.SubMenuimageView.alpha = 1
        }
        else
        {
            cell.lblimenuName.alpha = 0.25
            cell.SubMenuimageView.alpha = 0.25
        }
        
            let imgUrl = temp1.merchant_image
            let screenSize: CGRect = UIScreen.main.bounds
            let calculateHeight = (screenSize.width*600)/1024
            print(screenSize.width)
            print(calculateHeight)
            cell.SubMenuimageView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: calculateHeight)
        
       
        let urlString = imgUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
       let url = URL(string: urlString!)
         cell.lblimenuName.text  = temp1.merchant_name
        
//        cell.SubMenuimageView!.sd_setImage(with: imgUrl!, completed: nil)
//        cell.SubMenuimageView!.sd_setImage(with: imgUrl!, completed: nil)
        
        cell.SubMenuimageView.sd_setImage(with: url)
        
//        Alamofire.request(urlString!, method: .get).response { response in
//            guard let image = UIImage(data:response.data!) else {
//                // Handle error
//                autoreleasepool
//                {
//                     DispatchQueue.main.async
//                    {
//
//                    cell.SubMenuimageView.image = UIImage(named: "background_placeholder.png")
//                    }
//                }
//                return
//            }
//            autoreleasepool
//            {
//                DispatchQueue.main.async
//                {
//                let imageData = UIImageJPEGRepresentation(image,1.0)
//                cell.SubMenuimageView.image = UIImage(data : imageData!)
//                }
//            }
//        }

        
        return cell
    }
    var merIDRecent = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            var temp1 = recentVisitArray[indexPath.row]
//        UserDefaults.standard.setValue(temp1.merchant_id, forKey: "merchandID")
            merIDRecent = temp1.merchant_id
        
        //////////
        
        var parentID = temp1.partner_id
                if parentID == nil || parentID == "0"{
                    parentID = ""
                }
        UserDefaults.standard.setValue("", forKey: "fromParent")
        UserDefaults.standard.setValue(parentID, forKey: "parentID")
        UserDefaults.standard.synchronize()
                                                                                                         
        let checkStoredParentID = UserDefaults.standard.value(forKey: "parentID") as! String
        print(checkStoredParentID)
        
        ///////
        
            let CategaryListViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategaryListViewController") as! CategaryListViewController
            CategaryListViewController.FinalMerID = merIDRecent
        
        
        var statusShop = temp1.shop_open
//        if statusShop == true
//        {
//
//        }
        UserDefaults.standard.setValue(temp1.shop_open, forKey: "shop_open")
//        UserDefaults.standard.setValue(temp1.contamination_msg, forKey: "shop_open")
        UserDefaults.standard.synchronize()
        print(UserDefaults.standard.value(forKey: "shop_open"))
        setDefaultValue(keyValue: marchentName, valueIs: temp1.merchant_name)
        setDefaultValue(keyValue: KAddress, valueIs: temp1.address)
        setDefaultValue(keyValue: KkEmail, valueIs: temp1.email)
        setDefaultValue(keyValue: KMobile, valueIs: temp1.mobile_number)
        setDefaultValue(keyValue: ktagline, valueIs: temp1.merchant_tagline)
        setDefaultValue(keyValue: Klatitude, valueIs: temp1.latitude)
        setDefaultValue(keyValue: Klongitude, valueIs: temp1.longitude)
        setDefaultValue(keyValue: kMerchantImage, valueIs: temp1.merchant_image)
        
        setDefaultValue(keyValue: Khide_nutritional_info, valueIs:  temp1.hide_nutritional_info)
        print("hide_nutritional_info = \(temp1.hide_nutritional_info ?? "")")
        
        
        //var urlString = temp1.merchant_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        //appdelegate.MarImage =  UIImage(url: URL(string: urlString ?? ""))
          self.dismissKeyboard()
        if  appdelegate.categoryflag == 1
        {

            if getValueForKey(keyValue: KcontaminationMsg) != nil
            {
                let gp_item_id : String =  temp1.merchant_id
                  setDefaultValue(keyValue: knewMetaString, valueIs: temp1.merchant_id)
                  setDefaultValue(keyValue: kmenu_item_group_id, valueIs: temp1.merchant_id)
                
                
//                var catgoryVC = storyboard?.instantiateViewController(withIdentifier: "CategaryListViewController")as?CategaryListViewController
//                catgoryVC?.merchantId = temp1.merchant_id
                
                
                print("gp_item_id: \(gp_item_id)")
                _ = temp1.merchant_name
            let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
            let viewHeight = self.view.frame.size.height
            let popupHeight = self.staticPopUp.frame.size.height
            blankView.isHidden = false
                  lblCrossContanimation.text = temp1.contamination_msg
            self.staticPopUp.layer.cornerRadius = 10
            self.staticPopUp.clipsToBounds = true
                if lblCrossContanimation.text!.count > 200
                {
                     self.staticPopUp.frame = CGRect(x: 0, y: 0, width: 290, height: 300)
                }
                else
                {
                      self.staticPopUp.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
                }
            self.staticPopUp.center = self.view.center
               
                DispatchQueue.main.async
                {
                    if UserDefaults.standard.value(forKey: "shop_open") as! String == "true"
                    {
                        self.view.addSubview(self.staticPopUp)
                    }
                    else
                    {
//                        var offlineMSG = "This ANI Partner Location is unavailable at the moment please try again later."
                        var offlineMSG = "This ANI Partner Menu is unavailable at the moment please check with your server"
//                        self.view.makeToast(offlineMSG)
                        self.vcFor?.modalTransitionStyle = .crossDissolve
                        self.vcFor?.modalPresentationStyle = .overFullScreen
                        self.vcFor?.isLogin = true
                        self.vcFor?.actionFor = ""
                        self.vcFor?.titleFor =  offlineMSG
                        self.present(self.vcFor!, animated: true, completion: nil)
                        self.blankView.isHidden = true
                    }
                    
                }
                
            }
            else
            {
                let gp_item_id : String =  temp1.merchant_id
                
                setDefaultValue(keyValue: kmenu_item_group_id, valueIs: gp_item_id)
                  setDefaultValue(keyValue: knewMetaString, valueIs: temp1.merchant_id)
                print("gp_item_id: \(gp_item_id)")
                _ = temp1.merchant_name
                
                var catgoryVC = storyboard?.instantiateViewController(withIdentifier: "CategaryListViewController")as?CategaryListViewController
                catgoryVC?.merchantId = temp1.merchant_id
                self.navigationController?.pushViewController(catgoryVC!, animated: true)
            }

            
        }
        else if (appdelegate.recentflag == 2)
        {
            
            self.staticPopUp.center = self.view.center
            
            DispatchQueue.main.async {
                if UserDefaults.standard.value(forKey: "shop_open") as! String == "true"
                {
//                    self.view.addSubview(self.staticPopUp)
                    let gp_item_id : String =  temp1.merchant_id
                    
                    setDefaultValue(keyValue: kmenu_item_group_id, valueIs: gp_item_id)
                    self.appdelegate.merchant_id = temp1.merchant_id
                    print("gp_item_id: \(gp_item_id)")
                    _ = temp1.merchant_name
                    setDefaultValue(keyValue: kmarchentId, valueIs: temp1.merchant_id)
                    
                    
                    let recent = self.storyboard?.instantiateViewController(withIdentifier: "RecentViewController")as! RecentViewController
                    self.navigationController?.pushViewController(recent, animated: true)
                }
                else
                {
                    //                        var offlineMSG = "This ANI Partner Location is unavailable at the moment please try again later."
                    var offlineMSG = "This ANI Partner Menu is unavailable at the moment please check with your server"
//                    self.view.makeToast(offlineMSG)
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  offlineMSG
                    self.present(self.vcFor!, animated: true, completion: nil)
                    self.blankView.isHidden = true
                }
                
            }
            
          
            
        }
        else
            
        {
            
            DispatchQueue.main.async
            {
                if UserDefaults.standard.value(forKey: "shop_open") as! String == "true"
                {
//                    self.view.addSubview(self.staticPopUp)
                    
                    let gp_item_id : String =  temp1.merchant_id
                    
                    setDefaultValue(keyValue: kmenu_item_group_id, valueIs: gp_item_id)
                    self.appdelegate.merchant_id = temp1.merchant_id
                    print("gp_item_id: \(gp_item_id)")
                    _ = temp1.merchant_name
                    
                    setDefaultValue(keyValue: kmarchentId, valueIs: temp1.merchant_id)
                    
                    let fav = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteViewController")as! FavouriteViewController
                    self.navigationController?.pushViewController(fav, animated: true)
                }
                else
                {
                    //                        var offlineMSG = "This ANI Partner Location is unavailable at the moment please try again later."
                    var offlineMSG = "This ANI Partner Menu is unavailable at the moment please check with your server"
//                    self.view.makeToast(offlineMSG)
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  offlineMSG
                    self.present(self.vcFor!, animated: true, completion: nil)
                    self.blankView.isHidden = true
                }
                
            }
           
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        if (tableView != tableview) {
            //            let imgUrl = temp1.menu_item_image
            let screenSize: CGRect = UIScreen.main.bounds
            let screenScale = UIScreen.main.scale
            let calculateHeight = ((screenSize.width*600)/1024)
            print(screenSize.width)
            print(calculateHeight)
            return calculateHeight
//        }
//        else {
//            return 160
//        }
    }
}
extension RecentVisitViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.getSearchResult(str: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print(self.searchBar.text)
        self.getSearchResult(str: self.searchBar.text ?? "")
        self.dismissKeyboard()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchViewheight.constant = 0
        self.searchBar.showsCancelButton = true
         self.lblNoDataFound.isHidden = true
        self.searchBar.text = ""
        self.recentVisitArray = self.finalArray
        self.tableview.reloadData()
          self.dismissKeyboard()
//        self.viewDidLayoutSubviews()
        
    }
    
    func getSearchResult(str: String) {
        
        if str == "" {
            self.recentVisitArray = self.finalArray
               DispatchQueue.main.async {
            self.tableview.reloadData()
            }
        }
        else {
            self.recentVisitArray.removeAll()
            // Commented by Pratik for removing case sensitivity to the filter
            
//           self.recentVisitArray = self.finalArray.filter ({
//                $0.merchant_name.contains(str)
//            })
            
            // Done
            
            self.self.recentVisitArray = self.finalArray.filter {
                $0.merchant_name.localizedCaseInsensitiveContains(str) }
            
            
            DispatchQueue.main.async {
                  self.tableview.reloadData()
            }
          
        }
        if (self.recentVisitArray.count == 0){
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
//        self.viewDidLayoutSubviews()
        
    }
    
}

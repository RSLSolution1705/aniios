//
//  MasterScanViewController.swift
//  Ani
//
//  Created by Apple on 21/04/20.
//  Copyright © 2020 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import CoreData
import SVProgressHUD
import Floaty
import SDWebImage


class MasterScanViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PopUpDoneDelegate
{

    @IBOutlet var staticPopUp: UIView!
    var marchantName = ""
    
    @IBOutlet var btncontinue: UIButton!
    @IBOutlet var lblMarchentName: UILabel!
    @IBOutlet var marchantImage: UIImageView!
    @IBOutlet var MarchantName: UILabel!
    
    @IBOutlet var btnshare: UIButton!
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var lblContamination: UILabel!
    
    
    var childDataArray = [ProductListModelClass]()
    var localArray = [ProductListModelClass]()
    
    @IBOutlet weak var lblInactiveProduct: UILabel!
    var Id = ""
    @IBOutlet weak var searchView: UISearchBar!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var searchViewheight: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    var deleteId = ""
    let cellSpacingHeight: CGFloat = 10
    let context = AppDelegate().persistentContainer.viewContext
    var newAllergentIdArray = [String]()
    var productSubMenuArray = [ProductListModelClass]()
    var account_id = 0
    var passdata = [productallergnsClass]()
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    //    var isSelected = false
    var tableposition = 0
    var allergens_id_collectionview = ""
    var buffer = [allergentModelClass]()
    var isFavouriteRes = false
    var finalArray = [ProductListModelClass]()
    
//    var finalArray : NSMutableArray
    
    var allergntArray = [allergentModelClass]()
    var marchantArray = [marchantModelClass]()
    var accountID =  loginClass()
    var allergntArray1 = [productallergnsClass]()
    var allergensDetail = [productallergnsClass]()
    var nutritianDetails = [nutritianArrayObject]()
    var ingredientsDetails = [ingredientsArrayObject]()
    var merchantId = ""
    var ProductMenuArray = [ productMenuListModelClass]()
    var menu_item_id_fav = ""
    var recentMenuId = ""
    var flagfav = ""
    var nutritianArray1 = [nutritianArrayObject]()
    var ingredientsArray1 = [ingredientsArrayObject]()
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var tableview: UITableView!
     var vcFor : ValidationPopupViewController? = nil
//    var refreshControl = UIRefreshControl()    //  For pull to refresh functionality
    
    
    
    
    lazy var refreshControl: UIRefreshControl =
    {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(FavouriteViewController.handleRefresh),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        
        return refreshControl
    }()
    
    
    
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Select Sub-Partner"
        self.lblNoDataFound.isHidden = true
        self.lblInactiveProduct.isHidden = true
        popupView.isHidden = true
        staticPopUp.isHidden = true
        
        self.localArray = self.childDataArray
        
         print("MasterScanViewController")
        
        /////check subpartners
        if self.localArray.count == 0
        {
             self.lblNoDataFound.text = "No Sub Partners found"
             self.lblNoDataFound.isHidden = false
             self.tableview.isHidden = true
        }
        else{
             self.lblNoDataFound.isHidden = true
             self.tableview.isHidden = false
        }
        
        ///////
        
        self.addDoneButtonOnKeyboard()
//        tableview.indicatorStyle = UIScrollViewIndicatorStyle.defa
        
        // for pull to refresh functionality
        self.tableview.addSubview(self.refreshControl)
        
//        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
//        tableview.addSubview(refreshControl)
       
        
        //   Done
        
        self.searchView.delegate = self
        self.searchViewheight.constant = 0
        self.searchView.clipsToBounds = true
      
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
//        let btnRightbutton = UIButton(type: .custom)
//        btnRightbutton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
//        btnRightbutton.backgroundColor = UIColor.clear
//        btnRightbutton.addTarget(self, action: #selector(infobutton(sender:)), for: .touchUpInside)
//        btnRightbutton.setImage(UIImage(named: "ic_information_outline_white_48dp.png"), for: .normal)
//        let barButton = UIBarButtonItem(customView: btnRightbutton)
        
        let btnright = UIButton(type: .custom)
        btnright.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        btnright.backgroundColor = UIColor.clear
        btnright.addTarget(self, action: #selector(self.btnSearchTapped(_:)), for: .touchUpInside)
        btnright.setImage(UIImage(named: "ic_search_white"), for: .normal)
        let sideMenubarButton = UIBarButtonItem(customView: btnright)
        self.navigationItem.rightBarButtonItems = [sideMenubarButton]
        
        tableview.tableFooterView = UIView()
        if Reachability.isConnectedToNetwork()
        {
            self.startSpinner()
            print(childDataArray)
//            favouriteProduct()
            tableview.reloadData()
        }
        else
        {
            DispatchQueue.main.async
            {
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  Internet_Connection_Alert
            self.present(self.vcFor!, animated: true, completion: nil)
            }
        }
    }

    
        @IBAction func okButtonstaticPopup(_ sender: Any) {
            self.popupView.isHidden = false
             self.popupView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            UIView.animate(withDuration: 2.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                // HERE
    //                                    self.popupView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
                self.popupView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                
                let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
                let viewHeight = self.view.frame.size.height
                let popupHeight = self.staticPopUp.frame.size.height
                
                //                                                    self.popupView.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
                self.popupView.center = self.view.center
              self.view.addSubview(self.popupView)
                //
            }) { (finished) in
                UIView.animate(withDuration: 1, animations: {
    //                 self.popupView.transform = CGAffineTransform.identity
    //                            self.popupView.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
                    
                })
            }
            
             self.staticPopUp.removeFromSuperview()
            
           
            
        }
    
    
        @IBAction func buttonShare(_ sender: Any) {
            
            let originalString = "Using the ANI app at " + self.marchantName + " to check my food allergy and nutritional information.To get more details click on link"
            
            let image = appdelegate.MarImage
            
            self.appdelegate.marchentNameForshare = self.marchantName
            
            let sent = "Check out our ANI Partner at this location."
            let msg = "for more info."
            let addres = getValueForKey(keyValue: KAddress)
            print(addres)
            let strurl = addres.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            print(strurl)
            
            
            let link = NSURL(string:"http://maps.google.com/maps?q=\(strurl ?? "")")
            print(link!)

           let myWebsite1 = NSURL(string: "http://www.theaniapp.com/")
            
            
            let str = [originalString + "http://www.theaniapp.com/" + sent + "http://maps.google.com/maps?q=\(strurl ?? "")"] as? [Any]
           
            //        kmarchentId
            var marchentId = getValueForKey(keyValue: kmarchentId)
            if let site = NSURL(string: "https://www.theaniapp.com/merchant_info.php?mid=\(marchentId)")
            {
                let objectsToShare = [originalString,site]as! [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
                activityVC.setValue("Find food allergy and nutritional value", forKey: "subject")
                activityVC.popoverPresentationController?.sourceView = sender as! UIView
                self.present(activityVC, animated: true, completion: nil)
            }
            
            
    //        if let myWebsite = NSURL(string: "http://www.theaniapp.com/")        {
    //        let objectsToShare = str
    //            print(objectsToShare)
    //            let activityVC = UIActivityViewController(activityItems: objectsToShare as! [Any], applicationActivities: nil)
    //            //activityVC.setValue("Find food allergy and nutritional value", forKey: "Subject")
    //            activityVC.popoverPresentationController?.sourceView = self.btnshare
    //            self.present(activityVC, animated: true, completion: nil)
    //        }
    }
        @IBAction func buttonContinue(_ sender: Any)
        {
         
    //        merchantListWebservicesToCheckStatus()
    //        if self.checkRes == "true"
    //        {

            print("MerNameFromParent = \(self.marchantName)")
            print("MerIDFromParent = \(self.merchantId)")
            
//            self.btnRightbutton.isEnabled = true;
            self.popupView.removeFromSuperview()
//            self.qrCodePressButton.isHidden = false
//            self.qrCodePressButton.isEnabled = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategaryListViewController") as! CategaryListViewController
            
            vc.MerIDFromParent =  self.merchantId
            vc.MerNameFromParent = self.marchantName
            vc.comeFrom = "MasterScan"
            
            self.appdelegate.merchant_id = self.merchantId
            setDefaultValue(keyValue: kmarchentId, valueIs: self.merchantId)
            setDefaultValue(keyValue: marchentName, valueIs: self.marchantName)
            
            print("self.appdelegate.merchant_id = \(self.appdelegate.merchant_id)")
            
            self.navigationController?.pushViewController(vc, animated: true)
    //        }
    //        else
    //        {
    //
    //            self.navigationController?.popToRootViewController(animated: true)
    //        }
            
        }
    
    
    @IBAction func closeButton(_ sender: Any) {
        staticPopUp.removeFromSuperview()
        popupView.removeFromSuperview()

//         self.qrCodePressButton.isEnabled = true
    }

    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        self.lblNoDataFound.isHidden = true
        self.searchViewheight.constant = 0
        self.searchView.showsCancelButton = true
        self.searchView.text = ""
        
        self.lblNoDataFound.isHidden = true
        self.searchViewheight.constant = 0
        self.searchView.showsCancelButton = true
        self.searchView.text = ""
        
        
        favouriteProduct()
        tableview.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.lblInactiveProduct.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        
        if Reachability.isConnectedToNetwork()
        {
//            self.startSpinner()
            favouriteProduct()
            tableview.reloadData()
        }
        else
        {
            DispatchQueue.main.async
                {
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
            }
        }
        
    }

    
    @objc func btnSearchTapped(_ sender: Any)
    {
        
        if self.searchViewheight.constant == 0
        {
            self.searchViewheight.constant = self.navigationController!.navigationBar.frame.size.height
            self.searchView.becomeFirstResponder()
        }
        else
        {
//             self.lblNoDataFound.isHidden = true
//             self.tableview.reloadData()
//            self.searchViewheight.constant = 0
//            self.searchView.text = ""
//            _ = self.searchView.resignFirstResponder()
            
            self.searchViewheight.constant = 0
            self.searchView.showsCancelButton = true
            self.lblNoDataFound.isHidden = true
            self.searchView.text = ""
            self.childDataArray = self.finalArray
            self.tableview.reloadData()
            self.dismissKeyboard()
        }
    }
    
    
    
    
    
    func getTrueAllergensArray(arr: [productallergnsClass]) -> [productallergnsClass] {
        var trueArray = [productallergnsClass]()
        
        for dic in arr {
            if dic.flag == "true" {
                trueArray.append(dic)
            }
        }
        
        return trueArray
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @objc func infobutton(sender:UIButton)
    {
        
        let svc = storyboard?.instantiateViewController(withIdentifier: "marchentInfoViewController")as! marchentInfoViewController
        self.navigationController?.pushViewController(svc, animated: true)
    }
    
    @objc func backBtn(sender:UIButton)
    {
 self.navigationController?.popViewController(animated: true)
        
//        let back = storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController")as! sideMenuViewController
//
//        self.navigationController?.pushViewController(back, animated: true)

    }
    
    @IBAction func btnDelete(_ sender: Any) {
    }
    
   func favouriteProduct()
    {
       
        let headers = ["Content-Type":"Application/json"]
        let _url = get_all_fevorites_menuitems
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        
    
        var allergentId = getValueForKey(keyValue: allergentIdString)
        
        //            var allergentId = appdelegate.allergentIdString
        
        let parameters: Parameters = ["account_id" :  (data1[0].accId!),"allerganid" :  allergentId ,"merchant_id" : self.appdelegate.merchant_id]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                              self.stopSpinner()
                          
                                             let msg =  jsonDic.value(forKey: "msg") as? String
                                            if msg == "There are no menu itmes in favourite list."
                                            {
                                            self.finalArray.removeAll()
                                            self.productSubMenuArray.removeAll()
                                            }
//
                                            self.productSubMenuArray.removeAll()
                                            self.finalArray.removeAll()
                                            self.tableview.reloadData()
                                            self.lblInactiveProduct.isHidden = true
                                            self.lblInactiveProduct.text = msg
                                            
                                        //    self.lblNoDataFound.text = "No Sub Partners found"
                                        //    self.lblNoDataFound.isHidden = false
                                            
//                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "Ani", msgTitle:msg!)
                                        }
                                        else{
                                        //    self.lblNoDataFound.isHidden = true
                                            self.lblInactiveProduct.isHidden = true
//                                            self.stopSpinner()
                                            self.finalArray.removeAll()
                                            self.productSubMenuArray.removeAll()
                                            
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            let resultArray = jsonDic.object(forKey: "data") as! NSArray
//                                            for i in self.childDataArray
//                                            {
//                                                let tempobj = i as! NSDictionary
//                                                let temp = ProductListModelClass()
//                                                temp.menu_item_image = (tempobj.object(forKey: "menu_item_image") as! String)
//                                                //                                    var imageData = try! Data.init(contentsOf: URL(string: img)!)
//                                                //                                    temp.menu_item_image = UIImage(data: imageData)
//                                                temp.menu_item_description = tempobj.object(forKey: "menu_item_description")as! String
//                                                temp.menu_item_name = tempobj.object(forKey: "menu_item_name")as! String
//
//                                                temp.menu_item_flag = tempobj.object(forKey: "menuitem_flag")as! String
//
//                                                temp.ingredient_deceleration_info  = tempobj.object(forKey: "ingredient_deceleration_info")as! String
//
//
//                                                temp.menu_item_id = tempobj.object(forKey: "menu_item_id") as! String
//
//                                                 self.Id = temp.menu_item_id
//                                                print("temp.menu_item_id : \(temp.menu_item_id)")
//                                                //                                                temp.flag = tempobj.object(forKey: "flag")as! String
//                                                //                                                self.flagfav = temp.flag
//                                                //                                                self.menu_item_id_fav = temp.menu_item_id
//
//
//
//
//
//
//                                                var AllerngensArray = tempobj.object(forKey: "allergan_array")as! NSArray
//                                                let nutritianArray = tempobj.object(forKey: "nutritian_array")as! NSArray
//                                                let ingredientsArray = tempobj.object(forKey: "ingredients_array")as! NSArray
//
//                                                self.allergntArray1 = [productallergnsClass]()
//                                                self.nutritianArray1 = [nutritianArrayObject]()
//                                                self.ingredientsArray1 = [ingredientsArrayObject]()
//                                                //                                    self.allergntArray1.removeAll()
//
//                                                for j in AllerngensArray
//                                                {
//
//
//                                                    var tempallegens = j as! NSDictionary
//                                                    var tempmodel = productallergnsClass()
//                                                    tempmodel.allergan_Img = tempallegens.object(forKey: "allergan_images")as! String
//
//                                                    tempmodel.allergensName = tempallegens.object(forKey: "allergan")as! String
//
//                                                    tempmodel.allergen_id = tempallegens.object(forKey: "allergan_id")as! String
//                                                    self.allergens_id_collectionview =  tempmodel.allergen_id
//
//
//
//                                                    tempmodel.flag = tempallegens.object(forKey: "flag")as? String ?? ""
//
//                                                    if tempmodel.flag == "true"
//                                                    {
//                                                        self.allergntArray1.append(tempmodel)
//                                                        self.allergensDetail.append(tempmodel)
//                                                    }
//
//
//
//
//                                                }
//                                                //                                    print("count: \(self.allergntArray1.count)")
//                                                temp.allengsArray = self.allergntArray1
//                                                self.appdelegate.allergentArray = self.allergntArray1
//
//
//                                                for j in nutritianArray
//                                                {
//
//
//                                                    let tempallegens = j as! NSDictionary
//                                                    let tempmodel = nutritianArrayObject()
//
//                                                    tempmodel.name = (tempallegens.object(forKey: "name")as! String)
//
//                                                    tempmodel.quantity = (tempallegens.object(forKey: "quantity")as! String)
//                                                    tempmodel.unit = (tempallegens.object(forKey: "unit")as! String)
//
//                                                    print(tempmodel.name)
//
//
//
//                                                    self.nutritianArray1.append(tempmodel)
//
//                                                }
//                                                //                                    print("count: \(self.allergntArray1.count)")
//
//                                                temp.nutritianarray = self.nutritianArray1
//                                                self.appdelegate.nutritianArray = self.nutritianArray1
//                                                //self.productSubMenuArray.append(temp)
//
//                                                for j in ingredientsArray
//                                                {
//
//
//                                                    var tempallegens = j as! NSDictionary
//                                                    var tempmodel = ingredientsArrayObject()
//
//                                                    tempmodel.name = tempallegens.object(forKey: "name")as! String
//                                                    tempmodel.quantity = tempallegens.object(forKey: "quantity")as! String
//                                                    tempmodel.unit = tempallegens.object(forKey: "unit")as! String
//
//                                                    //            self.allergens_id_collectionview =  tempmodel.allergen_id!
//
//                                                    //      self.nutritianArray.append(tempmodel)
//                                                    self.ingredientsArray1.append(tempmodel)
//
//                                                }
//                                                //                                    print("count: \(self.allergntArray1.count)")
//
//                                                temp.ingredientsarray = self.ingredientsArray1
//                                                self.appdelegate.ingredientsArray = self.ingredientsArray1
//
//
//
//                                                self.productSubMenuArray.append(temp)
//                                                self.finalArray.append(temp)
//                                                DispatchQueue.main.async
//                                                    {
//                                                        self.tableview.reloadData()
//
//                                                }
//                                            }
                                            
                                            
                                            
                                          self.stopSpinner()
                                        }  //                                    print(self.marchantArray)
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                            {
                                self.stopSpinner()
//                                self.finalArray.removeAll()
                               
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                 
                                
                            }
                            
                            
        }
//        self.stopSpinner()
//        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        
    }
    
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return productSubMenuArray[collectionView.tag].allengsArray.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! favAllergnsCollectionViewCell
//
//        print("tableposition : \(tableposition)")
//
//
//
//
//
//        print("allengsArray count : \( productSubMenuArray[tableposition].allengsArray.count)")
//
//        print("allengsArray[indexPath.row] : \( indexPath.row)")
//
//
//        if productSubMenuArray[collectionView.tag].allengsArray[indexPath.row].flag == "true"
//        {
//            cell.favImage.image = UIImage(named: "allergens_placeholder.png")
//            Alamofire.request(self.productSubMenuArray[collectionView.tag].allengsArray[indexPath.row].allergan_Img, method: .get).response { response in
//                guard let image = UIImage(data:response.data!) else {
//                    // Handle error
//                    autoreleasepool {
//
//                        cell.favImage.image = UIImage(named: "allergens_placeholder.png")
//                    }
//                    return
//                }
//                autoreleasepool {
//                    let imageData = UIImagePNGRepresentation(image)
//                    cell.favImage.image = UIImage(data : imageData!)
//                    cell.backgroundColor = UIColor.clear
//                }
//            }
//
//
//        }
//
//
//        //        passdata = [productSubMenuArray[collectionView.tag].allengsArray[indexPath.row]]
//        //        self.passdata.append(productSubMenuArray[collectionView.tag].allengsArray[indexPath.row])
//        //cell.backgroundColor = UIColor.clear
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let collectionWidth = collectionView.bounds.width
//        var count: CGFloat =  CGFloat(productSubMenuArray[tableposition].allengsArray.count)
//        return CGSize(width: collectionWidth/count, height: collectionWidth/count)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return localArray.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! favTableViewCell
//        var temp1 = productSubMenuArray[indexPath.row]
//        if temp1.menu_item_flag == "active"
//        {
//            cell.allergensFlag = "1"
//            productStatusCheck = "0"
//            cell.productImage.alpha = 1
//            cell.productName.alpha = 1
//        }
//
//        else
//        {
//            cell.allergensFlag = "0"
//            productStatusCheck = "1"
//            cell.productImage.alpha = 0.25
//            cell.productName.alpha = 0.25
////            cell.gradientView.backgroundColor = .darkGray
////            cell.gradientView.alpha = 0.2
//
////             let cell2 = cell.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! favAllergnsCollectionViewCell
//
//        }
//
        print(childDataArray)
        
        var temp = localArray[indexPath.row]
//        var childDict = childDataArray[indexPath.row] as! NSDictionary
//        let merchantName = childDict.value(forKey: "merchant_name") as! String
//        let merchantImage = childDict.value(forKey: "merchant_image") as! String
        let merchantName = temp.merchantName
        let merchantImage = temp.merchantImage
        
        cell.productName.text = merchantName
        tableposition = indexPath.row
        let placeholderImage = UIImage(named: "background_placeholder.png")!
//        let imgUrl = temp1.menu_item_image
        
        let screenSize: CGRect = UIScreen.main.bounds
        let calculateHeight = (screenSize.width*600)/1024
        print(screenSize.width)
        print(calculateHeight)
        cell.productImage.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: calculateHeight)
        Alamofire.request(merchantImage!, method: .get).response
            {
                response in
                guard let image = UIImage(data:response.data!)
                    else
                {
                    // Handle error
                    autoreleasepool
                        {
                            cell.productImage.image = UIImage(named: "background_placeholder.png")
                    }
                    return
                }
                autoreleasepool
                {
                        let imageData = UIImageJPEGRepresentation(image,1.0)
                        cell.productImage.image = UIImage(data : imageData!)
                }
        }
        
      
//        cell.allergensArray = self.getTrueAllergensArray(arr: temp1.allengsArray)
//        cell.favCollectionView.reloadData()
        
        DispatchQueue.main.async
        {
            cell.favCollectionView.reloadData()
                
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print(indexPath.row)
//        var temp = childDataArray[indexPath.row] as! NSDictionary
//        print(temp)
        staticPopUp.isHidden = false
        popupView.isHidden = true

        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        let viewHeight = self.view.frame.size.height
        let popupHeight = self.staticPopUp.frame.size.height
                                                           
//        self.lblContamination.text = temp.contamination_msg
        self.staticPopUp.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
        self.staticPopUp.center = self.view.center
//        self.btnRightbutton.isEnabled = false;
        self.view.addSubview(self.staticPopUp)
        var temp = childDataArray[indexPath.row]
        self.marchantName = temp.merchantName
        let merImage = temp.merchantImage
        self.merchantId = temp.merchantID
        print(self.merchantId)
        self.lblMarchentName.text = "Using the ANI app at " + self.marchantName + " to check my food allergy and nutritional information."
        self.MarchantName.text = self.marchantName
        self.marchantImage.image = UIImage(url: URL(string: merImage!))
//        if temp.menu_item_flag == "active"
//        {
//
//            deleteId = productSubMenuArray[indexPath.row].menu_item_id
//            recentMenuId = temp.menu_item_id
//
//            //        RecentWebservice()
//            //        var temp1 = allergensDetail[indexPath.row]
//            //
//            var dvc = storyboard?.instantiateViewController(withIdentifier: "productDetailViewController")as! productDetailViewController
//
//            //        var allergens_id =
//
//            dvc.buffer = temp
//            print(temp.menu_item_name)
//            appdelegate.MenuName = temp.menu_item_name
//            //        setDefaultValue(keyValue: MenuName, valueIs: temp.menu_item_name)
//            //        dvc.product = [temp1]
//            //       appdelegate.allergentArray = allergensDetail
//            //        appdelegate.allergentArray = temp.allengsArray
//            Id = temp.menu_item_id
//            appdelegate.allergentArray = temp.allengsArray
//            self.appdelegate.ingredientsArray = temp.ingredientsarray
//            self.appdelegate.nutritianArray = temp.nutritianarray
//            RecentWebservice()
//            //        appdelegate.detailImage = temp.menu_item_image
//
//            //        print("temp1 : \([temp1])")
//
//            print("passdata : \(passdata.count)")
//
//
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
//        else
//        {
//            var msg = "This Menu Item info is unavailable at the moment please check with your server"
//            self.vcFor?.modalTransitionStyle = .crossDissolve
//            self.vcFor?.modalPresentationStyle = .overFullScreen
//            self.vcFor?.isLogin = true
//            self.vcFor?.actionFor = ""
//            self.vcFor?.titleFor =  msg
//            self.present(self.vcFor!, animated: true, completion: nil)
//        }
//
     
    }
    
    // For adding a product into the recently viewed products from favourite products - Pratik
    
    
    
    func RecentWebservice()
    {
        
        let headers = ["Content-Type":"Application/json"]
        let _url = view_menuitems
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
        }
        
        var parentID = ""
             if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                  parentID = ""
             }
             else{
                 if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                         //  parentID = ""
                    parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                       }
                       else{
                           parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                       }
             }
             print("Parent ID from UserDefaults : \(parentID)")
        
        
        let parameters: Parameters = ["account_id" :  (data1[0].accId!),"menuitem_id" : recentMenuId ,"merchant_id" : self.appdelegate.merchant_id,"partner_id":parentID]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            //popup
                                            self.stopSpinner()
                                            //                                            var str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            //
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            
                                            
                                            
//                                            let dvc = self.storyboard?.instantiateViewController(withIdentifier: "productDetailViewController")as! productDetailViewController
//                                            dvc.productStatusCheckDetail = msg!
//                                            self.view.makeToast(msg)
                                            
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
//                                            self.vcFor?.titleFor =  msg!
//                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                        }else
                                        {
                                            
                                            
                                            
                                            self.stopSpinner()
                                            
                                        }  //                                    print(self.marchantArray)
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                            {
                                //                                let httpError: NSError = response.result.error! as NSError
                                //                                let statusCode = httpError.code
                                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                                //                                print(error)
                                //
                                self.stopSpinner()
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                                
                                
                                //                                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                                //                                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                //                                self.addChildViewController(pop!)
                                //                                pop!.view.frame = self.view.frame
                                //                                self.view.addSubview(pop!.view)
                                //                                pop?.didMove(toParentViewController: self)
                                
                                //                                completion(dic,0)
                                
                            }
                            
                            
        }
        
        //    self.stopSpinner()
        //    showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        
    }
    
    
    // Done - Pratik
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    
    {
            return true
     
        
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
//    {
//        if editingStyle == .delete
//        {
//
//            let alert = UIAlertController(title: "ANI", message: "Are you sure you want to delete this menu item from favourite?", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
//            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler:  { action  in
//                self.deleteMenuItemFromFavourite(menu_id:"\(self.productSubMenuArray[indexPath.row].menu_item_id ?? "" )")
//                self.productSubMenuArray.remove(at: indexPath.row)
//            }))
//            self.present(alert, animated: true, completion: nil)
//
//////            productSubMenuArray.remove(at: indexPath.row)
////
//////            productSubMenuArray.filter { $0 ==  Id}
////            tableview.beginUpdates()
////            tableview.deleteRows(at: [indexPath], with: .automatic)
////            Delete()
////            tableview.endUpdates()
//
//        }
//
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenScale = UIScreen.main.scale
        let calculateHeight = ((screenSize.width*600)/1024)
        print(screenSize.width)
        print(calculateHeight)
        return calculateHeight
        
//        return 160
        
    }
//    func deleteMenuItemFromFavourite(menu_id:String)
//    {
////        self.startSpinner()
//        let headers = ["Content-Type":"Application/json"]
//        let _url = favourite_menuitem
//
//        var group_id = getValueForKey(keyValue: kmenu_item_group_id)
//        print("group_id : \(group_id)")
//        var merchant = getValueForKey(keyValue: kmarchentId)
//        print("merchant : \(merchant)")
//        var stringvalue = ""
//
//        var data1 = [Ani]()
//
//        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
//        var  result = try! self.context.fetch(fetch)
//        for data in result as! [NSManagedObject]
//        {
//            data1.append(data as! Ani)
//
//        }
//
//        print("menu_item_id_fav : \(menu_item_id_fav)")
//        let parameters: Parameters = ["merchant_id" :self.appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"type" : "delete", "menuitem_id" : menu_id]
//        print(parameters)
//
//        Alamofire.request(_url,
//                          method: .post,
//                          parameters: parameters,
//                          encoding: URLEncoding.queryString,
//                          headers: headers).responseJSON { (response) in
//                            print("Request  \(response.request)")
//                            print("RESPONSE \(response.result.value)")
//                            print("RESPONSE \(response.result)")
//                            print("RESPONSE \(response)")
//
//                            if response.result.isSuccess {
//                                print("SUKCES with \(response)")
//                                let jsonDic = response.result.value as! NSDictionary
//                                print(jsonDic)
//
//                                self.tableview.reloadData()
////                                self.stopSpinner()
//                                self.view.makeToast("")
//
//
////                                DispatchQueue.main.async{
////
////                                    //showDialogWithOneButton(viewControl: self, titleMsg: "ANI", msgTitle: jsonDic.value(forKey: "msg") as? String ?? "favrouites changed")
////                                }
//                                //                                sender.btnFav!.setImage(image, for: .normal)
//
//
//                            }
//                            else   if (response.result.isFailure) {
////                                self.stopSpinner()
//                                let str = response.result.error?.localizedDescription
//                                print(response.result.error?.localizedDescription)
//
//
//                                self.vcFor?.modalTransitionStyle = .crossDissolve
//                                self.vcFor?.modalPresentationStyle = .overFullScreen
//                                self.vcFor?.isLogin = true
//                                self.vcFor?.actionFor = ""
//                                self.vcFor?.titleFor =  str!
//                                self.present(self.vcFor!, animated: true, completion: nil)
//
//
//
//
//                            }
//
//
//                            DispatchQueue.main.async
//                            {
//
////                                self.stopSpinner()
//                                 self.favouriteProduct()
////                                self.tableview.reloadData()
//
//                            }
//        }
//
//
//
//
//
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        searchBar.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        searchBar.resignFirstResponder()
    }
}




extension MasterScanViewController : UISearchBarDelegate
{
    
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
//    {
//        if lblInactiveProduct.isHidden == true
//        {
////            lblNoDataFound.isHidden = false
//        }
//        else
//        {
//            lblNoDataFound.isHidden = true
//        }
//        self.searchView.showsCancelButton = true
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
//    {
//
//    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
      guard !searchText.isEmpty else {
          self.localArray = self.childDataArray
          self.tableview.reloadData()
          return
      }
        
        let temp = self.childDataArray.filter { (T) -> Bool in
            var bools = [Bool]()
            
            for object in [T.merchantName] {
                let bool = object!.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
                bools.append(bool)
            }
            
            return bools.contains(true)
        }
        self.localArray = temp
        self.tableview.reloadData()
        
//        self.getSearchResult(str: searchText)
    }
    
    
//    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//           searchBar.resignFirstResponder()
//       }
//
//       internal func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//           searchBar.resignFirstResponder()
//       }
       
       
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {

        print(self.searchView.text)
        self.getSearchResult(str: self.searchView.text ?? "")
        self.dismissKeyboard()

    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        self.searchViewheight.constant = 0
        self.searchView.showsCancelButton = true
         self.lblNoDataFound.isHidden = true
        self.searchView.text = ""
        self.productSubMenuArray = self.finalArray
        self.tableview.reloadData()
          self.dismissKeyboard()
        //        self.viewDidLayoutSubviews()

    }
    
    func getSearchResult(str: String)
    {
        
        
        if lblInactiveProduct.isHidden == true
        {
            //            lblNoDataFound.isHidden = false
            
            
            if str == ""
            {
                self.productSubMenuArray = self.finalArray
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                }
            }
            else {
                print(self.finalArray.filter ({
                    $0.menu_item_name.contains(str)
                }))
                self.productSubMenuArray.removeAll()
                // Commented by Pratik for removing case sensitivity to the filter
                //            self.productSubMenuArray = self.finalArray.filter ({
                //                $0.menu_item_name.contains(str)
                //            })
                
                // Done
                
                self.productSubMenuArray = self.finalArray.filter {
                    $0.menu_item_name.localizedCaseInsensitiveContains(str) }
                
                
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                }
                
            }
            if (self.productSubMenuArray.count == 0)
            {
                self.lblNoDataFound.isHidden = false
            }
            else
            {
                self.lblNoDataFound.isHidden = true
            }
            //        self.viewDidLayoutSubviews()
            

        }
        else
        {
            lblNoDataFound.isHidden = true
        }
        
        
    }
    
}

//
//  marchentInfoViewController.swift
//  Ani
//
//  Created by RSL-01 on 14/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import MapKit

class marchentInfoViewController: UIViewController,MKMapViewDelegate,PopUpDoneDelegate {

    @IBOutlet weak var marchentImage: UIImageView!
     var vcFor : ValidationPopupViewController? = nil
    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var marchentSentence: UILabel!
    var marchantName = ""
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var NameMarchent: UILabel!
    @IBOutlet var mapView: MKMapView!
     var appdelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()

         print("MerchantInfoViewController")
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationController?.isNavigationBarHidden = true
        
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        lbladdress.text = getValueForKey(keyValue: KAddress)
        lblEmail.text = getValueForKey(keyValue: KkEmail)
        lblMobileNumber.text = getValueForKey(keyValue: KMobile)
        
        marchentSentence.text = getValueForKey(keyValue: ktagline)
        NameMarchent.text = getValueForKey(keyValue: marchentName)
       let merchantImg = getValueForKey(keyValue: kMerchantImage)
        var urlString = merchantImg.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlString ?? "")!
        self.marchentImage.af_setImage(withURL:url)
        //marchentImage.image  =  appdelegate.MarImage
        
        let theSpan:MKCoordinateSpan = MKCoordinateSpanMake(0.01 , 0.01)
        
        let latValue : Double = NSString(string: getValueForKey(keyValue: Klatitude)).doubleValue
        
        let langValue : Double = NSString(string: getValueForKey(keyValue: Klongitude)).doubleValue


        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:latValue, longitude: langValue)

        let theRegion:MKCoordinateRegion = MKCoordinateRegionMake(location, theSpan)
        mapView.setRegion(theRegion, animated: true)

        
        
        let anotation = MKPointAnnotation()
        anotation.coordinate = location
        anotation.title = getValueForKey(keyValue: marchentName)
         anotation.subtitle = "This is the location !!!"
        mapView.addAnnotation(anotation)
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidDisappear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
           _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnshare(_ sender: Any) {
        let originalString = "Using the ANI app at " + getValueForKey(keyValue: marchentName) + " to check my food allergy and nutritional information.To get more details click on link"
        
        let image = appdelegate.MarImage
        
        self.appdelegate.marchentNameForshare = self.marchantName
        
        let sent = "Check out our ANI Partner at this location."
        let msg = "for more info."
        
        
        
        
        let addres = getValueForKey(keyValue: KAddress)
        print(addres)
        let strurl = addres.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(strurl)
        
        
        let link = NSURL(string:"http://maps.google.com/maps?q=\(strurl ?? "")")
        print(link!)
        
        let myWebsite1 = NSURL(string: "http://www.theaniapp.com/")
        
        
        let str = [originalString + "http://www.theaniapp.com/" + sent + "http://maps.google.com/maps?q=\(strurl ?? "")"] as? [Any]
//        kmarchentId
        var marchentId = getValueForKey(keyValue: kmarchentId)
        if let site = NSURL(string: "https://www.theaniapp.com/merchant_info.php?mid=\(marchentId)")
        {
            let objectsToShare = [originalString,site]as! [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.setValue("Find food allergy and nutritional value", forKey: "Subject")
            activityVC.popoverPresentationController?.sourceView = sender as! UIView
            self.present(activityVC, animated: true, completion: nil)
        }
        
        
      
    }
    
    @objc func backBtn(sender:UIButton)
    {
        
        _ = navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  signuppageViewController.swift
//  Ani
//
//  Created by RSL-01 on 13/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SVProgressHUD
import FBSDKLoginKit
import GoogleSignIn

import AuthenticationServices


//@available(iOS 13.0, *)
class signuppageViewController: UIViewController,PopUpDoneDelegate,GIDSignInUIDelegate,GIDSignInDelegate {
    
    
    let context = AppDelegate().persistentContainer.viewContext
    
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var termAndPolicy: UIButton!
    
//    var googleUrl = URL.self
    var vcFor : ValidationPopupViewController? = nil
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID =  "21530822958-jfm91b1q1bh3b4vgcqrd4n59e11kfngp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        GIDSignIn.sharedInstance().uiDelegate = self
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
       print("signuppageViewController")
        
    }
   
    

    @IBAction func btnAppleClicked(_ sender: Any) {
        
        if #available(iOS 13.0, *) {
        print("Greater 13")
        let appleIDProvider = ASAuthorizationAppleIDProvider()

        let request = appleIDProvider.createRequest()

        request.requestedScopes = [.fullName, .email]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])

        authorizationController.delegate = self

        authorizationController.presentationContextProvider = self

        authorizationController.performRequests()
            
        }
        else{
            // Fallback on earlier versions
            var alert = UIAlertController(title: "", message: "Apple sign in does not support below version iOS 13.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
              }))

            present(alert, animated: true, completion: nil)
            print("Below 13")
        }
        
    }

   
    @IBAction func btnGoogle(_ sender: Any) {
        
        print("firstcurrentUser =\(GIDSignIn.sharedInstance()?.currentUser != nil)") // true - signed in
      //  GIDSignIn.sharedInstance()?.disconnect()
        print("disconnect =\(GIDSignIn.sharedInstance()?.disconnect())")
        print("currentUser =\(GIDSignIn.sharedInstance()?.currentUser != nil)") // true - still signed in
        
        GIDSignIn.sharedInstance()?.signOut()
        
        if (GIDSignIn.sharedInstance()?.currentUser != nil){
          
            GIDSignIn.sharedInstance()?.signIn()
        }
        else{
           
            GIDSignIn.sharedInstance()?.signIn()
        }
        
    }
    
    // MARK: - GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print(GIDSignIn.sharedInstance()?.currentUser != nil) // false - signed out
        
        // Remove any data saved to your app obtained from Google's APIs for this user.
    }
//    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
//                withError error: NSError!)
//    {
//        if (error == nil)
//        {
//            // Perform any operations on signed in user here.
//            let userId = user.userID // For client-side use only!
//            let idToken = user.authentication.idToken //Safe to send to the server
//            let name = user.profile.name
//            let email = user.profile.email
//            let userImageURL = user.profile.imageURL(withDimension: 200)
//            // ...
////            self.getGoogleLogin(fname: givenName  ?? "", lname: familyName ?? "", email: email ?? "")
//        }
//        else
//        {
//            print("\(error.localizedDescription)")
//        }
//    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let googleUrl = user.profile.imageURL(withDimension: 100)
          
            
//            let url = URL(string:googleUrl)
            if let data = try? Data(contentsOf: googleUrl!)
            {
                let image: UIImage = UIImage(data: data)!
            }
            let defaults = UserDefaults.standard
           
                defaults.set( googleUrl?.absoluteString, forKey: "picture")
                defaults.synchronize()

        
            print(userId)
            print(idToken)
            print(fullName)
            print(givenName)
            print(familyName)
            print(email)
              print(googleUrl)
            
              self.startSpinner()
            
            self.getGoogleLogin(fname: givenName  ?? "", lname: familyName ?? "", email: email ?? "")
            // ...
        }
    }
    
//    self.getFacebookLogin(dic: result as! [String:Any]
//func getFacebookLogin(dic: [String:Any]) {
    
    
    

    func appleLoginAPI(fname: String, lname: String, email: String,appleId:String) {
            
            let headers = ["Content-Type":"Application/json"]
            let _url = user_social_login
            let parameters: Parameters = [ "first_name" :fname ,
                                           "last_name" : lname ,
                                           "email_id": email ,
                                           "date_of_birth": "",
                                           "gender" : "",
                                           "password" : "Social",
                                           "devicetoken": "asdasdasdasdasdasddasdasdasdasdasdasdas",//getValueForKey(keyValue: kdeviceToken),
                                            "device_type": kDeviceType,
                                            "login_type" : "apple",
                                            "apple_id" : appleId
                                            ]
        
            print(parameters)
            Alamofire.request(_url,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding.queryString,
                              headers: headers).responseJSON { (response) in
                                                                
                                print("Request  \(response.request)")
                                print("RESPONSE \(response.result.value)")
                                print("RESPONSE \(response.result)")
                                print("RESPONSE \(response)")
                                
                                if response.result.isSuccess {
                                    print("SUKCES with \(response)")
                                    if (response.result != nil)
                                    {
                                        let jsonDic = response.result.value as! NSDictionary
                                        print(jsonDic)
                                        if let res = jsonDic.value(forKey: "result") as? String
                                        {
                                            if res == "failed"
                                            {
                                                self.stopSpinner()
                                                //popup
                                                var msg = jsonDic.object(forKey: "msg")as? String
                                                
                                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                                self.vcFor?.isLogin = true
                                                self.vcFor?.actionFor = ""
                                                self.vcFor?.titleFor =  msg!
                                                self.present(self.vcFor!, animated: true, completion: nil)
                                                
                                            }
                                            else
                                            {
                                                self.stopSpinner()
                                              
                                                print("res=\(res)")
    //                                            if (jObj.getString("type").equals("Active"))
    //                                            {
    //                                                if (jObj.getString("role").equals("OtherUser"))
                                       
                                             //   let dic =  jsonDic as! [String:Any]
    //                                            let type = jsonDic["type"] as! [String:Any] as? String
    //                                            let role = jsonDic["role"] as! [String:Any] as? String
    //                                            let msg = jsonDic["msg"] as! [String:Any] as? String
                                                
                                                let type = jsonDic.value(forKey: "type") as? String
                                                let role = jsonDic.value(forKey: "role") as? String
                                                let msg = jsonDic.value(forKey: "msg") as? String
                                                
                                                if(type == "Active"){
                                                    
                                                    if (role == "OtherUser"){
                                                        
                                                        //msg display this email
                                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                                    self.vcFor?.isLogin = true
                                                    self.vcFor?.actionFor = ""
                                                    self.vcFor?.titleFor =  msg!
                                                    self.present(self.vcFor!, animated: true, completion: nil)
                                                    }
                                                    else{
                                                        //login
                                                        let mutableDic = NSMutableDictionary()
                                                        UserDefaults.standard.set(true, forKey: "isUserLog")
                                                        let temp = loginClass()
                                                        
                                                        temp.account_id = jsonDic.object(forKey: "account_id") as? String
                                                        UserDefaults.standard.set(temp.account_id, forKey: kregisterAccountId)
                                                        UserDefaults.standard.synchronize()
                                                        
                                                        print("\( temp.account_id)")
                                                        
                                                        let c1 = NSEntityDescription.insertNewObject(forEntityName: "Ani", into: self.context)as! Ani
                                                        //                                c1.setValue(temp.account_id, forKey: accId)
                                                        c1.accId = temp.account_id
                                                        
                                                        // txtName.text=""
                                                        // txtNumber.text=""
                                                        
                                                        print(c1)
                                                        
                                                        try! self.context.save()
                                                        
                                                        
                                                        
                                                        
                                                        //                                temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as! String
                                                        temp.first_name = jsonDic.object(forKey: "first_name") as! String
                                                        var firstName: String = temp.first_name
                                                        
                                                        setDefaultValue(keyValue: kfirstName, valueIs: firstName)
                                                        
                                                        //                                            getValueForKey(keyValue: T##String)
                                                        
                                                        temp.last_name = jsonDic.object(forKey: "last_name") as! String
                                                        
                                                        setDefaultValue(keyValue: klastName, valueIs: temp.last_name)
                                                        
                                                        temp.email_id = jsonDic.object(forKey: "email_id") as! String
                                                        
                                                        setDefaultValue(keyValue: kemail, valueIs: temp.email_id)
                                                        
                                                        
                                                        
                                                        var img = jsonDic.object(forKey: "profile_image") as! String
                                                        let defaults = UserDefaults.standard
                                                        if (img != nil && img != "")
                                                        {
                                                            var imageData = try? Data.init(contentsOf: URL(string: img)!)
                                                            if imageData != nil
                                                            {
                                                                temp.profile_image = UIImage(data: imageData!)
                                                            }
                                                            
                                                            
                                                            print("temp.profile_image =\(temp.profile_image)")
                                                            
                                                            defaults.set( img, forKey: "picture")
                                                            defaults.synchronize()
                                                            
                                                        }else
                                                        {
                                                            img = ""
                                                            
                                                            
                                                            
                                                            let imgUrl = defaults.object(forKey: "picture") as? String
                                                            
                                                            print("imgurl : \(imgUrl)")
                                                            defaults.set( imgUrl, forKey: "picture")
                                                            defaults.synchronize()
                                                        }
                                                        
                                                        
                                                        
                                                        //                                            defaults.set( temp.profile_image, forKey: "picture")
                                                        //                                            defaults.synchronize()
                                                        
                                                        
                                                        //                                            setDefaultValue(keyValue: kimageview as! String, valueIs: imgData)
                                                        
                                                        temp.mobile_number = jsonDic.object(forKey: "mobile_number") as? String
                                                        if temp.mobile_number != nil
                                                        {
                                                            setDefaultValue(keyValue: kmobNumber, valueIs:  temp.mobile_number)
                                                        }
                                                        else
                                                        {
                                                            setDefaultValue(keyValue: kmobNumber, valueIs: "")
                                                            
                                                        }
                                                        temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as? String
                                                        if temp.date_of_birth == nil
                                                        {
                                                            setDefaultValue(keyValue: KDOB, valueIs: "")
                                                        }
                                                        else
                                                        {
                                                            setDefaultValue(keyValue: KDOB, valueIs: temp.date_of_birth)
                                                        }
                                                        temp.role = jsonDic.object(forKey: "role") as! String
                                                        setDefaultValue(keyValue: krole, valueIs:  temp.role )
                                                        //
                                                        //                                temp.home_number = jsonDic.object(forKey: "home_number") as! String
                                                        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                                                        self.navigationController?.pushViewController(secondVC, animated: true)
                                                     
                                                        ///
                                                    }

                                                }
                                                else if (type == "other"){
                                                    
                                                    //popup this email
                                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                                    self.vcFor?.isLogin = true
                                                    self.vcFor?.actionFor = ""
                                                    self.vcFor?.titleFor =  msg!
                                                    self.present(self.vcFor!, animated: true, completion: nil)
                                                    
                                                }
                                                else{
                                                    //msg
                                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                                    self.vcFor?.isLogin = true
                                                    self.vcFor?.actionFor = ""
                                                    self.vcFor?.titleFor =  msg!
                                                    self.present(self.vcFor!, animated: true, completion: nil)
                                                }
                                                
                                              
                                            }
                                        }
                                    }
                                    
                                }
                                else if (response.result.isFailure)
                                {
                                    self.stopSpinner()
                                    var str = response.result.error?.localizedDescription
                                    print(response.result.error?.localizedDescription)
                                    
                                    if str ==
                                        "The Internet connection appears to be offline."
                                    {
                                        str = "You are offline please check your internet connection"
                                    }
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  str!
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                                }
                                
                                
            }
        }
    

    func getGoogleLogin(fname: String, lname: String, email: String) {
        
        
        
        let headers = ["Content-Type":"Application/json"]
        
        let _url = user_social_login
        let parameters: Parameters = [ "first_name" :fname ,
                                       "last_name" : lname ,
                                       "email_id": email ,
                                       "date_of_birth": "",
                                       "gender" : "",
                                       "password" : "Social",
                                       "devicetoken": "asdasdasdasdasdasddasdasdasdasdasdasdas",//getValueForKey(keyValue: kdeviceToken),
            "device_type": kDeviceType,
            "login_type" : "Google",
             "apple_id" : ""
        ]
        print(parameters)
        
     
        
//                let dic =  result as! [String:Any]
//                let pic = dic["picture"] as! [String:Any]
//                let dat = pic["data"] as! [String:Any]
//                let fburl = dat["url"] as! String
//
        //        let defaults = UserDefaults.standard
        //        defaults.set( fburl, forKey: "FBProfile")
        //        defaults.synchronize()
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }
                                        else
                                        {
                                            self.stopSpinner()
                                          
                                            print("res=\(res)")
//                                            if (jObj.getString("type").equals("Active"))
//                                            {
//                                                if (jObj.getString("role").equals("OtherUser"))
                                   
                                         //   let dic =  jsonDic as! [String:Any]
//                                            let type = jsonDic["type"] as! [String:Any] as? String
//                                            let role = jsonDic["role"] as! [String:Any] as? String
//                                            let msg = jsonDic["msg"] as! [String:Any] as? String
                                            
                                            let type = jsonDic.value(forKey: "type") as? String
                                            let role = jsonDic.value(forKey: "role") as? String
                                            let msg = jsonDic.value(forKey: "msg") as? String
                                            
                                            if(type == "Active"){
                                                
                                                if (role == "OtherUser"){
                                                    
                                                    //msg display this email
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                                self.vcFor?.isLogin = true
                                                self.vcFor?.actionFor = ""
                                                self.vcFor?.titleFor =  msg!
                                                self.present(self.vcFor!, animated: true, completion: nil)
                                                }
                                                else{
                                                    //login
                                                    let mutableDic = NSMutableDictionary()
                                                    UserDefaults.standard.set(true, forKey: "isUserLog")
                                                    let temp = loginClass()
                                                    
                                                    temp.account_id = jsonDic.object(forKey: "account_id") as? String
                                                    UserDefaults.standard.set(temp.account_id, forKey: kregisterAccountId)
                                                    UserDefaults.standard.synchronize()
                                                    
                                                    print("\( temp.account_id)")
                                                    
                                                    let c1 = NSEntityDescription.insertNewObject(forEntityName: "Ani", into: self.context)as! Ani
                                                    //                                c1.setValue(temp.account_id, forKey: accId)
                                                    c1.accId = temp.account_id
                                                    
                                                    // txtName.text=""
                                                    // txtNumber.text=""
                                                    
                                                    print(c1)
                                                    
                                                    try! self.context.save()
                                                    
                                                    
                                                    
                                                    
                                                    //                                temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as! String
                                                    temp.first_name = jsonDic.object(forKey: "first_name") as! String
                                                    var firstName: String = temp.first_name
                                                    
                                                    setDefaultValue(keyValue: kfirstName, valueIs: firstName)
                                                    
                                                    //                                            getValueForKey(keyValue: T##String)
                                                    
                                                    temp.last_name = jsonDic.object(forKey: "last_name") as! String
                                                    
                                                    setDefaultValue(keyValue: klastName, valueIs: temp.last_name)
                                                    
                                                    temp.email_id = jsonDic.object(forKey: "email_id") as! String
                                                    
                                                    setDefaultValue(keyValue: kemail, valueIs: temp.email_id)
                                                    
                                                    
                                                    
                                                    var img = jsonDic.object(forKey: "profile_image") as! String
                                                    let defaults = UserDefaults.standard
                                                    if (img != nil && img != "")
                                                    {
                                                        var imageData = try? Data.init(contentsOf: URL(string: img)!)
                                                        if imageData != nil
                                                        {
                                                            temp.profile_image = UIImage(data: imageData!)
                                                        }
                                                        
                                                        
                                                        print("temp.profile_image =\(temp.profile_image)")
                                                        
                                                        defaults.set( img, forKey: "picture")
                                                        defaults.synchronize()
                                                        
                                                    }else
                                                    {
                                                        img = ""
                                                        
                                                        
                                                        
                                                        let imgUrl = defaults.object(forKey: "picture") as? String
                                                        
                                                        print("imgurl : \(imgUrl)")
                                                        defaults.set( imgUrl, forKey: "picture")
                                                        defaults.synchronize()
                                                    }
                                                    
                                                    
                                                    
                                                    //                                            defaults.set( temp.profile_image, forKey: "picture")
                                                    //                                            defaults.synchronize()
                                                    
                                                    
                                                    //                                            setDefaultValue(keyValue: kimageview as! String, valueIs: imgData)
                                                    
                                                    temp.mobile_number = jsonDic.object(forKey: "mobile_number") as? String
                                                    if temp.mobile_number != nil
                                                    {
                                                        setDefaultValue(keyValue: kmobNumber, valueIs:  temp.mobile_number)
                                                    }
                                                    else
                                                    {
                                                        setDefaultValue(keyValue: kmobNumber, valueIs: "")
                                                        
                                                    }
                                                    temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as? String
                                                    if temp.date_of_birth == nil
                                                    {
                                                        setDefaultValue(keyValue: KDOB, valueIs: "")
                                                    }
                                                    else
                                                    {
                                                        setDefaultValue(keyValue: KDOB, valueIs: temp.date_of_birth)
                                                    }
                                                    temp.role = jsonDic.object(forKey: "role") as! String
                                                    setDefaultValue(keyValue: krole, valueIs:  temp.role )
                                                    //
                                                    //                                temp.home_number = jsonDic.object(forKey: "home_number") as! String
                                                    let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                                                    self.navigationController?.pushViewController(secondVC, animated: true)
                                                 
                                                    ///
                                                }

                                            }
                                            else if (type == "other"){
                                                
                                                //popup this email
                                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                                self.vcFor?.isLogin = true
                                                self.vcFor?.actionFor = ""
                                                self.vcFor?.titleFor =  msg!
                                                self.present(self.vcFor!, animated: true, completion: nil)
                                                
                                            }
                                            else{
                                                //msg
                                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                                self.vcFor?.isLogin = true
                                                self.vcFor?.actionFor = ""
                                                self.vcFor?.titleFor =  msg!
                                                self.present(self.vcFor!, animated: true, completion: nil)
                                            }
                                            
                                          
                                        }
                                    }
                                }
                                
                            }
                            else if (response.result.isFailure)
                            {
                                self.stopSpinner()
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                            }
                            
                            
        }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!)
    {
        // Perform any operations when the user disconnects from app here.
    }
    
  
    
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
        signUpButton.layer.cornerRadius = CGFloat(20)
        signUpButton.backgroundColor = UIColor.white
        signUpButton.layer.borderWidth = 1
        signUpButton.layer.borderColor = UIColor.white.cgColor
        signUpButton.clipsToBounds = true
        
        facebookButton.layer.cornerRadius = CGFloat(20)
        facebookButton.clipsToBounds = true
        
        btnGoogle.layer.cornerRadius = CGFloat(20)
        btnGoogle.clipsToBounds = true
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    

    
    
    @IBAction func facebookButton(_ sender: Any) {
        
        print("*****")
        //        let loginManager = LoginManager()
        //        loginManager.logIn(readPermissions: [.publicProfile , .email], viewController: self) { (result) in
        //            switch result
        //            {
        //            case .failed(let error) :
        //                print(error.localizedDescription)
        //            case  .cancelled :
        //                print("user cancelled the login")
        //            case .success( _, _, _):
        //                self.getUserInfo { userinfo ,error in
        //                    if let error = error { print(error.localizedDescription)}
        //                    if let  userinfo = userinfo , let id = userinfo["id"],let name = userinfo["name"] , let email = userinfo["email"]
        //                    {
        //                        print("*****")
        //
        //
        //                    }
        //
        //
        //                }
        //            }
        //        }
        //
        let fbLoginManager : LoginManager = LoginManager()

        let permisions = ["email"]
        self.stopSpinner()
        //fbLoginManager.loginBehavior = .browser
        fbLoginManager.logIn(permissions: permisions, from: self, handler: { (result, error) -> Void in
            
            print("result=\(result)")
            if (error == nil)
            {
                let fbloginresult : LoginManagerLoginResult = result!

                if result!.isCancelled
                {
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {

                    self.getFBUserData()

                }
            }
//            else if (result == nil){
//                self.getFBUserData()
//            }
            else
            {
                print("error: \(error?.localizedDescription)")
            }
        })
        
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender,birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil)
                {
                    print("result=\(result)")
                   
//                    let dic =  result as! [String:Any]
//                    let pic = dic["picture"] as! [String:Any]
//                    let dat = pic["data"] as! [String:Any]
//                    let url = dat["url"] as! String
//
//                    let defaults = UserDefaults.standard
//                    defaults.set( url, forKey: "FBProfile")
//                    defaults.synchronize()
                    
                    let dic =  result as! [String:Any]
                 //   let email = dic["email"] as! [String:Any]
                    
                    print("dic=\(dic)")
                    
                    let keyExists = dic["email"] != nil
                    
                    if keyExists{
                        print("The key is present in the dictionary")
                        self.startSpinner()
                        self.getFacebookLogin(dic: result as! [String:Any])
                        
                    } else {
                        print("The key is not present in the dictionary")
                        
                        
                        let str = "We couldn't find your email address on your facebook account. \n Please login with different facebook account which having email address."
                        
                        self.vcFor?.modalTransitionStyle = .crossDissolve
                        self.vcFor?.modalPresentationStyle = .overFullScreen
                        self.vcFor?.isLogin = true
                        self.vcFor?.actionFor = ""
                        self.vcFor?.titleFor =  str
                        self.present(self.vcFor!, animated: true, completion: nil)
                    }
                    
                   //
                }
            })
        }
    }
    
    func getFacebookLogin(dic: [String:Any]) {
        
        print("dic=\(dic)")
        
        let headers = ["Content-Type":"Application/json"]
        
        let _url = user_social_login
        let parameters: Parameters = [ "first_name" : dic["first_name"] as! String,
                                       "last_name" : dic["last_name"] as! String,
                                       "email_id": dic["email"] as! String,
                                       "date_of_birth": (dic["birthday"] as? String) ?? "",
                                       "gender" : "",
                                       "password" : "Social",
                                       "devicetoken": "asdasdasdasdasdasddasdasdasdasdasdasdas",//getValueForKey(keyValue: kdeviceToken),
            "device_type": kDeviceType,
            "login_type" : "Facebook",
             "apple_id" : ""
        ]
        print(parameters)
        
    //    let dic =  result as! [String:Any]
        let pic = dic["picture"] as! [String:Any]
        let dat = pic["data"] as! [String:Any]
        let fburl = dat["url"] as! String
        
//        let defaults = UserDefaults.standard
//        defaults.set( fburl, forKey: "FBProfile")
//        defaults.synchronize()
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            //popup
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                        }
                                        else
                                        {
                                            self.stopSpinner()
                                            
                                            let mutableDic = NSMutableDictionary()
                                            UserDefaults.standard.set(true, forKey: "isUserLog")
                                            let temp = loginClass()
                                            
                                            temp.account_id = jsonDic.object(forKey: "account_id") as? String
                                            
                            UserDefaults.standard.set(temp.account_id, forKey:kregisterAccountId)
                                            
                                            
                        
                           // UserDefaults.standard.set(temp.account_id, forKey: kregisterAccountId)
                           UserDefaults.standard.synchronize()
                                            
                      UserDefaults.standard.object(forKey:kregisterAccountId)
                        print("\( temp.account_id)")
                        print("defaults=\( UserDefaults.standard.object(forKey:kregisterAccountId) ?? "")")
                                            
                                            
                                            let c1 = NSEntityDescription.insertNewObject(forEntityName: "Ani", into: self.context)as! Ani
                                            //                                c1.setValue(temp.account_id, forKey: accId)
                                            c1.accId = temp.account_id
                                            
                                            // txtName.text=""
                                            // txtNumber.text=""
                                            
                                            print(c1)
                                            
                                             try! self.context.save()
                                            
                                            
                            
                                            //                                temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as! String
                                            temp.first_name = jsonDic.object(forKey: "first_name") as! String
                                            var firstName: String = temp.first_name
                                            
                                            setDefaultValue(keyValue: kfirstName, valueIs: firstName)
                                            
                                            //                                            getValueForKey(keyValue: T##String)
                                            
                                            temp.last_name = jsonDic.object(forKey: "last_name") as! String
                                            
                                            setDefaultValue(keyValue: klastName, valueIs: temp.last_name)
                                            
                                            temp.email_id = jsonDic.object(forKey: "email_id") as! String
                                            
                                            setDefaultValue(keyValue: kemail, valueIs: temp.email_id)
                                            
                                        
                                            
                                            var img = jsonDic.object(forKey: "profile_image") as! String
                                            let defaults = UserDefaults.standard
                                            if (img != nil && img != "")
                                            {
                                              //   let imageData = try! Data.init(contentsOf: URL(string: img)!)
                                            //    temp.profile_image = UIImage(data: imageData)
                                                
                                                if let data = try? Data(contentsOf: URL(string: img)!)
                                               {
                                                //  let image: UIImage = UIImage(data: data)
                                                temp.profile_image = UIImage(data: data)
                                               }
                                              
                                                print("temp.profile_image =\(temp.profile_image)")
                                                
                                                defaults.set( img, forKey: "picture")
                                                defaults.synchronize()
                                        
                                            }else
                                            {
                                                img = ""
                                                
                                                defaults.set( fburl, forKey: "picture")
                                                defaults.synchronize()
                                            }
                                            
                                           
                                            
//                                            defaults.set( temp.profile_image, forKey: "picture")
//                                            defaults.synchronize()
                                            
                                            
                                            //                                            setDefaultValue(keyValue: kimageview as! String, valueIs: imgData)
                                            
                                            temp.mobile_number = jsonDic.object(forKey: "mobile_number") as? String
                                              if temp.mobile_number != nil
                                            {
                                            setDefaultValue(keyValue: kmobNumber, valueIs:  temp.mobile_number)
                                            }
                                            else
                                              {
                                                   setDefaultValue(keyValue: kmobNumber, valueIs: "")
                                                
                                            }
                                            temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as? String
                                            if temp.date_of_birth == nil
                                            {
                                                setDefaultValue(keyValue: KDOB, valueIs: "")
                                            }
                                            else
                                            {
                                                setDefaultValue(keyValue: KDOB, valueIs: temp.date_of_birth)
                                            }
                                            temp.role = jsonDic.object(forKey: "role") as! String
                                            setDefaultValue(keyValue: krole, valueIs:  temp.role )
                                            //
                                            //                                temp.home_number = jsonDic.object(forKey: "home_number") as! String
                                            let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                                            self.navigationController?.pushViewController(secondVC, animated: true)
                                        }
                                    }
                                }
                                
                            }
                            else if (response.result.isFailure)
                            {
                                 self.stopSpinner()
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                            }
                            
                            
        }
        
    }
    
    
}


var info = ""
@available(iOS 13.0, *)
extension signuppageViewController: ASAuthorizationControllerDelegate {

     // ASAuthorizationControllerDelegate function for authorization failed

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {

        print(error.localizedDescription)

    }

       // ASAuthorizationControllerDelegate function for successful authorization

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {

        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {

            // Create an account as per your requirement

            let appleId = appleIDCredential.user

            let appleUserFirstName = appleIDCredential.fullName?.givenName

            let appleUserLastName = appleIDCredential.fullName?.familyName

            let appleUserEmail = appleIDCredential.email


            var emailId = ""

            if(appleUserEmail == nil){
                emailId = ""
            }
            else{
                emailId = appleUserEmail!
            }

            
            var fName = ""

            if(appleUserFirstName == nil){
                fName = ""
            }
            else{
                fName = appleUserFirstName!
            }
                   
            var lName = ""

            if(appleUserLastName == nil){
                lName = ""
            }
            else{
                lName = appleUserLastName!
            }
            

       //     print("appleIDCredential = \(appleIDCredential)")

            print("appleId = \(appleId)")
            print("appleUserFirstName = \(fName)")
            print("appleUserLastName = \(lName)")
            print("appleUserEmail = \(emailId)")

            info = " Apple ID : \(appleId)\n First Name : \(fName)\n Last Name : \(lName) \n Email ID : \(emailId)"
             self.appleLoginAPI(fname: "\(fName)", lname: "\(lName)", email: "\(emailId)",appleId: appleId)
            
            
//            appleLoginAPI(fname: "\(fName)", lname: "\(lName)", email: "\(emailId)",appleId: appleId)
            
    
    //        appleLoginAPI(fname: fn, lname: ln, email: emailId,appleId: appleId)
            
            
//            let alert = UIAlertController(title: "ANI", message: info, preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler:  { action  in
//
//                    self.appleLoginAPI(fname: "\(fName)", lname: "\(lName)", email: "\(emailId)",appleId: appleId)
//
//                }))
//                self.present(alert, animated: true, completion: nil)

            

            //Write your code
//            DispatchQueue.main.sync {

//                let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
//                vc.detailsOfUser = info
//            getAppleLogin(fname: appleUserFirstName ?? "", lname: appleUserLastName ?? "", email: appleUserEmail ?? "")
//                self.navigationController?.pushViewController(vc, animated: true)

//            self.present(vc, animated: true, completion: nil)
//            }

        }
        else if let passwordCredential = authorization.credential as? ASPasswordCredential {

            let appleUsername = passwordCredential.user

            let applePassword = passwordCredential.password
            
            //Write your code

//            let str = " appleUsername : \(appleUsername)\n  applePassword : \(applePassword)"
//
//            let alert = UIAlertController(title: "ANI", message: str, preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler:  { action  in
//
//                       //     self.appleLoginAPI(fname: "\(fName)", lname: "\(lName)", email: "\(emailId)",appleId: appleId)
//
//            }))
//            self.present(alert, animated: true, completion: nil)
            
        }

    }



//    @IBAction func btnAppleTapped(_ sender: Any) {
//        let appleIDProvider = ASAuthorizationAppleIDProvider()
//
//        let request = appleIDProvider.createRequest()
//
//        request.requestedScopes = [.fullName, .email]
//
//        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//
//        authorizationController.delegate = self
//
//        authorizationController.presentationContextProvider = self
//
//        authorizationController.performRequests()
//    }






//    func getAppleLogin(fname: String, lname: String, email: String) {
////        if Reachability.isConnectedToNetwork() {
////            self.setLoading()
//
//            consumerSocialRegistration(firstName: fname,
//                                            lastName: lname,
//                                            emailID: email,
//                                            dob: "",
//                                            gender: "",
//                                            loginType: "Apple")
//            { (success) in
//                if success {
//                    print("true")
//
////                    if self.comeFROM == "Cart" {
////                        self.navigationController?.popViewController(animated: true)
////                    }
////                    else {
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                    vc.detailsOfUser = info
//                    self.navigationController?.pushViewController(vc, animated: true)
////                    self.present(vc, animated: true, completion: nil)
////                        let mainNavigationController = storyboardConsumer.instantiateViewController(withIdentifier: "consumerNavigationController") as! UINavigationController
////                        let vc = storyboardConsumer.instantiateViewController(withIdentifier: "ConsumerCategoryViewController") as! ConsumerCategoryViewController
////                        self.present(vc, animated: false, completion: {
////                            mainNavigationController.pushViewController(vc, animated: false)
////                        })
////                    }
//
//                } else {
//                    print("false")
//                }
//            }
//
////        }
////        else {
////            self.simpleAlert(title: APP_NAME, details: Internet_Connection_Alert)
////        }
//
//    }
}

@available(iOS 13.0, *)
extension signuppageViewController: ASAuthorizationControllerPresentationContextProviding {

    //For present window

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {

        return self.view.window!

    }

}

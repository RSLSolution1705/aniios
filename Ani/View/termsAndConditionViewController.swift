//
//  termsAndConditionViewController.swift
//  Ani
//
//  Created by RSL-01 on 08/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//
import UIKit
import WebKit

class termsAndConditionViewController: UIViewController,WKNavigationDelegate,UINavigationControllerDelegate,PopUpDoneDelegate{
  
    var webView: WKWebView!
    
    var viewActivityLarge : SHActivityView?
    var vcFor : ValidationPopupViewController? = nil
    
    override func loadView() {
      webView = WKWebView()
      webView.navigationDelegate = self
      view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        self.navigationItem.title = "Terms & Conditions"
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
   //     webView.delegate = self
    //    webView.loadRequest(NSURLRequest(url: NSURL(string: "http://99.81.230.36/ANI/ws/Privacy_Template_ANI_0119.html")! as URL) as URLRequest)
   //     webView.loadRequest(NSURLRequest(url: NSURL(string: "https://testtheaniapp.tk/ws/Privacy_Template_ANI_0119.html")! as URL) as URLRequest)
        
        let url = URL(string: "https://testtheaniapp.tk/ws/Privacy_Template_ANI_0119.html")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
        //http://www.swiftdeveloperblog.com"
        //webView.loadRequest(NSURLRequest(url: NSURL(string: "http://www.swiftdeveloperblog.com")! as URL) as URLRequest)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func PopUpOkClick(status: Bool) {
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @objc func backBtn(sender:UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    

}

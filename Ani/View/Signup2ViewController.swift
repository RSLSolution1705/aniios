//
//  Signup2ViewController.swift
//  Ani
//
//  Created by RSL-01 on 14/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//
//    confirmation_code=22&device_type=ios&devicetoken=ca9a1badd37f915d9381a1edecf509c0fbdfbbe2d918a67bce3a8bbc7b783581&email_id=ssdf&first_name=eer&last_name=errf&login_type=Email&mobile_number=12345676335&password=qwert&profile_image=43643%20bytes)




import UIKit
import Alamofire
import SVProgressHUD
import CoreData



class Signup2ViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate,PopUpDoneDelegate {
  
    @IBOutlet weak var firstNameTextfield: designable2textfield!
    @IBOutlet weak var lastNameTextfield: designable2textfield!
    @IBOutlet weak var emailTextfield: designableTextfield!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var joinPopup: UIView!
    @IBOutlet weak var blurview: UIVisualEffectView!
    @IBOutlet weak var mainBlurView: UIVisualEffectView!
    @IBOutlet var ConformationPopup: UIView!
    @IBOutlet weak var comformationtextfield: UITextField!
    @IBOutlet weak var lblJoin: UILabel!
    @IBOutlet weak var mobileTextfield: designableTextfield!
    @IBOutlet var proccedB: UIButton!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var showPassword: UIButton!
    
    var nameValue = ""
    var namevalue1 = ""
    let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
    @IBOutlet weak var btnProceed: UIButton!
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    var profile: UIImage?
    var account : String = ""
    let context = AppDelegate().persistentContainer.viewContext
    var effect : UIVisualEffect!
    var isFromPopup = false
    var randamNumber : String!
    var deviceTokan = ""
    var flagShowPassword = true
    var vcFor : ValidationPopupViewController? = nil
    //let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
    // tap.delegate = self // This is not required
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameValue = firstNameTextfield.text!
        
        self.hideKeyboardWhenTappedAround()
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        self.btnProceed.layer.cornerRadius = self.btnProceed.frame.size.height / 2.0
        self.btnProceed.clipsToBounds = true
        self.view.isUserInteractionEnabled = true
        effect = blurview.effect
        blurview.effect = nil
        self.navigationController?.isNavigationBarHidden = true
        deviceTokan  = getValueForKey(keyValue: kdeviceToken)
        print("\(deviceTokan)")
        
        randamNumber = String(arc4random_uniform(999999))
        print ("randam Number : \(randamNumber)")
        
        mobileTextfield.delegate = self
        emailTextfield.delegate = self
        firstNameTextfield.delegate = self
        lastNameTextfield.delegate = self
        passwordTextfield.delegate = self
        comformationtextfield.delegate = self
        
        
        print("Signup2ViewController")
        
        //       = imageView.layer.borderWidth = 1.0
        imageView.layer.masksToBounds = false
        //        imageView.layer.borderColor = UIColor.clear as! CGColor
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        //   imageView.layer.cornerRadius = i
        
        imageView.clipsToBounds = true
        
        joinButton.layer.cornerRadius = 5
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(Signup2ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Signup2ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - 80
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    fileprivate func NumFunc(_ ischeckValidation: inout Bool) {
        //  SVProgressHUD.show()
        self.stopSpinner()
        ischeckValidation  = false;
        
        self.vcFor?.modalTransitionStyle = .crossDissolve
        self.vcFor?.modalPresentationStyle = .overFullScreen
        self.vcFor?.isLogin = true
        self.vcFor?.actionFor = ""
        self.vcFor?.titleFor = "Please enter valid mobile number"
        self.present(self.vcFor!, animated: true, completion: nil)
    }
    
    
    @IBAction func resendButton(_ sender: Any) {
        //SignInWebservice()
        ResendConfirmationCode()
    }
    
    @IBAction func btnShowPassward(_ sender: Any) {
        
        
        showPassword.setImage(UIImage(named: "ic_custom_hide_new") , for: .normal)
        
        if flagShowPassword == true
        {
            showPassword.setImage(UIImage(named: "ic_custom_show_new") , for: .normal)
            //    showPassword.currentBackgroundImage =
            //     self.showPassword.setImage(UIImage(named: "eye_grey"))
            passwordTextfield.isSecureTextEntry =  false
        }else
        {
            showPassword.setImage(UIImage(named: "ic_custom_hide_new") , for: .normal)
            passwordTextfield.isSecureTextEntry = true
        }
        
        flagShowPassword = !flagShowPassword
        
        
        
        
    }
    
    
    
    @IBAction func conformButton(_ sender: Any) {
        
        if comformationtextfield.text == String(randamNumber)
        {
            comformationtextfield.resignFirstResponder()
            conformationWebservices()
        }
        else
        {
            comformationtextfield.resignFirstResponder()
            
            //            setDefaultValue(keyValue: kmsg, valueIs:  "plese enter correct code")
            //            var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ValidationPopupViewController")as? ValidationPopupViewController
            //            self.addChildViewController(pop!)
            //            pop!.view.frame = self.view.frame
            //            self.view.addSubview(pop!.view)
            //            pop?.didMove(toParentViewController: self)
            
            showDialogWithOneButton(viewControl: self, titleMsg: "ANI", msgTitle: "plese enter correct code")
        }
    }
    
    @IBAction func closeConformation1(_ sender: Any) {
        animateOut2()
    }
    
    
    @IBAction func closebutton(_ sender: Any) {
        animateOut()
    }
    
    @IBAction func joinCancelButton(_ sender: Any)
    {
        animateOut()
        
    }
    
    @IBAction func joinProceedButton(_ sender: Any) {
        
        
        //        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        //        let viewHeight = self.view.frame.size.height
        //        let popupHeight = self.joinPopup.frame.size.height
        //        self.ConformationPopup.frame = CGRect(x: 16, y: ((viewHeight + navigationBarHeight)/2)-(popupHeight/2), width: 290, height: 250)
        animateOut()
        var login = storyboard?.instantiateViewController(withIdentifier: "loginViewController")as? loginViewController
        
        self.navigationController?.pushViewController(login!, animated: true)
        self.stopSpinner()
        //        animateIn1()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func imageView(_ sender: Any) {
        
        isFromPopup = true
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let isCameraAvalible = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            
            if(isCameraAvalible)
            {
                self.takePhoto()
            }
            else
            {
                // showDialogWithOneButton(animated: true, viewControl: self, titleMsg: kErrorMsg, msgTitle: kScanQrCodeError)
            }
        })
        let saveAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.PhotoLibray()
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageView.image = info [UIImagePickerControllerOriginalImage]  as? UIImage
        profile = imageView.image
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func joinButton(_ sender: Any) {
        //  SVProgressHUD.show()
        self.startSpinner()
        
        emailTextfield.resignFirstResponder()
        mobileTextfield.resignFirstResponder()
        firstNameTextfield.resignFirstResponder()
        lastNameTextfield.resignFirstResponder()
        passwordTextfield.resignFirstResponder()
        validateLoginform()
    }
    
    
    func ResendConfirmationCode()
    {
        //        SVProgressHUD.show()
        self.startSpinner()
        let headers = ["Content-Type":"Application/json"]
        let _url = send_confirmation_code
        
        let parameters: [String: Any] = [
            "email_id": emailTextfield.text!,
            "confirmation_code": randamNumber]
        print(parameters)
        Alamofire.request(_url,method: .post,parameters: parameters,encoding: URLEncoding.queryString,headers: headers).responseJSON { (response) in
            
            if response.result.isSuccess {
                print("SUKCES with \(response)")
                
                if (response.result != nil) {
                    let jsonDic = response.result.value as! NSDictionary
                    print(jsonDic)
                    if let res = jsonDic.value(forKey: "result") as? String
                    {
                        if res == "failed"
                        {
                            //popup
                            //  SVProgressHUD.show()
                            self.stopSpinner()
                            let msg = jsonDic.value(forKey: "msg") as? String
                            _ = response.result.error?.localizedDescription
                            print(response.result.error?.localizedDescription ?? " ")
                            
                            //                                var msg = jsonDic.object(forKey: "msg")as? String
                            
                            
                            self.vcFor?.modalTransitionStyle = .crossDissolve
                            self.vcFor?.modalPresentationStyle = .overFullScreen
                            self.vcFor?.isLogin = true
                            self.vcFor?.actionFor = ""
                            self.vcFor?.titleFor =  msg!
                            self.present(self.vcFor!, animated: true, completion: nil)
                            
                            
                            
                            
                            
                            //                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:msg ?? "Something went wrong!")
                        }else
                        {
                            //  SVProgressHUD.show()
                            self.startSpinner()
                            self.lblJoin.text = "Registration Successful. Confirmation code has been sent on your email.Please check"
                            self.animateIn()
                            //                            let msg = jsonDic.value(forKey: "msg") as? String
                            //                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:msg ?? "Something went wrong!")
                        }
                    }
                }
            }
                
            else if response.result.isFailure
                
            {
                //                                let httpError: NSError = response.result.error! as NSError
                //                                let statusCode = httpError.code
                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                //                                print(error)
                //
                var str = response.result.error?.localizedDescription
                print(response.result.error?.localizedDescription)
                
                if str ==
                    "The Internet connection appears to be offline."
                {
                    str = "You are offline please check your internet connection"
                }
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  str!
                self.present(self.vcFor!, animated: true, completion: nil)
                
                
                
                //                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                //                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signUpPopupViewController")as? signUpPopupViewController
                //                self.addChildViewController(pop!)
                //                pop!.view.frame = self.view.frame
                //                self.view.addSubview(pop!.view)
                //                pop?.didMove(toParentViewController: self)
                
                //                                completion(dic,0)
                
            }
            
            
        }
        //         //  SVProgressHUD.show()
        //  self.stopSpinner()
        //        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        //
    }
    
    
    func conformationWebservices()
    {
        //  SVProgressHUD.show()
        self.startSpinner()
        let headers = ["Content-Type":"Application/json"]
        let _url = email_confirmation
        
        let imageData = UIImageJPEGRepresentation((imageView.image!), 0.5)
        let base64String = imageData?.base64EncodedString()
        
        let parameters: [String: Any] = [
            "account_id": getValueForKey(keyValue: kregisterAccountId),
            "consumer_login_type": getValueForKey(keyValue: registerconsumer_login_type),]
        print(parameters)
        Alamofire.request(_url,method: .post,parameters: parameters,encoding: URLEncoding.queryString,headers: headers).responseJSON { (response) in
            
            if response.result.isSuccess {
                print("SUKCES with \(response)")
                //  SVProgressHUD.show()
                //     self.stopSpinner()
                if (response.result != nil) {
                    let jsonDic = response.result.value as! NSDictionary
                    print(jsonDic)
                    if let res = jsonDic.value(forKey: "result") as? String
                    {
                        if res == "failed"
                        {
                            //popup
                            //  SVProgressHUD.show()
                            self.stopSpinner()
                            let msg = jsonDic.value(forKey: "msg") as? String
                            _ = response.result.error?.localizedDescription
                            print(response.result.error?.localizedDescription ?? " ")
                            
                            self.vcFor?.modalTransitionStyle = .crossDissolve
                            self.vcFor?.modalPresentationStyle = .overFullScreen
                            self.vcFor?.isLogin = true
                            self.vcFor?.actionFor = ""
                            self.vcFor?.titleFor =  msg!
                            self.present(self.vcFor!, animated: true, completion: nil)
                            
                            
                            
                            
                            //                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:msg ?? "Something went wrong!")
                        }else
                        {
                            //  SVProgressHUD.show()
                            self.stopSpinner()
                            let mutableDic = NSMutableDictionary()
                            UserDefaults.standard.set(true, forKey: "isUserLog")
                            let temp = loginClass()
                            
                            temp.account_id = jsonDic.object(forKey: "account_id") as? String
                            
                            UserDefaults.standard.synchronize()
                            
                            print("\( temp.account_id)")
                            
                            let c1 = NSEntityDescription.insertNewObject(forEntityName: "Ani", into: self.context)as! Ani
                            //                                c1.setValue(temp.account_id, forKey: accId)
                            c1.accId = temp.account_id
                            
                            // txtName.text=""
                            // txtNumber.text=""
                            
                            print(c1)
                            
                            try! self.context.save()
                            
                            
                            
                            
                            //                                temp.date_of_birth = jsonDic.object(forKey: "date_of_birth")as! String
                            temp.first_name = jsonDic.object(forKey: "first_name") as! String
                            var firstName: String = temp.first_name
                            
                            setDefaultValue(keyValue: kfirstName, valueIs: firstName)
                            
                            //                                            getValueForKey(keyValue: T##String)
                            temp.last_name = jsonDic.object(forKey: "last_name") as! String
                            
                            setDefaultValue(keyValue: klastName, valueIs: temp.last_name)
                            
                            temp.email_id = jsonDic.object(forKey: "email_id") as! String
                            
                            setDefaultValue(keyValue: kemail, valueIs: temp.email_id)
                            
                            var img = jsonDic.object(forKey: "profile_image") as! String
                            let defaults = UserDefaults.standard
                            if (img != nil && img != "")
                            {
                                var imageData = try! Data.init(contentsOf: URL(string: img)!)
                                temp.profile_image = UIImage(data: imageData)
                                
                                //                                                self.appdelegate.profile_image =  temp.profile_image
                                
                                //var image = temp.profile_image
                                //defaults.set(temp.profile_image, forKey: "picture")
                                
                            }else
                            {
                                img = ""
                                
                            }
                            
                            
                            defaults.set(img, forKey: "picture")
                            defaults.synchronize()
                            //                                            var imgData = UIImageJPEGRepresentation(temp.profile_image!, 1)
                            //
                            
                            
                            
                            //                                            setDefaultValue(keyValue: kimageview as! String, valueIs: imgData)
                            temp.mobile_number = jsonDic.object(forKey: "mobile_number") as! String
                            
                            setDefaultValue(keyValue: kmobNumber, valueIs:  temp.mobile_number)
                            if (temp.date_of_birth) == nil
                                
                            {
                                setDefaultValue(keyValue: KDOB, valueIs: "")
                                
                                
                            }
                            else
                            {
                                temp.date_of_birth = jsonDic.object(forKey: "Date")as! String
                                setDefaultValue(keyValue: KDOB, valueIs: temp.date_of_birth)
                                
                            }
                            
                            temp.role = jsonDic.object(forKey: "role") as! String
                            setDefaultValue(keyValue: krole, valueIs:  temp.role )
                            //
                            //                                temp.home_number = jsonDic.object(forKey: "home_number") as! String
                            let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                            self.navigationController?.pushViewController(secondVC, animated: true)
                        }
                    }
                }
            }
                
            else if response.result.isFailure
                
            {
                //                                let httpError: NSError = response.result.error! as NSError
                //                                let statusCode = httpError.code
                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                //                                print(error)
                //
                var str = response.result.error?.localizedDescription
                print(response.result.error?.localizedDescription)
                
                if str ==
                    "The Internet connection appears to be offline."
                {
                    str = "You are offline please check your internet connection"
                }
                
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  str!
                self.present(self.vcFor!, animated: true, completion: nil)
                
                
                
                
            }
            
            
        }
        
    }
    
    
    func SignInWebservice()
    {
        let myUrl = NSURL(string: consumer_registration1);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = ["first_name": firstNameTextfield.text!,"last_name": lastNameTextfield.text!,"email_id": emailTextfield.text!,"password":passwordTextfield.text!,"mobile_number":mobileTextfield.text!,"devicetoken": deviceTokan,"device_type": kDeviceType,"login_type": kLoginTypeEmail,"confirmation_code": randamNumber!]
        
        print(param)
        
        let boundary = generateBoundaryString()
        //        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = UIImageJPEGRepresentation(imageView.image!, 0.5)
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "profile_image", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
               
                self.stopSpinner()
                
               
                
                DispatchQueue.main.async {
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                }
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
//                self.vcFor?.actionFor = ""
//                self.vcFor?.titleFor =  error?.localizedDescription ?? "Something went wrong!!"
//                self.present(self.vcFor!, animated: true, completion: nil)
                
                
                
             
                
                return
            }
            
            print("******* response = \(String(describing: response))")
            
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print(jsonDic as Any)
                var temp2 = response
                
                
                
                
                let temp = signUpResponse()
                let string = jsonDic?.object(forKey: "msg")as? String!
                DispatchQueue.main.async {
                    self.lblJoin.text = "Thank you for registering with ANI." + "\n" + "You are just one step away from activating your account, we have sent an email with verification link  on your email, please check and verify it."
                    //"Registration Successful. Confirmation code has been sent on your email.Please check"
                }
                if let res = jsonDic!.value(forKey: "result") as? String
                {
                    //  let temp = signUpResponse()
                    if res == "failed"
                    {
                        //  SVProgressHUD.show()
                        
                        DispatchQueue.main.async {
                            
                            
                            self.stopSpinner()
                            let str = jsonDic?.object(forKey: "msg")as? String
                            print(str as Any)
                            
                            self.vcFor?.modalTransitionStyle = .crossDissolve
                            self.vcFor?.modalPresentationStyle = .overFullScreen
                            self.vcFor?.isLogin = true
                            self.vcFor?.actionFor = ""
                            self.vcFor?.titleFor =  str!
                            self.present(self.vcFor!, animated: true, completion: nil)
                        }
                        //                        setDefaultValue(keyValue: kmsg, valueIs: str!)
                        //                        var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signUpPopupViewController")as? signUpPopupViewController
                        //                        self.addChildViewController(pop!)
                        //                        pop!.view.frame = self.view.frame
                        //                        self.view.addSubview(pop!.view)
                        //                        pop?.didMove(toParentViewController: self)
                        //                        print(error)
                        //
                        
                        
                        
                        //                       showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:string ?? "Somthing went wrong")
                    }
                    else
                        
                    {
                        
                        
                        let msg =  jsonDic!.value(forKey: "msg") as? String
                        //showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:msg ?? "msg not found")
                        
                        var temp1 = response
                        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("****** response data = \(responseString!)")
                        //if(jsonDic?.value(forKey: "type") as! String = "Send_Code_By_Email"){
                        temp.account_id = jsonDic?.value(forKey: "account_id") as! String
                        
                        self.account = temp.account_id
                        UserDefaults.standard.set(self.account, forKey: kregisterAccountId)
                        UserDefaults.standard.synchronize()
                        // setDefaultValue(keyValue: kregisterAccountId, valueIs:   ((jsonDic?.value(forKey: "account_id") as? String)!))
                        temp.consumer_login_type = (jsonDic?.value(forKey: "consumer_login_type") as! String)
                        setDefaultValue(keyValue:registerconsumer_login_type, valueIs:  (jsonDic?.value(forKey: "consumer_login_type") as! String))
                        
                        temp.type = jsonDic?.value(forKey: "type") as! String
                        
                        setDefaultValue(keyValue:registertype, valueIs:  jsonDic?.value(forKey: "type") as! String)
                        
                        
                        DispatchQueue.main.async {
                            let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
                            let viewHeight = self.view.frame.size.height
                            let popupHeight = self.joinPopup.frame.size.height
                            self.joinPopup.frame = CGRect(x: 16, y: ((viewHeight + navigationBarHeight)/2)-(popupHeight/2), width: 280, height: 300)
                            self.animateIn()
                            //  SVProgressHUD.show()
                            self.stopSpinner()
                        }
                        
                        //setDefaultValue(keyValue:krole, valueIs:  jsonDic?.value(forKey: "role") as! String)
                        
                    }
                }
                
            } catch
            {
                //  SVProgressHUD.show()
                //  self.stopSpinner()
                
                
                
                print(error)
            }
            
        }
        
        task.resume()
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func SignInWebservice1() {
        
//        let url = "http://52.202.56.230/ANI/ws/consumer_registration1.php"
        let url = consumer_registration1
        
        let para : [String: String] = ["first_name": firstNameTextfield.text!,
                                       "last_name": lastNameTextfield.text!,
                                       "email_id": emailTextfield.text!,
                                       "password": passwordTextfield.text!,
                                       "mobile_number":mobileTextfield.text!,
                                       "devicetoken": deviceTokan,
                                       "device_type": kDeviceType,
                                       "login_type": kLoginTypeEmail,
                                       "confirmation_code": randamNumber!]
        //“profile_image” - Multipart data
        print(para)
        
        
        print(url)
        //        let image = UIImage.init(named: (imageView!.image))
        //        var profileImg = imageView.image
        let imageData = UIImageJPEGRepresentation(imageView.image!, 1)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(self.imageView.image!, 1) {
                multipartFormData.append(imageData, withName: "profile_image")
            }
            
            for (key, value) in para {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url)
        { (result) in
            switch result {
            case .success(let upload, _,_ ):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })
                
                upload.responseJSON { response in
                    //print response.result
                    //                    self.hideLoading()
                    print(response.result)
                    
                    if response.result.value != nil {
                        print("SUKCES with \(response)")
                        
                        if (response.result != nil)
                        {
                            let jsonDic = response.result.value as! NSDictionary
                            print(jsonDic)
                            if let res = jsonDic.value(forKey: "result") as? String
                            {
                                if res == "failed"
                                {
                                    
                                    self.proccedB.isHidden = true
                                    //popup
                                    //                                             //  SVProgressHUD.show()
                                    self.stopSpinner()
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    var msg = jsonDic.object(forKey: "msg")as? String
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  msg!
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                                    
                                    //                                    var str = response.result.error?.localizedDescription
                                    //                                    print(response.result.error?.localizedDescription)
                                    //
                                    //
                                    //                                    showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle: msg!)
                                }else
                                {
                                    
                                    //                                             //  SVProgressHUD.show()
                                    self.stopSpinner()
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    
                                    let temp = signUpResponse()
                                    
                                    var string = jsonDic.object(forKey: "msg")as? String
                                    
                                    self.lblJoin.text = string
                                    
                                    temp.account_id = (jsonDic.object(forKey: "account_id")as! String)
                                    
                                    self.account = temp.account_id
                                    //                                        setDefaultValue(keyValue:registerAccountNo, valueIs:  temp.account_id)
                                    
                                    temp.consumer_login_type = jsonDic.object(forKey: "consumer_login_type")as! String
                                    setDefaultValue(keyValue:registerconsumer_login_type, valueIs:  temp.consumer_login_type)
                                    
                                    temp.type = jsonDic.object(forKey: "type")as! String
                                    
                                    setDefaultValue(keyValue:registertype, valueIs:  temp.type)
                                    
                                    
                                    
                                    let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
                                    let viewHeight = self.view.frame.size.height
                                    let popupHeight = self.joinPopup.frame.size.height
                                    self.joinPopup.frame = CGRect(x: 16, y: ((viewHeight + navigationBarHeight)/2)-(popupHeight/2), width: 290, height: 250)
                                    self.animateIn()
                                    //                                            self.view.addSubview(self.joinPopup)
                                    //"Password reset link sent to your email.";
                                    
                                }
                            }
                        }
                    }
                    
                }
            case .failure(let encodingError):
                
                //             var str = encodingError.result.error?.localizedDescription
                
                
                DispatchQueue.main.async {
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                }
                
                
                //                showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "Error", msgTitle:encodingError.localizedDescription)
                //print encodingError.description
                //                self.hideLoading()
                //                self.simpleAlert(title: “Error”, details: encodingError.localizedDescription)
                //                completion(false, SignUpDetails())
                
            }
        }
        
    }
    
    
    
    
    func SignInWebservice3()
    {
        let headers = ["Content-Type":"Application/json"]
//        let _url = "http://52.202.56.230/ANI/ws/consumer_registration.php"
        let _url = consumer_registration
        let imageData = UIImageJPEGRepresentation(imageView.image!, 1)
        
        if(imageData==nil)  { return; }
        
        //    var imageData = UIImageJPEGRepresentation((imageView.image)!, 0.9)
        //    var base64String = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.fromRaw(0)!)
        //
        
        //    let imageData = UIImageJPEGRepresentation((imageView.image!), 0.9)
        //    let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)
        //            print(base64String)
        
        let parameters: [String: Any] = [
            "first_name": firstNameTextfield.text!,
            "last_name": lastNameTextfield.text!,
            "email_id": emailTextfield.text!,
            "password": passwordTextfield.text!,
            "mobile_number":mobileTextfield.text!,
            "devicetoken": deviceTokan,
            "device_type": kDeviceType,
            "login_type": kLoginTypeEmail,
            "confirmation_code": randamNumber!,
            "profile_image" : imageData!
        ]
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            //popup
                                            //                                             //  SVProgressHUD.show()
                                            self.stopSpinner()
                                            var str = response.result.error?.localizedDescription
                                            print(response.result.error?.localizedDescription)
                                            
                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "ANI", msgTitle:"Entered email is not registered with us")
                                        }else
                                        {
                                            
                                            //                                             //  SVProgressHUD.show()
                                            self.stopSpinner()
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            
                                            var temp = signUpResponse()
                                            
                                            var string = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.lblJoin.text = "Registration Successful.Verification link has been sent on your email.Please check"//string
                                            
                                            temp.account_id = jsonDic.object(forKey: "account_id")as! String
                                            
                                            self.account = temp.account_id
                                            //                                        setDefaultValue(keyValue:registerAccountNo, valueIs:  temp.account_id)
                                            
                                            temp.consumer_login_type = jsonDic.object(forKey: "consumer_login_type")as! String
                                            setDefaultValue(keyValue:registerconsumer_login_type, valueIs:  temp.consumer_login_type)
                                            
                                            temp.type = jsonDic.object(forKey: "type")as! String
                                            
                                            setDefaultValue(keyValue:registertype, valueIs:  temp.type)
                                            
                                            
                                            
                                            let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
                                            let viewHeight = self.view.frame.size.height
                                            let popupHeight = self.joinPopup.frame.size.height
                                            self.joinPopup.frame = CGRect(x: 16, y: ((viewHeight + navigationBarHeight)/2)-(popupHeight/2), width: 290, height: 250)
                                            self.animateIn()
                                            //                                            self.view.addSubview(self.joinPopup)
                                            //"Password reset link sent to your email.";
                                            
                                        }
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                            {
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                //                                let httpError: NSError = response.result.error! as NSError
                                //                                let statusCode = httpError.code
                                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                                //                                print(error)
                                //
                                print(response.result.error?.localizedDescription)
                                
                                //                                completion(dic,0)
                                
                            }
        }
    }
    
    func validateLoginform() -> Bool {
        var ischeckValidation = true;
        do{
            self.stopSpinner()
            
            var showMsg = "";
            
            
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
            
            let isValudateemailAddress = isValidEmail(testStr: (self.emailTextfield?.text)!);
            // let isValudateemailAddressCompany = isValidEmail(testStr: (self.txtfieldCompanyEmail?.text)!);
            
            
            
            
            
            if((self.firstNameTextfield?.text?.characters.count)! <= 0 )
            {
                //  SVProgressHUD.show()
                self.stopSpinner()
                ischeckValidation  = false;
                
                
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  " Please enter first name"
                self.present(self.vcFor!, animated: true, completion: nil)
                
                
            }
            else if((self.firstNameTextfield?.text?.characters.count)! <= 1)
            {
                //  SVProgressHUD.show()
                self.stopSpinner()
                ischeckValidation  = false;
                
                
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  "Please enter more than one alphabet in first name"
                self.present(self.vcFor!, animated: true, completion: nil)
                
                
                
                
            }
//            else
//                if((self.firstNameTextfield?.text?.characters.count)! <= 2 )
//            {
//                //  SVProgressHUD.show()
//                self.stopSpinner()
//                ischeckValidation  = false;
//
//
//                self.vcFor?.modalTransitionStyle = .crossDissolve
//                self.vcFor?.modalPresentationStyle = .overFullScreen
//                self.vcFor?.isLogin = true
//                self.vcFor?.actionFor = ""
//                self.vcFor?.titleFor =  " Please enter more than one character in first name"
//                self.present(self.vcFor!, animated: true, completion: nil)
//
//
//            }
            else
                if((self.firstNameTextfield?.text?.characters.count)! >= 30 )
                {
                    //  SVProgressHUD.show()
                    self.stopSpinner()
                    ischeckValidation  = false;
                    
                    
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  "Please enter maximum 30 characters for first name"
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
                    
                    
                }
                else if(regex.firstMatch(in: ((self.firstNameTextfield?.text!)!), options: [], range: NSMakeRange(0, (self.firstNameTextfield?.text?.characters.count)!)) != nil )
                {
                    //  SVProgressHUD.show()
                    self.stopSpinner()
                    ischeckValidation  = false;
                    
                    
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =   "Please enter only characters in first name"
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
                    
                    
                    
                }
                    
//                else if(self.firstNameTextfield?.text?.containsOnlyCharactersIn("0123456789"))!
//                {
//                    //  SVProgressHUD.show()
//                    self.stopSpinner()
//                    ischeckValidation  = false;
//
//
//                    self.vcFor?.modalTransitionStyle = .crossDissolve
//                    self.vcFor?.modalPresentationStyle = .overFullScreen
//                    self.vcFor?.isLogin = true
//                    self.vcFor?.actionFor = ""
//                    self.vcFor?.titleFor =   "Please enter only character"
//                    self.present(self.vcFor!, animated: true, completion: nil)
//
//
//
//
//                }
                else if((self.lastNameTextfield?.text?.characters.count)! <= 0 )
                {
                    //  SVProgressHUD.show()
                    self.stopSpinner()
                    ischeckValidation  = false;
                    
                    
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  " Please enter last name"
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
                    
                    
                }
//                else
//                    if((self.lastNameTextfield?.text?.characters.count)! <= 2 )
//                    {
//                        //  SVProgressHUD.show()
//                        self.stopSpinner()
//                        ischeckValidation  = false;
//
//
//                        self.vcFor?.modalTransitionStyle = .crossDissolve
//                        self.vcFor?.modalPresentationStyle = .overFullScreen
//                        self.vcFor?.isLogin = true
//                        self.vcFor?.actionFor = ""
//                        self.vcFor?.titleFor =  " Please enter more than one character in last name"
//                        self.present(self.vcFor!, animated: true, completion: nil)
//
//
//                    }
                    else if(regex.firstMatch(in: ((self.lastNameTextfield?.text!)!), options: [], range: NSMakeRange(0, (self.lastNameTextfield?.text?.characters.count)!)) != nil )
                    {
                        //  SVProgressHUD.show()
                        self.stopSpinner()
                        ischeckValidation  = false;
                        
                        
                        self.vcFor?.modalTransitionStyle = .crossDissolve
                        self.vcFor?.modalPresentationStyle = .overFullScreen
                        self.vcFor?.isLogin = true
                        self.vcFor?.actionFor = ""
                        self.vcFor?.titleFor =   "Please enter only characters in last name"
                        self.present(self.vcFor!, animated: true, completion: nil)
                        
                        
                        
                        
                    }
                    else if((self.lastNameTextfield?.text?.characters.count)! <= 0)
                    {
                        //  SVProgressHUD.show()
                        self.stopSpinner()
                        ischeckValidation  = false;
                        self.vcFor?.modalTransitionStyle = .crossDissolve
                        self.vcFor?.modalPresentationStyle = .overFullScreen
                        self.vcFor?.isLogin = true
                        self.vcFor?.actionFor = ""
                        self.vcFor?.titleFor =   "Please enter last name"
                        self.present(self.vcFor!, animated: true, completion: nil)
                        
                        
                        
                    }
                    else if((self.lastNameTextfield?.text?.characters.count)! <= 1)
                    {
                        //  SVProgressHUD.show()
                        self.stopSpinner()
                        ischeckValidation  = false;
                        
                        
                        self.vcFor?.modalTransitionStyle = .crossDissolve
                        self.vcFor?.modalPresentationStyle = .overFullScreen
                        self.vcFor?.isLogin = true
                        self.vcFor?.actionFor = ""
                        self.vcFor?.titleFor =  "Please enter more than one alphabet in last name"
                        self.present(self.vcFor!, animated: true, completion: nil)
                        
                        
                        
                        
                        
                    }
//                    else if(self.lastNameTextfield?.text?.containsOnlyCharactersIn("0123456789"))!
//                    {
//                        //  SVProgressHUD.show()
//                        self.stopSpinner()
//                        ischeckValidation  = false;
//                        
//                        
//                        self.vcFor?.modalTransitionStyle = .crossDissolve
//                        self.vcFor?.modalPresentationStyle = .overFullScreen
//                        self.vcFor?.isLogin = true
//                        self.vcFor?.actionFor = ""
//                        self.vcFor?.titleFor =  "Please enter only character"
//                        self.present(self.vcFor!, animated: true, completion: nil)
//                        
//                        
//                        
//                        
//                    }
                    
                    
                else if((self.emailTextfield?.text?.characters.count)! <= 0)
                {
                    //  SVProgressHUD.show()
                    self.stopSpinner()
                    ischeckValidation  = false;
                    
                    
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  "Please enter email id "
                    self.present(self.vcFor!, animated: true, completion: nil)
                    
                    
                    
                }
                    else if !(isValidEmail(testStr: (self.emailTextfield?.text)!))
                    {
                        //  SVProgressHUD.show()
                        self.stopSpinner()
                        ischeckValidation  = false;
                        
                        
                        
                        self.vcFor?.modalTransitionStyle = .crossDissolve
                        self.vcFor?.modalPresentationStyle = .overFullScreen
                        self.vcFor?.isLogin = true
                        self.vcFor?.actionFor = ""
                        self.vcFor?.titleFor =   "please enter valid mail id"
                        self.present(self.vcFor!, animated: true, completion: nil)
                        
                        
                        
                        
                    }
                  
                    
                      
                        else
                            if((self.passwordTextfield?.text?.characters.count)! <= 0 )
                            {
                                //  SVProgressHUD.show()
                                self.stopSpinner()
                                ischeckValidation  = false;
                                
                                
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  " Please enter password"
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                            }
                            else
                                
                                if ((self.passwordTextfield?.text?.characters.count)! > 50)
                                {
                                    //  SVProgressHUD.show()
                                    self.stopSpinner()
                                    ischeckValidation  = false;
                                    
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor = "Enter password length between 4 to 30"
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                     self.passwordTextfield.text = ""
                                    
                                    
                                }
                                else
                                    if ((self.passwordTextfield?.text?.characters.count)! < 4)
                                    {
                                        //  SVProgressHUD.show()
                                        self.stopSpinner()
                                        ischeckValidation  = false;
                                        
                                        
                                        self.vcFor?.modalTransitionStyle = .crossDissolve
                                        self.vcFor?.modalPresentationStyle = .overFullScreen
                                        self.vcFor?.isLogin = true
                                        self.vcFor?.actionFor = ""
                                        self.vcFor?.titleFor =  "Enter password length between 4 to 30"
                                        self.present(self.vcFor!, animated: true, completion: nil)
                                         self.passwordTextfield.text = ""
                                        
                                        
                                        
                                        
                                    }
                                   
                                        
                                
        
//                                    else if((self.mobileTextfield?.text?.characters.count)! <= 0)
//                                    {
//                                        //  SVProgressHUD.show()
//                                        self.stopSpinner()
//                                        ischeckValidation  = false;
//
//
//                                        self.vcFor?.modalTransitionStyle = .crossDissolve
//                                        self.vcFor?.modalPresentationStyle = .overFullScreen
//                                        self.vcFor?.isLogin = true
//                                        self.vcFor?.actionFor = ""
//                                        self.vcFor?.titleFor =  "Please enter mobile number"
//                                        self.present(self.vcFor!, animated: true, completion: nil)
//
//
//
//                                    }
//                                    else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("0"))!
//                                    {
//                                        NumFunc(&ischeckValidation)
//                                    }
//                                    else
//
//                                        if ((self.mobileTextfield?.text?.characters.count)! >= 14)
//                                        {
//                                            //  SVProgressHUD.show()
//                                            self.stopSpinner()
//                                            ischeckValidation  = false;
//
//
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
//                                            self.vcFor?.titleFor =  "Please enter minimum 8 to 14 digit in mobile number"
//                                            self.present(self.vcFor!, animated: true, completion: nil)
//
//                                        }
                                        else
                                            if( (self.mobileTextfield?.text != "") && (self.mobileTextfield?.text?.characters.count)! < 8)
                                            {
                                                //  SVProgressHUD.show()
                                                self.stopSpinner()
                                                ischeckValidation  = false;

                                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                                self.vcFor?.isLogin = true
                                                self.vcFor?.actionFor = ""
                                                self.vcFor?.titleFor = "Please enter minimum 8 to 14 digit in mobile number"
                                                self.present(self.vcFor!, animated: true, completion: nil)



                                            }
//                                            else
//                                                if ((self.mobileTextfield.text)! == nil)
//                                                {
//                                                    //  SVProgressHUD.show()
//                                                    self.stopSpinner()
//                                                    ischeckValidation  = false;
//
//                                                    self.vcFor?.modalTransitionStyle = .crossDissolve
//                                                    self.vcFor?.modalPresentationStyle = .overFullScreen
//                                                    self.vcFor?.isLogin = true
//                                                    self.vcFor?.actionFor = ""
//                                                    self.vcFor?.titleFor = "Please enter mobile number"
//                                                    self.present(self.vcFor!, animated: true, completion: nil)
//
//
//
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn(" "))!
//                                                {self.stopSpinner()
//                                                    ischeckValidation  = false;
//
//                                                    self.vcFor?.modalTransitionStyle = .crossDissolve
//                                                    self.vcFor?.modalPresentationStyle = .overFullScreen
//                                                    self.vcFor?.isLogin = true
//                                                    self.vcFor?.actionFor = ""
//                                                    self.vcFor?.titleFor = "Please enter mobile number"
//                                                    self.present(self.vcFor!, animated: true, completion: nil)
//                                                }
//
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("0"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("1"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("2"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("3"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("4"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("5"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("6"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("7"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("8"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//                                                else if(self.mobileTextfield?.text?.containsOnlyCharactersIn("9"))!
//                                                {
//                                                    NumFunc(&ischeckValidation)
//                                                }
//
                                                
                                                    
                                                else
                                                {
                                                    DispatchQueue.main.async
                                                        {
                                                            if Reachability.isConnectedToNetwork() {
                                                                //  SVProgressHUD.show()
                                                                self.startSpinner()
                                                                self.SignInWebservice()
                                                            }
                                                            else {
                                                                //  SVProgressHUD.show()
                                                                  self.stopSpinner()
                                                                DispatchQueue.main.async {
                                                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                                                self.vcFor?.isLogin = true
                                                                self.vcFor?.actionFor = ""
                                                                self.vcFor?.titleFor =  Internet_Connection_Alert
                                                                self.present(self.vcFor!, animated: true, completion: nil)
                                                                }
                                                                
                                                            }
                                                            
                                                            
                                                    }
            }
            
            
            
        } catch { }
        return ischeckValidation;
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParentViewController: nil)
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == firstNameTextfield{
            guard let textFieldText = firstNameTextfield.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
      
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 30
        }
        if textField == firstNameTextfield {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
            
            
        }

            
        else if textField == lastNameTextfield {
            guard let textFieldText = lastNameTextfield.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 30
        }
        else if textField == emailTextfield{
            guard let textFieldText = emailTextfield.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 50
        }
        else if textField == passwordTextfield{
            guard let textFieldText = passwordTextfield.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 30
        }
        else if textField == mobileTextfield{
            guard let textFieldText = mobileTextfield.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 14
        }
            
        else {
            return true
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension Signup2ViewController {
    func takePhoto()
    {
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func PhotoLibray()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate =  self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension Signup2ViewController {
    func animateIn()
    {
        

        
        self.blurview.isHidden = false
        self.mainBlurView.isHidden = false
        //        self.extraView.isHidden = false
        self.view.addSubview(joinPopup)
        joinPopup.center = self.view.center
        joinPopup.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        joinPopup.alpha = 0
        UIView.animate(withDuration: 0.1) {
            self.blurview.effect = self.effect
            self.joinPopup.alpha = 1
            self.joinPopup.transform = CGAffineTransform.identity
        }
    }
    func animateOut()
    {
        self.blurview.isHidden = true
        self.mainBlurView.isHidden = true
        //        self.extraView.isHidden = true
        UIView.animate(withDuration: 0.1, animations: {
            self.blurview.effect = nil
            self.joinPopup.alpha = 0
            self.joinPopup.transform = CGAffineTransform.identity
        }) { (success: Bool) in
            self.joinPopup.removeFromSuperview()
//            self.ConformationPopup.removeFromSuperview()
        }
        
        
    }
    
    
    func animateIn1()
    {
        self.blurview.isHidden = false
        self.mainBlurView.isHidden = false
        //        self.extraView.isHidden = false
        self.view.addSubview(ConformationPopup)
        ConformationPopup.center = self.view.center
        ConformationPopup.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        ConformationPopup.alpha = 0
        UIView.animate(withDuration: 0.1) {
            self.blurview.effect = self.effect
            self.ConformationPopup.alpha = 1
            self.ConformationPopup.transform = CGAffineTransform.identity
        }
    }
    func animateOut2()
    {
        self.blurview.isHidden = true
        self.mainBlurView.isHidden = true
        //        self.extraView.isHidden = true
        UIView.animate(withDuration: 0.1, animations: {
            self.blurview.effect = nil
            self.ConformationPopup.alpha = 0
            self.ConformationPopup.transform = CGAffineTransform.identity
        }) { (success: Bool) in
            self.ConformationPopup.removeFromSuperview()
        }
        
        
    }
}
extension String {
    
    // Returns true if the string has at least one character in common with matchCharacters.
    func containsCharactersIn(_ matchCharacters: String) -> Bool {
        let characterSet = CharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet) != nil
    }
    
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(_ matchCharacters: String) -> Bool {
        let disallowedCharacterSet = CharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }
    
    // Returns true if the string has no characters in common with matchCharacters.
    func doesNotContainCharactersIn(_ matchCharacters: String) -> Bool {
        let characterSet = CharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet) == nil
    }
    
    // Returns true if the string represents a proper numeric value.
    // This method uses the device's current locale setting to determine
    // which decimal separator it will accept.
    func isNumeric() -> Bool
    {
        let scanner = Scanner(string: self)
        
        // A newly-created scanner has no locale by default.
        // We'll set our scanner's locale to the user's locale
        // so that it recognizes the decimal separator that
        // the user expects (for example, in North America,
        // "." is the decimal separator, while in many parts
        // of Europe, "," is used).
        scanner.locale = Locale.current
        
        return scanner.scanDecimal(nil) && scanner.isAtEnd
    }
    
}


extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

//func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//    if string == ""
//    {
//        /* if textField == promocodeTF
//         {
//         promo = ""
//         self.applyBtn.backgroundColor = self.colorWithHexString("#069E0E")
//         self.promocodeTF.textColor = UIColor.black
//         self.applyBtn.setTitle("Apply", for: UIControlState.normal)
//         self.applyBtn.isEnabled = true
//         }*/
//        return true
//    }
//
//    if string == txtfieldUserName
//    {
//        if (txtfieldUserName.utf16.count) > 13
//        {
//            return false
//        }
//        else
//        {
//            if string.containsOnlyCharactersIn("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
//            {
//                return true
//            }
//            else
//                if string.doesNotContainCharactersIn("@#$%&*()^<>!±")
//
//                {
//                    return true
//                }
//                else
//                {
//                    return false
//            }
//
//        }
//    }
//        //  txtfieldEmail
//    else if string == txtfieldEmail
//    {
//        if (txtfieldEmail.utf16.count) > 19
//        {
//            return false
//        }
//        else
//        {
//            return true
//        }
//    }
//
//    else if string == txtfieldPhoneNo
//    {
//        if (txtfieldPhoneNo.utf16.count) > 14
//        {
//            return false
//        }
//        else
//        {
//            if string.containsOnlyCharactersIn("0123456789")
//            {
//                return true
//            }
//
//            else
//            {
//
//                return false
//            }
//        }
//    }
//        //        else if string == txtfieldpassword
//        //        {
//        //            if (txtfieldpassword.utf16.count) > 15
//        //            {
//        //                return false
//        //            }
//        //            else
//        //            {
//        //                return true
//        //                /*if string.containsOnlyCharactersIn("0123456789")
//        //                 {
//        //                 return true
//        //                 }
//        //                 else
//        //                 {
//        //
//        //                 return false
//        //                 }*/
//        //
//        //            }
//
//
//        //}
//    else if isValidEmail(testStr: txtfieldEmail)
//    {
//        return true
//    }else
//    {
//        return false
//    }
//
//    return true
//}
//
//func isValidEmail(testStr:String) -> Bool {
//    print("validate emilId: \(testStr)")
//    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,64}"
//    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//    let result = emailTest.evaluate(with: testStr)
//    return result
//}

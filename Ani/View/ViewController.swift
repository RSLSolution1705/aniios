//
//  ViewController.swift
//  Ani
//
//  Created by RSL-01 on 05/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//
//Latest
//7 sep 2020


import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var lblKnowYourFood: NSLayoutConstraint!
    
    @IBOutlet weak var lunchView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     print("ViewController")
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.lunchView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            // HERE
            //                                    self.popupView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            self.lunchView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            
            let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
            let viewHeight = self.view.frame.size.height
            //            let popupHeight = self.lunchView.frame.size.height
            
            //                                                    self.popupView.frame = CGRect(x: 0, y: 0, width: 290, height: 250)
            self.lunchView.center = self.view.center
            
            //
        }) { (finished) in
            UIView.animate(withDuration: 1, animations: {
                
                var signin = self.storyboard?.instantiateViewController(withIdentifier: "signuppageViewController")as? signuppageViewController
                self.navigationController?.pushViewController(signin!, animated: true)
            })
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}


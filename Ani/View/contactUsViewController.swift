//
//  contactUsViewController.swift
//  Ani
//
//  Created by RSL-01 on 09/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class contactUsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PopUpDoneDelegate {
    
    @IBOutlet weak var dropDownButton: UIButton!
    var selectflag = 0
    
    @IBOutlet weak var lblInfoPlaceholder: UILabel!
    @IBOutlet weak var txtinfo: UITextView!
    @IBOutlet weak var dropDownTableView: UITableView!
    
    var categoryList = ["Privacy","Suggestion","Complaint","other"]
    
    var vcFor : ValidationPopupViewController? = nil
    
    
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
           self.hideKeyboardWhenTappedAround()
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        
        dropDownTableView.isHidden = true
        dropDownButton.layer.borderWidth = 1
        dropDownButton.layer.borderColor = UIColor.lightGray.cgColor
        
        txtinfo.delegate = self
        txtinfo.layer.borderWidth = 1
        txtinfo.layer.borderColor = UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationItem.title = "Contact Us"
        
    }
    
    func PopUpOkClick(status: Bool) {
        
        if status {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @objc func backBtn(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //validation remain
    @IBAction func btnSend(_ sender: Any) {
        
        txtinfo.resignFirstResponder()
        
        validateLoginform()
        
    }
    
    func sendWebservices()
    {
        //let headers = ["Content-Type":"Application/json"]
        let _url = send_mails
        
        let emailId = getValueForKey(keyValue: kemail)
        let name = getValueForKey(keyValue: kfirstName)
        let parameters: Parameters = ["email" :emailId ,"msgtxt" :txtinfo.text!  ,"username" : name ,"category" : (dropDownButton.titleLabel?.text!)!,"account_id": getValueForKey(keyValue: kregisterAccountId) ]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: nil).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                           self.stopSpinner()
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor = msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            
                                        }else
                                        {
                                            
                                            //                                            self.stopSpinner()
                                            let jsonDic = response.result.value as! NSDictionary
                                            print(jsonDic)
                                            
                                            self.stopSpinner()
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = "home"
                                            self.vcFor?.titleFor =  "Thank you for your feedback.Our Team will reach you soon"
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                           
                                            
                                        }  //                                    print(self.marchantArray)
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                            {
                             //   self.stopSpinner()
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor = str ?? "Something went wrong!!"
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                self.stopSpinner()
                            }
                            
        }
        
        
    }
    
    
    @IBAction func dropDownButton(_ sender: Any) {
        
        if dropDownButton.tag == 1
        {
            dropDownTableView.isHidden = true
            dropDownButton.tag = 0
        }
        else
        {
            dropDownTableView.isHidden = false
            dropDownButton.tag = 1
            
        }
        
        
    }
    
    func animated(toogle : Bool)
    {
        if (toogle)
        {
            UIView.animate(withDuration: 0.3)
            {
                
                self.dropDownTableView.isHidden = false
            }
        }else
        {
            UIView.animate(withDuration: 0.3)
            {
                self.dropDownTableView.isHidden = true
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        txtinfo.resignFirstResponder()
        return true
    }
    
    
    func validateLoginform() {
        
        var ischeckValidation = true;
        
        
        
       
       
            
         if dropDownButton.currentTitle == "" || (dropDownButton.titleLabel?.text!)! == "Select category"
        {
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Please select category"
            self.present(self.vcFor!, animated: true, completion: nil)
            
          
        }
        else  if((self.txtinfo?.text?.count)! >= 250)
        {
            self.stopSpinner()
            ischeckValidation  = false;
            
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor = "Please enter maximum 250 charcters"
            self.present(self.vcFor!, animated: true, completion: nil)
            
            
            
        }
            else
            if((self.txtinfo?.text?.count)! <= 0)
            {
                self.stopSpinner()
                ischeckValidation  = false;
                
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor = "Please type your message"
                self.present(self.vcFor!, animated: true, completion: nil)
                
            }
            
            
         
        else
        {
            if Reachability.isConnectedToNetwork() {
                self.startSpinner()
                sendWebservices()
            }
            else {
                
          //      self.stopSpinner()
                DispatchQueue.main.async {
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  Internet_Connection_Alert
                self.present(self.vcFor!, animated: true, completion: nil)
                }
                
            }
            
          
            
        }
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! DropDownTableViewCell
        cell.lblDropDown.text = categoryList[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var index = categoryList[indexPath.row]
        self.dropDownButton.setTitle(index, for: .normal)
        self.dropDownTableView.isHidden = true
        //        selectflag = 0
        //        if (indexPath.row == 0)
        //        {
        //            selectflag = 1
        //
        //           self.dropDownButton.titleLabel!.text = "Privacy"
        //            dropDownTableView.isHidden = true
        //        }
        //        else
        //           if (indexPath.row == 1)
        //              {
        //                 selectflag = 2
        //                self.dropDownButton.titleLabel!.text = "Suggestion"
        //                  dropDownTableView.isHidden = true
        //           }
        //        else
        //        if (indexPath.row == 2)
        //            {
        //                 selectflag = 3
        //                self.dropDownButton.titleLabel!.text = "Complaint"
        //                  dropDownTableView.isHidden = true
        //            }
        //
        //
        //        else
        //            if (indexPath.row == 3)
        //            {
        //                 selectflag = 4
        //                self.dropDownButton.titleLabel!.text = "other"
        //                  dropDownTableView.isHidden = true
        //            }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension contactUsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if self.txtinfo.text.count == 0 {
            self.lblInfoPlaceholder.isHidden = false
        }
        else {
            self.lblInfoPlaceholder.isHidden = true
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if self.txtinfo.text.count == 0 {
            self.lblInfoPlaceholder.isHidden = false
        }
        else {
            self.lblInfoPlaceholder.isHidden = true
        }
        
        return textView.text.count + (text.count - range.length) < 250
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.txtinfo.text == "" {
            self.lblInfoPlaceholder.isHidden = false
        }
        else {
            self.lblInfoPlaceholder.isHidden = true
        }
    }
    
}

//
//  PageViewController.swift
//  Ani
//
//  Created by RSL-01 on 11/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController,UIPageViewControllerDataSource{
    
    var pageControl = UIPageControl()
    
    fileprivate lazy var orderedViewController: [UIViewController] = {
        return [self.getViewController(withIdentifier: "PageViewController1"),
                self.getViewController(withIdentifier: "PageViewController2"),
                self.getViewController(withIdentifier: "PageViewController3"),
                self.getViewController(withIdentifier: "PageViewController4")]
        
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         print("PageViewController")
        
        dataSource = self
        configuarePageControl()
        if let firstViewController = orderedViewController.first {
            setViewControllers([firstViewController],direction: .forward,animated: true,completion: nil)
        }
        
        
        // Do any additional setup after loading the view.
    }
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return (storyboard?.instantiateViewController(withIdentifier: identifier))!
    }
    
    func configuarePageControl()
    {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 80, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = orderedViewController.count
        pageControl.currentPage = 0
        
        pageControl.tintColor = UIColor.black
        pageControl.pageIndicatorTintColor = UIColor.white
        pageControl.currentPageIndicatorTintColor = UIColor(hex: "41b93d")
        self.view.addSubview(pageControl)
        
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        let pageviewcontroller = pageViewController.viewControllers?[0]
        //        self.pageControl.currentPage = orderedViewController.index(of: pageviewcontroller!)
        self.pageControl.currentPage = pageViewController.viewControllers!.first!.view.tag //Page Index
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewController.count > previousIndex else {
            return nil
        }
        
        return orderedViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewController.index(of: viewController)else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewController[nextIndex]
    }
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return orderedViewController.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        guard let _ = viewControllers?.first,
            let firstViewControllerIndex = navigationController?.viewControllers.index(of: self) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
}

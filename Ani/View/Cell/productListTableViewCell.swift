//
//  productListTableViewCell.swift
//  Ani
//
//  Created by RSL-01 on 02/04/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class productListTableViewCell: UITableViewCell {

    @IBOutlet weak var allergencollectionview: UICollectionView!
    @IBOutlet weak var SubMenuimageView: UIImageView!
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var btnFav: UIButton?
    @IBOutlet weak var lblimenuName: UILabel!
    
    
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    
    @IBOutlet weak var rightArrowImgView: UIImageView!
    @IBOutlet weak var leftArrowImgView: UIImageView!
    
    @IBOutlet weak var leftBtnWidthConstant: NSLayoutConstraint! //25
    @IBOutlet weak var leftBtnImgWidthConstant: NSLayoutConstraint! //15
    
    @IBOutlet weak var rightBtnWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var rightBtnImgWidthConstant: NSLayoutConstraint!
    
    var allergensArray = [productallergnsClass]()
    
    var productSubMenuArray = [ProductListModelClass]()

    
    var flag = ""
    
     var appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
   //      createGradientLayer(view: self.gradientView, width: self.gradientView.frame.size.width, height: self.gradientView.frame.size.height)
        
        self.allergencollectionview.dataSource = self
        self.allergencollectionview.delegate = self
        
        
//        self.rightBtn.isHidden = true
//        self.leftBtn.isHidden = true
//        self.rightArrowImgView.isHidden = true
//        self.leftArrowImgView.isHidden = true
        
    
        
        // Initialization code
        
        if let layout = allergencollectionview.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal  // .horizontal
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
      ////////
        @IBAction func leftBtnAction(_ sender: Any) {
               
            let collectionBounds = self.allergencollectionview.bounds
            let contentOffset = CGFloat(floor(self.allergencollectionview.contentOffset.x - collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset)
            
            self.rightBtn.isHidden = false
            self.rightArrowImgView.isHidden = false
            self.leftBtn.isHidden = true
            self.leftArrowImgView.isHidden = true
            
            self.leftBtnWidthConstant.constant = 0
            self.leftBtnImgWidthConstant.constant = 0
            
            self.rightBtnWidthConstant.constant = 25
            self.rightBtnImgWidthConstant.constant = 20
            
           }
           
           @IBAction func rightBtnAction(_ sender: Any) {
               
            let collectionBounds = self.allergencollectionview.bounds
            let contentOffset = CGFloat(floor(self.allergencollectionview.contentOffset.x + collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset)
            
            self.rightBtn.isHidden = true
            self.rightArrowImgView.isHidden = true
            self.leftBtn.isHidden = false
            self.leftArrowImgView.isHidden = false
            
            self.leftBtnWidthConstant.constant = 25
            self.leftBtnImgWidthConstant.constant = 20
            
            self.rightBtnWidthConstant.constant = 0
            self.rightBtnImgWidthConstant.constant = 0
           }

        func moveCollectionToFrame(contentOffset : CGFloat) {

             let frame: CGRect = CGRect(x : contentOffset ,y : self.allergencollectionview.contentOffset.y ,width : self.allergencollectionview.frame.width,height : self.allergencollectionview.frame.height)
             self.allergencollectionview.scrollRectToVisible(frame, animated: true)
         }
    ///////

}


// Done by Pratik for handling the offline products

//extension productListTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
//{
//
//
//func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//
//    return allergensArray.count
//}
//
//
//func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! allergensCollectionViewCell
//    let dic = allergensArray[indexPath.row]
//
//
//
//    if dic.flag == "true"
//    {
//        if dic.allergan_Img != nil && dic.allergan_Img != "" {
//            let imgURL = dic.allergan_Img!
//            let url = URL(string: imgURL)
//            cell.allegensImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "allergens_placeholder"), options: .retryFailed, completed: nil)
//            cell.allegensImage.contentMode = .scaleAspectFill
//
//        }
//        else {
//            cell.allegensImage.image = #imageLiteral(resourceName: "allergens_placeholder")
//            cell.allegensImage.contentMode = .scaleAspectFit
//        }
//    }
//
//    return cell
//}
//
//func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
////
//
////      if UIDevice.current.modelName == "iPhone 5c"
////      {
////        if (allergensArray.count > 5)
////            {
////                let collectionWidth = collectionView.bounds.width
////                var count: CGFloat =  CGFloat(allergensArray.count)
////                return CGSize(width: collectionWidth/count, height: collectionWidth/count)
////        }
////    }
//    let collectionWidth = collectionView.bounds.width
//    var count: CGFloat =  CGFloat(allergensArray.count)
//    return CGSize(width: 50, height:50)
//}
//
//func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//    return 0
//}
//func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//    return 0
//}
//
//}
extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

//
//  RecentItemTableViewCell.swift
//  Ani
//
//  Created by RSL-01 on 03/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class RecentItemTableViewCell: UITableViewCell {

    @IBOutlet weak var recentImage: UIImageView!
    
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var recentLabel: UILabel!
   
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var recentCollectionView: UICollectionView!
    
    
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    
    
    @IBOutlet weak var rightArrowImg: UIImageView!
    @IBOutlet weak var leftArrowImg: UIImageView!
    
    @IBOutlet weak var leftBtnWidthConstant: NSLayoutConstraint!//25
    @IBOutlet weak var leftBtnImgWidthConstant: NSLayoutConstraint!//15
    
    @IBOutlet weak var rightBtnImgWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var rightBtnWidthConstant: NSLayoutConstraint!
    
    var allergensArray = [productallergnsClass]()
    
    var allergensFlagRecent = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        createGradientLayer(view: self.gradientView, width: self.gradientView.frame.size.width, height: self.gradientView.frame.size.height)

        
//        self.rightBtn.isHidden = true
//        self.leftBtn.isHidden = true
//        self.rightArrowImg.isHidden = true
//        self.leftArrowImg.isHidden = true
        
        // Initialization code
        
        if let layout = recentCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal  // .horizontal
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    ////////
    @IBAction func leftBtnAction(_ sender: Any) {
           
        let collectionBounds = self.recentCollectionView.bounds
        let contentOffset = CGFloat(floor(self.recentCollectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        
              self.rightBtn.isHidden = false
               self.rightArrowImg.isHidden = false
               self.leftBtn.isHidden = true
               self.leftArrowImg.isHidden = true
                          
               self.leftBtnWidthConstant.constant = 0
               self.leftBtnImgWidthConstant.constant = 0
                          
               self.rightBtnWidthConstant.constant = 25
               self.rightBtnImgWidthConstant.constant = 20
       }
       
       @IBAction func rightBtnAction(_ sender: Any) {
           
        let collectionBounds = self.recentCollectionView.bounds
        let contentOffset = CGFloat(floor(self.recentCollectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        
                self.rightBtn.isHidden = true
                 self.rightArrowImg.isHidden = true
                 self.leftBtn.isHidden = false
                 self.leftArrowImg.isHidden = false
                         
                 self.leftBtnWidthConstant.constant = 25
                 self.leftBtnImgWidthConstant.constant = 20
                         
                 self.rightBtnWidthConstant.constant = 0
                 self.rightBtnImgWidthConstant.constant = 0
       }

    func moveCollectionToFrame(contentOffset : CGFloat) {

         let frame: CGRect = CGRect(x : contentOffset ,y : self.recentCollectionView.contentOffset.y ,width : self.recentCollectionView.frame.width,height : self.recentCollectionView.frame.height)
         self.recentCollectionView.scrollRectToVisible(frame, animated: true)
     }
///////
    
}


// Done by Pratik for handling the offline products

//extension RecentItemTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
//{
//
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//
//        return allergensArray.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RecentItemCollectionViewCell
//        let dic = allergensArray[indexPath.row]
//
//
//
//        if dic.flag == "true"
//        {
//            if dic.allergan_Img != nil && dic.allergan_Img != "" {
//                let imgURL = dic.allergan_Img!
//                let url = URL(string: imgURL)
//
//
//                cell.recentAllergentImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "allergens_placeholder"), options: .retryFailed, completed: nil)
//                cell.recentAllergentImage.contentMode = .scaleAspectFill
//
//            }
//            else {
//                cell.recentAllergentImage.image = #imageLiteral(resourceName: "allergens_placeholder")
//                cell.recentAllergentImage.contentMode = .scaleAspectFit
//            }
//        }
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        //    let collectionWidth = collectionView.bounds.width
//        //    var count: CGFloat =  CGFloat(productSubMenuArray[tableposition].allengsArray.count)
//        return CGSize(width: 50, height: 50)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//}

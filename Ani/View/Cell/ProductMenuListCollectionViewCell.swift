//
//  ProductMenuListCollectionViewCell.swift
//  Ani
//
//  Created by RSL-01 on 30/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class ProductMenuListCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var lblMenu_Item_Name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mainView.layer.cornerRadius = 5.0
        self.mainView.clipsToBounds = true
        createGradientLayer(view: self.gradientView, width: self.gradientView.frame.size.width, height: self.gradientView.frame.size.height)
    
    }
    
    
}

//
//  detailAllergensTableViewCell.swift
//  Ani
//
//  Created by RSL-01 on 17/04/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class detailAllergensTableViewCell: UITableViewCell {

    @IBOutlet weak var allergensImage: UIImageView!
    
    @IBOutlet weak var allergensName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

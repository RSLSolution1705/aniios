//
//  NutritionTableViewCell.swift
//  Ani
//
//  Created by Nikhil Malwade on 04/06/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class NutritionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var name_nutrition_lbl: UILabel!
    @IBOutlet weak var quantity_nutritian_lbl: UILabel!
    @IBOutlet weak var unit_nutritian_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, margins)
    }

}

//
//  productSecondViewController.swift
//  Ani
//
//  Created by RSL-01 on 04/04/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import AlamofireImage
import SVProgressHUD
import SDWebImage

var productCheckProductSecond = "1"

class productSecondViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PopUpDoneDelegate {
    let context = AppDelegate().persistentContainer.viewContext
    
    @IBOutlet weak var blankView: UIView!
    
    @IBOutlet weak var lblInactiveMerchand: UILabel!
    
    
    let doneButton = UIButton()
    

    var FilterBtnCheck = "0"
    var ingredient_info = ""
    var newAllergentIdArray = [String]()
    var productSubMenuArray = [ProductListModelClass]()
    
    var tempProductSubMenuArray = [ProductListModelClass]()
    
    var account_id = ""
    var passdata = [productallergnsClass]()
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    //   var isSelected = false
    var tableposition = 0
    
    var buffer1 : String!
    var allergens_id_collectionview = ""
    var buffer = [allergentModelClass]()
    var isFavouriteRes = false
    @IBOutlet var filterView: UIView!
    var allergntArray = [allergentModelClass]()
    var marchantArray = [marchantModelClass]()
    var accountID =  loginClass()
    
    var allergntArray1 = [productallergnsClass]()
    var nutritianArray1 = [nutritianArrayObject]()
    var ingredientsArray1 = [ingredientsArrayObject]()
    
    var allergensDetail = [productallergnsClass]()
    var nutritianDetails = [nutritianArrayObject]()
    var ingredientsDetails = [ingredientsArrayObject]()
    var merchantId = ""
    var ProductMenuArray = [ productMenuListModelClass]()
    var menu_item_id_fav = ""
    var recentMenuId = ""
    var flagfav = ""
    @IBOutlet weak var tableview: UITableView!
    var vcFor : ValidationPopupViewController? = nil
    @IBOutlet weak var filterTableview: UITableView!
    var flagdidselect = 0
    var flag = 0
    
    var isFirstLoad = false
    
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    

    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(FavouriteViewController.handleRefresh),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.black
            return refreshControl
    }()
    
    
    
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    //---------------------------------------------- ANI LOADER ----------------------------------------------
    var viewActivityLarge : SHActivityView?
    
    func startSpinner()
    {
        viewActivityLarge = SHActivityView.init()
        viewActivityLarge?.backgroundColor = UIColor.clear
        viewActivityLarge?.spinnerSize = .kSHSpinnerSizeLarge
        //  viewActivityLarge?.disableEntireUserInteraction = true
        
        viewActivityLarge?.stopShowingAndDismissingAnimation = true
        self.view.addSubview(viewActivityLarge!)
        viewActivityLarge?.showAndStartAnimate()
        viewActivityLarge?.frame = CGRect(x: self.view.frame.size.width/2 - 60,y: self.view.frame.size.height/2 - 60,width: 120,height: 120)
        
    }
    
    func stopSpinner()
    {
        self.viewActivityLarge?.dismissAndStopAnimation()
    }
    //--------------------------------------------------------------------------------------------------------
    
    @IBAction func btncancelFilter(_ sender: Any) {
        flagdidselect = 0
        DispatchQueue.main.async {
            if Reachability.isConnectedToNetwork() {
                self.startSpinner()
                self.allergensList()
            }
            else {
                //    self.stopSpinner()
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  Internet_Connection_Alert
                self.present(self.vcFor!, animated: true, completion: nil)
                //                self.simpleAlert(title: "ANI", details: Internet_Connection_Alert)
            }
        }
        blankView.isHidden = true
        filterView.removeFromSuperview()
        //        self.filterView.isHidden = true
        //self.tableview.reloadData()
    }
    
    
    @objc func someAction(sender:UITapGestureRecognizer){
        // do other task
        var variable = buffer[sender.view!.tag]
        var newAllergentId =  variable.allergan_id!
        //             print(“flag1 : \(buffer[indexPath.row].isSelected)“)
        if(!self.newAllergentIdArray.contains(newAllergentId))
        {
            print(" variable.isSelected1 : \(variable.isSelected)")
            variable.isSelected = true
            self.newAllergentIdArray.append(newAllergentId)
            appdelegate.appdeldgtenewArray = newAllergentIdArray
            print("newAllergentIdArray : \(newAllergentIdArray)")
        }
        else
        {
            print("variable.isSelected2 : \(variable.isSelected)")
            variable.isSelected = false
            //var newAllergentId =  variable.allergan_id!
            newAllergentIdArray = newAllergentIdArray.filter { $0 != newAllergentId}
            appdelegate.appdeldgtenewArray = newAllergentIdArray
        }
        filterTableview.reloadData()
    }
    
    @IBAction func btnclearfilter(_ sender: Any)
    {
        flag = 1
        flagdidselect = 0
        //let str = “”
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey:allergentIdString)
        //        setDefaultValue(keyValue: allergentIdString, valueIs: str)
        
        if self.newAllergentIdArray.count != 0
        {
            self.newAllergentIdArray.removeAll()
            appdelegate.appdeldgtenewArray.removeAll()
            self.appdelegate.allergentArray.removeAll()
            appdelegate.clearArray = 1
            DispatchQueue.main.async {
                if Reachability.isConnectedToNetwork() {
                                self.startSpinner()
                    self.allergensList()
                }
                else {
                    //              self.stopSpinner()
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                    //                self.simpleAlert(title: "ANI", details: Internet_Connection_Alert)
                }
            }
            //
            let array_buffer : [allergentModelClass] = self.buffer
            self.buffer = [allergentModelClass]()
            for item in array_buffer
            {
                let tempobjItem = allergentModelClass()
                tempobjItem.allerganName = item.allerganName
                tempobjItem.allergan_id = item.allergan_id
                tempobjItem.isSelected = false
                self.buffer.append(tempobjItem)
                
            }
            self.filterTableview.reloadData()
            
            
            
        }
        filterView.removeFromSuperview()
        blankView.isHidden = true
    }
    
    
    
    @IBAction func btnapllyfilter(_ sender: Any) {
        flagdidselect = 1
        DispatchQueue.main.async {
        if Reachability.isConnectedToNetwork() {
            
//            self.startSpinner()
            self.allergensList()
        }
        else {
            //     self.stopSpinner()
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  Internet_Connection_Alert
            self.present(self.vcFor!, animated: true, completion: nil)
            //            self.simpleAlert(title: "ANI", details: Internet_Connection_Alert)
        }
        
        self.dismiss(animated: true, completion: nil)
            self.filterView.removeFromSuperview()
            self.blankView.isHidden = true
        }
        //        tempobjItem.isSelected = false
        
       
        //    self.filterView.isHidden = true
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // self.stopSpinner()
        
        print("ProductSecondViewController")
        
        self.tableview.addSubview(self.refreshControl)
        tableview.reloadData()
        
        self.title = appdelegate.Mname
        print(UIDevice.current.modelName)
        if UIDevice.current.modelName == "iPhone 5c"
        {
            tableViewTop.constant = 64.0
            //            titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 12.0).isActive = true
            
        }
        else if UIDevice.current.modelName == "iPhone 5s"
        {
            tableViewTop.constant = 64.0
        }
        else if UIDevice.current.modelName == "iPhone 5"
        {
            tableViewTop.constant = 64.0
        }
        else
            
        {
            tableViewTop.constant = 0.0
        }
        
        vcFor = self.storyboard?.instantiateViewController(withIdentifier: "ValidationPopupViewController") as? ValidationPopupViewController
        vcFor?.delegate = self
        
        let alrgstr = getValueForKey(keyValue: allergentIdString)
        self.newAllergentIdArray = alrgstr.components(separatedBy: ",")
        
        self.filterTableview.delegate = self
        self.filterTableview.dataSource = self
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.navigationBar.backgroundColor = UIColor(hex: "41b93d")
        let button1 = UIBarButtonItem(image: UIImage(named: kback), style: .plain, target: self, action: #selector(backBtn(sender:)))
        self.navigationItem.leftBarButtonItem  = button1
        
        
//        let button2 = UIBarButtonItem(image: UIImage(named: "filter_white"), style: .plain, target: self, action: #selector(filterBtn(sender:)))
//        self.navigationItem.rightBarButtonItem  = button2
        
        let btnRightbutton = UIButton(type: .custom)
             btnRightbutton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
             btnRightbutton.backgroundColor = UIColor.clear
             btnRightbutton.addTarget(self, action: #selector(filterBtn(sender:)), for: .touchUpInside)
             btnRightbutton.setImage(UIImage(named: "filter_white"), for: .normal)
            let barButton = UIBarButtonItem(customView: btnRightbutton)
        self.navigationItem.rightBarButtonItems = [barButton]
        
        //        self.navigationItem.title = "Ani"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
  //      DoneButton()
        tableview.tableFooterView = UIView()
        
        //        infoimage.image = #imageLiteral(resourceName: "ic_information_outline_white_48dp")
        
                //self.productlist()
                
                if Reachability.isConnectedToNetwork()
                {
//                                        self.startSpinner()
                    self.appdelegate.firstLoadScrollFlag = true
                    self.allergensList()
                }
                else
                {
                    //   self.stopSpinner()
                    self.vcFor?.modalTransitionStyle = .crossDissolve
                    self.vcFor?.modalPresentationStyle = .overFullScreen
                    self.vcFor?.isLogin = true
                    self.vcFor?.actionFor = ""
                    self.vcFor?.titleFor =  Internet_Connection_Alert
                    self.present(self.vcFor!, animated: true, completion: nil)
                    //                    self.simpleAlert(title: "ANI", details: Internet_Connection_Alert)
                }
                
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        //        self.startSpinner()
        self.allergensList()
        tableview.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    func PopUpOkClick(status: Bool)
    {
        
        if status
        {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
    
//    fileprivate func DoneButton() {
//        SVProgressHUD.setInfoImage(UIImage(named: "ani.png")!)
//        //        SVProgressHUD.setInfoImage(img!)
//
//        //250 /500
////        let doneButton = UIButton(frame: CGRect(x:(self.view.frame.size.width-(self.view.frame.size.width/3) ) + 50, y:(self.view.frame.size.height - self.view.frame.size.height/3) + 130, width: 50, height: 50))
//
//        self.doneButton.frame = CGRect(x:(self.view.frame.size.width-(self.view.frame.size.width/3) ) + 50, y:(self.view.frame.size.height - self.view.frame.size.height/3) + 130, width: 50, height: 50)
//
//        //        // here is what you should add:
//        ////        doneButton.center = footerView.center
//        //
//
//
//
//        ////        doneButton.setTitle("become a captian?", for: .normal)
//        let image = UIImage(named: "filter_white") as UIImage?
//        doneButton.setImage(image, for: .normal)
//        doneButton.backgroundColor = UIColor(hex: "41b93d")
//        //        doneButton.layer.cornerRadius = 10.0
//
//        let radius = doneButton.frame.size.width / 2
//        doneButton.layer.cornerRadius = radius
//        doneButton.layer.masksToBounds = true
//        doneButton.addTarget(self, action: #selector(showFilter), for: .touchUpInside)
//        ////        doneButton.shadow = true
//        ////        doneButton.addTarget(self, action: #selector(hello(sender:)), for: .touchUpInside)
////        if self.FilterBtnCheck == "1"
////        {
////            print("Btn should be Enable")
//////            doneButton.isHidden = false
////
////        }
////        else
////        {
//////            self.view.willRemoveSubview(doneButton)
////            print("Btn should be Disable")
////            let image = UIImage(named: "") as UIImage?
////            doneButton.setImage(image, for: .normal)
////            doneButton.isUserInteractionEnabled = false
//////            doneButton.isHidden = true    filter_white-2
////        }
//        self.view.addSubview(doneButton)
//
//    }
    
    override func viewWillAppear(_ animated: Bool)
    {
//        tableview.scrollsToTop = true
        tableview.reloadData()
        lblInactiveMerchand.isHidden = true
        //self.stopSpinner()
        self.tableview.reloadData()
        if Reachability.isConnectedToNetwork()
        {
            //                                        self.startSpinner()
            
            self.allergensList()
        }
        else
        {
            //   self.stopSpinner()
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  Internet_Connection_Alert
            self.present(self.vcFor!, animated: true, completion: nil)
            //                    self.simpleAlert(title: "ANI", details: Internet_Connection_Alert)
        }
        self.navigationController?.isNavigationBarHidden = false
        blankView.isHidden = true
        
        
    }
    @objc func backBtn(sender:UIButton)
    {
        
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func filterBtn(sender:UIButton)
    {
           
        self.showFilter()
    }
    
    @objc func showFilter()
    {
        flagdidselect = 1
        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        let viewHeight = view.frame.size.height
        let popupHeight = filterView.frame.size.height
        
        filterView.frame = CGRect(x: 16, y: ((viewHeight + navigationBarHeight)/2)-(popupHeight/2) , width: (view.frame.width) - 60, height: (view.frame.height) - 140)
        self.filterView.center = self.view.center
        self.view.addSubview(filterView)
        filterTableview.reloadData()
        blankView.isHidden = false
        
        
        
    }
    
    //mark :- Api for Allegent list show on collection view
    
    func allergensList()
    {
//        self.startSpinner()
        let headers = ["Content-Type":"Application/json"]
        let _url = get_all_menuitems
        account_id = appdelegate.kaccountIdString
        print("var account_id : \(account_id)")
        var group_id = getValueForKey(keyValue: kmenu_item_group_id)
        print("group_id : \(group_id)")
        var merchant = getValueForKey(keyValue: kmarchentId)
        print("merchant : \(merchant)")
        
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        var stringvalue = ""
        var newstr = ""
        
        
        for str in newAllergentIdArray
        {
            //self.stopSpinner()
            stringvalue += str + ","
            if let lastchar = stringvalue.characters.last {
                if [",", ".", "-", "?"].contains(lastchar) {
                    newstr = String(stringvalue.characters.dropLast())
                    print("newstr : \(newstr)")
                }
            }
            
            print("stringvalue : \(stringvalue)")
            
            let c1 = NSEntityDescription.insertNewObject(forEntityName: "Ani", into: self.context)as! Ani
            //                                c1.setValue(temp.account_id, forKey: accId)
            c1.allergensId = newstr
            
            // txtName.text=""
            // txtNumber.text=""
            
            print(c1)
            
            try! self.context.save()
            
            
        }
        
        setDefaultValue(keyValue: allergentIdString, valueIs: newstr)
        appdelegate.allergentIdString = newstr
        
 //       var parentID = UserDefaults.standard.value(forKey: "parentID") as! String
 //       print("Parent ID from UserDefaults : \(parentID)")
        
        var parentID = ""
        if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
             parentID = ""
        }
        else{
            if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                //   parentID = ""
                   parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                  }
                  else{
                      parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                  }
        }
        print("Parent ID from UserDefaults : \(parentID)")
    
        print("self.appdelegate.merchant_id = \(self.appdelegate.merchant_id)")
        
        let parameters: Parameters = ["merchant_id" :self.appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"group_id" : group_id, "allerganid" : newstr, "partner_id" : parentID]
        
        print("Parameters are : \(parameters)")
        print("URL is : \(_url)")
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            print("---------------------------------Allergenslist-----------------------------------------)")
                            print("Request  \(response.request)")
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                self.stopSpinner()
                                print("SUKCES with \(response)")
                                let jsonDic = response.result.value as! NSDictionary
                                print(jsonDic)
                                
                                
                                
                                
                                
                                if (response.result != nil)
                                {
                                    self.stopSpinner()
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            
                                            
                                            //popup
                                            //                                            var str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            
                                            
//                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            var msg = "This ANI Partner Menu is unavailable at the moment please check with your server"
//                                            self.tableview.isHidden = true
                                            self.productSubMenuArray.removeAll()
                                            self.tableview.reloadData()
                                            self.lblInactiveMerchand.isHidden = false
                                            self.FilterBtnCheck = "0"
                                            self.doneButton.isHidden = true
//                                            self.vcFor?.modalTransitionStyle = .crossDissolve
//                                            self.vcFor?.modalPresentationStyle = .overFullScreen
//                                            self.vcFor?.isLogin = true
//                                            self.vcFor?.actionFor = ""
//                                            self.vcFor?.titleFor =  msg
//                                            self.present(self.vcFor!, animated: true, completion: nil)
//                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
//                                            self.navigationController?.pushViewController(vc, animated: true)
//                                            self.navigationController?.popToRootViewController(animated: true)
//                                            self.navigationController?.popToViewController(vc, animated: true)
//                                            print("Got it...!!!")
                                            
                                            
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                            //
                                        }
                                        else
                                        {
                                  //          self.doneButton.isHidden = false
                                            self.FilterBtnCheck = "1"
                                    //        self.DoneButton()
                                            self.tableview.isHidden = false
                                            self.lblInactiveMerchand.isHidden = true
                                            var resultArray = jsonDic.object(forKey: "data") as! NSArray
                                            self.productSubMenuArray.removeAll()
                                            self.tempProductSubMenuArray.removeAll()
                                            
                                            
                                            for i in resultArray
                                            {
                                              
                                                var tempobj = i as! NSDictionary
                                                var temp = ProductListModelClass()
                                                var temp2 = ProductListModelClass()
                                                
                                                temp.menu_item_image = tempobj.object(forKey: "menu_item_image") as! String
                                                temp2.menu_item_image = tempobj.object(forKey: "menu_item_image") as! String
                                                //                                    var imageData = try! Data.init(contentsOf: URL(string: img)!)
                                                //                                    temp.menu_item_image = UIImage(data: imageData)
                                                temp.menu_item_description = tempobj.object(forKey: "menu_item_description")as! String
                                                temp.menu_item_name = tempobj.object(forKey: "menu_item_name")as! String
                                                
                                                temp.menu_item_flag = tempobj.object(forKey: "menuitem_flag")as! String
                                                
                                                temp2.menu_item_description = tempobj.object(forKey: "menu_item_description")as! String
                                                temp2.menu_item_name = tempobj.object(forKey: "menu_item_name")as! String
                                                //menu_item_name
                                                
                                                temp.ingredient_deceleration_info  = tempobj.object(forKey: "ingredient_deceleration_info")as! String
                                                temp2.ingredient_deceleration_info  = tempobj.object(forKey: "ingredient_deceleration_info")as! String
                                                
                                                temp.menu_item_id = tempobj.object(forKey: "menu_item_id") as! String
                                                temp2.menu_item_flag = tempobj.object(forKey: "menuitem_flag") as! String
                                                
                                                print("temp.menu_item_id : \(temp.menu_item_id)")
                                                
                                                temp.flag = tempobj.object(forKey: "flag") as! String
                                                temp2.flag = tempobj.object(forKey: "flag") as! String
                                                
                                                self.flagfav = temp.flag
                                                
                                                self.menu_item_id_fav = temp.menu_item_id
                                                
                                                
                                                
                                                
                                                
                                                let AllerngensArray = tempobj.object(forKey: "allergan_array")as! NSArray
                                                let nutritianArray = tempobj.object(forKey: "nutritian_array")as! NSArray
                                                let ingredientsArray = tempobj.object(forKey: "ingredients_array")as! NSArray
                                                
                                                
                                                self.allergntArray1 = [productallergnsClass]()
                                                self.nutritianArray1 = [nutritianArrayObject]()
                                                self.ingredientsArray1 = [ingredientsArrayObject]()
                                                //                                    self.allergntArray1.removeAll()
                                                
                                                for j in AllerngensArray
                                                {
                                                    let tempallegens = j as! NSDictionary
                                                    let tempmodel = productallergnsClass()
                                                    
                                                    tempmodel.allergan_Img = tempallegens.object(forKey: "allergan_images")as! String
                                                    tempmodel.allergen_id = (tempallegens.object(forKey: "allergan_id")as! String)
                                                    tempmodel.allergensName = tempallegens.object(forKey: "allergan")as! String
                                                    
                                                    self.allergens_id_collectionview =  tempmodel.allergen_id
                                                    
                                                    tempmodel.flag = tempallegens.object(forKey: "flag")as? String ?? ""
                                                    
                                                    //                                                    if tempmodel.flag == "true"
                                                    //                                                    {
                                                    self.allergntArray1.append(tempmodel)
                                                    self.allergensDetail.append(tempmodel)
                                                    //                                                    }
                                                }
                                                
                                                temp.allengsArray = self.allergntArray1
                                                self.appdelegate.allergentArray = self.allergntArray1
                                                
                                                for j in nutritianArray
                                                {
                                                    let tempallegens = j as! NSDictionary
                                                    let tempmodel = nutritianArrayObject()
                                                    
                                                    tempmodel.name = (tempallegens.object(forKey: "name")as! String)
                                                    tempmodel.quantity = (tempallegens.object(forKey: "quantity")as! String)
                                                    tempmodel.unit = (tempallegens.object(forKey: "unit")as! String)
                                                    print(tempmodel.name)
                                                    
                                                    self.nutritianArray1.append(tempmodel)
                                                }
                                                
                                                temp.nutritianarray = self.nutritianArray1
                                                self.appdelegate.nutritianArray = self.nutritianArray1
                                                
                                                for j in ingredientsArray
                                                {
                                                    var tempallegens = j as! NSDictionary
                                                    var tempmodel = ingredientsArrayObject()
                                                    
                                                    tempmodel.name = tempallegens.object(forKey: "name")as! String
                                                    tempmodel.quantity = tempallegens.object(forKey: "quantity")as! String
                                                    tempmodel.unit = tempallegens.object(forKey: "unit")as! String
                                                    
                                                    //            self.allergens_id_collectionview =  tempmodel.allergen_id!
                                                    
                                                    //      self.nutritianArray.append(tempmodel)
                                                    self.ingredientsArray1.append(tempmodel)
                                                    
                                                }
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                
                                                temp.ingredientsarray = self.ingredientsArray1
                                                self.appdelegate.ingredientsArray = self.ingredientsArray1
                                                self.productSubMenuArray.append(temp)
                                                
                                                self.tempProductSubMenuArray.append(temp)
                                                
                                                
                                                DispatchQueue.main.async
                                                    {
                                                        self.tableview?.reloadData()
                                                        
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                                 self.stopSpinner()
                            }
                            else if response.result.isFailure
                            {
                                self.stopSpinner()
                                
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                       
                            }
                            self.stopSpinner()
        }
        
   //     self.DoneButton()
        //        showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
    }
    
    
    //mark :- Api for Product list show on tableview view
    
    @objc func productlist()
    {
//        self.stopSpinner()
        let headers = ["Content-Type":"Application/json"]
        let _url = get_all_menuitems
        account_id = appdelegate.kaccountIdString
        print("var account_id : \(account_id)")
        var group_id = getValueForKey(keyValue: kmenu_item_group_id)
        print("group_id : \(group_id)")
        var merchant = getValueForKey(keyValue: kmarchentId)
        print("merchant : \(merchant)")
        var data1 = [Ani]()
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        let parentID = UserDefaults.standard.value(forKey: "parentID") as! String
        print("Parent ID from UserDefaults : \(parentID)")
        
        let parameters: Parameters = ["merchant_id" :appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"group_id" : group_id]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            print("---------------------------------productlist-----------------------------------------)")
                            
                            print("Request  \(response.request)")
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                let jsonDic = response.result.value as! NSDictionary
                                print(jsonDic)
                                
                                self.stopSpinner()
                                
                                if (response.result != nil)
                                {
                                    
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            self.stopSpinner()
                                            
                                            //popup
                                            //                                            var str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                        }
                                        else{
                                            self.stopSpinner()
                                            
                                            var resultArray = jsonDic.object(forKey: "data") as! NSArray
                                            self.productSubMenuArray.removeAll()
                                            self.tempProductSubMenuArray.removeAll()
                                            
                                            for i in resultArray
                                            {
                                                self.stopSpinner()
                                                var tempobj = i as! NSDictionary
                                                var temp = ProductListModelClass()
                                                temp.menu_item_image = tempobj.object(forKey: "menu_item_image") as! String
                                                
                                                temp.menu_item_description = tempobj.object(forKey: "menu_item_description")as! String
                                                temp.menu_item_name = tempobj.object(forKey: "menu_item_name")as! String
                                                //menu_item_name
                                                
                                                temp.ingredient_deceleration_info  = tempobj.object(forKey: "ingredient_deceleration_info")as! String
                                                
                                                
                                                
                                                
                                                
                                                temp.menu_item_id = tempobj.object(forKey: "menu_item_id") as! String
                                                print("temp.menu_item_id : \(temp.menu_item_id)")
                                                temp.flag = tempobj.object(forKey: "flag") as! String
                                                self.flagfav = temp.flag
                                                self.menu_item_id_fav = temp.menu_item_id
                                                
                                                
                                                
                                                
                                                
                                                let AllerngensArray = tempobj.object(forKey: "allergan_array")as! NSArray
                                                let nutritianArray = tempobj.object(forKey: "nutritian_array")as! NSArray
                                                let ingredientsArray = tempobj.object(forKey: "ingredients_array")as! NSArray
                                                
                                                
                                                self.allergntArray1 = [productallergnsClass]()
                                                self.nutritianArray1 = [nutritianArrayObject]()
                                                self.ingredientsArray1 = [ingredientsArrayObject]()
                                                //                                    self.allergntArray1.removeAll()
                                                self.stopSpinner()
                                                for j in AllerngensArray
                                                {
                                                    let tempallegens = j as! NSDictionary
                                                    let tempmodel = productallergnsClass()
                                                    tempmodel.allergan_Img = tempallegens.object(forKey: "allergan_images") as? String
                                                    tempmodel.allergen_id = (tempallegens.object(forKey: "allergan_id") as! String)
                                                    tempmodel.allergensName = tempallegens.object(forKey: "allergan") as? String
                                                    tempmodel.flag = tempallegens.object(forKey: "flag")as? String ?? ""
                                                    self.allergens_id_collectionview =  tempmodel.allergen_id
                                                    
                                                    self.allergntArray1.append(tempmodel)
                                                    self.allergensDetail.append(tempmodel)
                                                    
                                                }
                                                
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                temp.allengsArray = self.allergntArray1
                                                self.appdelegate.allergentArray = self.allergntArray1
                                                //self.productSubMenuArray.append(temp)
                                                
                                                for j in nutritianArray
                                                {
                                                    let tempallegens = j as! NSDictionary
                                                    let tempmodel = nutritianArrayObject()
                                                    
                                                    tempmodel.name = (tempallegens.object(forKey: "name")as! String)
                                                    
                                                    tempmodel.quantity = (tempallegens.object(forKey: "quantity")as! String)
                                                    tempmodel.unit = (tempallegens.object(forKey: "unit")as! String)
                                                    
                                                    print(tempmodel.name)
                                                    
                                                    self.nutritianArray1.append(tempmodel)
                                                    
                                                }
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                
                                                temp.nutritianarray = self.nutritianArray1
                                                self.appdelegate.nutritianArray = self.nutritianArray1
                                                //self.productSubMenuArray.append(temp)
                                                
                                                for j in ingredientsArray
                                                {
                                                    
                                                    var tempallegens = j as! NSDictionary
                                                    var tempmodel = ingredientsArrayObject()
                                                    
                                                    tempmodel.name = tempallegens.object(forKey: "name")as! String
                                                    tempmodel.quantity = tempallegens.object(forKey: "quantity")as! String
                                                    tempmodel.unit = tempallegens.object(forKey: "unit")as! String
                                                    
                                                    //            self.allergens_id_collectionview =  tempmodel.allergen_id!
                                                    
                                                    //      self.nutritianArray.append(tempmodel)
                                                    self.ingredientsArray1.append(tempmodel)
                                                    
                                                }
                                                //                                    print("count: \(self.allergntArray1.count)")
                                                
                                                temp.ingredientsarray = self.ingredientsArray1
                                                self.appdelegate.ingredientsArray = self.ingredientsArray1
                                                self.productSubMenuArray.append(temp)
                                                
                                                
                                                DispatchQueue.main.async {
                                                    
                                                    self.tableview?.reloadData()
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (response.result.isFailure) {
                                
                                self.stopSpinner()
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (tableView != filterTableview) {
//            let imgUrl = temp1.menu_item_image
            let screenSize: CGRect = UIScreen.main.bounds
            let screenScale = UIScreen.main.scale
            let calculateHeight = ((screenSize.width*600)/1024)
            print(screenSize.width)
            print(calculateHeight)
            return calculateHeight
//            return 55
        }
       else if (tableView == filterTableview)
        {
            return 40
        }
        else
        {
            return 160
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView != filterTableview)
        {
            return productSubMenuArray.count
        }
        else
        {
            return buffer.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellToReturn = UITableViewCell()
        
        if (tableView != filterTableview)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellred", for: indexPath) as! productListTableViewCell
            
         
            //var coltnCell = ProductListModelClass()
            //let placeholderImage = UIImage(named: "background_placeholder.png")!
            var temp1 = productSubMenuArray[indexPath.row]
            
            
            if temp1.menu_item_flag == "active"
            {
                
                cell.SubMenuimageView.alpha = 1
                cell.btnFav?.alpha = 1
                cell.lblimenuName.alpha = 1
                productCheckProductSecond = "0"
                cell.flag = "true"
                
            }
            else
            {
                cell.flag = "false"
                productCheckProductSecond = "1"
                cell.SubMenuimageView.alpha = 0.25
                cell.btnFav?.alpha = 0.25
                cell.lblimenuName.alpha = 0.25
            }
            
            let widthInPixels = cell.SubMenuimageView.frame.width * UIScreen.main.scale
            let heightInPixels = cell.SubMenuimageView.frame.height * UIScreen.main.scale
            
            let imgUrl = temp1.menu_item_image
            let screenSize: CGRect = UIScreen.main.bounds
            let screenScale = UIScreen.main.scale
            let calculateHeight = ((screenSize.width / (screenSize.width*600)/1024)) * ((UIScreen.main.scale) * 160.0)
            print(screenSize.width)
            print(calculateHeight)
            
            cell.SubMenuimageView.image = UIImage(named: "background_placeholder.png")
            
            Alamofire.request(imgUrl!, method: .get).response { response in
                guard let image = UIImage(data:response.data!) else {
                    // Handle error
                    autoreleasepool {
                        cell.SubMenuimageView.image = UIImage(named: "background_placeholder.png")
                    }
                    return
                }
                autoreleasepool {
                    let imageData = UIImageJPEGRepresentation(image,1.0)
                    
//                    cell.SubMenuimageView.image = UIImage(data : imageData!)
                    cell.SubMenuimageView!.sd_setImage(with: URL(string: imgUrl!), completed: nil)
                    cell.SubMenuimageView.contentMode = .scaleToFill
                }
            }
            
            if(temp1.flag == "dislike") {
                
                let image = UIImage(named: "unfav_icon_1")
                cell.btnFav?.setImage(image, for: .normal)
                //               self.stopSpinner()
            }
            else {
                let image = UIImage(named: "fav_icon_1")
                cell.btnFav?.setImage(image, for: .normal)
                //               self.stopSpinner()
            }
            
            cell.btnFav!.tag = indexPath.row
            cell.btnFav?.addTarget(self, action: #selector(clickFavButton(sender:)), for: .touchUpInside)
            
            cell.lblimenuName.text = productSubMenuArray[indexPath.row].menu_item_name
            cell.lblimenuName.numberOfLines = 0
            cell.lblimenuName.lineBreakMode = NSLineBreakMode.byWordWrapping
            tableposition = indexPath.row
            
            // cell.allergencollectionview.delegate = self
            // cell.allergencollectionview.dataSource = self
            // cell.allergencollectionview.tag = indexPath.row
            
            cell.allergensArray = self.getTrueAllergensArray(arr: temp1.allengsArray)
            
         //   let lastElement = cell.allergensArray.endIndex
        //    print("lastElementIndex = \(String(describing: lastElement))")
            
            
            if UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone SE"
                 {
                    if  cell.allergensArray.count > 5{
                        
                        cell.rightBtn.isHidden = false
                        cell.rightArrowImgView.isHidden = false
                        cell.leftBtn.isHidden = true
                        cell.leftArrowImgView.isHidden = true
                    }
                    else{
                        cell.rightBtn.isHidden = true
                        cell.rightArrowImgView.isHidden = true
                        cell.leftBtn.isHidden = true
                        cell.leftArrowImgView.isHidden = true
                        
                        cell.leftBtnWidthConstant.constant = 0
                        cell.leftBtnImgWidthConstant.constant = 0
                        cell.rightBtnWidthConstant.constant = 0
                        cell.rightBtnImgWidthConstant.constant = 0
                    }
                    
                }
              else  if UIDevice.current.modelName == "iPhone 6" || UIDevice.current.modelName == "iPhone 6s" || UIDevice.current.modelName == "iPhone 7" || UIDevice.current.modelName == "iPhone 8" || UIDevice.current.modelName == "iPhone X" || UIDevice.current.modelName == "iPhone XS"
                {
                          if  cell.allergensArray.count > 6{
                                                 
                                cell.rightBtn.isHidden = false
                                cell.rightArrowImgView.isHidden = false
                                cell.leftBtn.isHidden = true
                                cell.leftArrowImgView.isHidden = true
                            }
                            else{
                                cell.rightBtn.isHidden = true
                                cell.rightArrowImgView.isHidden = true
                                cell.leftBtn.isHidden = true
                                cell.leftArrowImgView.isHidden = true
                            
                            cell.leftBtnWidthConstant.constant = 0
                            cell.leftBtnImgWidthConstant.constant = 0
                            cell.rightBtnWidthConstant.constant = 0
                            cell.rightBtnImgWidthConstant.constant = 0
                            }
                }
                else  if UIDevice.current.modelName == "iPhone 6 Plus" || UIDevice.current.modelName == "iPhone 6s Plus" || UIDevice.current.modelName == "iPhone 7 Plus" || UIDevice.current.modelName == "iPhone 8 Plus" || UIDevice.current.modelName == "iPhone XS Max" || UIDevice.current.modelName == "iPhone XR"
                    {
                                 if  cell.allergensArray.count > 7{
                                                        
                                    cell.rightBtn.isHidden = false
                                    cell.rightArrowImgView.isHidden = false
                                    cell.leftBtn.isHidden = true
                                    cell.leftArrowImgView.isHidden = true
                                    }
                                    else{
                                    cell.rightBtn.isHidden = true
                                    cell.rightArrowImgView.isHidden = true
                                    cell.leftBtn.isHidden = true
                                    cell.leftArrowImgView.isHidden = true
                                    
                                    cell.leftBtnWidthConstant.constant = 0
                                    cell.leftBtnImgWidthConstant.constant = 0
                                    cell.rightBtnWidthConstant.constant = 0
                                    cell.rightBtnImgWidthConstant.constant = 0
                                }
                    }
            else{
                if  cell.allergensArray.count > 7{
                                       
                        cell.rightBtn.isHidden = false
                        cell.rightArrowImgView.isHidden = false
                        cell.leftBtn.isHidden = true
                        cell.leftArrowImgView.isHidden = true
                        }
                        else{
                        cell.rightBtn.isHidden = true
                        cell.rightArrowImgView.isHidden = true
                        cell.leftBtn.isHidden = true
                        cell.leftArrowImgView.isHidden = true
                    
                    cell.leftBtnWidthConstant.constant = 0
                    cell.leftBtnImgWidthConstant.constant = 0
                    cell.rightBtnWidthConstant.constant = 0
                    cell.rightBtnImgWidthConstant.constant = 0
                    }
            }
            
//            if (cell.allergencollectionview.visibleCells.count) {
//                // There are invisible cells
//
//            }
            
         
            
            cell.allergencollectionview.reloadData()
            
            /*DispatchQueue.main.async {
                cell.allergencollectionview.reloadData()
            }*/
            
            cellToReturn = cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellgreen", for: indexPath) as! filterTableViewCell
            
            cell.allergensName.text = buffer[indexPath.row].allerganName
            print("id : \(buffer[indexPath.row].allergan_id)")
            
            //      if (flagdidselect == 1)
            //       {
            
            //        var variable = buffer[indexPath.row]
            
            
            print("flag : \(buffer[indexPath.row].isSelected)")
            if  newAllergentIdArray.contains(buffer[indexPath.row].allergan_id) {
                
                let image = UIImage(named: "checkBox_rect.png")
                cell.btncheck.setImage(image, for: .normal)
            }
            else {
                let image1 = UIImage(named: "ic_crop_square")
                cell.btncheck.setImage(image1, for: .normal)
            }
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.someAction))
            cell.btncheck.tag = indexPath.row
            cell.btncheck.addGestureRecognizer(gesture)
            cellToReturn = cell
            
        }
        
        return cellToReturn
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("bool :\(flagdidselect)")
        if (flagdidselect == 1)
        {
            print("bool :\(flagdidselect)")
            var variable = buffer[indexPath.row]
            
            var newAllergentId =  variable.allergan_id!
            //             print("flag1 : \(buffer[indexPath.row].isSelected)")
            if(!self.newAllergentIdArray.contains(newAllergentId))
            {
                print(" variable.isSelected1 : \(variable.isSelected)")
                variable.isSelected = true
                
                
                self.newAllergentIdArray.append(newAllergentId)
                appdelegate.appdeldgtenewArray = newAllergentIdArray
                print("newAllergentIdArray : \(newAllergentIdArray)")
                
                
                
            }
            else
            {
                print(" variable.isSelected2 : \(variable.isSelected)")
                variable.isSelected = false
                //var newAllergentId =  variable.allergan_id!
                newAllergentIdArray = newAllergentIdArray.filter { $0 != newAllergentId}
                appdelegate.appdeldgtenewArray = newAllergentIdArray
                
            }
            filterTableview.reloadData()
        }
        else
        {
            
            var temp = productSubMenuArray[indexPath.row]
            recentMenuId = temp.menu_item_id
            
            if temp.menu_item_flag == "active"
            {
                RecentWebservice()
                
                // var temp1 = allergensDetail[indexPath.row]
                
                var dvc = storyboard?.instantiateViewController(withIdentifier: "productDetailViewController")as! productDetailViewController
                
                var allergens_id =
                    
                    dvc.buffer = temp
                appdelegate.MenuName = temp.menu_item_name
                //        setDefaultValue(keyValue: MenuName, valueIs: temp.menu_item_name)
                //        dvc.product = [temp1]
                //       appdelegate.allergentArray = allergensDetail
                appdelegate.allergentArray = temp.allengsArray
                self.appdelegate.ingredientsArray = temp.ingredientsarray
                self.appdelegate.nutritianArray = temp.nutritianarray
                //        appdelegate.detailImage = temp.menu_item_image
                
                //            print("temp1 : \([temp1])")
                
                print("passdata : \(passdata.count)")
                
                //            dvc.issearch = "0"
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            else
            {
                var msg = "This Menu Item info is unavailable at the moment please check with your server"
                self.vcFor?.modalTransitionStyle = .crossDissolve
                self.vcFor?.modalPresentationStyle = .overFullScreen
                self.vcFor?.isLogin = true
                self.vcFor?.actionFor = ""
                self.vcFor?.titleFor =  msg
                self.present(self.vcFor!, animated: true, completion: nil)
            }
            
        }
    }
    
    func getTrueAllergensArray(arr: [productallergnsClass]) -> [productallergnsClass] {
        var trueArray = [productallergnsClass]()
        
        for dic in arr {
            if dic.flag == "true" {
                trueArray.append(dic)
            }
        }
        
        return trueArray
    }
    
    
    func RecentWebservice()
    {
        
        let headers = ["Content-Type":"Application/json"]
        let _url = view_menuitems
        
        var data1 = [Ani]()
        
        let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
        var  result = try! self.context.fetch(fetch)
        for data in result as! [NSManagedObject]
        {
            data1.append(data as! Ani)
            
        }
        
        
        var parentID = ""
                    if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                         parentID = ""
                    }
                    else{
                        if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                               //   parentID = ""
                            parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                              }
                              else{
                                  parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                              }
                    }
                    print("Parent ID from UserDefaults : \(parentID)")
        
        let parameters: Parameters = ["account_id" :  (data1[0].accId!),"menuitem_id" : recentMenuId ,"merchant_id" : self.appdelegate.merchant_id,"partner_id":parentID]
        print(parameters)
        
        Alamofire.request(_url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.queryString,
                          headers: headers).responseJSON { (response) in
                            
                            print("Request  \(response.request)")
                            
                            print("RESPONSE \(response.result.value)")
                            print("RESPONSE \(response.result)")
                            print("RESPONSE \(response)")
                            
                            if response.result.isSuccess {
                                print("SUKCES with \(response)")
                                
                                if (response.result != nil)
                                {
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    if let res = jsonDic.value(forKey: "result") as? String
                                    {
                                        if res == "failed"
                                        {
                                            //popup
                                            self.stopSpinner()
                                            //                                            var str = response.result.error?.localizedDescription
                                            //                                            print(response.result.error?.localizedDescription)
                                            //
                                            var msg = jsonDic.object(forKey: "msg")as? String
                                            
                                            self.vcFor?.modalTransitionStyle = .crossDissolve
                                            self.vcFor?.modalPresentationStyle = .overFullScreen
                                            self.vcFor?.isLogin = true
                                            self.vcFor?.actionFor = ""
                                            self.vcFor?.titleFor =  msg!
                                            self.present(self.vcFor!, animated: true, completion: nil)
                                            
                                            //                                            showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:str!)
                                        }else
                                        {
                                            
                                            
                                            
                                            self.stopSpinner()
                                            
                                        }  //                                    print(self.marchantArray)
                                    }
                                }
                            }
                                
                            else if response.result.isFailure
                                
                            {
                                //                                let httpError: NSError = response.result.error! as NSError
                                //                                let statusCode = httpError.code
                                //                                let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                                //                                print(error)
                                //
                                self.stopSpinner()
                                
                                var str = response.result.error?.localizedDescription
                                print(response.result.error?.localizedDescription)
                                
                                if str ==
                                    "The Internet connection appears to be offline."
                                {
                                    str = "You are offline please check your internet connection"
                                }
                                self.vcFor?.modalTransitionStyle = .crossDissolve
                                self.vcFor?.modalPresentationStyle = .overFullScreen
                                self.vcFor?.isLogin = true
                                self.vcFor?.actionFor = ""
                                self.vcFor?.titleFor =  str!
                                self.present(self.vcFor!, animated: true, completion: nil)
                                
                                
                                
                                
                                //                                setDefaultValue(keyValue: kmsg, valueIs: ((response.result.error?.localizedDescription)!))
                                //                                var pop = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController")as? PopupViewController
                                //                                self.addChildViewController(pop!)
                                //                                pop!.view.frame = self.view.frame
                                //                                self.view.addSubview(pop!.view)
                                //                                pop?.didMove(toParentViewController: self)
                                
                                //                                completion(dic,0)
                                
                            }
                            
                            
        }
        
        //    self.stopSpinner()
        //    showDialogWithOneButton(animated: true, viewControl: self, titleMsg: "", msgTitle:"Please Internet Connection")
        
    }
    
    
    
    @objc func clickFavButton(sender : UIButton)
    {
        if Reachability.isConnectedToNetwork() {
            self.startSpinner()
            self.favUnFavSet(indexTag: sender.tag)
        }
        else {
            //    self.stopSpinner()
            self.vcFor?.modalTransitionStyle = .crossDissolve
            self.vcFor?.modalPresentationStyle = .overFullScreen
            self.vcFor?.isLogin = true
            self.vcFor?.actionFor = ""
            self.vcFor?.titleFor =  Internet_Connection_Alert
            self.present(self.vcFor!, animated: true, completion: nil)
            //            self.simpleAlert(title: "ANI", details: Internet_Connection_Alert)
        }
        
    }
    
    func favUnFavSet(indexTag: Int) {
        
        let temp1 = productSubMenuArray[indexTag]
        
        if(temp1.flag == "dislike")
        {
            
            let headers = ["Content-Type":"Application/json"]
            let _url = favourite_menuitem
            
            let group_id = getValueForKey(keyValue: kmenu_item_group_id)
            print("group_id : \(group_id)")
            let merchant = getValueForKey(keyValue: kmarchentId)
            print("merchant : \(merchant)")
            
            var data1 = [Ani]()
            
            let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
            let  result = try! self.context.fetch(fetch)
            
            for data in result as [NSManagedObject]
            {
                data1.append(data as! Ani)
                
            }
            
            print("menu_item_id_fav : \(temp1.menu_item_id!)")
            
            var parentID = ""
                 if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                      parentID = ""
                 }
                 else{
                     if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                              // parentID = ""
                        parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                           }
                           else{
                               parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                           }
                 }
                 print("Parent ID from UserDefaults : \(parentID)")
            
            
            let parameters: Parameters = ["merchant_id" :self.appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"type" : "add", "menuitem_id" : temp1.menu_item_id!,"partner_id":parentID]
            
            print(parameters)
            
            Alamofire.request(_url,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding.queryString,
                              headers: headers).responseJSON { (response) in
                                print("--------------------------------------------------------------------------)")
                                print("Request  \(response.request)")
                                print("RESPONSE \(response.result.value)")
                                print("RESPONSE \(response.result)")
                                print("RESPONSE \(response)")
                                
                                if response.result.isSuccess {
                                    print("SUKCES with \(response)")
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    let res = jsonDic.value(forKey: "result") as? String
                                    if (res == "success"){
                                        let fav = jsonDic.value(forKey: "favourite") as? String
                                        if (fav == "yes") {
                                            temp1.flag="like"
                                        } else {
                                            temp1.flag="dislike"
                                        }
                                    }
                                    
                                    self.tableview.reloadData()
                                    self.stopSpinner()
                                    let msg = jsonDic.value(forKey: "msg") as? String
                                    self.view.makeToast(msg)
//                                    showDialogWithOneButton(viewControl: self, titleMsg: "ANI", msgTitle: jsonDic.value(forKey: "msg") as? String ?? "favrouites changed")
                                    
                                    //                                    DispatchQueue.main.async{
                                    //
                                    //                                    }
                                    
                                }
                                else if (response.result.isFailure) {
                                    
                                    self.stopSpinner()
                                    var str = response.result.error?.localizedDescription
                                    print(response.result.error?.localizedDescription)
                                    
                                    if str ==
                                        "The Internet connection appears to be offline."
                                    {
                                        str = "You are offline please check your internet connection"
                                    }
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  str!
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                                }
            }
            
        }
        else {
            
            let headers = ["Content-Type":"Application/json"]
            let _url = favourite_menuitem
            
            let group_id = getValueForKey(keyValue: kmenu_item_group_id)
            print("group_id : \(group_id)")
            let merchant = getValueForKey(keyValue: kmarchentId)
            print("merchant : \(merchant)")
            
            var data1 = [Ani]()
            
            let fetch = NSFetchRequest<Ani>.init(entityName: "Ani")
            let  result = try! self.context.fetch(fetch)
            for data in result as [NSManagedObject]
            {
                data1.append(data as! Ani)
                
            }
            
            print("menu_item_id_fav : \(menu_item_id_fav)")
            
            var parentID = ""
                           if UserDefaults.standard.value(forKey: "fromParent") as? String == nil {
                                parentID = ""
                           }
                           else{
                               if UserDefaults.standard.value(forKey: "fromParent") as! String == ""{
                                     //    parentID = ""
                                parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                     }
                                     else{
                                         parentID = UserDefaults.standard.value(forKey: "parentID") as! String
                                     }
                           }
                           print("Parent ID from UserDefaults : \(parentID)")
            
            
            let parameters: Parameters = ["merchant_id" :self.appdelegate.merchant_id, "account_id" : (data1[0].accId!) ,"type" : "delete", "menuitem_id" : temp1.menu_item_id!,"partner_id":parentID]
            
            print(parameters)
            
            Alamofire.request(_url,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding.queryString,
                              headers: headers).responseJSON { (response) in
                                print("Request  \(response.request)")
                                print("RESPONSE \(response.result.value)")
                                print("RESPONSE \(response.result)")
                                print("RESPONSE \(response)")
                                
                                if response.result.isSuccess {
                                    print("SUKCES with \(response)")
                                    let jsonDic = response.result.value as! NSDictionary
                                    print(jsonDic)
                                    let res = jsonDic.value(forKey: "result") as? String
                                    if (res == "success") {
                                        let fav = jsonDic.value(forKey: "favourite") as? String
                                        if (fav == "yes") {
                                            temp1.flag="like"
                                        } else {
                                            temp1.flag="dislike"
                                        }
                                    }
                                    
                                    self.tableview.reloadData()
                                    self.stopSpinner()
                                    
                                    let msg = jsonDic.value(forKey: "msg") as? String
                                    self.view.makeToast(msg)
//                                    showDialogWithOneButton(viewControl: self, titleMsg: "ANI", msgTitle: jsonDic.value(forKey: "msg") as? String ?? "favrouites changed")
                                    //                                    DispatchQueue.main.async{
                                    //
                                    //                                    }
                                    
                                }
                                else if (response.result.isFailure) {
                                    
                                    self.stopSpinner()
                                    var str = response.result.error?.localizedDescription
                                    print(response.result.error?.localizedDescription)
                                    
                                    if str ==
                                        "The Internet connection appears to be offline."
                                    {
                                        str = "You are offline please check your internet connection"
                                    }
                                    
                                    self.vcFor?.modalTransitionStyle = .crossDissolve
                                    self.vcFor?.modalPresentationStyle = .overFullScreen
                                    self.vcFor?.isLogin = true
                                    self.vcFor?.actionFor = ""
                                    self.vcFor?.titleFor =  str!
                                    self.present(self.vcFor!, animated: true, completion: nil)
                                    
                                }
            }
            
        }
        
    }
}


extension productListTableViewCell:
   UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return allergensArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! allergensCollectionViewCell
        let dic = allergensArray[indexPath.row]
        
        if flag == "false"
        {
            cell.allegensImage.alpha = 0.25
        }
        else
        {
          cell.allegensImage.alpha = 1
        }
        
        
        if dic.flag == "true"
        {
            if dic.allergan_Img != nil && dic.allergan_Img != "" {
                let imgURL = dic.allergan_Img!
                let url = URL(string: imgURL)
                cell.allegensImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "allergens_placeholder"), options: .retryFailed, completed: nil)
                cell.allegensImage.contentMode = .scaleAspectFill
                
            }
            else {
                cell.allegensImage.image = #imageLiteral(resourceName: "allergens_placeholder")
                cell.allegensImage.contentMode = .scaleAspectFit
            }
        }
        
//        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
//        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
//        print("visibleIndexPath = \(visibleIndexPath?.row)")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        
        //      if UIDevice.current.modelName == "iPhone 5c"
        //      {
        //        if (allergensArray.count > 5)
        //            {
        //                let collectionWidth = collectionView.bounds.width
        //                var count: CGFloat =  CGFloat(allergensArray.count)
        //                return CGSize(width: collectionWidth/count, height: collectionWidth/count)
        //        }
        //    }
        let collectionWidth = collectionView.bounds.width
        var count: CGFloat =  CGFloat(allergensArray.count)
        return CGSize(width: 50, height:50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
     //for Scroll Arrow displaying
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone SE"
                       {
                          if  allergensArray.count > 5{
                              if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                  
                                       //Load more data & reload your collection view
                                        
                                        print("Last cell is visible")
                                        
                                        rightBtn.isHidden = true
                                        rightArrowImgView.isHidden = true
                                       self.rightBtnWidthConstant.constant = 0
                                       self.rightBtnImgWidthConstant.constant = 0
                                                   
                                     }
                                     else{
                                        print("Last cell is invisible")
                                        
                                        rightBtn.isHidden = false
                                        rightArrowImgView.isHidden = false
                                        self.rightBtnWidthConstant.constant = 25
                                        self.rightBtnImgWidthConstant.constant = 20
                                                              
                                    }
                             
                        //    let lastElement = allergensArray.last
                        //    print("lastElement = \(String(describing: lastElement))")
                            
//                          let  firstElementIndex = self.allergencollectionview.indexPathsForVisibleItems.first
//                             print("firstElementIndex = \(String(describing: firstElementIndex))")
//
//                            let isCellVisible = self.allergencollectionview.visibleCells.map { self.allergencollectionview.indexPath(for: $0) }.contains([0,0])
//                            print("isCellVisible = \(String(describing: isCellVisible))")
                            
                            
//                            if (self.appdelegate.firstLoadScrollFlag == true){
//
//                                self.appdelegate.firstLoadScrollFlag = false
//
//                                print("first load cell is visible")
//                                print("indexPath.row = \(indexPath.row)")
//
//                                leftBtn.isHidden = true
//                                leftArrowImgView.isHidden = true
//                                self.leftBtnWidthConstant.constant = 0
//                                self.leftBtnImgWidthConstant.constant = 0
//                            }
//                            else{
                               if (indexPath.row == 0 ) { //it's your first cell
                                        //Load more data & reload your collection view
                                                                              
                                print("first cell is visible")
                                print("indexPath.row = \(indexPath.row)")
                                                                      
                                leftBtn.isHidden = true
                                leftArrowImgView.isHidden = true
                                self.leftBtnWidthConstant.constant = 0
                                self.leftBtnImgWidthConstant.constant = 0
                                                                      
                                }
                                else{
                                print("first cell is invisible")
                                print("indexPath.row = \(indexPath.row)")
                                                                      
                                leftBtn.isHidden = false
                                leftArrowImgView.isHidden = false
                                self.leftBtnWidthConstant.constant = 25
                                self.leftBtnImgWidthConstant.constant = 20
                              }
                     //       }
                            
                                   
                            
//                            let visible = allergencollectionview.indexPathsForVisibleItems
//                            print("visibleCell = \(visible))")
//                            print("indexPath = \(indexPath)")
                            
                          }
                        
                      }
                    else  if UIDevice.current.modelName == "iPhone 6" || UIDevice.current.modelName == "iPhone 6s" || UIDevice.current.modelName == "iPhone 7" || UIDevice.current.modelName == "iPhone 8" || UIDevice.current.modelName == "iPhone X" || UIDevice.current.modelName == "iPhone XS"
                      {
                                if  allergensArray.count > 6{
                                                       if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                                           
                                                                //Load more data & reload your collection view
                                                                 
                                                                 print("Last cell is visible")
                                                                 
                                                                 rightBtn.isHidden = true
                                                                 rightArrowImgView.isHidden = true
                                                                self.rightBtnWidthConstant.constant = 0
                                                                self.rightBtnImgWidthConstant.constant = 0
                                                                            
                                                              }
                                                              else{
                                                                 print("Last cell is invisible")
                                                                 
                                                                 rightBtn.isHidden = false
                                                                 rightArrowImgView.isHidden = false
                                                                 self.rightBtnWidthConstant.constant = 25
                                                                 self.rightBtnImgWidthConstant.constant = 20
                                                                                       
                                                             }
                                                             
                                                              if (indexPath.row == 0 ) { //it's your first cell
                                                                        //Load more data & reload your collection view
                                                                         
                                                                 print("first cell is visible")
                                                                    
                                                                 leftBtn.isHidden = true
                                                                 leftArrowImgView.isHidden = true
                                                                 self.leftBtnWidthConstant.constant = 0
                                                                 self.leftBtnImgWidthConstant.constant = 0
                                                                 
                                                                 }
                                                                 else{
                                                                   print("first cell is invisible")
                                                                 
                                                                 leftBtn.isHidden = false
                                                                 leftArrowImgView.isHidden = false
                                                                 self.leftBtnWidthConstant.constant = 25
                                                                 self.leftBtnImgWidthConstant.constant = 20
                                                             }
                                    
                                  }
                                 
                      }
                      else  if UIDevice.current.modelName == "iPhone 6 Plus" || UIDevice.current.modelName == "iPhone 6s Plus" || UIDevice.current.modelName == "iPhone 7 Plus" || UIDevice.current.modelName == "iPhone 8 Plus" || UIDevice.current.modelName == "iPhone XS Max" || UIDevice.current.modelName == "iPhone XR"
                          {
                                    if  allergensArray.count > 7{
                                                              if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                                                  
                                                                       //Load more data & reload your collection view
                                                                        
                                                                        print("Last cell is visible")
                                                                        
                                                                        rightBtn.isHidden = true
                                                                        rightArrowImgView.isHidden = true
                                                                       self.rightBtnWidthConstant.constant = 0
                                                                       self.rightBtnImgWidthConstant.constant = 0
                                                                                   
                                                                     }
                                                                     else{
                                                                        print("Last cell is invisible")
                                                                        
                                                                        rightBtn.isHidden = false
                                                                        rightArrowImgView.isHidden = false
                                                                        self.rightBtnWidthConstant.constant = 25
                                                                        self.rightBtnImgWidthConstant.constant = 20
                                                                                              
                                                                    }
                                                                    
                                                                     if (indexPath.row == 0 ) { //it's your first cell
                                                                               //Load more data & reload your collection view
                                                                                
                                                                        print("first cell is visible")
                                                                           
                                                                        leftBtn.isHidden = true
                                                                        leftArrowImgView.isHidden = true
                                                                        self.leftBtnWidthConstant.constant = 0
                                                                        self.leftBtnImgWidthConstant.constant = 0
                                                                        
                                                                        }
                                                                        else{
                                                                          print("first cell is invisible")
                                                                        
                                                                        leftBtn.isHidden = false
                                                                        leftArrowImgView.isHidden = false
                                                                        self.leftBtnWidthConstant.constant = 25
                                                                        self.leftBtnImgWidthConstant.constant = 20
                                                                    }
                                        
                                    }
                                         
                          }
                  else{
                      if  allergensArray.count > 7{
                                             if (indexPath.row == allergensArray.count - 1 ) { //it's your last cell
                                                 
                                                      //Load more data & reload your collection view
                                                       
                                                       print("Last cell is visible")
                                                       
                                                       rightBtn.isHidden = true
                                                       rightArrowImgView.isHidden = true
                                                      self.rightBtnWidthConstant.constant = 0
                                                      self.rightBtnImgWidthConstant.constant = 0
                                                                  
                                                    }
                                                    else{
                                                       print("Last cell is invisible")
                                                       
                                                       rightBtn.isHidden = false
                                                       rightArrowImgView.isHidden = false
                                                       self.rightBtnWidthConstant.constant = 25
                                                       self.rightBtnImgWidthConstant.constant = 20
                                                                             
                                                   }
                                                   
                                                    if (indexPath.row == 0 ) { //it's your first cell
                                                              //Load more data & reload your collection view
                                                               
                                                       print("first cell is visible")
                                                          
                                                       leftBtn.isHidden = true
                                                       leftArrowImgView.isHidden = true
                                                       self.leftBtnWidthConstant.constant = 0
                                                       self.leftBtnImgWidthConstant.constant = 0
                                                       
                                                       }
                                                       else{
                                                         print("first cell is invisible")
                                                       
                                                       leftBtn.isHidden = false
                                                       leftArrowImgView.isHidden = false
                                                       self.leftBtnWidthConstant.constant = 25
                                                       self.leftBtnImgWidthConstant.constant = 20
                                                   }
                             
                        }
                           
                  }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
         print("didEndDisplaying = \(indexPath.row)")
    }
    
    
}



public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
     
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,3":                  return "iPhone 8 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
            
            
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}




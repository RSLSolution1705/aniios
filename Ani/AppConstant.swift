//
//  AppConstant.swift
//  Ani
//
//  Created by RSL-01 on 14/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//
//bit bucket path
//name- aniios - origin1


import Foundation
import UIKit
import PopupDialog
import Alamofire

typealias apiSuccess = (_ result: NSDictionary?) -> Void
typealias apiProgress = (_ result: NSDictionary?) -> Void // when you want to download or upload using Alamofire..
var gradientLayer: CAGradientLayer!
typealias apiFailure = (_ error: NSDictionary?) -> Void
let kemail = "kemail"
let kmobNumber = "kmobNumber"
let KDOB = "KDOB"
let kSuccessResponse = "1"
let kFailureResponse = "0"
let UDID = UIDevice.current.identifierForVendor!.uuidString
let kaccountIdnumber = ""
let kdeviceToken = "deviceToken"
let kTYpEIOS = "ios"
let ksideMenubutton = "menubar"
let kDeviceType = "ios"
let kLoginTypeEmail = "Email"
let kmetaString = "kmetaString"
let kprefferedmerchant = "prefferedmerchant"
let kspecialoffer = "specialoffer"
let kfirstName = "user"
let klastName = "klastName"
let kProfileName = "kProfileName"
//let kimageview = "userprofile"
var MenuName = "MenuName"
var kimageview : AnyObject = "" as AnyObject
let kmarchentId = "kmarchentId"
let kmenu_item_group_id = ""
var Mname = "Mname"
let knewMetaString = "knewMetaString"
let krole = "krole"
let allergentIdString = "allergenids"
let kErrorMsg = "Oops!"
let kUserId = "usrId"
let kIsUserLogin = "1"
let kIsUserLogout = "1"
let kUserObject = "usrObject"
let kback = "ic_arrow_back_white"
let kmarchentName = "kmarchentName"
let kMerchantImage = "kmerchantImage"
//image
let kplaceHolderImge = "background_placeholder"

let loginEmailNotvalid = "Please enter email address"
let loginPaswordNotvalid = "Please enter password"
let loginEmailIsNotValid = "Please enter valid email address"
let loginPhoneNo = "Please enter phone number"
let signupNot6 = "Password must be between 6 - 12 characters"
let signupNot12 = signupNot6
let paswodEmpty = "Password should not be empty"
let Klatitude = "Klatitude"
let Klongitude = "Klongitude"
let KkEmail = "KkEmail"
let KMobile = "KMobile"
let KAddress = "KAddress"
 let ktagline  = "ktagline"
let khome = "khome"
let kgender = "kgender"
let kmsg = "kmsg"
let kregisterAccountId = "kregisterAccountId"
let KmarchentAddress = "KmarchentAddress"
let Khide_nutritional_info = "hide_nutritional_info"

let loginNametvalid = "Please enter name"
let loginPasswordvalid = "Please enter password"
let marchentName = "marchentName"
//let registerAccountNo = Int!
let registertype = "registertype"
let registerconsumer_login_type = "registerconsumer_login_type"

let kScanQrCodeError = "Your device does not support scanning a code from an item. Please use a device with a camera."
let KcontaminationMsg = "KcontaminationMsg"

////////////////////
//Testing
//let BaseUrl = "http://test.theaniapp.com/ws/"

//Live
//Latest
let BaseUrl = "https://www.theaniapp.com/ws_1.0/"

//old
//let BaseUrl = "https://www.theaniapp.com/ws/"

//////////////////////////
//url
//let BaseUrl = "http://test.theaniapp.com/ws/"

//let BaseUrl = "https://testtheaniapp.tk/ws/"

//let BaseUrl = "https://theaniapp.com/ws/"

//live
//https://www.theaniapp.com/ws/

//live url : https://theaniapp.com/ws/
//testing Url : http://99.81.230.36/ANI_TEST/ws/
let get_all_prefered_merchants_new = BaseUrl+"get_all_prefered_merchants_new.php"
let get_scanned_partner = BaseUrl+"get_scanned_partner.php"
let partner_lists_by_recentview = BaseUrl+"partner_lists_by_recentview.php"
let partner_lists_by_favourite = BaseUrl+"partner_lists_by_favourite.php"

let user_login_new = BaseUrl+"user_login_new.php"
let user_social_login = BaseUrl+"social_registration.php"
let forgot_password = BaseUrl+"forgot_password.php"
let check_forUpdate = BaseUrl+"ios_app_version.php"
let email_confirmation = BaseUrl+"email_confirmation.php"
let consumer_registration1 = BaseUrl+"consumer_registration1.php"
let send_confirmation_code = BaseUrl+"send_confirmation_code.php"
let consumer_registration = BaseUrl+"consumer_registration.php"
let user_logout = BaseUrl+"user_logout.php"
                
let merchant_wise_menuitems = BaseUrl+"merchant_wise_menuitems.php"

let global_search = BaseUrl+"global_search.php"
let favourite_menuitem = BaseUrl+"favourite_menuitem.php"
let view_menuitems = BaseUrl+"view_menuitems.php"
let get_all_menuitems = BaseUrl+"get_all_menuitems.php"
let get_all_fevorites_menuitems = BaseUrl+"get_all_fevorites_menuitems.php"
let get_view_menuitems = BaseUrl+"get_view_menuitems.php"
let send_mails = BaseUrl+"send_mails.php"
let recommanded_merchant = BaseUrl+"recommanded_merchant.php"
let update_consumer_information = BaseUrl+"update_consumer_information.php"
let change_profile_image1 = BaseUrl+"change_profile_image1.php"
let consumer_information = BaseUrl+"consumer_information.php"
let remove_profile_image = BaseUrl+"remove_profile_image.php"

let Privacy_Template_ANI_0119 = BaseUrl+"Privacy_Template_ANI_0119.pdf"

 

let Internet_Connection_Alert = "You are offline. Please check your internet connection."
extension UIViewController
{
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
    	
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func simpleAlert(title: String, details: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(details)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}


func createGradientLayer(view : UIView, width: CGFloat, height: CGFloat) {
    gradientLayer = CAGradientLayer()
    //gradientLayer.frame = view.frame
    gradientLayer.frame.origin.x = 0
    gradientLayer.frame.origin.y = 0
    gradientLayer.frame.size = CGSize(width: 500, height: height)
    gradientLayer.colors = [UIColor.clear.withAlphaComponent(0.0).cgColor, UIColor.black.withAlphaComponent(0.8).cgColor]
    
    view.layer.addSublayer(gradientLayer)
    view.layoutIfNeeded()
}



func showDialogWithOneButton(animated: Bool = true, viewControl: UIViewController, titleMsg: String, msgTitle: String)
{
    // Prepare the popup
    let title = titleMsg
    let message = msgTitle
    
    // Create the dialog
    let popup = PopupDialog(title: title, message: message, buttonAlignment: .horizontal, transitionStyle: .zoomIn, gestureDismissal: true) {
        print("Completed")
    }
    let overlayAppearance = PopupDialogOverlayView.appearance()
    
    overlayAppearance.color       = UIColor.black
    overlayAppearance.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
    overlayAppearance.blurRadius  = 20
    overlayAppearance.blurEnabled = true
    overlayAppearance.opacity     = 0.7
    
    // Create first button
    let buttonOne = CancelButton(title: "Ok") {
        
    }
    
    // Add buttons to dialog
    popup.addButtons([buttonOne])
    
    // Present dialog
    viewControl.present(popup, animated: animated, completion: nil)
}

//func isUserLogin() -> Bool
//{
//    let defaults = UserDefaults.standard
//    defaults.synchronize()
//    UserDefaults.standard.synchronize()
//    
//    let usrIsLogin = defaults.value(forKey: kUserId) as! String
//    if(usrIsLogin == kIsUserLogin)
//    {
//        return true
//    }
//    else{
//        return false
//    }
//}
//
//func isSocialLogin() -> Bool
//{
//    let defaults = UserDefaults.standard
//    defaults.synchronize()
//    UserDefaults.standard.synchronize()
//    
//    let usrIsLogin = defaults.value(forKey: "isSocialLogin") as! Bool
//    
//    return usrIsLogin
//    
//}




func logoutUser()
{
    let defaults = UserDefaults.standard
    defaults.set("", forKey: kUserId)
    defaults.set("", forKey: kUserObject)
}

// MARK: - User Default Function

func setDefaultValue(keyValue: String, valueIs: String)
{
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    usrdefault.set(valueIs, forKey: keyValue)
    usrdefault.synchronize()
}

func removeobjectforkey(keyValue: String)
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    defaults.removeObject(forKey: keyValue)
    defaults.synchronize()
}

func setArrayDefaultValue(keyValue: String, valueIs: NSMutableArray)
{
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    usrdefault.set(valueIs, forKey: keyValue)
    usrdefault.synchronize()
}

func getArrayofStrings(keyValue: String) -> NSMutableArray
{
    var objectsAre = NSMutableArray()
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    
    if usrdefault.value(forKey: keyValue) == nil
    {
        
    }
    else
    {
        objectsAre = usrdefault.value(forKey: keyValue) as! NSMutableArray
    }
    
    return objectsAre
}



func getValueOfString(keyValue: String) -> String
{
    var valueOfThatKey = ""
    
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    
    if usrdefault.value(forKey: keyValue) == nil
    {
        valueOfThatKey = ""
    }
    else
    {
        valueOfThatKey = usrdefault.value(forKey: keyValue) as! String
    }
    
    
    return valueOfThatKey
}
func isValidEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
//    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
//
//    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//    return emailTest.evaluate(with: testStr)
    //    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    //
    //    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    //    return emailTest.evaluate(with: testStr)
    
    print("validate emilId: \(testStr)")
    let emailRegEx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    
    return emailTest.evaluate(with: testStr)
    let result = emailTest.evaluate(with: testStr)
    return result
    
}
//".*[^A-Za-z ].*"
func isValidName(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    //    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    //
    //    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    //    return emailTest.evaluate(with: testStr)
    //    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    //
    //    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    //    return emailTest.evaluate(with: testStr)
    
    print("validate emilId: \(testStr)")
    let nameRegEx = ".*[^A-Za-z ].*"
    let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
    
    return nameTest.evaluate(with: testStr)
    let result = nameTest.evaluate(with: testStr)
    return result
    
}
func getValueForKey(keyValue: String) -> String
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    UserDefaults.standard.synchronize()
    
    let valueForStr = defaults.value(forKey: keyValue)
    if(valueForStr != nil)
    {
        return valueForStr as! String
    }
    else{
        return ""
    }
    
}

//func showDialogWithOneButton(animated: Bool = true, viewControl: UIViewController, titleMsg: String, msgTitle: String)
//{
//    // Prepare the popup
//    let title = titleMsg
//    let message = msgTitle
//
//    // Create the dialog
//    let popup = PopupDialog(title: title, message: message, buttonAlignment: .horizontal, transitionStyle: .zoomIn, gestureDismissal: true) {
//        print("Completed")
//    }
//
//    // Create first button
//    let buttonOne = CancelButton(title: "Ok") {
//
//    }
//
//    // Add buttons to dialog
//    popup.addButtons([buttonOne])
//
//    // Present dialog
//    viewControl.present(popup, animated: animated, completion: nil)
//}

//// MARK: - Internet connected check
//func isConnectedToNetwork() -> Bool {
//
//
//    if Reachability.isConnectedToNetwork() == true
//    {
//        return true
//    }
//    else
//    {
//        return false
//    }
//}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}


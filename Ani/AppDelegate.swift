//
//  AppDelegate.swift
//  Ani
//
//  Created by RSL-01 on 05/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

//4 sep 2020

import UIKit
import CoreData
import UserNotifications
import Firebase
import FirebaseAnalytics
import PKHUD
import IQKeyboardManagerSwift
import GoogleSignIn


import FBSDKCoreKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate,GIDSignInDelegate {
    
    
    func sign(_ signIzn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    
    var window: UIWindow?
    var kaccountIdString = ""
    var merchant_id = ""
  //  var account_id = ""
    var isSelected = false
    var clearArray = 0
    var  allergentIdString = ""
    var profile_image : UIImage!
    var MenuName = ""
    var Mname = ""
    var detailImage :String!
    var detailName: String!
    var MarImage  : UIImage!
    var profileImage : UIImage!
    var flagscanQr = 0
    var marchentNameForshare : String!
    var allergentArray = [productallergnsClass]()
    var nutritianArray = [nutritianArrayObject]()
    var ingredientsArray = [ingredientsArrayObject]()
    var appdeldgtenewArray = [String]()
    //    var islogin
    var recentflag = 0
    var favflag = 0
    var categoryflag = 0
    var closePopUpFlag = 0
    
    var firstLoadScrollFlag = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        
        
        
           IQKeyboardManager.shared.enable = true
        
        // Override point for customization after application launch.
      /*
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        //        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        
        */
    
        
        
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = "21530822958-jfm91b1q1bh3b4vgcqrd4n59e11kfngp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        //       UserDefaults.standard.set(false, forKey: "isUserLog")
        
        
        
        //        let navigationController: UINavigationController? = (self.window?.rootViewController as? UINavigationController)
        //        if  UserDefaults.standard.bool(forKey: "isUserLog") == true
        //        {
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //
        //            let vc : sideMenuViewController = storyboard.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
        //            navigationController?.pushViewController(vc, animated: true)
        //
        //        }
        //        else
        //        {
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //
        //            let vc1 : signuppageViewController = storyboard.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
        //            navigationController?.pushViewController(vc1, animated: true)
        //        }
        
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //FBSDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }

    
  
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {

        return GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)

    }
    
//        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//    }
   
  
//  func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//
//        if url.scheme == "fbxxxxxxxxxxxx" {
//            return FBSDKCoreKit.ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//        }
//        else {
//            let options: [String: AnyObject] = [UIApplication.OpenURLOptionsKey.sourceApplication.rawValue: sourceApplication! as AnyObject,
//                                                UIApplication.OpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
//
//            return FBSDKCoreKit.ApplicationDelegate.shared.application(
//                application,
//                open: url,
//                sourceApplication: sourceApplication,
//                annotation: annotation) ||
//                GIDSignIn.sharedInstance().handle(url, sourceApplication: options["UIApplicationOpenURLOptionsSourceApplicationKey"] as? String, annotation: nil)
//        }
//
//
//    }
    
    ////////////
    //Google
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
//    {
//        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
////        return ApplicationDelegate.shared.application(app, open: url, options: options)
//    }
    
    //Facebook
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//
//           let handled = ApplicationDelegate.shared.application(app, open: url, options: options)
//
//                 return handled
//       }
    
//facebook and Google
  func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    
     //return facebook and Google
        return ApplicationDelegate.shared.application(app, open: url, options: options) ||
                    GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    
}
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //        if let instanceIdToken = InstanceID.instanceID().token() {
    //            print("New token \(instanceIdToken)")
    //            sharedData["instanceIdToken"] = instanceIdToken
    //        }
    //    }
    
    /*func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //        InstanceID.instanceID().instanceID { (result, error) in
        //            if let error = error {
        //                print("Error fetching remote instance ID: \(error)")
        //            } else if let result = result {
        //                print("Remote instance ID token: \(result.token)")
        ////                self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
        //            }
        //        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        
        setDefaultValue(keyValue: kdeviceToken, valueIs: token)
        //        print("\(deviceToken)")
        //     Messaging.messaging().apnsToken = deviceToken
        //
        //        if let instanceIdToken = InstanceID.instanceID().token() {
        //            print("New token \(instanceIdToken)")
        //            sharedData["instanceIdToken"] = instanceIdToken
        //        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    */
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Ani")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
 
    
   /////
}
extension UIImageView
{
    func setimage(url : String)
    {
        
    }
}
extension UIImageView {
    
    func setRounded() {
        let radius = self.frame.size.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}

////
////  AppDelegate.swift
////  Ani
////
////  Created by RSL-01 on 05/03/19.
////  Copyright © 2019 rsl-01. All rights reserved.
////
//
//import UIKit
//import CoreData
////import FacebookCore
//import UserNotifications
//import Firebase
//import FirebaseAnalytics
//import PKHUD
//
//@UIApplicationMain
//class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
//
//    var window: UIWindow?
//    var kaccountIdString = ""
//    var merchant_id = ""
//    var isSelected = false
//    var  allergentIdString = ""
//    var profile_image : UIImage!
//    var MenuName = ""
//    var Mname = ""
//    var detailImage :String!
//    var detailName: String!
//    var MarImage  : UIImage!
//    var profileImage : UIImage!
//    var flagscanQr = 0
//    var marchentNameForshare : String!
//    var allergentArray = [productallergnsClass]()
//    var nutritianArray = [nutritianArrayObject]()
//    var ingredientsArray = [ingredientsArrayObject]()
//    var appdeldgtenewArray = [String]()
//    //    var islogin
//
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        // Override point for customization after application launch.
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//        //        Messaging.messaging().delegate = self
//        application.registerForRemoteNotifications()
//        //        FirebaseApp.configure()
//        //       UserDefaults.standard.set(false, forKey: "isUserLog")
//
//
//
////        let navigationController: UINavigationController? = (self.window?.rootViewController as? UINavigationController)
////        if  UserDefaults.standard.bool(forKey: "isUserLog") == true
////        {
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////
////            let vc : sideMenuViewController = storyboard.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
////            navigationController?.pushViewController(vc, animated: true)
////
////        }
////        else
////        {
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////
////            let vc1 : signuppageViewController = storyboard.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
////            navigationController?.pushViewController(vc1, animated: true)
////        }
//
//
//        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
//        return true
//    }
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
//    }
//
//
//    func applicationWillResignActive(_ application: UIApplication) {
//        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//    }
//
//    func applicationDidEnterBackground(_ application: UIApplication) {
//        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    }
//
//    func applicationWillEnterForeground(_ application: UIApplication) {
//        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//    }
//
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    }
//
//    func applicationWillTerminate(_ application: UIApplication) {
//        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        // Saves changes in the application's managed object context before the application terminates.
//        self.saveContext()
//    }
//
//    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//    //        if let instanceIdToken = InstanceID.instanceID().token() {
//    //            print("New token \(instanceIdToken)")
//    //            sharedData["instanceIdToken"] = instanceIdToken
//    //        }
//    //    }
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        //        InstanceID.instanceID().instanceID { (result, error) in
//        //            if let error = error {
//        //                print("Error fetching remote instance ID: \(error)")
//        //            } else if let result = result {
//        //                print("Remote instance ID token: \(result.token)")
//        ////                self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
//        //            }
//        //        }
//    }
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        print(token)
//
//        setDefaultValue(keyValue: kdeviceToken, valueIs: token)
//        //        print("\(deviceToken)")
//        //     Messaging.messaging().apnsToken = deviceToken
//        //
//        //        if let instanceIdToken = InstanceID.instanceID().token() {
//        //            print("New token \(instanceIdToken)")
//        //            sharedData["instanceIdToken"] = instanceIdToken
//        //        }
//    }
//
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//
//        print("i am not available in simulator \(error)")
//    }
//
//
//
//
//    // MARK: - Core Data stack
//
//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//         */
//        let container = NSPersistentContainer(name: "Ani")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }
//
//}
//extension UIImageView
//{
//    func setimage(url : String)
//    {
//
//    }
//}
//extension UIImageView {
//
//    func setRounded() {
//        let radius = self.frame.size.width / 2
//        self.layer.cornerRadius = radius
//        self.layer.masksToBounds = true
//    }
//}

//
//  RecentVisitArray.swift
//  Ani
//
//  Created by Mac on 22/07/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import Foundation

class RecentVisitArray: NSObject {
    
    var merchant_id : String!
    var merchant_name : String!
    var merchant_image : String!
    var qr_code : String!
    var latitude : String!
    var longitude : String!
    var address : String!
    var mobile : String!
   
}


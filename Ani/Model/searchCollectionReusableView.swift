//
//  searchCollectionReusableView.swift
//  Ani
//
//  Created by RSL-01 on 15/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class searchCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var searchBar: UISearchBar!
}

//
//  productMenuListModelClass.swift
//  Ani
//
//  Created by RSL-01 on 30/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import Foundation
import UIKit
//class productMenuListModelClass
//{
//    var menu_item_group_image: String!
//    var menu_item_group_id : String!
//    var menu_item_group_name : String!
//}


class productMenuListModelClass : NSObject {
    
    var menu_item_group_image : String?
    var menu_item_group_id : String?
    var menu_item_group_name : String?
    var menu_item_description : String?
    var ingredient_deceleration_info : String?
    var type : String?
    var allengsArray = [productallergnsClass]()
    var nutritianarray = [nutritianArrayObject]()
    var ingredientsarray = [ingredientsArrayObject]()
    var menu_item_flag : String?
    var shopopen : String?
    override init() {
    }
    
    init(fromDictionary dictionary: [String:Any]){
        if let val = dictionary["menu_item_group_image"] as? String {
            menu_item_group_image = val
        }
        if let val = dictionary["menu_item_image"] as? String {
            menu_item_group_image = val
        }
        if let val = dictionary["menu_item_group_id"] as? String {
            menu_item_group_id = val
        }
        if let val = dictionary["menu_item_id"] as? String {
            menu_item_group_id = val
        }
        if let val = dictionary["menu_item_group_name"] as? String {
            menu_item_group_name = val
        }
        if let val = dictionary["menu_item_name"] as? String {
            menu_item_group_name = val
        }
        if let val = dictionary["ingredient_deceleration_info"] as? String {
            ingredient_deceleration_info = val
        }
        if let val = dictionary["menu_item_description"] as? String {
            menu_item_description = val
        }
        
        // Done by Pratik
        
        if let val = dictionary["menuitem_flag"] as? String {
            menu_item_flag = val
        }
        
        // Done
        type = dictionary["type"] as? String
        
        
        allengsArray = [productallergnsClass]()
        if let AllergensArray = dictionary["allergan_array"] as? [[String:Any]] {
            for dic in AllergensArray{
               let value = productallergnsClass()
                value.allergan_Img = dic["allergan_images"] as? String
                value.allergen_id = dic["allergan_id"] as? String
                value.allergensName = dic["allergan"] as? String
                allengsArray.append(value)
            }
        }
        

        
        nutritianarray = [nutritianArrayObject]()
        if let NutritionArray = dictionary["nutritian_array"] as? [[String:Any]] {
            for dic in NutritionArray{
                let value = nutritianArrayObject()
                
                value.name = dic["name"] as? String
                value.unit = dic["unit"] as? String
                value.quantity = dic["quantity"] as? String
                nutritianarray.append(value)
            }
        }
       
        
        ingredientsarray = [ingredientsArrayObject]()
        if let IngridentArray = dictionary["ingredients_array"] as? [[String:Any]] {
            for dic in IngridentArray{
                //                let value = MenuSubProductItemDetail(fromDictionary: dic)
                var value = ingredientsArrayObject()
                value.name = dic["name"] as? String
                value.unit = dic["unit"] as? String
                value.quantity = dic["quantity"] as? String
                ingredientsarray.append(value)
            }
        }
        
        
        
    }
    
    
    
    
}




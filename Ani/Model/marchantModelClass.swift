//
//  marchantModelClass.swift
//  Ani
//
//  Created by RSL-01 on 29/03/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import Foundation
import UIKit
class marchantModelClass
{
    var merchant_id : String!
    var contamination_msg : String!
    var merchant_name : String!
    var delivery_type : String!
    var food_type: String!
    var merchant_image : String!
    var opening_time: String!
    var closing_time: String!
    var distance: String!
    var latitude: String!
     var longitude: String!
    var address: String!
    var mobile_number: String!
    var email: String!
    var home_number: String!
    var delivery_charges: String!
    var mininum_order: String!
    var avgPreparationTime: String!
    var delivery_time: String!
    var merchant_account_id: String!
    var shop_open: String!
    var percentage: String!
    var flag: String!
    var merchant_tagline: String!
    var partner_id: String!
    var hide_nutritional_info: String!
    
    var is_master: String!
    
    var childArray: NSMutableArray!
}

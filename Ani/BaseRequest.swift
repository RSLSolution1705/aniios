//
//  BaseRequest.swift
//  Ani
//
//  Created by RSL-01 on 09/05/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
import Alamofire

class BaseRequest {
    
    typealias apiSuccess = (_ result: NSDictionary?) -> Void
    typealias apiProgress = (_ result: NSDictionary?) -> Void // when you want to download or upload using Alamofire..
    typealias apiFailure = (_ error: NSDictionary?) -> Void
    
    // MARK: - Call JSON POST Request
    func callJSONrequestPOST(url:String, params:[String: AnyObject]?, success successBlock :@escaping apiSuccess,
                             failure failureBlock :@escaping apiFailure) {
        print("callJSONrequestPOST parameters \(params)")
        
        
        let baseURL = url;
        print("callJSONrequestPOST baseURL \(baseURL)")
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        ]
        
        
        
//        Alamofire.request(_url,
                          //                          method: .post,
        //                          parameters: parameters,
        //                          encoding: URLEncoding.queryString,
        //                          headers: headers).responseJSON { (response) in
        
        
        
        //appDelet.loadOrHideView(hides: false)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        //manager.session.configuration.
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.queryString, headers: headers)
            .responseJSON { response in
                print("11111")
                print(JSONEncoding.default)
                
                //                appDelet.loadOrHideView(hides: true)
                print("callJSONrequestPOST response \(response)")
                
                if response.result.isSuccess
                {
                    
                    
                    let jsonDic = response.result.value as! NSDictionary
                    
                    
                    successBlock(jsonDic)
                    
                } else {
                    let httpError: NSError = response.result.error! as NSError
                    let statusCode = httpError.code
                    let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                    
                    failureBlock(error)
                }
        }
    }
    
    // MARK: - Call JSON GET Request
    func callJSONrequestGET(url:String, params:[String: AnyObject]?, success successBlock :@escaping apiSuccess,
                            failure failureBlock :@escaping apiFailure) {
        print("callJSONrequestGET parameters \(params)")
        
        let baseURL = url;
        print("callJSONrequestGET baseURL \(baseURL)")
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        ]
        
        //  appDelet.loadOrHideView(hides: false)
        
        Alamofire.request(baseURL, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                //                appDelet.loadOrHideView(hides: true)
                print("callJSONrequestGET response \(response)")
                
                if response.result.isSuccess {
                    let jsonDic = response.result.value as! NSDictionary
                    successBlock(jsonDic)
                    
                } else {
                    let httpError: NSError = response.result.error! as NSError
                    let statusCode = httpError.code
                    let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                    failureBlock(error)
                }
        }
    }
    
    func callogleGoPlacesAPI(url:String, success successBlock :@escaping apiSuccess,
                             failure failureBlock :@escaping apiFailure) {
        
        let baseURL = url;
        
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        //  appDelet.loadOrHideView(hides: false)
        
        Alamofire.request(baseURL, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding(options: []), headers: headers)
            .responseJSON { response in
                
                //                appDelet.loadOrHideView(hides: true)
                //  debugPrint("RESPONSE: \(response)")
                
                if response.result.isSuccess {
                    let jsonDic = response.result.value as! NSDictionary
                    
                    successBlock(jsonDic)
                    
                } else {
                    let httpError: NSError = response.result.error! as NSError
                    let statusCode = httpError.code
                    let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                    
                    failureBlock(error)
                }
        }
    }
    
    // MARK: - Call JSON PUT Request
    func callJSONrequestPut(url:String, params:[String: AnyObject]?, success successBlock :@escaping apiSuccess,
                            failure failureBlock :@escaping apiFailure) {
        print("callJSONrequestPut parameters \(params)")
        let baseURL = url;
        print("callJSONrequestPut baseURL \(baseURL)")
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        ]
        
        //        appDelet.loadOrHideView(hides: false)
        
        Alamofire.request(baseURL, method: HTTPMethod.put, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                //                appDelet.loadOrHideView(hides: true)
                debugPrint("RESPONSE: \(response)")
                print("callJSONrequestPut response \(response)")
                if response.result.isSuccess {
                    let jsonDic = response.result.value as! NSDictionary
                    
                    successBlock(jsonDic)
                    
                } else {
                    let httpError: NSError = response.result.error! as NSError
                    let statusCode = httpError.code
                    let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                    
                    failureBlock(error)
                }
        }
    }
}

//
//  WebServiceClass.swift
//  Ani
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 rsl-01. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import SVProgressHUD

class WebServiceClass: NSObject {
    
    
    func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getWebserviceLink(_ url:URL, parameter:String, callback: @escaping (_ data: NSData?, _ error: NSError?)->())
    {
        
        if isConnectedToInternet() {
      //      print("Yes! internet is available.")
            // do some tasks..
            Alamofire.request(url).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseData { response in
                    switch response.result {
                    case .success:
                        print("Validation Successful")
                        
                        if let json = response.result.value {
                            print("JSON: \(json)")
                            callback(json as NSData?, nil)
                        }
                    case .failure( _):
                        callback(nil, nil)
                    }
            }
        }
        else
        {
            callback(nil, nil)
        }
    }
    
    
    func handleDownload(_ studentNum:Int) -> (Data?, Error?) -> Void {
        return { (data: Data?, error: Error?) -> Void in
            // code for function
        }
    }
    
    
    
    
    func getDataFromURL (_ url :URL,completion : @escaping (_ data :NSData? ,_ error :NSError?)->Void){
        
        if isConnectedToInternet() {
         //   print("Yes! internet is available.")
            

            Alamofire.request(url).responseData { (resData) -> Void in
                
                if resData.result.value != nil
                {
                   // print(resData.result.value!)
                    let strOutput = String(data : resData.result.value!, encoding : String.Encoding.utf8)
                //    print(strOutput)
                    
                    completion(resData.result.value as NSData?, nil)
                }
                else
                {
                    completion(nil, nil)
                }
                
            }
        }
        else
        {
            completion(nil, nil)
        }
    }
    

}

//
//  TempViewController.swift
//  Ani
//
//  Created by Mac on 16/10/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class TempViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //  Responce from Global Search API keyword : Apple
        
        
//        {"result":"success","data":[{"type":"0","menu_item_group_id":"211","menu_item_group_name":"Lime Hut Sauces","menu_item_group_description":"Lime Hut","menu_item_group_image":"http:\/\/99.81.230.36\/ANI_TEST\/admin\/admin_new\/\/group\/155886735961698689.png","menu_item_group_qty_allowed":null,"menu_item_group_type":"1","menu_item_id":"2299","flag":"dislike","menu_item_name":"Pineapple & Habanero ","menu_item_description":"Sweet, Spice & Delicious ","menu_item_best_price":"0.00","menu_item_preparation_time":null,"menu_item_image":"http:\/\/99.81.230.36\/ANI_TEST\/admin\/admin_new\/\/menuitem\/1559217436942129971.png","percentage":0,"MenuItemIngredients":[],"size_data":[],"allergan_array":[],"nutritian_array":[{"name":"Energ_KJ","quantity":"502.08","unit":"kJ"},{"name":"Energ_Kcal","quantity":"120.00","unit":"kCal"},{"name":"FA_Sat","quantity":"0.50","unit":"g"},{"name":"Lipid_Tot","quantity":"0.10","unit":"g"},{"name":"Carbohydrt","quantity":"29.00","unit":"g"},{"name":"Sugar_Tot","quantity":"25.00","unit":"g"},{"name":"Sugar_Added","quantity":"0.00","unit":"g"},{"name":"Fiber_TD","quantity":"0.00","unit":"g"},{"name":"Protein","quantity":"0.50","unit":"g"},{"name":"Salt","quantity":"0.92","unit":"g"},{"name":"Sodium","quantity":"0.36","unit":"g"},{"name":"Energ_Kcal_from_Fat","quantity":"4.50","unit":"KCal"},{"name":"FA_Mono","quantity":"0.00","unit":"g"},{"name":"FA_Poly","quantity":"0.00","unit":"g"},{"name":"FA_Trans","quantity":"0.00","unit":"g"},{"name":"Star","quantity":"0.00","unit":"g"},{"name":"Water","quantity":"0.00","unit":"g"},{"name":"Nitrogen","quantity":"0.00","unit":"g"},{"name":"Cholestrl","quantity":"0.00","unit":"mg"},{"name":"Potassium","quantity":"0.00","unit":"mg"},{"name":"Calcium","quantity":"0.00","unit":"mg"},{"name":"Magnesium","quantity":"0.00","unit":"mg"},{"name":"Phosphorus","quantity":"0.00","unit":"mg"},{"name":"Iron","quantity":"0.00","unit":"mg"},{"name":"Copper","quantity":"0.00","unit":"mg"},{"name":"Zinc","quantity":"0.00","unit":"mg"},{"name":"Chloride","quantity":"0.00","unit":"mg"},{"name":"Manganese","quantity":"0.00","unit":"mg"},{"name":"Selenium","quantity":"0.00","unit":"ug"},{"name":"Iodine","quantity":"0.00","unit":"ug"},{"name":"Retinol","quantity":"0.00","unit":"ug"},{"name":"Carotene","quantity":"0.00","unit":"ug"},{"name":"Alpha_Carot","quantity":"0.00","unit":"ug"},{"name":"Beta_Carot","quantity":"0.00","unit":"ug"},{"name":"Vit_D_mcg","quantity":"0.00","unit":"ug"},{"name":"Vit_D_IU","quantity":"0.00","unit":"IU"},{"name":"Vit_E","quantity":"0.00","unit":"mg"},{"name":"Thiamin","quantity":"0.00","unit":"mg"},{"name":"Riboflavin","quantity":"0.00","unit":"mg"},{"name":"Niacin","quantity":"0.00","unit":"mg"},{"name":"Tryptophan60","quantity":"0.00","unit":"mg"},{"name":"Vit_B6","quantity":"0.00","unit":"mg"},{"name":"Vit_B12","quantity":"0.00","unit":"ug"},{"name":"Folate_Tot","quantity":"0.00","unit":"ug"},{"name":"Panto_Acid","quantity":"0.00","unit":"mg"},{"name":"Biotin","quantity":"0.00","unit":"ug"},{"name":"Vit_C","quantity":"0.00","unit":"mg"},{"name":"Ash","quantity":"0.00","unit":"g"},{"name":"Folic_Acid","quantity":"0.00","unit":"ug"},{"name":"Food_Folate","quantity":"0.00","unit":"ug"},{"name":"Folate_DFE","quantity":"0.00","unit":"ug"},{"name":"Choline_Tot","quantity":"0.00","unit":"ug"},{"name":"Vit_A_IU","quantity":"0.00","unit":"IU"},{"name":"Vit_A_RAE","quantity":"0.00","unit":"ug"},{"name":"Beta_Crypt","quantity":"0.00","unit":"ug"},{"name":"Lycopene","quantity":"0.00","unit":"ug"},{"name":"Lut_Zea","quantity":"0.00","unit":"ug"},{"name":"Vit_K","quantity":"0.00","unit":"ug"}],"ingredients_array":[{"name":"Water, Glucose-Fructose Syrup, Mango Puree (12%), Sugar, Spirit Vinegar, Concentrated Pineapple Juice (4%), Modified Maize Starch, Apricot Puree Concentrate, (contains Antioxidant: Absorbic Acid), Salt, Habanero Chilli Puree (Habanero Chillies (0.5%), Salt, Acidity Regulator: Acetic Acid), Tomato Paste, Concentrated Lime Juice, Coriander Leaf, Dried Red Bell Peppers, Preservative (Potassium Sorbate)","quantity":"20","unit":"g"}],"ingredient_deceleration_info":"Water, Glucose-Fructose Syrup, Mango Puree (12%), Sugar, Spirit Vinegar, Concentrated Pineapple Juice (4%), Modified Maize Starch, Apricot Puree Concentrate, (contains Antioxidant: Absorbic Acid), Salt, Habanero Chilli Puree (Habanero Chillies (0.5%), Salt, Acidity Regulator: Acetic Acid), Tomato Paste, Concentrated Lime Juice, Coriander Leaf, Dried Red Bell Peppers, Preservative (Potassium Sorbate)."}
//  ]
//
// }
        
        
        
    }

}

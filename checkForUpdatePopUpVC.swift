//
//  checkForUpdatePopUpVC.swift
//  Ani
//
//  Created by Mac on 14/11/19.
//  Copyright © 2019 rsl-01. All rights reserved.
//

import UIKit

class checkForUpdatePopUpVC: UIViewController
{
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var btnLaterOutlet: UIButton!
    @IBOutlet weak var btnUpgradeNowOutlet: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.outerView.layer.cornerRadius = 5.0
        self.outerView.clipsToBounds = true
        
        self.btnLaterOutlet.layer.cornerRadius = self.btnLaterOutlet.frame.size.height / 2.0
        self.btnLaterOutlet.clipsToBounds = true
        
        self.btnUpgradeNowOutlet.layer.cornerRadius = self.btnUpgradeNowOutlet.frame.size.height / 2.0
        self.btnUpgradeNowOutlet.clipsToBounds = true
        
    }
    @IBAction func btnLaterTapped(_ sender: Any)
    {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        if  UserDefaults.standard.bool(forKey: "isUserLog") == true
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "signuppageViewController") as! signuppageViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }
        self.removeFromParentViewController()
    }
    
    @IBAction func btnUpgradeNowTapped(_ sender: Any)
    {
       var AppStoreLink =  "https://itunes.apple.com/us/app/myapp/id1470074950?ls=1&mt=8"
        
        UIApplication.shared.openURL(NSURL(string: AppStoreLink)! as URL)

        
    }
    
    
    
}
